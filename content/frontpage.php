<h2><a name="section1">New IA Training Site</a></h2>
<img src="./images/iatraining_shot.gif" border="New Design for IA Training" style="float:left">
<p>
Welcome to the new redesigned <b>IA Training</b> website. The new design
will help you locate and access information you need when you need it. 
</p>
<p>
At this time the site is <b>80%</b> complete with a few areas unfinished. 
For this reason a link to the old site is provided under the <b>Links</b> menu, 
on the left to give you a way to locate information that is not on the new site yet. So, if 
you do not see it here, check the old site for that information, and enjoy using the new one.
</p>
<p class="note">
<b>Note:</b> 
If you are looking for <i>DSL Support</i>, it has been renamed <b>Broadband</b>.
</p>
<p> 
<i>The new site was designed with you in mind, along with what your job requires.
You will find that information is now organized in a logical and structured manner, 
allowing you to find information quickly. So, take a moment and look around, but 
keep in mind that we are still under construction</i>. 
<p>
<i><b>-- Clayton</b></i>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
</p>
<p>
<h2><a name="section2">New Face on Info Avenue</a></h2>
<a href="http://www.infoave.net">
<img src="./images/infoave_shot.gif" border="New Look for Info Avenue" style="float:left">
</a>
Info Avenue has joined forces with three other industry-leading technology companies
to provide networks, telephone, Internet, Web and SS7/database products and services 
for telephone companies, Internet Service Providers, CLECs and RBOCs and all sizes 
of business enterprises throughout the Southeast and the nation. The name of this powerful 
new group is:
<p align="center">
<img src="./images/spirit_logo_small.gif" border="The New Spirit Telecom &reg; Logo" style="float:center"> 
</p>
<p>
This is what visitors saw when they surfed to the <a href="http://www.infoave.net">Info
Avenue</a> home page on Friday. This screen and the new look are part of our rebirth
as <b><a href="http://www.spirittelecom.com" target="_blank">Spirit Telecom</a></b> to the world. 
Look for more changes to come in the new year.
<br>
<a href="http://www.infoave.net" target="_blank">More...</a> 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->