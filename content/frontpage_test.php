<h2><a name="section1">Old Infoave Intranet Disabled</a></h2>
<p>
Content that once existed the old Infoave Intranet (http://intranet.infoave.net) 
moved, and will be updated and exist on the new Spirit Telecom Intranet (http://intranet.spirittelecom.com). 
Techs looking for links that were located at this location should modify any existing 
list to that location to point to the Spirit Telecom Intranet. 
</p>
<p class="note">
<b>Note:</b> 
A link to the 
</p>
<i><b>-- Clayton</b></i>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
</p>
<p>
<h2><a name="section2">New Face on Info Avenue</a></h2>
<a href="http://www.infoave.net">
<img src="./images/infoave_shot.gif" border="New Look for Info Avenue" style="float:left">
</a>
Info Avenue has joined forces with three other industry-leading technology companies
to provide networks, telephone, Internet, Web and SS7/database products and services 
for telephone companies, Internet Service Providers, CLECs and RBOCs and all sizes 
of business enterprises throughout the Southeast and the nation. The name of this powerful 
new group is:
<p align="center">
<img src="./images/spirit_logo_small.gif" border="The New Spirit Telecom &reg; Logo" style="float:center"> 
</p>
<p>
This is what visitors saw when they surfed to the <a href="http://www.infoave.net">Info
Avenue</a> home page on Friday. This screen and the new look are part of our rebirth
as <b><a href="http://www.spirittelecom.com" target="_blank">Spirit Telecom</a></b> to the world. 
Look for more changes to come in the new year.
<br>
<a href="http://www.infoave.net" target="_blank">More...</a> 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->