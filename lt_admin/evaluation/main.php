<?
/*
File: index.php
created by Clayton barnette claytonbarnette@yahoo.com

This section displays the Date, Textural Date and Time.
$today: displays the date in the following format: 01/03/03
$texttoday: displays the date in the following format: January 1,2003 
$time: displays time in the following format: 10:00:00 pm
*/
// Date
$today = date("m/d/y");
// Textural Date 
$texttoday = date("- F j,Y [ l ]");
// Time 
$time = date("[ g:i:s a ]"); 
// end of script
?>

<!-- import javascript file for validating. -->
<script src="./evaluation/validate.js"></script>
<!-- end of javacript file validating. -->
<h1>LT Evaluation Form</h1>
<p>
<table class="list_table">
 <tr><td class="list_header">Table of Content</td></tr>
 <tr><td class="content_link"><a href="?content=./main.php">Main Menu</a></td></tr>
</table>
</p>
<form name="form" action="?content=./evaluation/confirm_eval.php" method="post" onsubmit="return validate()">
<input type="hidden" name="time" value="<?echo($time);?>">
<!-- first table starts here -->
<p>
<table class="list_table">
<tr>
 <td class="list_header" colspan=4>Evaluation Form</td>
</tr>
<tr>
 <td class="content_list">Date:</td>
 <td class="content_list"><b><?echo($today);?></b></td>
</tr>
<tr>
 <td>Log ID:</td>
 <td><input type="text" name="log_id" size="10" maxlength="10"></td>
</tr>
<tr>
 <td>Provider:</td>
 <td><input type="text" name="provider" size="20" maxlength="40"></td>
</tr>
<tr>
 <td>User Name:</td>
 <td><input type="text" name="username" size="20" maxlength="40"></td>
</tr>
<tr>
 <td>Name of Tech:</td>
 <td><input type="text" name="evaluatee" size="20" maxlength="40"></td>
</tr>
<tr>
 <td>Talk Time:</td>
 <td><input type="text" name="talk_time" size="20" maxlength="40"></td>
</tr>
<tr>
 <td>Name of LT:</td>
 <td><input type="text" name="evaluator" size="20" maxlength="40" value="<?echo($_SERVER['PHP_AUTH_USER']);?>">
  
 </td>
</tr>
<tr>
 <td>E-Mail Address:</td>
 <td><input type="text" name="email_address" size="25" maxlength="40" value="<?echo($_SERVER['PHP_AUTH_USER']."@infoave.net");?>">
 
 </td>
</tr>
<tr>
 <td width="25%">Shift of Evaluation:</td>
 <td width="25%"><input type="radio" value="First Shift" name="shift" checked > First</td>
 <td width="25%"><input type="radio" value="Second Shift" name="shift" > Second</td>
 <td width="25%"><input type="radio" value="Third Shift" name="shift" > Third</td>
</tr>
<tr>
 <td class="list_header" colspan=4>Comments</td>
</tr>
<tr>
 <td colspan=4>Please include any additional comments or notes that should be included in this evaluation.</td>
</tr>
<tr>
 <td colspan=4>
 <p align="center">
 <textarea rows="10" name="comments" cols="100%" WRAP="physical" style="background-color:#cccccc"></textarea>
 </p>
 </td>
</tr>
<tr>
 <td colspan=4>
 <p align="center">
  <input type="submit" value="  Next " name="submit"> 
  <input type="reset" value="Reset Form" name="clear">
 </p>
 </td>
</tr>
</table>
</p>
</form>
<?
// This section logs who lgs into evaluation section
require("./evaluation/logit.php");
?>
<p><b>[</b><a href="?content=./evaluation/admin/main.php">Admin</a><b>]</b></p>
<!-- Table ends Here -->
<!-- END TEXT HERE -->










