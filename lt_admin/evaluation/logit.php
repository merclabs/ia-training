<?
// logit.php
// writen by Clayton Barnette
// 01-07-03
// claytonbarnette@yahoo.com 

// Todays Date mm/dd/yy
$log_entry = $today;
// Current Time
$log_entry .= ":".$time;
// Authenticated user
$log_entry .= ":".$_SERVER['PHP_AUTH_USER'];
// Remote address
$log_entry .= ":".$_SERVER['REMOTE_ADDR'];
// Request Method
$log_entry .= ":".$_SERVER['REQUEST_METHOD'];
// Refer'n Document
$log_entry .= ":".$_SERVER['HTTP_REFERER'];
// User Agent
$log_entry .= ":".$_SERVER['HTTP_USER_AGENT'];
// Lanuage
$log_entry .= ":".$_SERVER['HTTP_ACCEPT_LANGUAGE']."\r\n";
// echo it all out in to a log file named access_log.
// the variable $log_entry contains all log entried that
// are writen to the access_log file.

// open file for writing
$al = fopen("./evaluation/admin/access_log","a+");
// append access info to file.
fwrite($al,$log_entry);
// close connection
fclose($al);
// end of writing to file. 
?>


