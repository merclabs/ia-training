// validate.js
// Validates form elements.

function validate()
{
 if(document.form.log_id.value == "")
 {
  alert("Please supply a valid Log ID.");
  document.form.log_id.focus();
	return false;
 }
 if(document.form.provider.value == "")
 {
  alert("Please enter a provider.");
  document.form.provider.focus();
	return false;
 }
 if(document.form.username.value == "")
 {
  alert("Please supply user name of caller.");
  document.form.username.focus();
	return false;
 }
 if(document.form.evaluatee.value == "")
 {
  alert("Please provide the name of the tech being evaluated.");
  document.form.evaluatee.focus();
	return false;
 }
 if(document.form.talk_time.value == "")
 {
  alert("Please enter the talk time for this call.");
  document.form.talk_time.focus();
	return false;
 }
 if(document.form.evaluator.value == "")
 {
  alert("Please provide your name.");
  document.form.evaluator.focus();
	return false;
 }
 if(document.form.email_address.value == "")
 {
  alert("Please provide your e-mail address.");
  document.form.email_address.focus();
	return false;
 }
 switch("Select One")
 {
 case document.form.YesNo.value:
  alert("Please select a value of Yes or No");
	document.form.YesNo.focus();
	return false;
 break;
  case document.form.eval1.value:
  alert("Please select a value");
	document.form.eval1.focus();
	return false;
 break;
 case document.form.eval2.value:
 alert("Please select a value");
	document.form.eval2.focus();
	return false;
 break;
 case document.form.eval3.value:
  alert("Please select a value");
	document.form.eval3.focus();
	return false;
 break;
 case document.form.eval4.value:
  alert("Please select a value");
	document.form.eval4.focus();
	return false;
 break;
 case document.form.eval5.value:
 alert("Please select a value");
	document.form.eval5.focus();
	return false;
 break;
 case document.form.eval6.value:
 alert("Please select a value");
	document.form.eval6.focus();
	return false;
 break;
 case document.form.eval7.value:
 alert("Please select a value");
	document.form.eval7.focus();
	return false;
 break;
 case document.form.eval8.value:
 alert("Please select a value");
	document.form.eval8.focus();
	return false;
 break;
 case document.form.eval9.value:
 alert("Please select a value");
	document.form.eval9.focus();
	return false;
 break;
 case document.form.eval10.value:
 alert("Please select a value");
	document.form.eval10.focus();
	return false;
 break;
 }
return true;
}
// end of script.
