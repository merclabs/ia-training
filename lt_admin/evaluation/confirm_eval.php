<?
/*
File: index.php
created by Clayton barnette claytonbarnette@yahoo.com

This section displays the Date, Textural Date and Time.
$today: displays the date in the following format: 01/03/03
$texttoday: displays the date in the following format: January 1,2003 
$time: displays time in the following format: 10:00:00 pm
*/
// Date
$today = date("m/d/y");
// Textural Date 
$texttoday = date("- F j,Y [ l ]");
// Time 
$time = date("[ g:i:s a ]"); 
// end of script
?>

<!-- import javascript file for validating. -->
<script src="./evaluation/validate.js"></script>
<!-- end of javacript file validating. -->
<h1>LT Evaluation Form - Confirm</h1>
<p>
<table class="list_table">
 <tr><td class="list_header">Table of Content</td></tr>
 <tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Escalations</a></td></tr>
 <tr><td class="content_link"><a href="?content=./ontheboard/admin/editor.php">On the Board Editor</a></td></tr>
</table>
</p>
<p class="note">
Please take a moment to review the <b>Evaluation Form</b> you are about to submit. If you
wish to edit any information, click <a href="javascript:history.back()">back</a>.
</p>
<p>
<form name="form" action="?content=./evaluation/submit_eval.php" method="post" onsubmit="return validate()">
<input type="hidden" name="time" value="<?echo($time);?>">
<!-- first table starts here -->
<p>
<table class="list_table">
<tr>
 <td class="list_header" colspan=4>Evaluation Form</td>
</tr>
<tr>
 <td class="content_list">Date:</td>
 <td class="content_list"><b><?echo($today);?></b></td>
</tr>
<tr>
 <td>Log ID:</td>
 <td>
  <b><?echo($log_id);?></b>
  <input type="hidden" name="log_id" size="10" value="<?echo($log_id);?>">
 </td>
</tr>
<tr>
 <td>Provider:</td>
 <td>
 <b><?echo($provider);?></b>
 <input type="hidden" name="provider" size="20"  value="<?echo($provider);?>">
 </td>
</tr>
<tr>
 <td>User Name:</td>
 <td>
 <b><?echo($username)?></b>
 <input type="hidden" name="username" size="20" value="<?echo($username)?>">
 </td>
</tr>
<tr>
 <td>Name of Tech:</td>
 <td>
 <b><?echo($evaluatee)?></b>
 <input type="hidden" name="evaluatee" size="20" value="<?echo($evaluatee)?>">
 </td>
</tr>
<tr>
 <td>Talk Time:</td>
 
 <td><b><?echo($talk_time)?></b><input type="hidden" name="talk_time" size="20" value="<?echo($talk_time)?>">
 </td>
</tr>
<tr>
 <td>Name of LT:</td>
 
 <td><b><?echo($evaluator)?></b><input type="hidden" name="evaluator" size="20" value="<?echo($evaluator)?>">
  
 </td>
</tr>
<tr>
 <td>E-Mail Address:</td>

 <td><b><?echo($email_address);?></b><input type="hidden" name="email_address" size="25" value="<?echo($email_address);?>">
 </td>
</tr>
<tr>
 <td width="25%">Shift of Evaluation:</td>
 <td width="25%"><b><?echo($shift)?></b><input type="hidden"  name="shift" value="<?echo($shift)?>"></td>
</tr>
<tr>
 <td class="list_header" colspan=4>Comments</td>
</tr>
<tr>
 <td colspan=4>Please include any additional comments or notes that should be included in this evaluation.</td>
</tr>
<tr>
 <td colspan=4>
 <p align="center">
 <textarea rows="8" name="comments" cols="100" WRAP="physical" style="background-color:#cccccc">
 <?echo($comments)?>
 </textarea>
 </p>
 </td>
</tr>
<tr>
 <td colspan=4>
 <p align="center">
  <input type="submit" value="  Next " name="submit"> 
  <input type="reset" value="Reset Form" name="clear">
 </p>
 </td>
</tr>
</table>
</p>
</form>

 

<p>
<!-- Table Starts Here --></p>
<form name="form" action="./evaluation/submit_eval.php" method="post" onsubmit="validate()">













