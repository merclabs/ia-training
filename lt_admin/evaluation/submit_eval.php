<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<?
// Title of e-mail
$subject = "Evaluation for Log ID:$log_id sent by $evaluator.";

// E-mail List
// add email recipients here from email_list.php file
// to edit the e-mail list, edit the e-mail_list.php file.
//
$eml = fopen("./evaluation/test_email_list.php","r");
$email_list = fread($eml,1024);
//
// Uncomment next line for testing only.
//echo($email_list);
fclose($eml);

// Header info
$app = "Tech Support Evaluation Form";
$from = "From: $email_address\r\n";
$reply_to = "Reply-To: $email_address\r\n";
$mailer = "X-Mailer: $app";
$all_headers = $from.$reply_to.$mailer; 

// message 
$message = "\n";
$message .= "========================================================\n";
$message .= "                    EVALUATION FORM\n";
$message .= "========================================================\n";
$message .= "Time: $time\n";
$message .= "Date: $today\n";
$message .= "Evaluating LT: $evaluator\n";
$message .= "Return Address: $email_address\n";
$message .= "Name of Evaluated Tech: $evaluatee\n";
$message .= "Shift of Evaluation: $shift\n";
$message .= "Log ID: $log_id\n";
$message .= "\n";
$message .= "========================================================\n";
$message .= "                     Comments\n";
$message .= "========================================================\n";
$message .= "$comments\n";
$message .= "--------------------------------------------------------\n";
$message .= "\n";
mail($email_list, "$subject", $message,"$all_headers");
?>
<h1>LT/Supervisor Administration</h1>
<p>
<h2>Logged In User [<?echo($_SERVER['PHP_AUTH_USER']);?>]</h2>
<p>
<table class="list_table">
 <tr><td class="list_header">Table of Content</td></tr>
 <tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Escalations</a></td></tr>
 <tr><td class="content_link"><a href="?content=./evaluation/main.php">Submit another Evaluation</a></td></tr>
 <tr><td class="content_link"><a href="?content=./ontheboard/admin/editor.php">On the Board Editor</a></td></tr>
</table>
</p>
<table class="list_table">
  <tr>
	 <td class="list_header">Evaluation Form</td>
	</tr>
  <tr>
    <td class="content_list">
		<p>
		Your evaluation for was sucessfully sent to all Supervisors via e-mail.
		You should also receive a copy of this evaluation in a few moments.
		</p>
		<p>
    Thank you for submiting an evaluation and keep up the good work.<br>
		 -- Supervisors
		</p>
     </td>
  </tr>
</table>
<p class="note">
If you are finish with your evaluation, <a href="http://www.iatraining.net/newdesign">click here</a> 
to return to the <b>IA Training Website</b>.
</p>


