<h1>Fort Mill(Comporium) Escalation Procedure</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./lt_admin/escalations/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<p>
<!-- First table -->
<table class="list_table">
<tr>
<td class="list_header"><b>DSL/Cable Modem Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_list">
<br>
<ol>
<li>Refer DSL and Cable Modem customers to <b>802-7447</b> 
</ol>
</td>
</tr>
</table>
<br>
<!-- Start of Second Table -->
<table class="list_table">
<tr>
<td  class="list_header"><b>Dail-Up Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_list">
<br>
<ol>
<li>Troubleshoot call as normal.<br>(<i>cannot connect, slow connect speeds, connection dropping out etc�</i>)
<li>If connection problem has not been resolved and this is the customers 4th call,
check with an LT to get approval to escalate call. 
<li>
<b style="color:red;">Get the following information from the customer:</b>
 <ol type="a">
  <li>How many phones do you have hooked up and what type are they (wired or cordless)?</li>
  <li>What telephone number(s) do you use to access the Internet and what are those numbers?</li>
  <li>How many telephone lines(numbers) do you have in your home/business and which ones do you use for Internet dial up?</li>
  <li>What location are you trying to access the Internet from?</li>
 </ol>
<li>Complete a write up sheet including all of the above information plus all necessary information needed on the write up sheet.
<li>Give write up to LT who approved escalation.
</ol>
</td>
</tr>
</table>
</p>
