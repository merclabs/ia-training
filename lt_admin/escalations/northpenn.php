<h1>North Penn Access Escalation Procedure</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./lt_admin/escalations/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="List_table">
<tr>
<td class="list_header"><b>Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_link">
<br>
<ol>
<li>Get permission to write up from Lead Tech.
<li>Complete a "Cable and DSL Write Up Sheet".
<li>Gather all necessary infornation:
 <ul type="square">
  <li>User Name</li>
  <li>Customer's Full Name</li>
  <li>Street Address(include city)</li>
  <li>Contact Number</li>
  <li>PTS Log ID</li>
  <li>DSL Line Number or Account Number</li>
  <li>Operating System</li>
  <li>Connection Type<br>( DSL, ADSL, 1 Way Cable, 2 Way Cable )</li>
  <li>Description of problem</li>
  <li>Can they Ping(Domain Name, IP Address, NIC)</li>
  <li>What you have checked<br>( Network Settings, Power Cycle, PPoE )</li>
 </ul>
<li>Check with Lead Tech to make sure you have all information from customer before you let
customer go.
<li>Give "Cable and DSL Write Up Sheet" to Lead Tech or DSL Tech.
<li>Give customer Log ID.
</ol>
</td>
</tr>
</table>
</p>

