<h1>Mebtel Communications Escalation Procedure</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./lt_admin/escalations/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr>
<td class="list_header"><b>Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_link">
<br>

<ul>
 <li>The following things should be in your ticket:
 <ul>
 	<li>ALL the troubleshooting steps you took in detail.
	<li>Has the DSL worked before?
	<li>The Serial Number for Lucent DSL Pipes or the MAC address for Speedstream modems
	<li>Customerís Name
	<li>Contact Number
	<li>Alternate Contact Number (such as cell phone or work number)
 </ul>
 <li>Get permission from Lead Tech to transfer.
 <li>Give customer "Ticket Number"
 <li>Transfer customer to "Customer Service" for billing or account related issues, "Second Level" for connection and hardware issues.
</ul>
</td>
</tr>
</table>
</p>
