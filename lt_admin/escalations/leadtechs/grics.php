<h1>Gallatin River Communications Escalation and E-Mail Tool</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr><br />
<tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr>
<td class="list_header" colspan=2><b>Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_list">
<b>Customer Service:</b>
<ul>
<li>1-800-223-1851</li>
</ul>
<b>Second Level:</b>
<ul>
<li>1-800-771-4926</li>
</ul>
<p>
<b>Second Level Hours:</b>
</p>
<b>Monday - Friday:</b>
<ul>
<li>9:00 am - 11:00 pm EST</li>
<li>8:00 am - 10:00 pm CST</li>
</ul>
<b>Saturday:</b>
<ul>
<li>9:00 - to 6:00 pm EST</li>
<li>8:00 - to 5:00 pm CST</li>
</ul>
<b>Sunday:</b>
<ul>
<li>Closed</li>
</ul>
</td>
</tr>
</table>
</p>
<p align="center">
  <input type="button" value="Back" onclick="history.back()"> 
</p>