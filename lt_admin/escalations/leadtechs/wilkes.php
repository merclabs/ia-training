<h1>Wilkes Communications Escalation and E-Mail Tool</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr><br />
<tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<table class="list_table">
<tr>
<td class="list_header"><b>Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_list">
Escalated calls should be writen up by Tech, information verified by LT, then submitted
via the E-mail form below by LT.
</td>
</tr>
</table>
<br>
<p class="note">
This form e-mails all Supervisors and LTs then Wilkes Communications at the following address:<br>
<a href="mailto:wtmcsvc@wilkes.net">wtmcsvc@wilkes.net</a>
</p>
<script src="./escalations/leadtechs/validateform.js"></script>
<!-- start of form -->
<form name="form" action="?content=./escalations/leadtechs/emailwilkes.php" method="post" onSubmit="return validateForm()">
<!-- 
 This hidden field contans the e-mail address to which this form's information is emailed.	
 To change the e-mail address just edit the value section of the hidded input field.
-->
<input type="hidden" value="wtmcsvc@wilkes.net" name="email">
<input type="hidden" value="Wilkes Communication" name="provider">
<!-- Notice: please see above information -->
<table class="list_table">
  <tr>
    <td class="list_header" colspan=2>Escalation Form</td>
  </tr>
  <tr>
    <td>Date of Call</td>
    <td><input type="text" name="date_of_call" size="8" value="<?$today = date("m/d/y");echo($today);?>"></td>
  </tr>
  <tr>
    <td>Time of Call</td>
    <td><input type="text" name="time_of_call" size="8"></td>
  </tr>
  <tr>
    <td>Customer's Name</td>
    <td><input type="text" name="customer_name" size="50"></td>
  </tr>
  <tr>
     <td>Account Number</td>
		 <td><input type="text" name="account_number" size="12"></td>
  </tr>
  <tr>
    <td>Street Address</td>
    <td><input type="text" name="street_address" size="50"></td>
  </tr>
  <tr>
    <td>City,State</td>
		<td><input type="text" name="city" size="25"></td>
  </tr>
  <tr>
    <td>Log ID Number</td>
    <td><input type="text" name="log_id_number" size="10"></td>
  </tr>
  <tr>
   <td>Call-Back Number</td> 
	 <td><input type="text" name="call_back_number" size="12"></td>
  </tr>
  <tr>
    <td>DSL Serial/Line Number</td>
    <td><input type="text" name="dsl_line_number" size="12"></td>
  </tr>
  <tr>
    <td>NIC Type</td>
		<td><input type="text" name="network_card" size="20"></td>
  </tr>
	 <tr>
    <td >Operating System</td>
    <td><select name="operating_system" size="1">
      <option selected value="none">-- Select One --</option>
      <option value="Windows 3.1">Windows 3.1</option>
      <option value="Windows 95">Windows 95a</option>
      <option value="Windows 95b">Windows 95b</option>
      <option value="Windows 95c">Windows 95c</option>
      <option value="Windows 98">Windows 98</option>
      <option value="Windows 98 SE">Windows 98 SE</option>
      <option value="Windows ME">Windows ME</option>
      <option value="Windows 2000">Windows 2000</option>
      <option value="Windows NT">Windows NT</option>
      <option value="Windows XP">Windows XP</option>
      <option value="Linux ">Linux </option>
      <option value="Macintoch OS">Macintoch OS</option>
      <option value="Web TV">Web TV</option>
    </select></td>
  </tr>
  <tr>
    <td >Connection Type</td>
    <td><select name="connection_type" size="1">
      <option selected value="none">-- Select One --</option>
      <option value="Cable Modem - 1 Way">Cable Modem - 1 Way</option>
      <option value="Cable Modem - 2 Way">Cable Modem - 2 Way</option>
			<option value="DLS">DSL</option>
      <option value="SDLS">SDSL</option>
      <option value="ADSL">ADSL</option>
			<option value="VDSL">VDSL</option>
    </select></td>
  </tr>
  <tr>
    <td colspan="2" class="list_header">Description of Problem</td>
  </tr>
  <tr>
    <td colspan="2">
		<textarea rows="8" name="description" cols="100"></textarea>
		</td>
  </tr>
  <tr>
    <td colspan="2" class="list_header">Comments</td>
  </tr>
  <tr>
    <td colspan="2">
		 <textarea rows="8" name="comments" cols="100"></textarea>
		</td>
  </tr>
</table>
<p align="center">
  <input type="button" value="Back" onclick="history.back()"> 
   <input type="reset" value="Clear"> 
  <input type="submit" value="Send" style="font-weight: bold">
</p>
</form> 
<!-- end of form -->


