<h1>LT Escalation and E-Mail Tool</h1>
<h2>Logged In User [<?echo($_SERVER['PHP_AUTH_USER']);?>]</h2>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<table class="list_table">
<tr>
<td class="list_header"><b>Providers</b>
</td>
</tr>
<tr>
<td class="content_link">
<b>Instructions:</b> Click on a providers name to view its escalations procedure. 
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/blue_mountain.php">Blue Mountain Cable</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/chestertel.php">Chester Long Distance Service, Inc. (CHESTER)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/coastalnow.php">Coastal Communications (CLDS)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/etinternet.php">Ellerbe Telephone Company&nbsp; (ELLERBE)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/comporium_fortmill.php">Fort Mill Telephone Company (FORTMILL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/cyberlink.php">Galaxy Holdings Inc. (CYBERLINK)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/grics.php">Gallatin River Communications (GALLATIN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/graceba.php">Graceba Total Communications,Inc. (GRACEBA)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/gulftel.php">GulfTel Communications (GULFTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/hardynet.php">Hardynet Telecommunications (HARDY)</a>
</td>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/hargray.php">Hargray Telephone (HARGRAY)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/horry.php">Horry Telephone Cooperative, Incorporated (HORRY)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/comporium_lancaster.php">Lancaster Telephone Company (LANCTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/lexington.php">Lexington Telephone Company (LXTN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/mebtel.php">MebTel Communications (MEBTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/northpenn.php">North Penn Access (NORTHPEN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/pbtcomm.php">PBT Communications (PONDBR)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/comporium_rockhill.php">Rock Hill Telephone Company (ROCKHILL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./escalations/leadtechs/wilkes.php">Wilkes Communications (WILKES)</a>
</td>
</tr>
</table>
</p>