function validateForm(){
// Validation script for form
 
if(document.form.date_of_call.value == ""){
alert("Please provide todays date.");
document.form.date_of_call.focus();
return false;
}
if(document.form.time_of_call.value == ""){
alert("Please include time of call.");
document.form.time_of_call.focus();
return false;
}

if(document.form.customer_name.value == ""){
alert("You must provide Customers Name.");
document.form.customer_name.focus();
return false;
}

//case document.form.account_name.value
//alert("Please provide account number.");
//break;
if(document.form.street_address.value == ""){
alert("You must provide Customer's Address.");
document.form.street_address.focus();
return false;
}

if(document.form.city.value == ""){
alert("You must provide the name of the City and State");
document.form.city.focus();
return false;
}
if(document.form.log_id_number.value == ""){
alert("You must provide the Log ID Number.");
document.form.log_id_number.focus();
return false;
}

if(document.form.call_back_number.value == ""){
alert("You must provide the Customer's Contact Number.");
document.form.call_back_number.focus();
return false;
}

if(document.form.dsl_line_number.value == ""){
alert("You must provide the Customers Line Number.");
document.form.dsl_line_number.focus();
return false;
}

if(document.form.description.value == ""){
alert("You must provide a brief description of the problem.");
document.form.description.focus();
return false;
}


if(document.form.operating_system.value == "none"){
alert("You must select an Operating System.");
 if(document.form.account_number.value == ""){
 document.form.account_number.value = "Not Available";
 }
document.form.operating_system.focus();
return false;
}

if(document.form.connection_type.value == "none"){
alert("Please select a Connection Type.");
 if(document.form.network_card.value == ""){
 document.form.network_card.value = "Not Available";
 }
document.form.connection_type.focus();
return false;
}

return true;
}