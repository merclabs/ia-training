<?
// e-Mail Maler script
/*
This Script written by Clayton Barnette claytonbarnette@yahoo.com

This Script send the forms contents to the specified e-mail address
that is in a hidden filed on the form named "email", the value= section
holds the e-mail address that is to receive the e-mail this script generates.

Note: the $slt variable includes Supervising Lead Techs.

Datecode: 071103

*/
// email code for e-mailing providers and SLT's.
//
// Title of e-mail
$subject = "Log ID Number: $log_id_number";
// =========================================
//      E-mail Configuration Section
// ========================================
// Uncomment next line for testing only.
// $email_list = "claytonbarnette@yahoo.com";
//
// Uncomment to enable sending to all LT's
// Added 11-12-03 to send e-mail to all LT's(requested by John Welch)

$lt_email_list = "seant@infoave.net,"; // Sean Thompkins
$lt_email_list .= "jmayhue@infoave.net,"; // Jack Mayhue
$lt_email_list .= "solthof@infoave.net,"; // Scott Olthof
//$lt_email_list .= "svinson@infoave.net,"; // Steve Vinson(No Longer Works Here)
$lt_email_list .= "jamesroberts@infoave.net,"; // Kevin Roberts
$lt_email_list .= "bryantp@infoave.net,"; // Patrick Bryant
$lt_email_list .= "gvarnador@infoave.net,"; // Gene Varnadore
$lt_email_list .= "danieldavis@infoave.net"; // Daniel Davis
$email_copy = $lt_email_list;

//
// Uncomment is SLT's need to be e-mailed also.
$slt = "doyle@infoave.net,billyl@infoave.net,perrott@infoave.net,johnrwelch@infoave.net";
//
// Send an copy of this e-mail to the person submitting the form
if($email_copy == ""){
$email_list = $email.",".$slt;
}else{
// Sends e-mail to Provider and SLT and submitter(see above.) )
$email_list = $email.",".$slt.",".$email_copy;
}
//
// Sends e-mail to Provider only
// Note: This variable is pulled from a hidden tag on the form labled "email".
// the value part of the element can be used to specify the e-mail address that
// this forms contents is sent to. 
//$email_list = "$email";
//$email_list = "$slt";
// ===========================================
//     End of e-mail Configuration Section
// ===========================================

// Header info
$app = "Lead Tech Escalation E-Mail Tool";
$from = "From: support@infoave.net\r\n";
$reply_to = "Reply-To: support@infoave.net\r\n";
$bcc = "Bcc: $slt\r\n";
$mailer = "X-Mailer: $app";
$all_headers = $from.$reply_to.$mailer; 

// Date
$today = date("m/d/y");

// Textural Date 
$texttoday = date("F j,Y [ l ]");

// Time 
$time = date(" g:i:s [ a ]"); 

// e-mail message 
$message = "\n";
$message .= "=============================================================\n";
$message .= "    Escalation E-Mail for $provider \n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "Today's Date: $texttoday\n";
$message .= "Time Sent: $time\n";
$message .= "Sent by User: {$_SERVER['PHP_AUTH_USER']}@infoave.net\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "                    Log Information\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "Date of Call: $date_of_call\n";
$message .= "Time of Call: $time_of_call\n";
$message .= "Log ID: $log_id_number\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "                    Customer Information\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "Account Number: $account_number\n";
$message .= "Customer's Name: $customer_name\n";
$message .= "Street Address: $street_address\n";
$message .= "City: $city\n";
$message .= "Contact Number: $call_back_number\n";
//$message .= "DSL Serial/Line Number: $dsl_line_number\n";
//$message .= "Network Card: $network_card\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "                    System Information\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "Operating System: $operating_system\n";
$message .= "Connection Type: $connection_type\n";
//$message .= "DSL Serial/Line Number: $dsl_line_number\n";
//$message .= "Network Card: $network_card\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "                          Description\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "$description\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "                           Comments\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "$comments\n";
$message .= "\n";
$message .= "=============================================================\n";
$message .= "\n";
$message .= "Please send a reply to the message letting us know that.\n";
$message .= "the problem has been resolved.\n";
$message .= "\n";
$message .= "The information provided in this e-mail can only be sent by a\n";
$message .= "Lead Tech or Supervisor. If you are missing any necessary information\n";
$message .= "that is not provided in this e-mail, please reply to this message\n";
$message .= "letting us know what information you needed or call the Technical\n";
$message .= "Support Hotline of a more immediate response.\n";
$message .= "\n";
$message .= "Techical Support\n";
$message .= "support@infoave.net\n";
$message .= "\n";
// end of message section

// This does the actual e-mail
mail($email_list, "$subject", $message,"$all_headers");
?>
