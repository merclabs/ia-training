<?
// e-Mail Maler script
/*
This Script written by Clayton Barnette claytonbarnette@yahoo.com

This Script send the forms contents to the specified e-mail address
that is in a hidden filed on the form named "email", the value= section
holds the e-mail address that is to receive the e-mail this script generates.

Note: the $slt variable includes Supervising Lead Techs.

Datecode: 071103

*/
// email code for e-mailing providers and SLT's.
//
// Title of e-mail
$subject = "$logid";
// =========================================
//      E-mail Configuration Section
// ========================================
// Uncomment next line for testing only.
// $email_list = "claytonbarnette@yahoo.com";
//
// Uncomment to enable sending to all LT's
// Added 11-12-03 to send e-mail to all LT's(requested by John Welch)

$lt_email_list = "seant@infoave.net,"; // Sean Thompkins
$lt_email_list .= "jmayhue@infoave.net,"; // Jack Mayhue
$lt_email_list .= "solthof@infoave.net,"; // Scott Olthof
//$lt_email_list .= "svinson@infoave.net,"; // Steve Vinson(No Longer Works Here)
$lt_email_list .= "jamesroberts@infoave.net,"; // Kevin Roberts
$lt_email_list .= "bryantp@infoave.net,"; // Patrick Bryant
$lt_email_list .= "gvarnador@infoave.net,"; // Gene Varnadore
$lt_email_list .= "danieldavis@infoave.net"; // Daniel Davis
$email_copy = $lt_email_list;

//
// Uncomment is SLT's need to be e-mailed also.
$slt = "doyle@infoave.net,billyl@infoave.net,perrott@infoave.net,johnrwelch@infoave.net";
//
// Send an copy of this e-mail to the person submitting the form
if($email_copy == ""){
$email_list = $email.",".$slt;
}else{
// Sends e-mail to Provider and SLT and submitter(see above.) )
$email_list = $email.",".$slt.",".$email_copy;
}
// Sends e-mail to Provider only
// Note: This variable is pulled from a hidden tag on the form labled "email".
// the value part of the element can be used to specify the e-mail address that
// this forms contents is sent to. 
//$email_list = "$email";
//$email_list = "$slt";
// ===========================================
//     End of e-mail Configuration Section
// ===========================================

// Header info
$app = "Tech Support Escalation Form";
$from = "From: support@infoave.net\r\n";
$reply_to = "Reply-To: support@infoave.net\r\n";
$bcc = "Bcc: $slt\r\n";
$mailer = "X-Mailer: $app";
$all_headers = $from.$reply_to.$mailer; 
// SMS Format
$message .= "Customer: $customer_name\n";
$message .= "Number: $customer_phone\n";
$message .= "$sms\n";
// This does the actual e-mail
mail($email_list, "$subject", $message,"$all_headers");
?>
