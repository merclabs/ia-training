function validateform(){

if(document.form.logid.value == ""){
alert("You must provide the Log ID.");
document.form.logid.focus();
return false;
}
if(document.form.customer_name.value == ""){
alert("You must provide the customers name");
document.form.customer_name.focus();
return false;
}
if(document.form.customer_phone.value == ""){
alert("You must provide the customers Phone Number.");
document.form.customer_phone.focus();
return false;
}
if(document.form.sms.value == ""){
alert("You can not send a \"Blank\" Message.");
document.form.sms.focus();
return false;
}
return true;
}
