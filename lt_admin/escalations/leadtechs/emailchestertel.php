<?require("emailcode4.php")?>
<h1>LT Escalation and E-Mail Tool</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr><br />
<tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Main Menu</a></td></tr>
</td></tr>
</table>
</p>
<table class="list_table">
<tr>
<td class="list_header" colspan=2><b>E-mail Sent...</b></td>
</tr>
<tr>
<td class="content_list">
<p>
The information you have provided has been submitted to <b><?echo($provider);?></b> at the following
address: <b><?echo($email)?></b>. All <b>LTs</b> and <b>Supervisors</b> will also receive a copy
of this message as well. 
</p>
</td>
</tr>
</table>
<p class="note">
<b>Note:</b>
If you do not receive a copy of this message, please check with an LT or Supervisor to see 
if they have not received a copy as well. If this is the case, then e-mail 
<a href="mailto:cbarnette@infoave.net">webmaster</a> about this problem. If others have 
received this message and you have not, please check your spam filter. Click here to check your 
<a href="http://login.postini.com/exec/login" target="_blank">Postini Message Center</a>.
</p>
<p align="right">
<!-- This button simply goes back in history 1 page. -->
<input type="button" value=" Done " onclick="location.href='?content=./main.php'">
</p>
