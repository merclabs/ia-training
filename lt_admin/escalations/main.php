<h1>Provider Escalation Procedures</h1>
<p>
<p class="note"><b>Note:</b> Providers not listed here are not escalated, but sent 
to customer service, <i>see an LT if you are not sure</i>.
</p>
<!--
<p>
<table class="List_table">
<tr>
<td class="list_header"><b>Standard Escalation Procedure</b></td>
</tr>
<tr>
<td class="content_link">
<br>
<ol>
<li>Troubleshoot according to steps for specific DSL/Cable Modem on <b>IA Training Broadband</b> section.
<li>Get permission to write up from Lead Tech.
<li>Complete a "Cable and DSL Write Up Sheet".
<li>Gather all necessary infornation:
 <ul type="square">
  <li>User Name</li>
  <li>Customer's Full Name</li>
  <li>Street Address(include city)</li>
  <li>Contact Number</li>
  <li>PTS Log ID</li>
  <li>DSL Line Number or Account Number</li>
  <li>Operating System</li>
  <li>Connection Type<br>( DSL, ADSL, 1 Way Cable, 2 Way Cable )</li>
  <li>Description of problem</li>
  <li>Can they Ping(Domain Name, IP Address, NIC)</li>
  <li>What you have checked<br>( Network Settings, Power Cycle, PPoE )</li>
 </ul>
<li>Check with Lead Tech to make sure you have all information from customer before you let
customer go.
<li>Give "Cable and DSL Write Up Sheet" to Lead Tech or DSL Tech.
<li>Give customer Log ID.
</ol>
<p class="note">
<b>Note:</b> <b>LT</b> will review log to determine if additional troubleshooting is needed. If needed,
callback will be given to a <u>callback tech</u>. If not needed, <b>LT</b> will escalate according to 
providers specific escalation procedure.    
</p>
</td>
</tr>
</table>
</p>
-->
<table class="list_table">
<tr>
<td class="content_link">
<b>Instructions:</b> Click on a providers name to view their escalation procedure. 
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/blue_mountain.php">Blue Mountain Cable</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/chestertel.php">Chester Long Distance Service, Inc. (CHESTER)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/coastalnow.php">Coastal Communications (CLDS)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/etinternet.php">Ellerbe Telephone Company&nbsp; (ELLERBE)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/comporium_fortmill.php">Fort Mill Telephone Company (FORTMILL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/cyberlink.php">Galaxy Holdings Inc. (CYBERLINK)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/grics.php">Gallatin River Communications (GALLATIN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/graceba.php">Graceba Total Communications,Inc. (GRACEBA)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/gulftel.php">GulfTel Communications (GULFTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/hardynet.php">Hardynet Telecommunications (HARDY)</a>
</td>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/hargray.php">Hargray Telephone (HARGRAY)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/horry.php">Horry Telephone Cooperative, Incorporated (HORRY)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/comporium_lancaster.php">Lancaster Telephone Company (LANCTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/lexington.php">Lexington Telephone Company (LXTN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/mebtel.php">MebTel Communications (MEBTEL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/northpenn.php">North Penn Access (NORTHPEN)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/pbtcomm.php">PBT Communications (PONDBR)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/comporium_rockhill.php">Rock Hill Telephone Company (ROCKHILL)</a>
</td>
</tr>
<tr>
<td class="content_link"><a href="?content=./lt_admin/escalations/wilkes.php">Wilkes Communications (WILKES)</a>
</td>
</tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->