<h1>LT/Supervisor Administration</h1>
<p>
<h2><span style="color:#000000;">You are logged in as: [</span><span style="color:#ff3300;"><?echo($_SERVER['PHP_AUTH_USER']);?></span><span style="color:#000000;">]</span></h2>
<p class="note">
<b>Att:</b> Once you log into this section, you will not have to login again <i>unless</i> you close
all instances of your <i>Web Browser</i>. If you <i>navigate</i> to another section of this web site, you can get back to 
this section by clicking on the <b>LT LOGIN</b> button and clicking <b>OK</b>. This should let you back into
this section without prompting for your user/pass again.   
<br><br>
<i><b>--Clayton</b></i>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./escalations/leadtechs/main.php">Escalations</a></td></tr>
<tr><td class="content_link"><a href="?content=./evaluation/main.php">Evaluations</a></td></tr>
<tr><td class="content_link"><a href="http://accounts.infoave.net/" target="_blank">Account Lookup</a></td></tr>
<tr><td class="content_link"><a href="?content=./ontheboard/admin/editor.php">On the Board Editor</a></td></tr>
<tr><td class="content_link"><a href="http://www.iatraining.net/newdesign/webblog/" target="_blank">Daily change blog for IA Training.</a></td></tr>
<tr><td class="content_link"><a href="http://www.iatraining.net/newdesign/schedule/">Technical Support Schedule Page [BETA]</a></td></tr>
</td></tr>
</table>
</p>