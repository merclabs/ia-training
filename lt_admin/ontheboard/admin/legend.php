<table class="list_table">
<tr><td class="level_header" colspan=2>On the Board Message Description</td></tr>
<tr><td class="1">Level 1</td><td class="message">Notice - Standard Posts</td></tr>
<tr><td class="2">Level 2</td><td class="message">Caution - Temporary or Unofficial Posts</td></tr>
<tr><td class="3">Level 3</td><td class="message">Warning - Officially Down Posts</td></tr>
<tr><td class="4">Level 4</td><td class="message">All-Clear - Officially Resolved Posts</td></tr>
</table>
