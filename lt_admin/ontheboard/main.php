<h1>On the Board - Last Update: <?echo($last_posted_date);?></h1><br />
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="#section1">Our DNS Numbers</a></td></tr>
<tr><td class="content_link"><a href="#section2">Common Modem Init Strings...</a></td></tr>
<tr><td class="content_link"><a href="#section3">Providers that are no longer with us.</a></td></tr>
<tr><td class="content_link"><a href="#section4">Other Provider Support Numbers.</a></td></tr>
<tr><td class="content_link"><a href="#section5">Support Numbers for Computer Manufactures</a></td></tr>
</td></tr>
</table>
</p>

<p class="note">
<b>Content Moved:</b> If you are looking for the "On the Board" section, this is now no the 
main page of IA Training. 
</p>

<p>
<table class="list_table">
<tr><td class="list_header"><a name="section1">Our DNS Numbers</a></td></tr>
<tr><td>
<br>
<ul>
<li>PRIMARY - <b>206.74.254.2</b>                 
<li><i>Secondary</i> - <b>204.116.57.2</b> 
</ul>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<table class="list_table">
<tr><td class="list_header"><a name="section2">Common Modem Init Strings...</a></td></tr>
<tr><td>
<br>
<p align="center">
<i><u>ANYTHING</u> THAT IS LESS THAN A <b>56 K MODEM</b> USE</i> <b>AT&F&C1&D2</b>
</p>
<ul>
<li>US ROBOTICS - <b>s32=98</b> or <b>s32=34</b> or <b>s32=32</b>
<li>ROCKWELL HCF - 56K - <b>+ms=v34</b> or <b>+ms=v90</b>
<li>LT WINMODEM - <b>s38=0</b> or <b>s28=0</b>
<li>CONEXANT HCF 56K - <b>+ms=v34</b> or <b>+ms=v90</b>
<li>HSP MICROMODEM - <b>atn0s37=14</b> or <b>atn0s37=12</b> or <b>%b0</b>
<li>MOTOROLA SM 56 - <b>*mm1l</b> or <b>%b0</b>
<li>U MODEMS  - <b>s38=0</b> or <b>s32=34</b>
<li>56 INTERNET FAX - <b>s32=34</b>
<li>SUPRA 56ki (Diamond) - <b>at&fw2</b>
<li>COMPAQ PRESARIO 336 DF - <b>at&f&q6</b>            
</ul>
<i>For unknown modems (Hardware) use <b>+ms=v90</b>  or (Software) <b>s38=0</b></i>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<table class="list_table">
<tr><td class="list_header"><a name="section3">Providers that are no longer with us.</a></td></tr>
<tr><td>
<br>
<ul>
<li>Lancaster telephone has a number that no longer works Old # 286-8137
<li><b>Note:</b> We still support <i>Wilkes(GA)</i> but <u>not</u> <b>Wilkes(NC)</b>
<li>Ardmore (they need to call 1-888-878-4354) 
<li>CTC=Vnet(they can call 1-800-230-7808) 
<li>Kiwash 
<li>Lowcountry(Oburg) 
<li>Northstate 
<li>Oregon Farmers 
<li>Pembroke(g-net) 
<li>Randolph telephone 
<li>R&B comm 
<li>RTC online (they can call 504-531-1246) 
<li><b style="color:#ff0000;">Sandhill DSL customers with DSL trouble can call 843-658-3471</b> 
<li>Skyline(Skybest) 
<li>Yadtel
</ul>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p>
<table class="list_table">
<tr><td class="list_header"><a name="section4">Other Provider Support Numbers.</a></td></tr>
<tr><td>
<br>
<ul>
<li>CTC=(VNET) - <b>1-800-230-7808</b>
<li>CTNIS - <b>1-775-777-0947</b>
<li>SKYLINE - <b>1-888-813-7039</b>
<li>SKYNET - <b>1-941-624-4401</b>
<li>INSITE - <b>1-877-377-1336</b>
<li>HARGRAY DSL Dail - <b>611</b>
</ul>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<table class="list_table">
<tr><td class="list_header"><a name="section5">Support Numbers for Computer Manufactures</a></td></tr>
<tr><td>
<br>
<ul>
<li>MAC Support - <b>1-800-767-2775</b>                
<li>NETSCAPE - <b>1-800-411-0707</b>
<li>MICROSOFT WIN98 - <b>1-425-635-7222</b> 
<li>WIN95 - <b>1-425-635-7000</b>                 
</ul>
</td></tr>
</table>
</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->