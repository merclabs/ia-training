<h1>Technical Support Rules</h1>
<p>
<p class="caution">
If you have any issues reguarding these rules, notify an LT or Shift Supervisor. Failure to follow
these rules or any other unprofesional actions may result in disiplinary action fom an LT or Supervisor.
</p>
<ol>
<li>No eating in tech room, all drinks must have a spill-proof lid.
<li>No listining to music, watching movies, chatting, while on a call. 
<li>Follow dress code. (shorts only on the weekend, sandles only with socks, no sleevless shirts.)
<li>No offinsive T-shirts.
<li>No bookbags on the tech floor(Spirit Telecom bags are allowed).
<li>Do not use phone system for personal phone calls, use phone in break room during break. 
<li>Keep cell phones off, do not place or answer your cell phone while on the tech floor.
<li>Do not install any software on computers without LT approval.
<li>Keep you work area clean, including the parking lot(no spitting in parking lot).
<li>Do not view pornographic or offinsive material on work computers.
<li>Do not talk loudly, be considerate of techs on the phone around you, customers CAN and WILL hear you. 
<li>Do not log into work computers from home or log into home or outside computers from work.
<li>Keep feet off workstations and computers, keep them on the floor.  
<li>Take care when using the mute button or covering the headset mic with finger, customers may still be able to hear you. 
<li>Be on time from breaks and lunches.
<li>The hotline number is to be used to reach you in emergencys only, no personal call or bill collectors.
<li>Any friend, guest, or family member that comes here to see you must notify a Supervisor and wait in the break room.
<li>Sexual harrasment will <u>NOT</u> be tolerated, be careful how you interact with co-workers.
<li>Do not remove equipment(including headsets) from a workstation, if your workstations is missing a headset
or the computer/phone is not working, notify an LT. 
<li>Bandwidth is to be used for work purposes, do not download anything or view streaming audio/video.
<li>Do not use chat programs for the first <u>two weeks</u> minimum <b>AFTER</b> training ends.
<li>No un-naturally colored hair.
<li>No facial peircings.
<li>Badge access is to be used by the employee that it is issued to, if you have lost or misplaced your
badge, notify a supervisor.  
</ol>
</p>
<h3>10 hour Midshift employee's (10 to 9 and 11 to 10)</h3>
<p>
<b><u>Anyone</u></b> who currently is scheduled to work either <b>10am to 9pm</b> or <b>11am to 
10pm</b> needs to take a 1 hour lunch(not skipping it) from now on as well 
as your normal 15 minute breaks.  Please start this by tomorrow if you 
aren't already doing it.
</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->