<?
// This file contains Location information for hard links

// [URLs for Site Location 
//Define Base Address
$baseaddress = "http://www.iatraining.net/newdesign";

// [Content for main page]
// Define Default Menu
$default_menu = "content/menu.php";
$lt_default_menu = "content/lt_menu.php";
// Define Default SideBar
$default_sidebar = "content/sidebar.php";
$lt_default_sidebar = "content/lt_sidebar.php";
// Define Default Toolbar
$default_toolbar = "content/toolbar.php";
$lt_default_toolbar = "content/lt_toolbar.php";
// Define location of Lead Tech location for evaluations and ecalations
$lt_admin = "./lt_admin/?content=./main.php";
// Defiine Default Content(Home Content)
if($content == ""){
 $content = "content/main.php";
}
// Define Default "On the Board" File
//(Displays current outages, allows outages to be posted and deleted daily)
$ontheboard = "./lt_admin/ontheboard/main.php";

// Configurations for Frontpage...
/* 
There are 3 sections:
 frontpage - top of front page, updated daily...
 healines - bottom-left of front page
 weather -  bottom-right of front page
 
 These content files are located in the "./content" directory
*/
$frontpage = "./content/frontpage.php";
$reminders = "./reminders/reminders.php";
$weather = "./content/weather.php";
$lt_weather = "../content/weather.php";

?>

