<?
// ./cgi-bin/config.php

// Host Name Location
// This variable is the location of the main website.
$hostname = "http://www.iatrainin.net";

// Todays Date              // Exapmles
//$today = date("M j, Y"); // Mar 1, 20004
$today = date("F j, Y"); // March 1, 2002

// Default Title for Page
$default_title = "IA Training - Spirit Telecom Technical Support";

// Define Default Logo and alt text
$default_logo = "./images/intranetheader_clean.png";
$default_logo_alt = "The New Spirit Telecom&reg; Technical Support Site";

?>
