<h1>HOW TO... Disable Autodial in Widnows XP</h1>
<p>
<img src="./howto/images/training.gif" 
alt="HOW TO..." style="float:left;">
<b>Modem Automatically Attempts to Establish a Dial-Up Connection When You Start 
Your Computer or Start a Program (Q316530)</b>.
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./howto/main.php">Back to HOW TO...</a></td></tr>
</td></tr>
</table>
</p>
<p>
<h2>CAUSE</h2>
This behavior can occur if any of the following conditions are true: 
<ul>
 <li>The autodial feature is enabled. Autodial is enabled by default in Windows XP. 
 <li>The program that you start automatically checks for updated components or Web pages. 
 <li>A program that dials your ISP is located in the Startup folder or on the registry's Run key. 
 <li>A registry entry is set to dial . 
</ul>
</p>
<br>
<p>
<h2>RESOLUTION</h2>
To resolve this issue, do the following: 
<ul>
<li><i>Disable</i> <b>autodial</b>. 
</ul>
To do this, follow these steps:
<ol>
<li>Click <b>Start</b> , and then click <b>Control Panel</b> .  
<li>If you are in Category View, switch to Classic View. 
<li>Click <b>Network Connections</b>, and then on the <b>Advanced</b> menu, select <b>Dial-up Preferences</b>. 
<li>Click the <b>Autodial tab</b>.  
<li>Click to clear the check boxes that are listed under <b>Enable autodial by location</b>. 
<li>Check the <b>Always ask me before autodialing</b> check box (if not already selected). 
<li>Check the <b>Disable autodial while I am logged on</b> check box. 
<li>Click <b>OK</b> , and then close the <b>Network Connections</b> dialog box. 
</ol>
</p>
<p class="note">
Also try selecting <b>Never dial a connection</b> on the <b>Connection</b> tab in <b>Internet Options</b>.
</p>
