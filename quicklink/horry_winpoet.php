<h1>Quick Link - Horry Winpoet Software</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/horry.php">Horry</a></td></tr>
<tr><td class="content_link"><a href="http://www.iatraining.net/winpoet.php" target="_blank">WinPoET Software - <i>Screenshots</i></a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
.
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Downloading Horry's Winpoet Software</td></tr>
<tr><td>
<p>
<p>
Since <b>DSL customers</b> can <i>dialup</i> with their <b>DSL accounts</b>, you can point customers 
who need to <i>reinstall</i> <b>WinPoET</b> on their <b>PC</b> to the following addresses to download
the <b>WinPoET Software</b>:
<ul>
<li><b><a href="http://elmer.sccoast.net/download/winpoet.zip">http://elmer.sccoast.net/download/winpoet.zip</a></b> - If they have 
<a href="http://www.winzip.com" target="_blank"><b>WinZip</b></a> installed on their computer.
<li><b><a href="http://elmer.sccoast.net/download/winpoet.exe">http://elmer.sccoast.net/download/winpoet.exe</a></b> - If they do not have 
<a href="http://www.winzip.com" target="_blank"><b>WinZip</b></a> installed.
<p> 
</p>
</td></tr>
</table>
</p>
