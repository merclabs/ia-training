<h1>Quick Link - Hargray</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
</td></tr>
</table>
</p>

<table class="list_table">
<tr><td class="list_header">Hargray New Information</td></tr>
<tr><td class="content_link">
<p>
Here is the transition link sent to Hargray customers. They will 
be continuing to use Postini. As far as I know the transition is scheduled 
for the end of this month. Here is their mail info and DNS numbers which we
should not use until the changeover. And remember the link to change 
their passwords is: 
<a href="http://myaccount.hargray.com" target="_blank">http://myaccount.hargray.com</a>
</p>
<p>
<b>Hargray's New DNS:</b>
<ul>
<li><b>64.203.254.30</b>
<li><b>64.203.254.31</b>
</ul>
<br>
<b>Email Settings</b>
<ul>
<li><b>mail.hargray.com</b>
<li><b>smtp.hargray.com</b>
</ul>
</p>
<br>
<i>Thanks</i>,<b>Doyle</b>
</td></tr>
</table>