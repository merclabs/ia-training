<h1>Quick Link - </h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/horry.php">Horry</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Check List</td></tr>
<tr><td>
<br>
<ul>
<li>
<li>
<li>
<li>
</ul>
</td></tr>
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Horry VDSL Modems</td></tr>
<tr><td>
<br>
<p>
<ol type="a">
<li><b>Residential Gateway:</b>customers use <b>WinPoET</b> software except for <b>XP</b> users. 
    If the customer is getting <b>WinPoET</b> error messages, check for <b>error number</b>. 
    Check the <u>status</u> of the lights on <b>Gateway</b>. <b>Power</b> should be on <i>Red</i>, <b>Network</b>
    should be <i>Green</i>, and <b>Online</b> should be <i>Green</i>. 

<li><b>Have the customer check for video on TV:</b> If the customer has <u>video</u> on <b>TV</b>, 
    <b>check the lights</b> on the modem.  If the <b>Network</b> light is flashing,
		<i><b>see:</b> <a href="?content=./lt_admin/escalations/horry.php">Ecalation Procedure</a>.</i> 
		If the <b>Online</b> light is amber, the <b>Ethernet</b> connection between the <b>VDSL</b> and 
    <b>PC</b> is established, but VDSL is  <u><b>not provisioned</b></u> for data. Write up customer 
    to have box reprogrammed. <i><b>see:</b> <a href="?content=./lt_admin/escalations/horry.php">Ecalation Procedure</a>.</i> 
</ol>
</p>
</td></tr>
</table>
</p>
