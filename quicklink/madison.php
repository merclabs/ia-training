<h1>Quick Link - MadisonRiver</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
</td></tr>
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Madison Remedy Links</td></tr>
<tr><td class="content_link"><a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVFixedView.jsp" target="_blank"> First Level Remedy Login</a></td></tr> 
<tr><td class="content_link"><a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/ISPwebHelpdesk/start.jsp" target="_blank">Second Level Remedy</a></td></tr> 
<!-- Old Remedy Address: Changed:05-Mar-2004 07:42 AM -cb <tr><td class="content_link"><a href="https://secureisp.madisonriver.net/remedy" target="_blank"> First Level Remedy</a></td></tr> -->

</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Madison River Support Pages</td></tr>
<tr><td class="content_link"><a href="http://support.gulftel.com" target="_blank">Gulftel</a></td></tr>  
<tr><td class="content_link"><a href="http://support.mebtel.com" target="_blank">Mebtel</a></td></tr>  
<tr><td class="content_link"><a href="http://support.coastalnow.net" target="_blank">Coastal</a></td></tr>  
<tr><td class="content_link"><a href="http://support.grics.net" target="_blank">Gallatin</a></td></tr> 
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Madison Repair Numbers</td></tr>
<tr><td class="content_link">
<i>New Information Pending</i>
<!--
<br>
<p>
 <ul>
    <li><b>Mebtel</b> 1-800-770-9783 
    <li><b>Gulftel</b> 1-800-771-2718 
    <li><b>Grics</b> 1-800-771-4926 
    <li><b>Coastal</b> 1-800-773-2415  
  </ul>
</p>
-->
</td></tr>
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Madison Customer Service Numbers</td></tr>
<tr><td class="content_link">
<i>New Information Pending</i>
<!--
<br>
<p>
 <ul>
    <li><b>Mebtel</b> 1-919-563-9111 
    <li><b>Gulftel</b> 1-251-952-5100
    <li><b>Grics</b> 1-800-223-1851 
    <li><b>Coastal</b> 1-912-369-9000
  </ul>
</p>
-->
</td></tr>
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">CD Registration Key Codes</td></tr>
<tr><td class="content_link">
<br>
<p>
 <ul>
   <li><b>MRC</b> - MDRV1 0352 379279 MRC
   <li><b>Coastal</b> - MDRV2 0346 367880 MRC
   <li><b>Gallatin</b> - MDRV3 0347 357676 MRC
   <li><b>Mebtel</b> - MDRV4 0348 347428 MRC
   <li><b>Gulftel</b> - MDRV5 0349 337136 MRC
  </ul>
</p>
</td></tr>
</table>
</p>

<p>
<table class="list_table">
<tr><td class="list_header">Quick ADSL Troubleshooting for the 5600 series</td></tr>
<tr><td class="content_link">
<br>
<p>
<ol>
<li>
<b>Check Network:</b> Client for Microsoft Networks Dial up Adapter Tango PPoE Adapter 
NDIS Tango PPoE Adapter Ethernet Card TCP/IP>Ethernet Card TCP/IP>dial up Adapter 
</li>
<li>
<b>Remove Network Components:</b> Make sure all <u><b>unnecessary or unused</b></u> Network components are removed (verify w/ the 
customer before removing any component). 
</li>
<li>
<b>Devices:</b> Devices such as fax machines, caller ID boxes, or phones that share the same phone 
number as the DSL require a line filter, which prevents modem noise from disrupting the 
DSL signal on the phone line. A line filter is not needed for the line the DSL modem is 
on unless the line filter used is a Dual Line Filter that connects both DSL and Phone 
on the same line. 
</li>
<li>
<b>Check IP's:</b> Make sure IP and Gateway in TCP/IP's for both NIC and Dial up Adapter are set to 
Obtain Automatically and DNS #'s are entered (Primary-64.40.72.25 Secondary-64.40.75.20). 
</li>
<li>
<b>Dialers:</b> Delete all dialers from dial up networking. 
</li>
<li>
<b>PPoE Connection(XP):</b> Create a new dialer named ADSL from the Make New Connection icon in the Network Connections window
(making sure it is set to use the PPoE client and that there is something in the VPN server name box (this can be anything- it does not matter)). 
</li>
<li>
<b>Shortcut:</b> Create a Shortcut to this dialer on the desktop. 
</li>
<li>
<b>Internet Options(XP):</b> Go into Start > Control Panel > Internet Options > Connection tab. 
Make sure the only dialer is ADSL and it is the Default connection. Check the Always Dial my Default Connection option. Make 
sure nothing is checked in LAN settings. 
</li>
<li>
<b>Power Cycle:</b> Power cycle the ADSL pipe and PC (there is a button on the back of the unit- push once to 
turn it off and a second time to turn it back on) 
</li>
<li>
<b>Try to Connect:</b> Have them try to connect using the Desktop ADSL shortcut (<u>not Tango</u>). If it does 
not connect- troubleshoot further according to the DUN error received, just like it 
was a regular dial up connection. If error is 691- username or password error, verify 
that they should or should not be using ".grics.net". 
</li>
<li>
<b>Other Devices:</b> Cordless phones, Dish Cable receivers, Direct TV receivers can cause interference 
with ADSL if located too close to the computer. If a customer has these products in 
their home they need to have them as far apart as possible so that they will not cause 
interference. Two line phones can not be filtered properly with the inline filters. 
Our solution to this problem is to let the customer know so he can discontinue using 
the two line phone or the customer will need a separate line for adsl. 
</li>
</ol>
</p>
</td></tr>
</table>
</p>


<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

 

 
