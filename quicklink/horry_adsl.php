<h1>Quick Link - Horry ADSL Modems</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/horry.php">Horry</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Business Customers</b> use the <i>Speed Stream DSL Router/Modems</i> and <b>Residential Customers</b> 
use the <i>Westell ADSL modems</i>.
</p>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Check List</td></tr>
<tr><td>
<br>
<ul>
<li><a href="#step1">Businesses Customers</a>
    <p>
		<ul>
		 <li>If the Speed Stream is setup for Router mode
		 <li>If all lights are on the modem
		 <li>If the DSL light is flashing
		 <li>If the Enet light is not on
		 <li>If the business has a router
		</ul>
		</p>
<li><a href="#step2">Residential Customers</a>
       <p>
		<ul>
		 <li>WinPoET PPOE software
		 <li>Power Cycle the modem
		 <li>If Ready Light flashing red or green
		 <li>If Link Light is not on
		</ul>
		</p>
</ul>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Horry ADSL Modems</td></tr>
<tr><td>
<br>
<p>
<ol>
<li><a name="#step1"><b>The Speed Stream for Businesses Customers:</b> <i>can be configured for Router mode or 
    Bridge mode.  No WinPoET software is required.</i></a>
<ol type="a">
     <li>If the Speed Stream is setup for Router mode, you will be able to open
         your web browser and pull up the configuration page of the modem:
         <b>http://192.168.100.1/</b> Here you will be able to see if the 
         modem has a WAN IP of <b>66.153.xxx.xxx</b>.
        
     <li>If all lights are on the modem, but cannot browse, or WAN IP address 
         is <b>0.0.0.0</b>, click on <b>Simple setup</b> on the left of the screen and go to the 
         <i>username/password</i>. Have the customer type their <i>user/password</i> and 
         <u>reset</u> the modem. If all lights are on, and their <i>username/password</i> are
         not working, and the modem is getting an IP of <b>66.153.xxx.xxx</b>.
				 <i><b>see:</b> <a href="?content=./lt_admin/escalations/horry.php">Ecalation Procedure</a>.</i>
        
     <li>Check the status on the modem and <b>power cycle</b> the modem. If the <b>DSL</b>
         light is flashing, this means there is a <u>problem</u> with the <b>DSL line</b>. Make 
         sure the line is plugged into the modem securely and properly. If so and
         the <b>DSL light</b> is still <u>flashing</u>.
				 <i><b>see:</b> <a href="?content=./lt_admin/escalations/horry.php">Ecalation Procedure</a>.</i>. 
         
     <li>If the <b>Enet</b> light is not on, then the <b>Ethernet</b> connection to the modem
         and <b>PC</b> are not linked.  Check <b>network cables</b>. connected to modem and PC.
 
    <li>If the business has a <b>router</b>; such as, <i>Linksys Router</i>, the <b>Speed Stream 
         modem</b> should be in <u><b>bridge mode</b></u>. If the modem is in <b>bridge mode</b>, you 
         will <u>not</u> be able to access router <i>config page</i>. The <b>Linksys Router</b> will 
         need to be setup for the <b>PPOE</b> connection. The <b>WAN</b> address on the 
         router should have the <b>66.153.xxx.xxx</b> address. 
 </ol>
<!-- Back to Top button -->
<p align="right">
  <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<li><a name="#step2"><b>Residential Customers:</b> <i>Westell Modem for Residential Customers.</i></a>
 <ol type="a">
    <li>All customers need to use <b>WinPoET PPOE</b> software to connect with their <b>DSL</b> 
        modem. Except for <u><b>XP users</b></u> they need to use the <b>PPOE</b> with <b>XP</b>. If the 
        customer is getting errors connecting with the software.

    <li>Check the status of the lights on the modem. <b>Power Cycle</b> the modem. 

    <li>Top 3 lights should be on: <b>POWER</b>, <b>READY</b>, and <b>LINK</b>. If the Ready
        light is flashing red or green, then there is a problem with the DSL connection.
				<i><b>see:</b> <a href="?content=./lt_admin/escalations/horry.php">Ecalation Procedure</a>.</i>

    <li>If the <b>Link</b> light is <u>not</u> on, then the <b>Ethernet</b> link to the modem and pc
        is <u>not</u> established; <b>check physical connections</b>. 
 </ol>
<!-- Back to Top button -->
<p align="right">
  <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
</ol>
</p>
</td></tr>
</table>
</p>
