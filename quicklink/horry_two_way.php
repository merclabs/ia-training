<h1>Quick Link - Horry Two-Way Cable Modems</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/horry.php">Horry</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Two-Way Cable Modems</b> <u>do not</u> have a telco return, only the <b>Surfboard 3100D</b>
cabel modem can be either <i>One-Way</i> or <i>Two-Way</i>.
 If a customer is getting an IP starting with 
<b><i>66...</i></b>, this means that they are pulling an IP address from <b>HTC</b>. If
a customer is getting an IP of <b><i>192...</i></b>, then have the customer release and 
renew, according to the version of windows they are using to obtain an IP address starting
with <b><i>66...</i></b>.  
</p>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Check List</td></tr>
<tr><td>
<br>
<ul>
<li><a href="#step1">If all the lights are on</a>
<li><a href="#step2">If the Receive light is flashing</a>
<li><a href="#step3">If the Send light is flashing</a>
<li><a href="#step4">If the Online light is flashing</a>
</ul>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Two-Way Cable Modems</td></tr>
<tr><td>
<p>
<b>First, check the status lights on the front of the cable modem.</b>
<ol>
<li><a name="step1"><b>If all the lights are on the modem:</b> <i>the modem is online.</i></a>
             <ol type="a">
                <li>If the customer is unable to browse, check the IP address on 
                    the PC's network card that is connected to the cable modem.
                    The customer's IP address on their network card should be 
                     66.153.xxx.xxx.

                <li>If it is 192.168.100.11, have the customer to release and renew
                    their IP address of their network card bound to the cable modem.

                <li>If it is a 169.xxx.xxx.xxx, which is a default windows IP address, 
                     make sure the customer has not changed PCs or NIC cards
                     since installation of modem. If so, the new Network card will need 
                     to be provisioned by HTC.  No further troubleshooting should be 
                     done if the customer has a new PC or new Network card. Refer the 
                     customer to Tier II. 843-369-8796

                <li>For Win9x and ME have the customer release and renew the IP 
                     address bound to the Network Card by clicking on the Release
                     and Renew buttons on the WINIPCFG screen. 
                     DO NOT click Release all and Renew All. It should give them a 
                     66.153.xxx.xxx address back. Have the customer try to browse. 

                <li>If the customer still cannot browse, check the Internet Options
                     in Internet Explorer. Make sure that there are no checks in any
                     of the boxes under LAN settings. Then, have the customer try 
                     and browse. 

                <li>Check their TCP/IP properties for the Network Adapter. Make sure 
                     that the network adapter is setup to obtain an IP address automatically. 
                     Also, make sure that DNS is disabled and TCP/IP is set to the default
                     protocol. 

                <li>If the customer is getting the proper IP address, but still cannot browse, 
                     If the customer can ping an IP address and not domain name, do the 
                     following. For Win 9.x and ME, uninstall Client for Microsoft Networks
                     and Dialup Networking. Restart the computer and reinstall. Again, make 
                     sure that you check for CABS or the Windows CD. 

                <li>Make sure any firewall software has been disabled.
						</ol>	
								<!-- Back to Top button -->
                <p align="right">
                  <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
                </p>
                <!-- Back to Top button -->

<li><a name="step2"><b>If the Receive light is flashing:</b> <i>Modem has not locked on the downstream
    channel....signal issues.</i></a>
      <ol type="a">
        <li>Check to make sure the CATV is working on the TV. This will verify the
            CATV is active in the house.

        <li>Check CATV connection to modem and make sure coax cable connector
            is screwed in tightly.

        <li>Reset and restart the cable modem. Go to: http://192.168.100.1/config.html
             to bring up the modem configuration page. Click on reset all defaults and
             restart the cable modem.

        <li>If after reset the receive light does not stay solid, there is a signal issue.
             Makes sure there is no splitters.
             No further troubleshooting should be done. Refer to Tier II support.
       </ol>  
				 <!-- Back to Top button -->
         <p align="right">
           <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
         </p>
         <!-- Back to Top button -->

<li><a name="step3"><b>If the Send light is flashing:</b> <i>Return path issues...signal issues...no further
      troubleshooting. Refer to Tier II.</i></a>
         <!-- Back to Top button -->
         <p align="right">
           <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
         </p>
         <!-- Back to Top button -->			
<li><a name="step4"><b>If the Online light is flashing:</b> <i>Modem is not provisioned by HTC. Possible 
      network problems. Refer to Tier II and Network Control.</i></a>
         <!-- Back to Top button -->
         <p align="right">
           <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
         </p>
         <!-- Back to Top button -->	
</ol>
</p>
</td></tr>
</table>
</p>
