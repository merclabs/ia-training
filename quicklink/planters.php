<h1>Quick Link - Planters</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
</td></tr>
</table>
</p>
<table class="list_table">
<tr><td class="list_header">General DSL Information</td></tr>
<tr><td>
<br>
<p>
<b>Planters</b> issues <b>Westel DSL Modems</b>, that use <b>PPPoA</b> (PPP over ATM) protocol
and does <b style="color:#ff0000;">NOT</b> require any <u>Software</u> or <u>Config Page</u> to set up the <b>DSL Modem</b>, 
These modems use a <b>Bridged</b> type connection and work with most Operating Systems. <b>DSL Customers</b>
are supplied with a <b>Static IP Address</b> from <b>Planters</b>, using the following DNS Servers: 
<ul>
<li><i>Primary</i> DNS is <b>209.221.47.26</b>
<li><i>Secondary</i> DNS is <b>209.221.47.30</b>
</ul>
The lights to watch are <b>Power</b>, <b>Ready</b>, and <b>Link 
Activity</b> , which uses a <b>Bridged</b> connection
and connects via <b>Ethernet</b>.
</p>
</td></tr>
</table>
