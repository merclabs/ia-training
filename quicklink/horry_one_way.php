<h1>Quick Link - Horry One-Way cable Modems</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/main.php">Quick Links</a></td></tr>
<tr><td class="content_link"><a href="?content=./quicklink/horry.php">Horry</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>One-Way Cable Modems</b> are <b>telco return</b> type modems, models used are mostly 
<b><i>Surfboard Cable Modems</i></b>. If a customer is getting an IP starting with 
<b><i>66...</i></b>, this means that they are pulling an IP address from <b>HTC</b>. If
a customer is getting an IP of <b><i>192...</i></b>, then have the customer release and 
renew, according to the version of windows they are using to obtain an IP address starting
with <b><i>66...</i></b>.  
</p>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Check List</td></tr>
<tr><td>
<br>
<ul>
<li><a href="#step1">If all lights are on.</a>
<li><a href="#step2">If the Receive light is flashing</a>
<li><a href="#step3">If the Send light is flashing</a>
<li><a href="#step4">If the Online light is flashing</a>
<li><a href="#step5">If the Send and Online lights are flashing</a>
</ul>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">One-Way Cable Modems</td></tr>
<tr><td>
<p>
<b>First, check the status lights on the front of the cable modem.</b>
<ol>
 <li><a name="step1"><b>If all lights are on the modem:</b> <i>The modem is online.</i></a> 
   <ol type="a"> 
    <li>If the customer is unable to browse, check the IP address on 
        the PC's network card that is connected to the cable modem. 
        The customer's IP address on their network card should be 
        66.153.xxx.xxx.
    <li>If it is 192.168.100.11, have the customer to release and renew 
        their IP address of their network card bound to cable modem.
    <li>If it is a 169.xxx.xxx.xxx default windows IP address, make sure
        the customer has not changed PCs or NIC cards since installation 
        of modem. If so, the new Network card will need to be provisioned 
        by HTC. No further troubleshooting should be done if the customer 
        has a new PC or network card. Refer the customer to Tier II at: 
        843-369-8796
    <li>For Win9x and ME, have the customer release and renew the IP
        address bound to the Network Card by clicking on the Release and 
        Renew buttons on the WINIPCFG window. Do not click Renew and Release all, just
        Renew and Release. It should give them a 66.153.xxx.xxx. address back, and then 
        have the customer try to browse.
 	  <li>If they still cannot browse, check Internet Options in Internet Explorer. 
        Make sure that no checks are in any of the boxes under LAN settings. 
        After you have checked all of the above, have the customer browse. 
		<li>Check their TCP/IP properties for the Network Adapter. Make sure that 
        the network adapter is setup to obtain an IP address automatically. Also, make 
        sure that DNS is disabled and TCP/IP is setup as the default protocol.
    <li>If the customer is getting the proper IP address, but still cannot browse, do 
        a ping test to see if they can ping IP addresses and not domain name. If they 
        can ping IP addresses, but no domain names, try this. For Win 9x and ME uninstall
        Client for Microsoft Networks and Dialup Networking. Restart the computer 
        and then reinstall. ***Make sure the customer has CABS or CD 
        before attempting these steps.
    <li>Make sure any firewall software has been disabled.
   </ol>
	 <!-- Back to Top button -->
    <p align="right">
    <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
    </p>
   <!-- Back to Top button -->
<li><a name="step2"><b>If the Receive light is flashing:</b> <i>Modem has not locked on the downstream 
    channel....signal issues.</i></a> 
     <ol type="a">
        <li>Check to make sure the CATV is working on the TV. This will verify the 
            CATV is active in the house. 
        <li>Check CATV connection to modem and make sure coax cable connector 
            is screwed in tightly. 
        <li>Reset and restart the cable modem. Go to: http://192.168.100.1/config.html 
             to bring up the modem configuration page. Click on reset all defaults and 
             restart the cable modem. 
        <li>If after reset the receive light does not stay solid, there is a signal issue. 
             Makes sure there is no splitters. 
             No further troubleshooting should be done. Refer to Tier II support.
     </ol>
		 <!-- Back to Top button -->
    <p align="right">
    <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
    </p>
    <!-- Back to Top button -->
<li><a name="step3"><b>If the Send light is flashing:</b> <i>Telco modem is trying to make the upstream
      connection via the phone line.</i></a> 
     <ol type="a">
        <li>If the customer only has one line, then the modem will not be able to dial 
            out while on the phone.  The customer will need to hang up and let the
            modem dial out. 
        <li>If the customer has a separate line and the send light is flashing on the 
            modem, and the modem does not dial out, reset and restart the cable modem.
        <li>If the modem will still not dial out on its own after you restart the cable modem,
            check the phone line for a dial tone. Have the customer plug a phone into the 
            line that is running into the cable modem to see if there is a dial tone on the line. 
            If there is a dial tone on the line, but the modem will not dial out, refer the 
            customer to Tier II support. 
     </ol>
		 <!-- Back to Top button -->
    <p align="right">
    <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
    </p>
    <!-- Back to Top button -->
<li><a name="step4"><b>If the Online light is flashing:</b> <i>Cable modem did not connect to service</i>.</a>
       <ol type="a"> 
			  <li>Modem is not provisioned in our system.
        <li>Possible network problems. 
        <li>Try resetting all defaults and restart modem. Let the modem redial and 
            attempt the connection again.  If the online light is still flashing, please 
            report these to Tier II and our Network Control.
       </ol>
			 <!-- Back to Top button -->
       <p align="right">
        <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
       </p>
       <!-- Back to Top button --> 
<li><a name="step5"><b>If the Send and Online lights are flashing together:</b></a> 
       <ol type="a"> 
        <li>Cable modem is attempting to connect but could not dial up and establish
            a connection.
        <li>If the customer only has one line, the modem will not dial out while on the 
             phone. You will need to hang up and let the modem dial out. 
        <li>If the customer has a separate line and the send light is flashing and the
             modem does not dial out, reset and restart cable modem. 
        <li>If the modem will still not dial out on its own after you restart the cable modem,
            check the phone line for a dial tone. Have the customer plug a phone into the 
            line that is running into the cable modem to see if there is a dial tone on the line. 
            If there is a dial tone on the line, but the modem will not dial out, refer the 
            customer to Tier II support. 
       </ol>
			 <!-- Back to Top button -->
       <p align="right">
       <a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
       </p>
       <!-- Back to Top button -->
 </ol>
</p>
</td></tr>
</table>
</p>
