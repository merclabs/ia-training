<?
// Variables for Company Informaiton
//
// contained in company.php
//
// Offical Name
$officialname = "";
// Street Address
$address = "";
// City
$city = "";
// State
$state = "";
// Zip Code
$zip = "";
// Mailing Address one string:(pobox,city,state,zip)
$mailingadddress = "";
// Company Phone Number
$companyphone = "";
// Company Fax Number
$companyfax = "";
// 24 hour Trouble Number
$twentyfourtrouble = "";


// Variables for Contact Information
//
// contained in file contacts.php
// 
// Depatment
$department[] = "";
// Contact Name
$contact_name[] = "";
// Contact Phone
$contact_phone[] = "";
// Contact E-Mail
$contact_email[] = "";


// Variables for After Hours Information
//
// contained in the file afterhours.php
//
// After Hours Name
$afterhours_name[] = "";
// After Hours Office Phone Number
$afterhours_officephone[] = "";
// After Hours Home Phone
$afterhours_homephone[] = "";
// After Hours Cell Phone
$afterhours_mobile[]= "";
// After Hours Pager
$afterhours_pager[] = "";

?>
