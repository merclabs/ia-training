<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/company.php");?>
<?require_once("./provider/".$id."/contacts.php");?>
<?require_once("./provider/".$id."/afterhours.php");?>
<?require_once("./provider/".$id."/services.php");?>
<?require_once("./provider/".$id."/accessnumbers.php");?>
<h1><?echo($officialname)?></h1>
</head>
<body>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<input type="hidden" name="directory" value="<?echo($directory);?>">
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top colspan=6>Control Panel</td>
</tr>
<tr>
<td class="heading" valign=top colspan=6>
Click a Control Panel Option Below to Update your Licensee Information.
</td>
</tr>
</table>
<p>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top>Company Information</td>
</tr>
<tr>
 <td class="content_link" valign="middle">
    <a href="?content=./provider/editor/edit_companyinfo.php&id=<?echo($id);?>">Edit Company Information</a>
 </td>
</tr>
</table>
<p>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top>Contact Information</td>
</tr>
<tr>
  <td class="content_link" valign="middle">
	 <a href="?content=./provider/editor/edit_contactsinfo.php&id=<?echo($id);?>">Edit Contact Information</a>
	</td>
</tr>
</table>
<p>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top>Afterhours Information</td>
</tr>
<tr>
 <td class="content_link" valign="middle">
	 <a href="?content=./provider/editor/edit_afterhoursinfo.php&id=<?echo($id);?>">Edit Afterhour Contact Information</a>
 </td>
</tr>
</table>
<p>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top>Internet Service Information</td>
</tr>
<tr>
 <td class="content_link" valign="middle">
  <a href="?content=./provider/editor/edit_servicesinfo.php&id=<?echo($id);?>">Edit Internet Services</a>
 </td>
</tr>
</table>
<p>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top>Service Area Information</td>
</tr>
<tr>
  <td class="content_link" valign="middle">
    <a href="?content=./provider/editor/edit_accessnumbersinfo.php&id=<?echo($id);?>">Edit Service Areas</a>
  </td>
</tr>
</table>
</p>
<p align="center">
<input type="button" value="Back to Listing" onclick="location.href='?content=./provider/editor/main.php'">
</p> 



