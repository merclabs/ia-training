<?
// write_servicesinfo.php 
//
// create file pointer to directory. or this can be the authen_user variable
// as long as authen_user is in the navigator file.
$cfp = fopen($directory."services.php",'w');

// use file pointer $cfp to write to file.
fwrite($cfp,"<?\n");
if($portal == ""){
fwrite($cfp,"\$portal = \"None\";\n");
}else{
fwrite($cfp,"\$portal = \"$portal\";\n");
}
//
if($webmail == ""){
fwrite($cfp,"\$webmail = \"None\";\n");
}else{
fwrite($cfp,"\$webmail = \"$webmail\";\n");
}
//
if($webhosting == ""){
fwrite($cfp,"\$webhosting = \"None\";\n");
}else{
fwrite($cfp,"\$webhosting = \"$webhosting\";\n");
}
//
if($domain == ""){
fwrite($cfp,"\$domain = \"None\";\n");
}else{
fwrite($cfp,"\$domain = \"$domain\";\n");
}
//
if($popserver == ""){
fwrite($cfp,"\$popserver = \"None\";\n");
}else{
fwrite($cfp,"\$popserver = \"$popserver\";\n");
}
//
if($smtpserver == ""){
fwrite($cfp,"\$smtpserver = \"None\";\n");
}else{
fwrite($cfp,"\$smtpserver = \"$smtpserver\";\n");
}
//
if($ftpserver == ""){
fwrite($cfp,"\$ftpserver = \"None\";\n");
}else{
fwrite($cfp,"\$ftpserver = \"$ftpserver\";\n");
}
//
if($newsserver == ""){
fwrite($cfp,"\$newsserver = \"None\";\n");
}else{
fwrite($cfp,"\$newsserver = \"$newsserver\";\n");
}
//
if($primarydns == ""){
fwrite($cfp,"\$primarydns = \"None\";\n");
}else{
fwrite($cfp,"\$primarydns = \"$primarydns\";\n");
}
//
if($secondarydns == ""){
fwrite($cfp,"\$secondarydns = \"None\";\n");
}else{
fwrite($cfp,"\$secondarydns = \"$secondarydns\";\n");
}
//
if($telnet == ""){
fwrite($cfp,"\$telnet = \"None\";\n");
}else{
fwrite($cfp,"\$telnet = \"$telnet\";\n");
}
//
fwrite($cfp,"?>\n");
// close file pointer
fclose($cfp);

// end of write_companyinfo.php
?>
<!-- Start -->
<?require_once("./provider/".$id."/company.php");?>
<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/services.php");?>
<h1><?echo($officialname)?></h1>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<p class="ok">
<b>Service Information for <?echo($officialname)?> has been Updated.</b>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top colspan=2>Internet Services</td>
</tr>
<tr>
<td class="list_title" valign=top><b>Portal</b></td>
<td class="content_link" valign=top><a href="<?echo("http://".$portal);?>" target="_blank"><?echo($portal);?></a</td>
</tr>
<tr>
<td class="list_title" valign=top><b>Web Mail</b></td>
<td class="content_link" valign=top><a href="<?echo("http://".$webmail);?>" target="_blank"><?echo($webmail);?></a></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Web Hosting</b></td>
<td class="content_link" valign=top><a href="<?echo("http://".$webhosting);?>" target="_blank"><?echo($webhosting);?></a></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Domain Name</b></td>
<td class="content_link" valign=top><a href="<?echo("http://".$domain);?>" target="_blank"><?echo("$domain");?></a></td>
</tr>
<tr>
<td class="list_title" valign=top><b>POP Server</b></td>
<td class="list_content_normal" valign=top><?echo($popserver);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>SMTP Server</b></td>
<td class="list_content_normal" valign=top><?echo($smtpserver);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>FTP Server</b></td>
<td class="list_content_normal" valign=top><?echo($ftpserver);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>News Server</b></td>
<td class="list_content_normal" valign=top><?echo($newsserver);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Primary DNS</b></td>
<td class="list_content_normal" valign=top><?echo($primarydns);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Secondary DNS</b></td>
<td class="list_content_normal" valign=top><?echo($secondarydns);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Telnet Server</b></td>
<td class="content_link" valign=top><a href="<?echo("telnet://".$telnet);?>"><?echo("$telnet");?></a></td>
</tr>
</table>
<p align="center">
<input type="button" value="    OK     " onclick="location.href='?content=./provider/editor/controlpanel.php&id=<?echo($id)?>'"></td>
</p>
