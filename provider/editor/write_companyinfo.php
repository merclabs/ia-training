<?
// write_companyinfo.php 
//
// create file pointer to directory. or this can be the authen_user variable
// as long as authen_user is in the navigator file.
$cfp = fopen($directory."company.php",'w');

// use file pointer $cfp to write to file.
fwrite($cfp,"<?\n");
if($officialname == ""){
fwrite($cfp,"\$officialname = \"None\";\n");
}else{
fwrite($cfp,"\$officialname = \"$officialname\";\n");
}
//
if($address == ""){
fwrite($cfp,"\$address = \"None\";\n");
}else{
fwrite($cfp,"\$address = \"$address\";\n");
}
//
if($city == ""){
fwrite($cfp,"\$city = \"None\";\n");
}else{
fwrite($cfp,"\$city = \"$city\";\n");
}
//
if($state == ""){
fwrite($cfp,"\$state = \"None\";\n");
}else{
fwrite($cfp,"\$state = \"$state\";\n");
}
//
if($zip == ""){
fwrite($cfp,"\$zip = \"None\";\n");
}else{
fwrite($cfp,"\$zip = \"$zip\";\n");
}
//
if($mailingadddress == ""){
fwrite($cfp,"\$mailingadddress = \"None\";\n");
}else{
fwrite($cfp,"\$mailingadddress = \"$mailingadddress\";\n");
}
//
if($companyphone == ""){
fwrite($cfp,"\$companyphone = \"None\";\n");
}else{
fwrite($cfp,"\$companyphone = \"$companyphone\";\n");
}
//
if($companyfax == ""){
fwrite($cfp,"\$companyfax = \"None\";\n");
}else{
fwrite($cfp,"\$companyfax = \"$companyfax\";\n");
}
if($twentyfourtrouble == ""){
fwrite($cfp,"\$twentyfourtrouble = \"None\";\n");
}else{
fwrite($cfp,"\$twentyfourtrouble = \"$twentyfourtrouble\";\n");
}
fwrite($cfp,"?>\n");
// close file pointer
fclose($cfp);
sleep(3);
// end of write_companyinfo.php
?>

<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/company.php");?>
<h1><?echo($officialname)?></h1>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<p class="ok">
<b>Company Information for <?echo($officialname)?> has been Updated.</b>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" colspan=2 valign=top>Company Information</td>
</tr>
<tr>
<td class="list_title" valign=top><b>Official Company Name</b></td>
<td class="list_content_normal" valign=top><?echo($officialname);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Street Address</b></td>
<td class="list_content_normal" valign=top><?echo($address);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>City</b></td>
<td class="list_content_normal" valign=top><?echo($city);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>State</b></td>
<td class="list_content_normal" valign=top><?echo($state);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Zip</b></td>
<td class="list_content_normal" valign=top><?echo($zip);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Mailing Address</b></td>
<td class="list_content_normal" valign=top><?echo($mailingadddress);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Company Phone</b></td>
<td class="list_content_normal" valign=top><?echo($companyphone);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Company Fax</b></td>
<td class="list_content_normal" valign=top><?echo($companyfax);?></td>
</tr>
<tr>
<td class="list_title" valign=top><b>24-hour Trouble Number</b></td>
<td class="list_content_normal" valign=top><?echo($twentyfourtrouble);?></td>
</tr>
</table>
<p align="center">
<input type="button" value="      OK       " onclick="location.href='?content=./provider/editor/controlpanel.php&id=<?echo($id)?>'">
<p>
