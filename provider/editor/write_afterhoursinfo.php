<?
// write_afterhoursinfo.php 
//
// create file pointer to directory. or this can be the authen_user variable
// as long as authen_user is in the navigator file.
$cfp = fopen($directory."afterhours.php",'w');

// write contact info, all fields
fwrite($cfp,"<?\n");
for($j = 0;$j < count($afterhours_name);$j++){
if($afterhours_name[$j] == ""){
continue;
}else{
fwrite($cfp,"\$afterhours_name[] = \"$afterhours_name[$j]\";\n");
}
if($afterhours_officephone[$j] == ""){
continue;
}else{
fwrite($cfp,"\$afterhours_officephone[] = \"$afterhours_officephone[$j]\";\n");
}
if($afterhours_homephone[$j] == ""){
continue;
}else{
fwrite($cfp,"\$afterhours_homephone[] = \"$afterhours_homephone[$j]\";\n");
}
if($afterhours_mobile[$j] == ""){
continue;
}else{
fwrite($cfp,"\$afterhours_mobile[] = \"$afterhours_mobile[$j]\";\n");
}
if($afterhours_pager[$j] == ""){
continue;
}else{
fwrite($cfp,"\$afterhours_pager[] = \"$afterhours_pager[$j]\";\n");
}
}
fwrite($cfp,"?>\n");
fclose($cfp);
?>
<!-- Start of page -->
<?require_once("./provider/".$id."/company.php");?>
<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/afterhours.php");?>
<h1><?echo($officialname)?></h1>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<p class="ok">
<b>Afterhours Information for <?echo($officialname)?> has been Updated.</b>
</p>
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top colspan=5>After Hours Contacts</td>
</tr>
<tr>
<td class="list_header2" valign=top><b>Contact</b></td>
<td class="list_header2" valign=top><b>Office Phone</b></td>
<td class="list_header2" valign=top><b>Home Phone</b></td>
<td class="list_header2" valign=top><b>Cell Phone</b></td>
<td class="list_header2" valign=top><b>Pager</b></td>
</tr>

<?
for($j = 0;$j < count($afterhours_name) - 4;$j++){
echo("<tr>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_name[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_officephone[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_homephone[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_mobile[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_pager[$j]</td>");
echo("</tr>");
}
?>
</table>
<p align="center">
<input type="button" value="    OK     " onclick="location.href='?content=./provider/editor/controlpanel.php&id=<?echo($id)?>'">
</p>