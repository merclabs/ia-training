<h1>Licensee Editor Listing</h1>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF SPITRIT TELECOM.
PRINTOUTS OF THIS PAGE SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<p class="note">
<b>Note:</b> The links on this page link to the Licensee Editor, which
is used to edit infomation on the Licensee Listings page. If you are 
not authurized to make changes here and you were looking for just the 
Licensee Listing, click <a href="?content=./provider/main.php">here</a>
</p>
<table>
<tr>
<td>
<!-- Start of main listing -->
<p>
<table class="list_table" cellspacing=1>
<tr><td class="list_header">Joint Ventures</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=infoave">Info Avenue Internet Services, LLC</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=charleston">Charleston.Net</a></td></tr>
</table>
</p>
<p>
<table class="list_table" cellspacing=1>
<tr><td class="list_header">Resellers</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=carolina">Carolina Online, Incorporated </a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=groupz">GroupZ.Net (C&amp;B Management, Inc.)</a></td></tr>
</table>
</p>
<p>
<table class="list_table" cellspacing=1>
<tr><td class="list_header">Agents for Certain Licensees</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=accesson">Access/ON (NC Group)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=illinetworks">IlliNetworks (Illinois Group)</a></td></tr>
</table>
</p>
<p>
<table class="list_table" cellspacing=1>
<tr><td class="list_header">Licensee Listing</td></tr>
<tr><td class="list_header2">A</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=accesspoint">AccessPoint (ACCESSPT)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=accessvidalia">Access Vidalia (ACCESSV)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=aiken">Aiken Electric (AIKEN)</a> </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=alhambra">Alhambra-Grantfork Telephone Company (ALHAMBRA)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=aquaaccess">Aqua Access(AQUAAC)</a> </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=ardmore">Ardmore Telephone Company, Inc. (ARDMORE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=atlantic">Atlantic Telephone Membership Corporation (ATLANTIC)</a></td></tr>
<tr><td class="list_header2">B</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=benlomand">Ben Lomand Rural Telephone Cooperative (BLOMAND)</a></td></tr>
<!-- add or fix<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=berkeley">Berkeley Electric Cooperative(BERKELEY)</a> </td></tr> -->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=bledsoe">Bledsoe Telephone Cooperative, Incorporated (BLEDSOE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=bulloch">Bulloch Telephone (BULLOCH)</a></td></tr>
<tr><td class="list_header2">C</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=casscomm">Cass Telephone Company (CASS)</a>  </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=chesnee">Chesnee Telephone Company (CHESNEE)</a> </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=chestertel">Chester Long Distance Service, Inc. (CHESTER)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=citizensnc">Citizens Communications Systems [NC] (CITZNSNC)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=citizenspa">Citizens Internet Services [PA] (CITZNSPA)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=citizensva">Citizens Telephone Cooperative [VA] (CITZNSVA)- SWVA</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=coastalnow">Coastal Communications (CLDS)</a> </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=comsouth">Com South Communications (SOMSOUTH)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=cooperriver">Cooper River ()</a></td></tr>
<tr><td class="list_header2">D</td></tr>
<tr><td class="list_header2">E</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=edisto">Edisto Electric (EDISTO)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=ellerbe">Ellerbe Telephone Company (ELLERBE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=empire">Empire Access ()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=enhanced">Enhanced Telecommunications Corp (SUNMAN) - <b><i>formally Sunman</i></b></a></td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=FarmersMutualTelephoneCompany.html">Farmers Mutual Telephone Company</a> (FMIDTEL) </td></tr> -->
<tr><td class="list_header2">F</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=farmers">Farmers Telephone Cooperative, Incorporated (FTC)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=fortmill">Fort Mill Telephone Company (FORTMILL)</a></td></tr>
<tr><td class="list_header2">G</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=beaufortnet">Galaxy Holdings Inc. (Beaufortnet)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=custom">Galaxy Holdings Inc. (Custom)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=cyberlink">Galaxy Holdings Inc. (Cyberlink)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=decaturnet">Galaxy Holdings Inc. (Decaturnet)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=galaxyinternet">Galaxy Holdings Inc. (Galaxyinternet)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=midlink">Galaxy Holdings Inc. (Midlink)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=surfpure">Galaxy Holdings Inc. (Surfpure)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=virtuallyfree">Galaxy Holdings Inc. (Virtuallyfree)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=grics">Gallatin River Communications (GALLATIN)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=graceba">GracebaTotalCommunications,Inc. (GRACEBA)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=gulftel">GulfTel Communications (GULFTEL)</a></td></tr>
<tr><td class="list_header2">H</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=hayneville_telephone">Hayneville Telephone Company, Inc. (ALHYVL)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=hayneville_fiber">Hayneville Fiber Transport()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=hardynet">Hardy Telecommunications, Inc(HARDY)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=hargray">Hargray Telephone (HARGRAY)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=heathsprings">Heath Springs Telephone (HEATH)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=highland">Highland Telephone Cooperative,Incorporated (HIGHLAND)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=home">Home Telephone Company (SC), Incorporated (HOME)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=horry">Horry Telephone Cooperative, Incorporated (HORRY)</a></td></tr>
<tr><td class="list_header2">I</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=infospree">Infospree (INFOSPRE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=interbel">Interbel Telephone Cooperative, Inc. (INTERBEL)</a></td></tr>
<tr><td class="list_header2">J</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=jefferson">Jefferson Electric Membership Corporation (JEMCORP)</a></td></tr>
<tr><td class="list_header2">K</td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=kiwash.html">Kiwash Enterprises Inc.</a> (KIWASH) </td></tr> -->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=klm">KLM Telephone Company (KLMTEL)</a>  </td></tr>
<tr><td class="list_header2">L</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=lancaster">Lancaster Telephone Company (LANCTEL)</a> </td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=lexington">Lexington Telephone Company (LXTN)</a></td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=LineDriveCommunications.html">Line Drive Communications</a> (GASVN1) </td></tr>-->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=loretto">Loretto Telephone Company, Inc. (LORETTO)</a> </td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=LowCountryCommunications.html">Low Country Communications</a> (LOWCNTYC) </td></tr>-->
<tr><td class="list_header2">M</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=madison">Madison Telephone Company (MADISON)</a>  </td></tr>
<tr><td class="content_link"><a href="/?content=./provider/editor/controlpanel.php&id=marlboro">Marlboro Electric Cooperative (MARLBORO)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=mebtel">MebTel Communications (MEBTEL)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=midrivers">Mid-Rivers Telephone Cooperative, Incorporated (MIDRIVRS)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=wheeler">Mount Wheeler Power (WHEELER)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=mgw">MGW Networks, LLC (MGW)</a></td></tr>
<tr><td class="list_header2">N</td></tr>
<!--  < a href="?content=./provider/editor/controlpanel.php&id=north.html">North State Telephone Company</a> (NSTATE) </td></tr>-->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=netbravo">NetBravo ()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=networkairwaves">NetworkAirwaves ()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=northpenn">North Penn Access ()</a></td></tr>
<tr><td class="list_header2">O</td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=one-eleven.html">One-Eleven Internet Services, Inc. &amp; bmmhnet</a>(1ELEVEN) </td></tr> -->
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=ofmtelco.html">Oregon Farmers Mutual Telephone Company</A>(OFMTELCO) </td></tr>-->
<tr><td class="list_header2">P</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=palmettorural">Palmetto Rural Telephone Company (PALMORTC)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=parksbrothers">Parks Brothers (PARKSOK)</a></td></tr>
<!-- <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=pembroke.html">Pembroke Telephone --G-Net </a>(PEMBROKE) </td></tr> -->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=piedmonttelephone">Piedmont Telephone Membership Company [NC] (PIEDMONT)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=piedmontrural">Piedmont Rural Telephone Cooperative [SC] (PDMTRTC)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=plant">Plant Telecommunication Sales and Service Inc. (PLANTTEL)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=planters">Planters Rural Telco (PLANTERS)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=pbtcomm">PBT Communications (PONDBR)</a></td></tr>
<tr><td class="list_header2">Q</td></tr>
<tr><td class="list_header2">R</td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=randolph.htm">Randolph Telephone Membership Corporation</a> AND <tr><td class="content_link"><a
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=randolphlib.htm" class="main">Randolph Telephone Company</a> (RANDOLPH) </td></tr>-->
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=Reserve.html"><u>Reserve Long Distance Company, Inc.</u></a>(RLDC) </td></tr>-->
<tr><td class="content_link"><a href="/?content=./provider/editor/controlpanel.php&id=roadlynx">Roadlynx (Marianna Telephone Company) (MARIANNA)</a></td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=rbcomm.htm"><u>R &amp; B Communications </u></a>(RBCOMM) </td></tr>-->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=rockhill">Rock Hill Telephone Company (ROCKHILL)</a></td></tr>
<tr><td class="list_header2">S</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=sandhill">Sandhill Telephone Cooperative, Inc. (SANDHILL)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=scnet">SC-Net (South Carolina Net, Inc.) (SCNET)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=shawnee">Shawnee Telephone Company, Inc. (SHAWNEE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=smartcity">SmartCityTelecom ()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=smithville">Smithville Telephone Company, Inc. (SMTHUL)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=southwestonline">Southwest Online Internet Services (SWINET)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=spruceknob">Spruce Knob Seneca Rocks Telephone, Inc. ()</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=springboard">Springboard Telecom (SBTELE)</a></td></tr>
<!-- <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=sunman.html">Sunman Telecommunications Corp.</a>(SUNMAN) </td></tr> -->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=skyline">Skyline Telephone Membership Corporation (SKYLINE)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=surry"> Surry Telephone (SURRY)</a></td></tr>
<tr><td class="list_header2">T</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=tcncom">TCNCommunications</a><br>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=tricounty">Tri-County Communications (TRICNTY)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=tricountyelectric">Tri-County Electric Co-op, Inc. (SCORB3)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=twinlakes">Twin Lakes Telephone Cooperative Corporation (TWLAKES)</a></td></tr>
<tr><td class="list_header2">U</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=uchub">UC Hub (UCHUB)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=united">United Telephone Company (UNITED)</a></td></tr>
<tr><td class="list_header2">V</td></tr>
<tr><td class="list_header2">W</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=waycross">WayCross Cable Company (GAWYCR)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=westcarolina">West Carolina Rural Telephone Cooperative, Incorporated (WCAR)</a></td></tr>
<!--  <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=WesternIdahoInternet.html">Western Idaho Internet</a> (WIDAHO) </td></tr>-->
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=winco">Western Illinois Network (WINCO)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=wilkes">Wilkes Communications (WILKES)</a></td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=wilkes-nu-z">Wilkes Telephone & Electric Company, GA (WILKESGA)</a></td></tr>
<tr><td class="list_header2">Y</td></tr>
<!-- <tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=yadkin.htm">Yadkin Valley Telecom </a>(YADKIN) -->
<tr><td class="list_header2">Z</td></tr>
<tr><td class="content_link"><a href="?content=./provider/editor/controlpanel.php&id=zoomband"> Zoomband ()</a></td></tr>
</table>
</p>
<!-- End of main listing -->
</td>
</tr>
</table>
