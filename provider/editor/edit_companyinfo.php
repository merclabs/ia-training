<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/company.php");?>
<h1><?echo($officialname)?></h1>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<form name="form" action="?content=./provider/editor/write_companyinfo.php" method="post">
<input type="hidden" value="<?echo($id);?>" name="id">
<input type="hidden" value="<?echo("./provider/".$id."/");?>" name="directory">
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" colspan=2 valign=top>Company Information</td>
</tr>
<tr>
<td class="list_title" valign=top><b>Official Company Name</b></td>
<td class="list_content_normal" valign=top><input type="text" name="officialname" size=45 value="<?echo($officialname);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Street Address</b></td>
<td class="list_content_normal" valign=top><input type="text" name="address" size=45 value="<?echo($address);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>City</b></td>
<td class="list_content_normal" valign=top><input type="text" name="city" size=35 value="<?echo($city);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>State</b></td>
<td class="list_content_normal" valign=top>

<select name="state">
<option selected value="<?echo($state);?>"><?echo($state);?></option>
<option  value="AK">AK</option>
<option  value="AL">AL</option>
<option  value="AR">AR</option>
<option  value="AZ">AZ</option>
<option  value="CA">CA</option>
<option  value="CO">CO</option>
<option  value="CT">CT</option>
<option  value="DC">DC</option>
<option  value="DE">DE</option>
<option  value="FL">FL</option>
<option  value="GA">GA</option>
<option  value="HI">HI</option>
<option  value="IA">IA</option>
<option  value="ID">ID</option>
<option  value="IL">IL</option>
<option  value="IN">IN</option>
<option  value="KS">KS</option>
<option  value="KY">KY</option>
<option  value="LA">LA</option>
<option  value="MA">MA</option>
<option  value="MD">MD</option>
<option  value="ME">ME</option>
<option  value="MI">MI</option>
<option  value="MN">MN</option>
<option  value="MO">MO</option>
<option  value="MS">MS</option>
<option  value="MT">MT</option>
<option  value="NC">NC</option>
<option  value="ND">ND</option>
<option  value="NE">NE</option>
<option  value="NH">NH</option>
<option  value="NJ">NJ</option>
<option  value="NM">NM</option>
<option  value="NV">NV</option>
<option  value="NY">NY</option>
<option  value="OH">OH</option>
<option  value="OK">OK</option>
<option  value="OR">OR</option>
<option  value="PA">PA</option>
<option  value="RI">RI</option>
<option  value="SC">SC</option>
<option  value="SD">SD</option>
<option  value="TN">TN</option>
<option  value="TX">TX</option>
<option  value="UT">UT</option>
<option  value="VA">VA</option>
<option  value="VT">VT</option>
<option  value="WA">WA</option>
<option  value="WV">WV</option>
<option  value="WY">WY</option>
</select>
</td>
</tr>
<tr>
<td class="list_title" valign=top><b>Zip</b></td>
<td class="list_content_normal" valign=top><input type="text" name="zip" size=10 value="<?echo($zip);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Mailing Address</b></td>
<td class="list_content_normal" valign=top><input type="text" name="mailingadddress" size=45 value="<?echo($mailingadddress);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Company Phone</b></td>
<td class="list_content_normal" valign=top><input type="text" name="companyphone" size=24 value="<?echo($companyphone);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>Company Fax</b></td>
<td class="list_content_normal" valign=top><input type="text" name="companyfax" size=24 value="<?echo($companyfax);?>"></td>
</tr>
<tr>
<td class="list_title" valign=top><b>24-hour Trouble Number</b></td>
<td class="list_content_normal" valign=top><input type="text" name="twentyfourtrouble" size=24 value="<?echo($twentyfourtrouble);?>"></td>
</tr>
</table>
<p align="center">
<input type="button" value="<< Back " onclick="history.back()"><input type="reset" value="Undo"><input type="submit" value="Update">
</p>
</form>



