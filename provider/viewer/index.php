<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/company.php");?>
<?require_once("./provider/".$id."/contacts.php");?>
<?require_once("./provider/".$id."/afterhours.php");?>
<?require_once("./provider/".$id."/services.php");?>
<?require_once("./provider/".$id."/accessnumbers.php");?>
<h1><?echo($officialname);?></h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./provider/main.php">Back to Licensee Listing</a></td></tr>
</td></tr>
</table>
</p>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<!-- Start Cut and Paste Here -->
 <!-- Company Information -->
 <!-- start -->
<p>
<table class="list_table" cellspacing=1>
 <tr>
  <td class="list_header" colspan=2 valign=top>Company Information</td>
 </tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Official Company Name</b></td>
 <td class="list_content_normal" valign=top><?echo($officialname);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Street Address</b></td>
 <td class="list_content_normal" valign=top><?echo($address);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>City</b></td>
 <td class="list_content_normal" valign=top><?echo($city);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>State</b></td>
 <td class="list_content_normal" valign=top><?echo($state);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Zip</b></td>
 <td class="list_content_normal" valign=top><?echo($zip);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Mailing Address</b></td>
 <td class="list_content_normal" valign=top><?echo($mailingadddress);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Company Phone</b></td>
 <td class="list_content_normal" valign=top><?echo($companyphone);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Company Fax</b></td>
 <td class="list_content_normal" valign=top><?echo($companyfax);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>24-hour Trouble Number</b></td>
 <td class="list_content_normal" valign=top><?echo($twentyfourtrouble);?></td>
</tr>
</table>
</p>
 <!-- End of Comany Information -->
 <!-- Internet Services -->
 <!-- start -->
<p>
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2>Internet Services</td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Portal</b></td>
 <td class="content_link" valign=top><a href="<?echo("http://".$portal);?>" target="_blank"><?echo("http://".$portal);?></a</td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Web Mail</b></td>
 <td class="content_link" valign=top><a href="<?echo("http://".$webmail);?>" target="_blank"><?echo("http://".$webmail);?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Web Hosting</b></td>
 <td class="content_link" valign=top><a href="<?echo("http://".$webhosting);?>" target="_blank"><?echo("http://".$webhosting);?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Domain Name</b></td>
 <td class="content_link" valign=top><a href="<?echo("http://".$domain);?>" target="_blank"><?echo("$domain");?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>POP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($popserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>SMTP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($smtpserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>FTP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($ftpserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>News Server</b></td>
 <td class="list_content_normal" valign=top><?echo($newsserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Primary DNS</b></td>
 <td class="list_content_normal" valign=top><?echo($primarydns);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Secondary DNS</b></td>
 <td class="list_content_normal" valign=top><?echo($secondarydns);?></td>
</tr>
<tr>
 <td class="list_title" valign=top NOWRAP><b>Telnet Server</b></td>
 <td class="content_link" valign=top><a href="<?echo("telnet://$telnet");?>"><?echo($telnet);?></a></td>
</tr>
</table>
</p> 
 <!-- end -->
 <!-- End of Internet Services -->

 <!-- Contact Information --> 
 <!-- start -->
<p>
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top  colspan=4>Departmental Contacts</td>
</tr>
<tr>
 <td class="list_header2" valign=top><b>Department</b></td>
 <td class="list_header2" valign=top><b>Name</b></td>
 <td class="list_header2" valign=top><b>Phone</b></td>
 <td class="list_header2" valign=top><b>E-Mail</b></td>
</tr>
<?
for($i = 0;$i < count($department);$i++){
echo("<tr>");
echo("<td class=\"list_title\" valign=top><b>$department[$i]</b></td>\n");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$contact_name[$i]</td>\n");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$contact_phone[$i]</td>\n");
echo("<td class=\"list_content_normal\" valign=top NOWRAP><a href=\"mailto:$contact_email[$i]\">$contact_email[$i]</a></td>\n");
echo("</tr>");
}
?>
</table>
</p>
 <!-- end -->
 <!-- End of Contact Information -->
 
 <!-- Start of Afterhour Contacts -->
 <!-- start -->
<p>
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=5>After Hours Contacts</td>
</tr>
<tr>
 <td class="list_header2" valign=top><b>Contact</b></td>
 <td class="list_header2" valign=top><b>Office Phone</b></td>
 <td class="list_header2" valign=top><b>Home Phone</b></td>
 <td class="list_header2" valign=top><b>Cell Phone</b></td>
 <td class="list_header2" valign=top><b>Pager</b></td>
</tr>

<?
for($j = 0;$j < count($afterhours_name);$j++){
echo("<tr>");
echo("<td class=\"list_title\" valign=top NOWRAP>$afterhours_name[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_officephone[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_homephone[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_mobile[$j]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$afterhours_pager[$j]</td>");
echo("</tr>");
}
?>
</table>
</p>
 <!-- end -->
 <!-- End of Afterhours Contacts --> 

 <!-- Service Area Content -->
 <!-- start -->
<p>
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=4>Service Areas</td>
</tr>
<tr>
 <td class="list_header2" valign=top><b>City, State</b></td>
 <td class="list_header2" valign=top><b>Modem Number</b></td>
 <td class="list_header2" valign=top><b>Customer Service</b></td>
 <td class="list_header2" valign=top><b>Tech Support</b></td>
</tr>

<?
 
for($k = 0;$k < count($citystate);$k++){
echo("<tr>");
echo("<td class=\"list_title\" valign=top NOWRAP>$citystate[$k]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$local_access_number[$k]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$local_custservice[$k]</td>");
echo("<td class=\"list_content_normal\" valign=top NOWRAP>$techsupport_number[$k]</td>");
echo("</tr>");
}
 
?>
</table>
</p>
 <!-- end -->
 <!-- End of Service Area Content -->
 <!-- End Cut and Paste Here -->
