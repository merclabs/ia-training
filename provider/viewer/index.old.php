<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<?require_once("../navigator/navigator.php");?>
<?require_once("../".$id."/company.php");?>
<?require_once("../".$id."/contacts.php");?>
<?require_once("../".$id."/afterhours.php");?>
<?require_once("../".$id."/services.php");?>
<?require_once("../".$id."/accessnumbers.php");?>
<title>Provider Information - [ <?echo($officialname)?> ]</title>
</head>
<body>
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<!-- Start Cut and Paste Here -->
<table width="100%" border=0 valign="top" align="left" cellspacing=10>
 <tr>
 <!-- Company Information -->
  <td valign="top" width=50%>
 <!-- start -->
<table class="main" cellspacing=1>
 <tr>
  <td class="master" colspan=2 valign=top>Company Information</td>
 </tr>
<tr>
 <td class="heading" valign=top><b>Official Company Name</b></td>
 <td class="field" valign=top><?echo($officialname);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Street Address</b></td>
 <td class="field" valign=top><?echo($address);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>City</b></td>
 <td class="field" valign=top><?echo($city);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>State</b></td>
 <td class="field" valign=top><?echo($state);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Zip</b></td>
 <td class="field" valign=top><?echo($zip);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Mailing Address</b></td>
 <td class="field" valign=top><?echo($mailingadddress);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Company Phone</b></td>
 <td class="field" valign=top><?echo($companyphone);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Company Fax</b></td>
 <td class="field" valign=top><?echo($companyfax);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>24-hour Trouble Number</b></td>
 <td class="field" valign=top><?echo($twentyfourtrouble);?></td>
</tr>
</table>
 <!-- end -->
  </td>
 <!-- End of Comany Information -->
 <!-- Internet Services -->
  <td valign="top">
 <!-- start -->
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Internet Services</td>
</tr>
<tr>
 <td class="heading" valign=top><b>Portal</b></td>
 <td class="field" valign=top><a href="<?echo("http://".$portal);?>" target="_blank"><?echo($portal);?></a</td>
</tr>
<tr>
 <td class="heading" valign=top><b>Web Mail</b></td>
 <td class="field" valign=top><a href="<?echo("http://".$webmail);?>" target="_blank"><?echo($webmail);?></a></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Web Hosting</b></td>
 <td class="field" valign=top><a href="<?echo("http://".$webhosting);?>" target="_blank"><?echo($webhosting);?></a></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Domain Name</b></td>
 <td class="field" valign=top><a href="<?echo("http://".$domain);?>" target="_blank"><?echo("$domain");?></a></td>
</tr>
<tr>
 <td class="heading" valign=top><b>POP Server</b></td>
 <td class="field" valign=top><?echo($popserver);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>SMTP Server</b></td>
 <td class="field" valign=top><?echo($smtpserver);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>FTP Server</b></td>
 <td class="field" valign=top><?echo($ftpserver);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>News Server</b></td>
 <td class="field" valign=top><?echo($newsserver);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Primary DNS</b></td>
 <td class="field" valign=top><?echo($primarydns);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Secondary DNS</b></td>
 <td class="field" valign=top><?echo($secondarydns);?></td>
</tr>
<tr>
 <td class="heading" valign=top><b>Telnet Server</b></td>
 <td class="field" valign=top><?echo($telnet);?></td>
</tr>
</table>
 <!-- end -->
 </td>
</tr>
 <!-- End of Internet Services -->
 <!-- Contact Information --> 
<tr>
  <td colspan=2>
 <!-- start -->
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top  colspan=3>Departmental Contacts</td>
</tr>
<tr>
 <td class="heading" valign=top><b>Department</b></td>
 <td class="heading" valign=top><b>Name</b></td>
 <td class="heading" valign=top><b>Phone</b></td>
 <td class="heading" valign=top><b>E-Mail</b></td>
</tr>
<?
for($i = 0;$i < count($department);$i++){
echo("<tr>");
echo("<td class=\"field\" valign=top><b>$department[$i]</b></td>\n");
echo("<td class=\"field\" valign=top>$contact_name[$i]</td>\n");
echo("<td class=\"field\" valign=top>$contact_phone[$i]</td>\n");
echo("<td class=\"field\" valign=top ><a href=\"mailto:$contact_email[$i]\">$contact_email[$i]</a></td>\n");
echo("</tr>");
}
?>
</table>
 <!-- end -->
  </td>
</tr>
 <!-- End of Contact Information -->
 <!-- Start of Afterhour Contacts -->
<tr >
  <td colspan=2>
<!-- start -->
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=5>After Hours Contacts</td>
</tr>
<tr>
 <td class="heading" valign=top><b>Contact</b></td>
 <td class="heading" valign=top><b>Office Phone</b></td>
 <td class="heading" valign=top><b>Home Phone</b></td>
 <td class="heading" valign=top><b>Cell Phone</b></td>
 <td class="heading" valign=top><b>Pager</b></td>
</tr>

<?
for($j = 0;$j < count($afterhours_name);$j++){
echo("<tr>");
echo("<td class=\"field\" valign=top>$afterhours_name[$j]</td>");
echo("<td class=\"field\" valign=top>$afterhours_officephone[$j]</td>");
echo("<td class=\"field\" valign=top>$afterhours_homephone[$j]</td>");
echo("<td class=\"field\" valign=top>$afterhours_mobile[$j]</td>");
echo("<td class=\"field\" valign=top>$afterhours_pager[$j]</td>");
echo("</tr>");
}
?>

</table>
 <!-- end -->
  </td>
</tr>
 <!-- End of Afterhours Contacts --> 
 <!-- Service Area Content -->
<tr colspan=2>
  <td colspan=2>
<!-- start -->
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=4>Service Areas</td>
</tr>
<tr>
 <td class="heading" valign=top><b>City, State</b></td>
 <td class="heading" valign=top><b>Modem Number</b></td>
 <td class="heading" valign=top><b>Customer Service</b></td>
 <td class="heading" valign=top><b>Tech Support</b></td>
</tr>

<?
 
for($k = 0;$k < count($citystate);$k++){
echo("<tr>");
echo("<td class=\"field\" valign=top>$citystate[$k]</td>");
echo("<td class=\"field\" valign=top>$local_access_number[$k]</td>");
echo("<td class=\"field\" valign=top>$local_custservice[$k]</td>");
echo("<td class=\"field\" valign=top>$techsupport_number[$k]</td>");
echo("</tr>");
}
 
?>

</table>
<!-- end -->
 </td>
</tr>
 <!-- End of Service Area Content -->
</table>
<!-- End Cut and Paste Here -->
<br>
</body>
</html>
