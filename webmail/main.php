<h1>Webmail Support</h1>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Most of our <b>Provider</b> have <b>Branded Webmail</b>, there are a few that
do not and a few that manage their own. This list contains all providers that have
<b>Branded Webmail</b> with us. Each address is a direct link to their webmail page.
</td></tr>
</table>
</p>

<p class="note">
If a <b>Provider</b> is not listed here, please direct their customers to <a href="http://www.toolkitmail.com" target="_blank">
http://www.toolkitmail.com</a>. 
</p>
<p>
<table class="list_table">
<tr><td class="list_header" colspan="2">Provider Branded Webmail </td></tr>
<?
$protocol = "http://";
include("listing.php");
reset($providers);
foreach($providers as $line){
echo("<tr>\n");
echo("<td class=\"list_content\" valign=top>\n");
$subline = substr($line,0,-4);
echo("$subline\n");
echo("</td>\n");
echo("<td class=\"content_link\" valign=\"top\">\n");
echo("<a href=\"http://webmail.".$line."\" target=\"_blank\">".$protocol."webmail.".$line."</a>\n");
echo("</td>\n");
echo("</tr>\n");
}
?>
</table>
</p>
<!-- Back to Top button -->
<div align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</div>
<!-- Back to Top button -->
