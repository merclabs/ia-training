<html>
<head>
<?include_once("./cgi-bin/config.php")?>
<?include_once("./cgi-bin/navigator.php")?>
<link rel="stylesheet" type="text/css" href="./css/main.css">
<!-- <script src="./javascript/clock.js"></script> -->
<title><?echo($default_title)?> </title>
<script src="./jsfunction.js"></script>
</head>
<body topmargin="0" leftmargin="2">

<table class="main" border="0" cellspacing="0">
  <tr>
    <td width="175" height="78" align="left">
		<!-- Logo Goes here -->
		<a name="top"><a href="<?echo($baseaddress);?>"><img src="<?echo($default_logo);?>" alt="<?echo($default_logo_alt);?>" border=0></a></a>
		</td>
    <td width="820" height="78" colspan="2" valign="top" align="left"></td>
  </tr>
  <tr>
    <td class="left_top_toolbar" valign="middle" align="center"><?echo($today);?></td>
    <td class="center_top_tooblar" valign="middle" align="center" NOWRAP>
		<?include_once($default_toolbar);?>
		</td>
    <td class="right_top_toolbar" valign="middle"><div align="center">
		<a class="link" href="javascript:lt_login()">LT ADMIN</a></div>
		</td>
  </tr>
  <tr>
    <td  class="menu" valign="top" align="left"><?include_once($default_menu);?></td>
    <td class="content" valign="top" align="left"><?include_once($content);?></td>
    <td class="sidebar" valign="top" align="left">
		<?include_once($default_sidebar);?></td>
  </tr>
  <tr>
    <td class="left_bottom_toolbar" valign="top" align="left">&nbsp;</td>
    <td class="center_bottom_tooblar" valign="top" align="left">&copy; 2003 - 2004 Spirit Telecom, All Rights Reserved</td>
    <td class="right_bottom_toolbar" valign="top">
		<div align="center">
		<a class="link" href="mailto:support@infoave.net,cbarnette@infoave.net">Contact Us</a>
		</div>
		</td>
  </tr>
</table>
</body>
</html>
