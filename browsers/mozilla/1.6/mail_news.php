<h1>Mozilla 1.6 - Mail and Newsgroups</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/mozilla/1.6/main.php">Mozilla 1.6</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Mozilla 1.6.
</td></tr>
</table>
</p>
<p>
<h3>Mail & Newsgroups</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups.gif" alt="Mail & Newsgroups">
</p>
<p>
<h3>Mail & Newsgroups - Windows</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_windows.gif" alt="Mail & Newsgroups - Windows">
</p>
<p>
<h3>Mail & Newsgroups - Message Display</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_message_display.gif" alt="Mail & Newsgroups - Message Display">
</p>
<p>
<h3>Mail & Newsgroups - Notification</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_notification.gif" alt="Mail & Newsgroups - Notification">
</p>
<p>
<h3>Mail & Newsgroups - Composition</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_composition.gif" alt="Mail & Newsgroups - Composition">
</p>
<p>
<h3>Mail & Newsgroups - Send Format</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_send_format.gif" alt="Mail & Newsgroups - Send Format">
</p>
<p>
<h3>Mail & Newsgroups - Addressing</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_addressing.gif" alt="Mail & Newsgroups - Addressing">
</p>
<p>
<h3>Mail & Newsgroups - Labels</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_labels.gif" alt="Mail & Newsgroups - Labels">
</p>
<p>
<h3>Mail & Newsgroups - Return Receipts"</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_return_receipts.gif" alt="Mail & Newsgroups - Return Receipts">
</p>
<p>
<h3>Mail & Newsgroups - Offline & Disk Space</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/mail_newsgroups_offline_disk_space.gif" alt="Mail & Newsgroups - Offline & Disk Space">
</p>