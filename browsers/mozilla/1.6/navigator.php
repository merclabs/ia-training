<h1>Mozilla 1.6 - Navigator</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/mozilla/1.6/main.php">Mozilla 1.6</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Mozilla 1.6.
</td></tr>
</table>
</p>
<p>
<p>
<h3>Navigator</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator.gif" alt="Navigator">
</p>
<p>
<h3>Navigator - History</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_history.gif" alt="Navigator - History">
</p>
<p>
<h3>Navigator - Languages</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_languages.gif" alt="Navigator - Languages">
</p>
<p>
<h3>Navigator - Helper Applications</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_helper_applications.gif" alt="Navigator - Helper Applications">
</p>
<p>
<h3>Navigator - Smart Browsing</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_smart_browsing.gif" alt="Navigator - Smart Browsing">
</p>
<p>
<h3>Navigator - Internet Search</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_internet_search.gif" alt="Navigator - Internet Search">
</p>
<p>
<h3>Navigator - Tabbed Browsing</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_tabbed_browsing.gif" alt="Navigator - Tabbed Browsing">
</p>
<p>
<h3>Navigator - Downloads</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/navigator_downloads.gif" alt="Navigator - Downloads">
</p>