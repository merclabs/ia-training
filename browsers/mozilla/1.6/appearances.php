<h1>Mozilla 1.6 - ChatZilla</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/mozilla/1.6/main.php">Mozilla 1.6</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Mozilla 1.6.
</td></tr>
</table>
</p>
<p>
<h3>Appearance</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/appearance.gif" alt="Appearance">
</p>
<p>
<h3>Appearance - Fonts</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/appearance_fonts.gif" alt="Appearance - Fonts">
</p>
<p>
<h3>Appearance - Colors</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/appearance_colors.gif" alt="Appearance - Colors">
</p>
<p>
<h3>Appearance - Themes</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/appearance_themes.gif" alt="Appearance - Themes">
</p>
<p>
<h3>Appearance - Lanuage/Content</h3>
</p>
<p>
<img src="./browsers/mozilla/1.6/images/appearance_language_content.gif" alt="Appearance - Lanuage/Content">
</p>


