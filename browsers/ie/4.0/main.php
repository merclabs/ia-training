<h1>Internet Explorer 4.0</h1>
<p align="center">
<img src="./browsers/ie/4.0/images/about_ie4.gif" alt="About Internet Explorer 4.0">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/ie/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/ie/4.0/internet_options.php">Internet Options</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
In most cases you will run into <b>Internet Explorer 4.0</b> if a customer is using <b>Windows 95</b>,
has not updated their browser or has either ran their <b>restore disk</b> or <b>Windows 95 CD</b> which
reinstalls <b>Internet Explorer 4.0</b> when ran.</td>
</tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->