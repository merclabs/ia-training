<h1>Netscape 7.0</h1>
<p align="center">
<img src="./browsers/netscape/7.0/images/splash.jpg" alt="Netscape 7.0">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/preferences.php">Netscape Preferences</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/appearances.php">Appearance</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/navigator.php">Navigator</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/composer.php">Composer</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/mail_news.php">Mail and Newgroups</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/instant_messenger.php">Instant Messenger</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/icq.php">ICQ</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/privacy_security.php">Privacy & Security</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/advanced.php">Advanced</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/offline_disk_space.php">Offline Disk Space</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Netscape 7.0</b> can be downloaded from the <b>Netscape</b> website. If a customer needs to upgrade 
or wants to reinstall <b>Netscape 7.0</b> please send them to <a href="http://channels.netscape.com/ns/browsers/download.jsp">Download 7.1</a>
or the current version of the <b>Netscape Browser</b> from the website. 
</p>
<p>
The above table of contents is an outline of <b>Netscape's Preferences</b>, select the section of proferences you wish to view. 
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
