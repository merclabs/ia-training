<h1>Netscape 7.0 - Privacy & Security</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<p>
<h3>Privacy & Security</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security.jpg" alt="Privacy & Security">
</p>
<p>
<h3>Privacy & Security - Cookies</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_cookies.jpg" alt=" - Cookies">
</p>
<p>
<h3>Privacy & Security - Images</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_images.jpg" alt="Privacy & Security - Images">
</p>
<p>
<h3>Privacy & Security - Popup Window Controls</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_popup_window_controls.jpg" alt="Privacy & Security - Popup Window Controls">
</p>
<p>
<h3>Privacy & Security - Forms</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_forms.jpg" alt="Privacy & Security - Forms">
</p>
<p>
<h3>Privacy & Security - Passwords</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_passwords.jpg" alt="Privacy & Security - Passwords">
</p>
<p>
<h3>Privacy & Security - Master Passwords</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_master_passwords.jpg" alt="Privacy & Security - Master Passwords">
</p>
<p>
<h3>Privacy & Security - SSL</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_ssl.jpg" alt="Privacy & Security - SSL">
</p>
<p>
<h3>Privacy & Security - Certificates</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_certificates.jpg" alt="Privacy & Security - Certificates">
</p>
<p>
<h3>Privacy & Security - Validation</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/privacy_security_validation.jpg" alt="Privacy & Security - Validation">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
