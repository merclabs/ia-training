<h1>Netscape 7.0 - ICQ</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<p>
<h3>ICQ</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/icq.jpg" alt="ICQ">
</p>

<p>
<h3>ICQ - Privacy & Authorization</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/icq_privacy_authorization.jpg" alt="ICQ - Privacy & Authorization">
</p>

<p>
<h3>ICQ - Notification & Sounds</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/icq_notification_sounds.jpg" alt="ICQ - Notification & Sounds">
</p>

<p>
<h3>ICQ - Away</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/icq_away.jpg" alt="ICQ - Away">
</p>
<p>
<h3>ICQ - Connection</h3>
 
</p>
<p>
<img src="./browsers/netscape/7.0/images/icq_connection.jpg" alt="ICQ - Connection">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
