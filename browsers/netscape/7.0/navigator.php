<h1>Netscape 7.0 - Navigator</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<p>
<h3>Navigator</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator.jpg" alt="Navigator">
</p>
<p>
<h3>Navigator - History</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_history.jpg" alt="Navigator - History">
</p>
<p>
<h3>Navigator - Languages</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_languages.jpg" alt="Navigator - Languages">
</p>

<p>
<h3>Navigator - Applications</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_helper_applications.jpg" alt="Navigator - Helper Applications">
</p>

<p>
<h3>Navigator - Smart Browsing</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_smart_browsing.jpg" alt="Navigator - Smart Browsing">
</p>

<p>
<h3>Navigator - Internet Search</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_internet_search.jpg" alt="Navigator - Internet Search">
</p>

<p>
<h3>Navigator - Tabbed Browsing</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_tabbed_browsing.jpg" alt="Navigator - Tabbed Browsing">
</p>
<p>
<h3>Navigator - Downloads</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator_downloads.jpg" alt="Navigator - Downloads">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->