<h1>Netscape 7.0 - Preferences</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<h3>Accessing Preferences</h3>
To access <b>Netscape's Preferences</b> click on <b>Edit</b> then <b>Preferences...</b>. 
</p>
<p>
<img src="./browsers/netscape/7.0/images/netscape_preferences.jpg" alt="Preferences">
</p>
<p>
<h3>Preferences Window</h3>
The <b>Preferences</b> window should display, with a catagory box on the left.
</p>
<p>
<img src="./browsers/netscape/7.0/images/navigator.jpg" alt="Preferences Window">
</p>
<p class="note">
To view other sections of <b>Netscape's Preferences</b> click the <b>Netscape 7.0</b> link above.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
