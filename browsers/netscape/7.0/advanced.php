<h1>Netscape 7.0 - Advanced</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<p>
<h3>Advanced</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced.jpg" alt="Advanced">
</p>
<p>
<h3>Advanced - Cache</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced_cache.jpg" alt="Advanced - Cache">
</p>
<p>
<h3>Advanced - Proxies</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced_proxies.jpg" alt="Advanced - Proxies">
</p>
<p>
<h3>Advanced - Software Installation</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced_software_installation.jpg" alt="Advanced - Advanced - Software Installation">
</p>
<p>
<h3>Advanced - Mouse Wheel</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced_mouse_wheel.jpg" alt="Advanced - Mouse Wheel">
</p>
<p>
<h3>Advanced - System</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/advanced_system.jpg" alt="Advanced - System">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->