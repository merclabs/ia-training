<h1>Netscape 7.0 - Instant Messenger</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/7.0/main.php">Netscape 7.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 7.0.
</td></tr>
</table>
</p>
<p>
<h3>Instant Messenger</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger.jpg" alt="Instant Messenger">
</p>
<p>
<h3>Instant Messenger - Privacy</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_privacy.jpg" alt="Instant Messenger - Privacy">
</p>
<p>
<h3>Instant Messenger - Buddy Icons</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_buddy_icons.jpg" alt="Instant Messenger - Buddy Icons">
</p>
<p>
<h3>Instant Messenger - Notification</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_notification.jpg" alt="Instant Messenger - Notification">
</p>
<p>
<h3>Instant Messenger - Away</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_away.jpg" alt="Instant Messenger - Away">
</p>
<p>
<h3>Instant Messenger - Styles</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_styles.jpg" alt="Instant Messenger - Styles">
</p>
<p>
<h3>Instant Messenger - Connection</h3>
</p>
<p>
<img src="./browsers/netscape/7.0/images/instant_messenger_connection.jpg" alt="Instant Messenger - Connection">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->