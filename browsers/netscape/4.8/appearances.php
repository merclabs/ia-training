<h1>Netscape 4.8 - Appearance</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/main.php">Netscape 4.8</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.8 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
</td></tr>
</table>
</p>
<h3>Appearance</h3>
To access <b>Netscape's Preferences</b> click on <b>Edit</b> then <b>Preferences...</b>. 
</p>
<p>
<img src="./browsers/netscape/4.8/images/preferences.gif" alt="Preferences">
</p>
<p>
<h3>Appearance</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/appearance.gif" alt="Appearance">
</p>
<p>
<h3>Appearance - Fonts</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/appearance_fonts.gif" alt="Appearance - Fonts">
</p>
<p>
<h3>Appearance - Colors</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/appearance_colors.gif" alt="Appearance - Colors">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->