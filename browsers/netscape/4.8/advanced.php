<h1>Netscape 4.8 - Advanced</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/main.php">Netscape 4.08</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.08 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
</td></tr>
</table>
</p>
<p>
<h3>Preferences</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/preferences.gif" alt="Preferences">
</p>
<p>
<h3>Advanced</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/advanced.gif" alt="Advanced">
</p>
<p>
<h3>Advanced - Cache</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/advanced_cache.gif" alt="Advanced - Cache">
</p>
<p>
<h3>Advanced - Clear Memory Cache</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/clear_memory_cache.gif" alt="Advanced - Clear Memory Cache">
</p>
<p>
<h3>Advanced - Clear Disk Cache</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/clear_disk_cache.gif" alt="Advanced - Clear Disk Cache">
</p>




<p>
<h3>Advanced - Proxie</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/advanced_proxies.gif" alt="Advanced - Proxie">
</p>
<p>
<h3>Advanced - SmartUpdate</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/advanced_smart_update.gif" alt="Advanced - SmartUpdate">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->