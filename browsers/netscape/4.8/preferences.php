<h1>Netscape 4.8 - Preferences</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/main.php">Netscape 4.8</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.8 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
</td></tr>
</table>
</p>
<h3>Accessing Preferences</h3>
To access <b>Netscape's Preferences</b> click on <b>Edit</b> then <b>Preferences...</b>. 
</p>
<p>
<img src="./browsers/netscape/4.8/images/preferences.gif" alt="Preferences">
</p>
<p>
<h3>Preferences Window</h3>
The <b>Preferences</b> window should display, with a catagory box on the left.
</p>
<p>
<img src="./browsers/netscape/4.8/images/navigator.gif" alt="Preferences Window">
</p>
<p class="note">
To view other sections of <b>Netscape's Preferences</b> click the <b>Netscape 4.08</b> link above.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->