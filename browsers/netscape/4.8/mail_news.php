<h1>Netscape 4.8 - Mail and Newsgroups</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/main.php">Netscape 4.8</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.8 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
</td></tr>
</table>
</p>
</p>
<h3>Mail and Newsgroups</h3>
To access <b>Netscape's Preferences</b> click on <b>Edit</b> then <b>Preferences...</b>. 
</p>
<p>
<img src="./browsers/netscape/4.8/images/preferences.gif" alt="Preferences">
</p>
<p>
<h3>Mail and Newsgroups</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news.gif" alt="Mail and Newsgroups">
</p>
<p>
<h3>Mail and Newsgroups - Identity</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_identity.gif" alt="Mail and Newsgroups - Identity">
</p>
<p>
<h3>Mail and Newsgroups - Mail Servers</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_mail_servers.gif" alt="Mail and Newsgroups - Mail Servers">
</p>
<p>
<h3>Mail and Newsgroups - Newsgroup Servers</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_news.gif" alt="Mail and Newsgroups - Newsgroup Servers">
</p>
<p>
<h3>Mail and Newsgroups - Addressing</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_addressing.gif" alt="Mail and Newsgroups - Addressing">
</p>
<p>
<h3>Mail and Newsgroups - Messages</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_messages.gif" alt="Mail and Newsgroups - Messages">
</p>
<p>
<h3>Mail and Newsgroups - Window Settings</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_window_settings.gif" alt="Mail and Newsgroups - Window Settings">
</p>
<p>
<h3>Mail and Newsgroups - Copies and Folders</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_copies_folders.gif" alt="Mail and Newsgroups - Copies and Folders">
</p>
<p>
<h3>Mail and Newsgroups - Formatting</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_formatting.gif" alt="Mail and Newsgroups - Formatting">
</p>
<p>
<h3>Mail and Newsgroups - Return Receipts</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_return_receipts.gif" alt="Mail and Newsgroups - Return Receipts">
</p>
<p>
<h3>Mail and Newsgroups - Disk Space</h3>
 
</p>
<p>
<img src="./browsers/netscape/4.8/images/mail_news_disk_space.gif" alt="Mail and Newsgroups - Disk Space">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->