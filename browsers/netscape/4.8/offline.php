<h1>Netscape 4.8 - Offline</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/main.php">Netscape 4.8</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.8 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
</td></tr>
</table>
</p>
<p>
<h3>Preferences</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/preferences.gif" alt="Preferences">
</p>
<p>
<h3>Offline</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/offline.gif" alt="Offline">
</p>
<p>
<h3>Offline - Download</h3>
</p>
<p>
<img src="./browsers/netscape/4.8/images/offline_download.gif" alt="Offline - Download">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->