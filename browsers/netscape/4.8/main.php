<h1>Netscape 4.8</h1>
<p align="center">
<img src="./browsers/netscape/4.8/images/splash.gif" alt="Netscape 4.8">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/preferences.php">Netscape Preferences</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/appearances.php">Appearance</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/navigator.php">Navigator</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/mail_news.php">Mail and Newgroups</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/roaming_access.php">Roaming Access</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/composer.php">Composer</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/offline.php">Offline</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/4.8/advanced.php">Advanced</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Netscape 4.8 was distributed with the our <b>Internet Toolkit Software</b>, for this reason 
we still support this version of Netscape. 
<br><br>
The above table of contents is an outline of <b>Netscape's Preferences</b>, select the section
of the proferences you wish to view. 
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->