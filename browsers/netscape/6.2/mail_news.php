<h1>Netscape 6.2 - Mail and Newsgroups</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/main.php">Netscape 6.2</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 6.2.
</td></tr>
</table>
</p>
<p>
<h3>Mail and Newsgroups</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/mail_newsgroups.jpg" alt="Mail and Newsgroups">
</p>
<p>
<h3>Mail and Newsgroups - Message Display</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/mail_newsgroups_message_display.jpg" alt="Mail and Newsgroups - Message Display">
</p>
<p>
<h3>Mail and Newsgroups - Composition</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/mail_newsgroups_message_composition.jpg" alt="Mail and Newsgroups - Composition">
</p>
<p>
<h3>Mail and Newsgroups - Send Format</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/mail_newsgroups_send_format.jpg" alt="Mail and Newsgroups - Send Format">
</p>
<p>
<h3>Mail and Newsgroups - Addressing</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/mail_newsgroups_addressing.jpg" alt="Mail and Newsgroups - Addressing">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->