<h1>Netscape 6.2 - Instant Messenger</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/main.php">Netscape 6.2</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 6.2.
</td></tr>
</table>
</p>
<p>
<h3>Instant Messenger</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/instant_messenger.jpg" alt="Instant Messenger">
</p>
<p>
<h3>Instant Messenger - Privacy</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/instant_messenger_privacy.jpg" alt="Instant Messenger - Privacy">
</p>
<p>
<h3>Instant Messenger - Notification</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/instant_messenger_notification.jpg" alt="Instant Messenger - Notification">
</p>
<p>
<h3>Instant Messenger - Away</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/instant_messenger_away.jpg" alt="Instant Messenger - Away">
</p>
<p>
<h3>Instant Messenger - Connection</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/instant_messenger_connection.jpg" alt="Instant Messenger - Connection">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->