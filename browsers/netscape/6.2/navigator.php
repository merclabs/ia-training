<h1>Netscape 6.2 - Navigator</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/main.php">Netscape 6.2</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 6.2.
</td></tr>
</table>
</p>
<p>
<h3>Navigator</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator.jpg" alt="Navigator">
</p>

<p>
<h3>Navigator - History</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator_history.jpg" alt="Navigator - History">
</p>

<p>
<h3>Navigator - Languages</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator_languages.jpg" alt="Navigator - Languages">
</p>

<p>
<h3>Navigator - Applications</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator_helper_applications.jpg" alt="Navigator - Helper Applications">
</p>
<p>
<h3>Navigator - Smart Browsing</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator_smart_browsing.jpg" alt="Navigator - Smart Browsing">
</p>
<p>
<h3>Navigator - Internet Search</h3>
 
</p>
<p>
<img src="./browsers/netscape/6.2/images/navigator_internet_search.jpg" alt="Navigator - Internet Search">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->