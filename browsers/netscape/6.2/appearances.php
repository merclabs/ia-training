<h1>Netscape 6.2 - Appearance</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/main.php">Netscape 6.2</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
We do support Netscape 6.2.
</td></tr>
</table>
<p>
<h3>Appearance</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/appearance.jpg" alt="Appearance">
</p>
<p>
<h3>Appearance - Fonts</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/appearance_fonts.jpg" alt="Appearance - Fonts">
</p>
<p>
<h3>Appearance - Colors</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/appearance_colors.jpg" alt="Appearance - Colors">
</p>
<p>
<h3>Appearance - Themes</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/appearance_themes.jpg" alt="Appearance - Themes">
</p>
<p>
<h3>Appearance - Content Packs</h3>
</p>
<p>
<img src="./browsers/netscape/6.2/images/appearance_content_packs.jpg" alt="Appearance - Content Packs">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
