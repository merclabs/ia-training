<h1>Netscape 6.2</h1>
<p align="center">
<img src="./browsers/netscape/6.2/images/splash.jpg" alt="Netscape 6.2">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/preferences.php">Netscape Preferences</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/appearances.php">Appearance</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/navigator.php">Navigator</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/composer.php">Composer</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/mail_news.php">Mail and Newgroups</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/instant_messenger.php">Instant Messenger</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/privacy_security.php">Privacy & Security</a></td></tr>
<tr><td class="content_link"><a href="?content=./browsers/netscape/6.2/advanced.php">Advanced</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Netscape 6.2</b> can be downloaded from the Netscape website. If a customer needs to upgrade 
or wants to reinstall <b>Netscape 6.2</b> please send them to <a href="http://wp.netscape.com/download/archive.html">Archived Products</a>
on the <b>Netscape</b> website. 
</p>
<p>
The above table of contents is an outline of <b>Netscape's Preferences</b>, select the section
of proferences you wish to view. 
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
