<h1>Software Downloads - Putty</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Main Menu</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/main.php">Back to Downloads</a></td></tr>
</table>
</p>
<p class="note">
PuTTY is a free implementation of Telnet and SSH for Win32 and Unix platforms, 
along with an xterm terminal emulator. It is written and maintained primarily by 
<a href="http://www.chiark.greenend.org.uk/~sgtatham/" target="_blank">Simon Tatham</a>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Putty</td></tr>
<tr><td class="content_link"><a href="http://www.chiark.greenend.org.uk/~sgtatham/putty/" target="_blank">Putty Home Page</a></td></tr>
<tr><td class="content_link"><a href="http://www.iatraining.net/newdesign/software/putty/putty.exe">Download Putty Now...</a></td></tr>
</table>
</p>

<!--
<p>
<table class="list_table">
<tr><td class="list_header"></td></tr>
<tr><td class="content_link"><a href="" target="_blank"></a></td></tr>
</table>
</p>
-->
