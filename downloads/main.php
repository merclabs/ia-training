<h1>Software Downloads</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Telnet Clients</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/putty.php">Putty</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Web Browsers</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/internet_explorer.php">Internet Explorer</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/netscape_navigator.php">Netscape Navigator</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/mozilla.php">Mozilla</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/opera.php">Opera</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">E-mail Clients</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/eudora.php">Eudora</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/incredimail.php">Incredimail</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Instant Messengers</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/icq.php">ICQ</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/msn_messenger.php">MSN Messenger</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/yahoo_messenger.php">Yahoo Messenger</a></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/aol_messenger.php">AOL Instant Messenger</a></td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<!--
<p>
<table class="list_table">
<tr><td class="list_header"></td></tr>
<tr><td class="content_link"><a href="?content=./downloads/"></a></td></tr>
</table>
</p>
-->
