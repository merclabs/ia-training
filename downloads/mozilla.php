<h1>Software Downloads - Mozilla</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Main Menu</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/main.php">Back to Downloads</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Mozilla</td></tr>
<tr><td class="content_link"><a href="http://www.mozilla.org" target="_blank">Mozilla Home Page</a></td></tr>
<tr><td class="content_link"><a href="http://www.mozilla.org/products/mozilla1.x/download/" target="_blank">Mozilla 1.5</a></td></tr>
<tr><td class="content_link"><a href="http://www.mozilla.org/products/firebird/" target="_blank">Mozilla - Firebird 0.7</a></td></tr>
<tr><td class="content_link"><a href="http://www.mozilla.org/products/thunderbird/" target="_blank">Mozilla - Thunderbird 0.4</a></td></tr>
<tr><td class="content_link"><a href="http://www.mozilla.org/products/camino/" target="_blank">Camino 0.7 for Mac OS X</a></td></tr>
</table>
</p>
 

<!--
<p>
<table class="list_table">
<tr><td class="list_header"></td></tr>
<tr><td class="content_link"><a href="" target="_blank"></a></td></tr>
</table>
</p>
-->
