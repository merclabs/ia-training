<h1>Software Downloads - Opera</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Main Menu</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/main.php">Back to Downloads</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Opera</td></tr>
<tr><td class="content_link"><a href="http://www.opera.com/" target="_blank">Opera Home Page</a></td></tr>
<tr><td class="content_link"><a href="http://www.opera.com/download/" target="_blank">Opera 7.23 for Windows, English (US) version</a></td></tr>
<tr><td class="content_link"><a href="http://www.opera.com/download/index.dml?platform=mac" target="_blank">Opera 6.03 for MacOS, English (US) version</a></td></tr>
<tr><td class="content_link"><a href="http://www.opera.com/download/index.dml?platform=linux" target="_blank">Opera 7.23 for Linux i386, English (US) version</a></td></tr>
</table>
</p>

<!--
<p>
<table class="list_table">
<tr><td class="list_header"></td></tr>
<tr><td class="content_link"><a href="" target="_blank"></a></td></tr>
</table>
</p>
-->
