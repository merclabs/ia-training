<h1>Software Downloads - Internet Explorer</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Main Menu</td></tr>
<tr><td class="content_link"><a href="?content=./downloads/main.php">Back to Downloads</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Microsoft Internet Explorer</td></tr>
<tr><td class="content_link"><a href="http://www.microsoft.com/windows/ie/default.asp" target="_blank">Internet Explorer Home Page</a></td></tr>
<tr><td class="content_link"><a href="http://www.microsoft.com/mac/products/internetexplorer/internetexplorer.aspx?pid=internetexplorer" target="_blank">Internet Explorer for Macintosh Home Page</a></td></tr>
<tr><td class="content_link"><a href="http://download.microsoft.com/download/ie6sp1/finrel/6_sp1/W98NT42KMeXP/EN-US/ie6setup.exe" target="_blank">Download Internet Explorer 6 for Windows Now...</a></td></tr>
<tr><td class="content_link"><a href="http://download.microsoft.com/download/6/d/e/6dec427f-2ddc-4146-8d15-6d5f90668425/InternetExplorer517EN.bin" target="_blank">Download Internet Explorer 5.1.7 for Mac OS 8.1 to 9.x Now...</a></td></tr>
<tr><td class="content_link"><a href="http://download.microsoft.com/download/c/f/5/cf515a77-5acb-44e7-acc8-57544d90cc6e/InternetExplorer523.dmg" target="_blank">Download Internet Explorer 5.2.3 for Mac OS X  Now...</a></td></tr>
</table>
</p>

<!--
<p>
<table class="list_table">
<tr><td class="list_header"></td></tr>
<tr><td class="content_link"><a href="" target="_blank"></a></td></tr>
</table>
</p>
-->
