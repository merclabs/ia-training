sorry, sent your other NON-EXISTING :0) address.  Hopefully this will 
help 
you with getting a head start on screen shots bud.  Doyle does want a 
"button" for them.

Thanks in advance


>Date: Fri, 14 Nov 2003 19:54:29 -0500
>To: LTs
>From: Patrick Bryant <bryantp@infoave.net>
>Subject: New Provider and Motorola 5100 Cable Modem
>Cc: claytonbarnette@infoave.net, Doyle Bullard <doyle@InfoAve.Net>
>
>Team,
>
>We are getting a new provider that distributes the MOTOROLA 5100 CABLE 
>modem to the customers.  I went searching and found the below 
>links.  Please review the below information so that you are prepared 
for 
>any troubleshooting steps needed.  You will want to specifically look 
over 
>the following LEDs and what they represent when Solid, Off, or 
>Flashing:  Power, Online, Send, Receive.  This modem does seem to be 
PNP 
>(plug-n-play) with no configuration page.  However, it does offer a 
USB 
>option and might require the installation of USB software.  **Please 
>Note** - You CAN connect two computers (see pg. 40/41 in the manual 
for 
>graphics) to the cable modem - 1 via Ethernet and the 2nd via USB 
>simultaneously.  Additionally, the modem can act as a gateway for up 
to 32 
>computers on a network.
>
>Thanks
>
>Manual
>http://gicout60.gic.gi.com/customer_docs/user_guides/501650-001-a.pdf 
>(better to download to desktop and view.  Pgs. 2, 3, and 44 discusses 
the 
>Front Panel LEDs, Rear Panel, and Error Codes)
>
>2-Page condensed but has the LEDs  and references flashing etc.
>http://gicout60.gic.gi.com/customer_docs/user_guides/501654-001-a.pdf
>
>2-Page but shows back of modem and cabling
>http://gicout60.gic.gi.com/customer_docs/user_guides/501653-001-b.pdf

