<h1>Links</h1>
<p class="note">
Links on this page are used by Techs that find external screesshots and 
information to aid with troubleshooting in addition to IA Training. If you find 
that a link here is broken, please let us know. 
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Links...</td></tr>
<tr><td class="list_header2">Internal Links</td></tr>
<tr><td class="content_link"><a href="http://www.iatraining.net/index2.php" target="_blank">Old IA Training</a></td></tr>


<tr><td class="list_header2">Troubleshooting Links</td></tr>
<tr><td class="content_link"><a href="http://www.network-tools.com" target="_blank">Network Tools</a></td></tr>
<tr><td class="content_link"><a href="http://www.dnsstuff.com/" target="_blank">DNS Stuff</a></td></tr>

<tr><td class="list_header2">Personal Website Links</td></tr>
<tr><td class="content_link"><a href="http://web.infoave.net/~robburris/support2.htm" target="_blank">Rob Burris's Site</a></td></tr>
<tr><td class="content_link"><a href="http://web.infoave.net/~dalove/" target="_blank">David Love's Site</a></td></tr>



<tr><td class="list_header2">Screenshots</td></tr>
<tr><td class="content_link"><a href="http://www.modemhelp.net/screenshots/index.shtml" target="_blank">Modemhelp</a></td></tr>
<tr><td class="content_link"><a href="http://support.bmi.net/Screens2.cfm" target="_blank">BMI Screen</a></td></tr>
<tr><td class="content_link"><a href="http://help.powerup.com.au/" target="_blank">Powerup Website</a></td></tr>
<tr><td class="content_link"><a href="http://www.net999.com/VirtualHelp/Email/index.html" target="_blank">Net999 Virtual Help - E-Mail</a></td></tr>
<tr><td class="content_link"><a href="http://www2.america.net/~chrsmith/modempages.html" target="_blank">DSL Lights</a></td></tr>
<tr><td class="content_link"><a href="http://help.cableone.net/cable/cm/docsis.aspx" target="_blank">CableONE-Certified Cable Modems</a></td></tr>


<tr><td class="list_header2">Help Site Links</td></tr>
<tr><td class="content_link"><a href="http://support.microsoft.com/default.aspx?scid=fh%3Ben-us%3Boemphone&LN=EN-US&x=10&y=10" target="_blank">Microsoft OEM Support Numbers</a></td></tr>
<tr><td class="content_link"><a href="http://help.stargate.net/broadband/" target="_blank">Stargate Help Site</a></td></tr>
<tr><td class="content_link"><a href="http://www.error629.com/" target="_blank">Error 629 Site</a></td></tr>
<tr><td class="content_link"><a href="http://www.netfaqs.com/" target="_blank">Netfaqs Website</a></td></tr>



<tr><td class="content_link"><a href="http://www.linksys.com/products/group.asp?grid=34&scid=29" target="_blank">Linksys Router Info</a></td></tr>


<!-- <tr><td class="content_link"><a href="" target="_blank"></a></td></tr> -->
</td></tr>
</table>
</p>