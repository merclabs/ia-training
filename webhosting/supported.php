<h1>Webtelpro Supported Providers</h1>
<?require_once("branded.php");?>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/main.php">Back to Web Hosting Support</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
This page contains a listing of <b>Providers</b> hosting with <b>Webtelpro</b>. The providers
on this page have purchased "<b>Branded Web Hosting</b>" from us, meaning that we <b><u>support</u></b> their 
web hosting customers the same as we support their dial-up customers. If a web hosting customer with a provider
listed on this page has a <b>billing issue</b> or displays as <b>Disabled</b> in our <b>Webtech Lookup Tool</b>,
then please direct the customer to <b>Customer Service</b> for that provider. 
<p class="note">
<b>Note:</b> <b>TriCounty Communications</b> is has hosting split between <b>Webtelpro</b> and their <a href="http://www.gotricounty.biz/webservices-hosting.htm" target="_blank">own</a> web hosting. Customers that
signed up through <b>Webtelpro</b> will show up in the <b>Webtech Look Up Tool</b>, and should be treated like any <i>standard</i> <b>Webtelpro</b> call.
If you <u>can not</u> find a customer in the <b>Webtech Look Up Tool</b>, chances are that they are hosted 
with <b>TriCounty's web hosting</b>. If so, have the customer contact <b>TriCounty's Web Services Department</b>. 
<br />
<br />
<b>Web Services Department</b><br />
TriCounty<br />
252-964-8000<br />
</p>
</p>
<p>
<h3>Webtelpro User Utilities</h3>
The <b>User Utilities</b> allows webhosting customers to manage different aspects of their website
such as <b>Turn on FrontPage Extensions</b>, <b>Request Mail Accounts</b>, <b>Manage Mail Accounts</b>
and <b>View log files</b>. If a webhosting customer is trying to reach their <b>User Utilities</b> 
please send the customer to their providers <b>Branded Web Hosting</b> page. A link to the <b>User 
Utilities</b> is located on each <b>Branded Web hosting</b> page labeled 
<a href="https://utilities.webtelpro.com/" target="_blank">Modify Existing Account</a>.
</p>
<p class="note">
If a customer can not reach the providers <b>Branded Web Hosting</b> page or has only one phoneline, they can reach their
<b>User Utilities</b> directly by going to <b>https://utilities.webtelpro.com/</b>.
</p>
</td></tr>
</table>
</p>
<p class="note">
<b>Note:</b> Some providers listed here maybe <b>VISP's</b> and only provide hosting.
</p>
<table class="list_table">
<tr><td class="list_header" colspan="2">Branded Web Hosting Provider Listing</td></tr>
<?
for($i = 0;$i < count($hoster);$i++){
echo("<tr>\n");
echo("<td class=\"list_content\"><b>$hoster[$i]</b></td><td class=\"content_link\"><a href=\"$prefix$webhostinglink[$i]\" target=\"_blank\">$prefix$webhostinglink[$i]</a></td>"); 
echo("</tr>\n");
}
?>
</table> 
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
