<h1>Webtelpro Overview - How to use PTS with Webtelpro Calls</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/main.php">Back to Web Hosting Support</a></td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/overview.php">Webtelpro Overview</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>

</td></tr>
</table>
</p>
<p>
<h3>PTS - Enter Domain Name</h3>
When you get a <b>Webtelpro</b> call, in <b>PTS</b> you enter their <b>Domain Name</b> in the <b>Dom. name*</b>. box on the 
<b>Search tab</b> without the <b>www's</b>. 
</p>
<p>
<img src="./webhosting/images/pts.jpg" alt="Enter Domain name of site in Domain name* box.">
</p>
<p>
<h3>PTS - Select Account</h3>
If the customer is hosted with us, then <b>PTS</b> should find the customers account. If not, find out 
who they purchased hosting through. If the provider is listed on the 
<a href="?content=./webhosting/unsupported.php"><b>Unsupported Providers</b></a> page, then
they should contact the <b>Provider</b> instead of us.
</p>
<p>
<img src="./webhosting/images/pick_a_call.gif" alt="Select Account">
</p>
<p class="note">
If the customer is has already given you his/her <b>Internet Account</b> user name and you have
already started a log, please copy your information to note pad or some other location then
cancel that log and start another one using the customers <b>Domain Name</b> so this call can be
logged correctly.
</p>
<p>
<h3>PTS - Select Problem</h3>
If no previous problems exist, click the <b>New Problem</b> button. 
</p>
<p>
<img src="./webhosting/images/pick_a_problem.gif" alt="Select Problem">
</p>
<p class="note">
If any previous problems 
exist, please take a moment to read the previous log, this may same you alot of time and
may also give you the answers you need before you start. 
</p>
<p>
<h3>PTS - Open with Webtelpro Call</h3>
Once <b>PTS</b> opens the log, Please complete all fields, <b>Phone</b>, <b>First Name</b>,
<b>Last Name</b> and <b>Connection Type</b>. This information is most always blank and needs
to be filled in. The rest is like a standard call. Please ask alot of questions and document 
everything in your log, even any <b>coding issues</b> that the customer may have at that time.  
</p>
<p>
<img src="./webhosting/images/pts_open_log.gif" alt="PTS - Open with Webtelpro Call">
</p>
<p class="note">
<b>Memo:</b> At no time should we do any coding or correcting for the customer, the customer is always
responsible for their own <b>HTML</b>, <b>Javascript</b>,  <b>Perl</b> or <b>PHP</b> code. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->