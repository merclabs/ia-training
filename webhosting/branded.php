<?
// Created 09/25/03 by Clayton Barnette
// cbarnette@infoave.net

/* Entry Templet 
To enter a new provider simply cut and paste
the following code where desired in this flie
then populate the ""'s with the information needed.
---------------------------------------------------

//provider name
$hoster[] = "";
$webhostinglink[] = "";

*/

// Provide HTTP Protocol prefix http://
$prefix = "http://";

//$hoster[] = "AAA Testing Services";
//$webhostinglink[] = "test.test.test";

$hoster[] = "Access Point, Inc.";
$webhostinglink[] = "webhosting.accesspipe.net";

$hoster[] = "Aiken Electric Cooperative";
$webhostinglink[] = "webhosting.aikenelectric.net";

$hoster[] = "Ben Lomand";
$webhostinglink[] = "webhosting.blomand.net";

$hoster[] = "Bledsoe Telephone Cooperative";
$webhostinglink[] = "webhosting.bledsoe.net";

$hoster[] = "Chester Long Distance";
$webhostinglink[] = "webhosting.chestertel.com";

$hoster[] = "COMPORIUM® Communications - Fort Mill, SC";
$webhostinglink[] = "webhostingfmtc.comporium.com";

$hoster[] = "COMPORIUM® Communications - Lancaster,SC";
$webhostinglink[] = "webhostingltc.comporium.com";

$hoster[] = "COMPORIUM® Communications - Rock Hill,SC";
$webhostinglink[] = "webhostingrhtc.comporium.com";

$hoster[] = "Dark III Marketing";
$webhostinglink[] = "webhosting.darkiii.com";

$hoster[] = "Enhanced Telecommunications";
$webhostinglink[] = "webhosting.nalu.net";

$hoster[] = "Highland Communications Corporation";
$webhostinglink[] = "webhosting.highland.net";

$hoster[] = "Home Telephone Company, Inc.";
$webhostinglink[] = "webhosting.hometelco.com";

$hoster[] = "Info Avenue Internet Services, LLC";
$webhostinglink[] = "webhosting.infoave.net";

$hoster[] = "Jefferson Electric Membership Corporation";
$webhostinglink[] = "webhosting.jeffersonenergy.com";

$hoster[] = "Lexcom Communications";
$webhostinglink[] = "webhosting.lexcominc.net";

$hoster[] = "Madison Telephone";
$webhostinglink[] = "webhosting.madisontelco.com";

$hoster[] = "MGW Networks, LLC";
$webhostinglink[] = "webhosting.mgwnet.com";

$hoster[] = "MultinetUSA";
$webhostinglink[] = "webhost.multinetusa.net";

$hoster[] = "Nameofbiz.com";
$webhostinglink[] = "webhosting.nameofbiz.com";

$hoster[] = "NU-Z.Net";
$webhostinglink[] = "host.nu-z.net";

$hoster[] = "PBT Communications";
$webhostinglink[] = "webhosting.pbtcomm.net";

$hoster[] = "PRTCommunications, LLC";
$webhostinglink[] = "webhosting.prtcnet.com";

$hoster[] = "Sandhill Telephone Cooperative";
$webhostinglink[] = "webhosting.shtc.net";

$hoster[] = "Smart City";
$webhostinglink[] = "webhosting.smartcity.net";

$hoster[] = "Southwest Online Internet Services";
$webhostinglink[] = "webhosting.swmail.net";

$hoster[] = "Spirit Telecom";
$webhostinglink[] = "webhosting.spirittelecom.com";

$hoster[] = "Spruce Knob Seneca Rocks Telephone, Inc";
$webhostinglink[] = "webhosting.spruceknob.net";

$hoster[] = "Surry Internet";
$webhostinglink[] = "webhosting.surry.net";

$hoster[] = "TownNetUSA.com";
$webhostinglink[] = "webhosting.townnetusa.com";

$hoster[] = "Training Company";
$webhostinglink[] = "hosttest.infoave.net";

$hoster[] = "TriCounty Communications";
$webhostinglink[] = "host.gotricounty.net";

$hoster[] = "Wilkes Communications";
$webhostinglink[] = "hosting.wilkes.net";

$hoster[] = "WINCO";
$webhostinglink[] = "webhosting.winco.net";


?>
