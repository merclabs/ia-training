<h1>Basic Web Hosting Information</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/main.php">Back to Web Hosting Support</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>

<h3>Server Information</h3>
<ul>
<li>Compaq TruUNIX50a using TruCluster (multiple servers)
<li>Apache/1.3.26 (Unix) 
<li>Communigate Pro 4.1.5 
</ul>

<h3>Server Side Support</h3>
<ul>
<li>PHP/4.2.2 
<li>mod_ssl/2.8.10
<li>OpenSSL/0.9.6e 
</ul>

<h3>What We Support</h3>
<ul>
<li>CGIs in UNIX binary 
<li>UNIX Shell scripts
<li>Perl
<li>PHP
</ul>

<h3>What We Don't Support</h3>
<ul>
<li>JSP
<li>Cold Fusion
<li>MySQL
<li>ASP
<li>No Telnet Accounts 
</ul>

<h3>Programs We Do Support</h3>
<ul>
<li>Text based FTP
<li>FrontPage 2000
<li>WS-FTP Pro
<li>HTML-Kit
<li>Dreamweaver
<li>Go-Live
<li>Microsoft Web Folders(<i>requires</i> FrontPage Extensions to be <b>enabled</b>.)
</ul>


<h3>Domain Names</h3>
<p>
If they are with <b>IA Registry</b> and their domain name has expired
they need to contact their provider to renew it. If they are calling to change whois
information on domain they need to call their provider or:
</p>
<ul>
<li><b>Toll-free in North America:</b> 1-877-932-2339 
<li><b>Outside North America:</b> 1-803-802-6552
<li><b>Fax:</b> 1-803-802-6599 or 803-802-4700
<li><b>E-mail:</b> info@iaregistry.com  
</ul>
</td></tr>
</table>
</p>


