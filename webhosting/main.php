<div align="center">
<h1>Web Hosting Support</h1>
<table border="0" width="75%" valign="center" cellspacing=15>
  <tr>
    <td align="center"><a href="?content=./webhosting/overview.php"><img src="./webhosting/images/overview2.jpg" alt="Webtelpro Overview" border=0></a>
		<div align="center">
		 <b>Webtelpro Overview</b>
		</div>
		</td>
    <td align="center"><a href="?content=./webhosting/webhosting_info.php"><img src="./webhosting/images/basic_hosting_info2.jpg" alt="Basic Webhosting Information" border=0></a>
		<div align="center">
		<b>Web Hosting Information</b>
		</div>
		</td>
		<td align="center"><a href="https://secure.homeigo.com/~intrsctg/webtech/index.php" target="_blank"><img src="./webhosting/images/webtech2.jpg" alt="Webtech Lookup Tool 
Use this to:
> look up webtelpro customers.
> Verify e-mail accounts.
> Resend/Reset Password.
> Troubleshoot Webtelpro web sites" border=0></a>
		<div align="center">
		<b>Webtech Lookup Tool</b>
		</div>
		</td>
  </tr>
  <tr>
    <td align="center"><a href="https://utilities.webtelpro.com" target="_blank"><img src="./webhosting/images/user_utilites2.jpg" alt="Webtelpro User Utilities
Use this to:
> Check user/pass for website.
> Enable Enable FrontPage Extensions.
> As a visual aid to guide customer.
NOTE: Do not help with HTML coding" border=0></a>
		<div align="center">
		<b>User Utilities</b>
		</div>
		</td>
		<td align="center"><a href="?content=./webhosting/supported.php"><img src="./webhosting/images/supported2.jpg" alt="List of Supported Providers
Use this to:
> To verify if Provider is supported.
> To Refer customers to Provider Web Hosting Page." border=0></a>
		<div align="center">
		<b>Supported Providers</b>
		</div>
		</td>
     <td align="center"><a href="?content=./webhosting/unsupported.php"><img src="./webhosting/images/unsupported2.jpg" alt="List of Unsupported Providers
Use this to:
> See if we do not support web hosting for a provider." border=0></a>
		<div align="center">
		<b>Unsupported Providers</b>
		</div>
		</td>
  </tr>
	<tr>
    <td align="center"><a href="http://www1.iaregistry.com/cgi-bin/whois2.cgi" target="_blank"><img src="./webhosting/images/iaregistry_whois.jpg" alt="Link to IA Registry Whois
Use this to:
> Look up Whois information on a Domain.
> Check expiration date on a Domain.
> Check where DNS is pointed on Domain
> Check ownership on a Domain.
Note: This looks up only site registered through IA Registry" border=0></a>
		<div align="center">
		<b>IA Registry Whois</b>
		</div>
		</td>
		<td align="center">
		</td>
    <td align="center">
		</td>
  </tr>
	
	<tr>
		<div align="center">
		<b></b>
		</div>
		</td>
  </tr>
	</table>
</div>
