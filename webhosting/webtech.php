<h1>Webtelpro Overview - How to use the Webtech Lookup Tool</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/main.php">Back to Web Hosting Support</a></td></tr>
<tr><td class="content_link"><a href="?content=./webhosting/overview.php">Webtelpro Overview</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
The <b>Webtech Lookup Tool</b> was designed by <b>Webservices</b> to make fielding 
a webtelpro call easier for <b>Technical Support</b>. Before this tool, every
aspect of  a <b>Webtelpro</b> account affecting <b>Account Status</b>,<b> Domain Name</b>, 
<b>E-mail Accounts</b>, <b>FrontPage Status</b>, <b>DNS</b>, <b>NSLookup</b> and <b>Domain Alias</b> 
had to be looked up separately by the same person and at times by other departments.
</p>
<p> 
Now with the aid of this tool you can now look up almost everything about a <b> Webtelpro</b>
hosted account in about 45 seconds. This tool is a very important part of our goal to provide a
better technical support experience for both you and our web hosting customer on behalf of our providers.  
</p>
</td></tr>
</table>
</p>
<p>
<h1>Accessing Webtech</h1>
<b>Webtech</b> is our web based lookup tool, you can access it by going to <b>
<a href="http://webtech.infoave.net" target="_blank">http://webtech.infoave.net</a></b>.
</p>
<p>
<img src="./webhosting/images/webtech_0.jpg" alt="Enter your Intranet User Name and Password">
</p>
<p class="note">
<b>Webtech</b> is a secure site, so it will prompt you for your Intranet password.
</p>
<p>
<h1>Opening Page</h1>
After entering your login information, the opening page should display. Enter the <b>Domain Name</b>
of the <b>Webtelpro</b> customer, excluding the <b><i>www's</i></b> and press <b>Enter</b> on your keyboard.
</p>
<p>
<img src="./webhosting/images/webtech_1.jpg" alt="Entered customers Doamin Name ie: hargray.com, infoave.net etc...">
</p>
<p class="note">
<b>Note:</b> The first lookup may take as much as a minute, for some unknown reason, so be patient;
also you may enter part of a domain name the search field, the tool will do a grep type search for 
the domain name you entered and return a listing of similar instances if multiples exist.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p>
<h1>Domain Name not found</h1>
Most cases <b>Webtech</b> will not have any problem finding a customers <b>Domain Name</b>, but there are times
when it will not. When this happens <b>Webtech</b> will return a <b>No results found for
<i>domain</i></b> error message. This means one of three things.
<ol>
<li><b>Domain Name</b> you entered is misspelled.</li>
<li>The customer hosts with an <a href="?content=./webhosting/unsupported.php"><b>Unsupported Provider</b></a>.</li>
<li>The Domain has not been created yet.</li>
</ol>
</p>
<p>
<img src="./webhosting/images/webtech_2.jpg" alt="Not Found">
</p>
<p class="note">
If you get this error message and believe everything is spelled correctly, check with the customer
to if the host with 
</p>
<p>
<h1>Web Account Found</h1>
If the <b>Domain Name</b> is found, the customers <b>Account/Webhosting</b> information should display.
This screen will contain <i>Account Information</i>, <i>Domain Information</i> and <i>Email Information</i>.
<i>Email Information</i> will display only if the customer has active e-mail accounts on <b>Webtelpro</b>, otherwise
this section will remain blank. 
</p>
<p>
<img src="./webhosting/images/webtech_3.jpg" alt="Web Account Found">
</p>
<p class="note">
<b>Note:</b> Each <b>Webtelpro</b> account comes with a few free e-mail accounts, the number depends on the
type of account they signed up for. These accounts are not turned on by default, customer wanting
to use these free e-mail accounts must request activation through their <b>User Utilities</b> on
<b>Webtelpro</b>.  
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p>
<h1>Account Information</h1>
This section displays information about the account in its current state in <b>Inavex</b>. Below is a
list of the type of information you will find in this section.
</p>
<p>
<img src="./webhosting/images/webtech_11.jpg" alt="Account Information">
</p>
<ul>
<li>Physical Address</li>
<li>E-mail Address [<i>see: Resend Password</i>]</li>
<li>Webtelpro User Name</li>
<li>Type of Hosting Account</li>
<li>Link to providers <b>Branded Web Hosting</b> page</li>
<li>Resend Password link [<i>see: Resend Password</i>]</li>
<li>Bill Account Status</li>
<li>Host Account Status</li>
<li>FrontPage Status</li>
</ul> 
<p class="caution">
<b>Caution:</b> If the <b>Bill Account Status</b> or <b>Host Account Status</b> indicated <b>Past Due</b> or <b>Disabled</b> and the
website is not working, then this is a billing issue, <i>No other troubleshooting should be done</i>.
The customer needs to contact <b>Customer Service</b>.
</p>
<p>
<h1>Viewing the Website</h1>
Almost every instance of a <b>Domain Name</b> is a <i>hyper-link</i> that you can click on to check
the condition of the website. Clicking on these <i>hyper-links</i> will open the site in its own window, so there
is no need to open a separate web browser window. 
</p>
<p>
<img src="./webhosting/images/webtech_4.jpg" alt="Viewing the Website">
</p>
<p class="caution">
<b>Caution:</b> If you have a <b>Pop-up Stopper</b> program running, this and other pop-up
windows may not work correctly. <i>Disable</i> this type of program while working on a 
<b>Webtelpro</b> call if you have problems this this page.  
</p>
<p>
<h1>View Files</h1>
The <b>view files</b> link in the <b>Account Information</b> section allows you to
view a listing of the files on the customers website. <b>Directories</b> 
with content are listed as <i>hyper-links</i>, when clicked their contents are displayed. 
This view is <b>read-only</b> and opens in its own window, so no files can be changed or 
added through this view. 
</p>
<p>
<img src="./webhosting/images/webtech_5.jpg" alt="View Files">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p>
<h1>Resend Password</h1>
The <b>resend password</b> link in the <b>Account Information</b> section when clicked 
opens a new small window with a <b>Resend Password</b> button, when clicked will resend
the password for this <b>Web Hosting Account</b> to the <b>e-mail address</b> listed 
on the screen. Use this link only if the customer has forgotten their <b>Webtelpro</b> 
password.
</p> 
<p>
Before resending the password, check with the customer to make sure that the 
e-mail address listed is valid and the customer can receive and access mail at this address. 
</p>
<p>

</p>
<p>
<img src="./webhosting/images/webtech_6.jpg" alt="Resend Password">
</p>
<p class="note">
If this e-mail address listed is not valid or this section is blank, <b>DO NOT</b>
attempt to resend the password. Notify an <b>LT</b> to get permission to send to 
customer service or for call back.
</p>
<p>
<h1>Domain Information</h1>
This section contains several tools that can be used to troubleshoot this sites network presence.
</p>
<p>
<img src="./webhosting/images/webtech_12.jpg" alt="Domain Information">
</p>
<ul>
<li><b>local</b> [To test the local DNS response on our network]</li>
<li><b>external</b> [To test a external DNS response on AT&T's network]</li>
<li><b>dig</b> [To check the authoritative answer]</li>
<li><b>whois</b> [To check domains expired status]</li>
</ul>
<p>
<h1>NSLookup - Local</h1>
Click on the <b>local</b> link to perform a local lookup through 
our <b>DNS Servers</b>. The correct response is a <b>165.166.123.150</b> address along 
with any <b>Domain Alias</b> that the site also replies to. 
</p>
<p>
<img src="./webhosting/images/webtech_7.jpg" alt="NSLookup - Local">
</p>
<p>
<h1>NSLookup - External</h1>
Click on the <b>External</b> link to perform an external lookup through 
<b>AT&T's</b> <b>DNS Servers</b>. The correct response again is a <b>165.166.123.150</b> 
address along with any <b>Domain Alias</b> that the site also replies to. 
</p>
</p>
<p>
<img src="./webhosting/images/webtech_8.jpg" alt="NSLookup - External">
</p>
<p>
<h1>Dig</h1>
Clicking the <b>dig</b> link performs an authoritative lookup, used to determine
who <b>answers</b> request for a domain name. The <i>Answer</i>,<i>Authority</i> and <i>Additional</i> sections
should point to the <b>165.166.123.150</b> IP Address, our DNS: <b>dns4.infoave.net</b> and <b>dns3.
infoave.net</b>, and <b>DNS to IP</b> respectively.
</p>
<p>
<img src="./webhosting/images/webtech_9.jpg" alt="Dig">
</p>
<p class="note">
If you should see another <b>IP</b> instead ours, then their may be a <b>DNS</b> issue.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p>
<h1>Whois</h1>
The <b>Whois</b> link, when clicked displays the domain's <b>public whois information</b>. The main thing to
look for is expiration date of the domain.
</p>
<p>
<img src="./webhosting/images/webtech_10.jpg" alt="Whois">
</p>
<p class="note">
If a <b>Domain Name</b> has expired, notify an <b>LT</b> to get permission to send the customer to
their respective <b>Domain Name Registrar</b>. 

If they are with <b>IA Registry</b> they need to call:
<ul>
<li><b>Toll-free in North America:</b> 1-877-932-2339 
<li><b>Outside North America:</b> 1-803-802-6552
<li><b>Fax:</b> 1-803-802-6599 or 803-802-4700
<li><b>E-mail:</b> info@iaregistry.com  
</ul>
</p>
<p>
<h1>Email Information</h1>
Tools in the <b>Email Information</b> section are the same as those in the <b>Domain Information</b> 
section. You should receive the same information with the <b>local</b>, <b>external</b> and <b>whois</b>
links in the <b>Domain Information</b> section. 
</p>
<p>
<img src="./webhosting/images/webtech_13.jpg" alt="E-mail Information">
</p>
<p class="note">
If you get a different response with lookups in this section, check to make sure the
customer does not have e-mail hosted some where else, have <b>Virtual Mail</b> or has
a valid working account. 
</p>
<p>
<h1>Email Dig</h1>
<b>E-mail Dig</b> is similar to the dig in the <b>Domain Information</b> section, but 
returns <b>MX Record</b> information instead.
</p>
<p>
<img src="./webhosting/images/webtech_14.jpg" alt="Email Dig">
</p>
<!-- Back to Top button -->
<div align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</div>
<!-- Back to Top button -->
