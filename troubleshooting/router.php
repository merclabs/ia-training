<h1>Generic Router Troubleshooting</h1>
 
<p>
<table class="list_table">
<tr><td class="list_header">Router Troubleshooting</td></tr>
<tr><td class="content_link"><a 
href="?content=./content/main.php">Back to Main Page</a></td></tr>
<tr><td class="content_link"><a href="#1">The IP address is assigned 
automatically</a></td></tr>
<tr><td class="content_link"><a href="#2">PPPoE is needed to connect 
(not done on modem)</a></td></tr>
<tr><td class="content_link"><a href="#3">A static IP address needs to 
be specified</a></td></tr>
</table>
</p>

<p class="note">
The address that you go to in the internet browser to access the router 
configuration is the same as the Gateway IP that is being automatically 
assigned to the ethernet adapter.
</p>

<p>
<h3><a name="1">The IP address is assigned automatically</a></h3>
<br>

<ul>
<li>On the setup screen, it should be set to obtain automatically by 
DCHP.
<li>Go to the status screen.
<li>Here, check the external/internet/WAN IP address that is being 
assigned
<ul>
	<li>If it is a real/public IP, and the internet is not working, and 
there is an option on this screen to release and renew, then try that
	<li>If the IP address being assigned is what should be assigned based 
on the type of modem they have, and the internet is still not working, 
then this is a computer issue, and you would want to check for 
firewall/spyware programs
	<li>If it is not getting assigned the appropriate IP, this could be an 
issue between the router and the modem
	<ul><li>Try bypassing the router
		<ul><li> If it works, then it was a problem with the router
		    <li> If it doesn't work, it was a problem with the modem 
</ul></ul>
</ul></ul>

</p>
<p class="note">
<b>Sharing an Internet connection that has a private IP address</b><br> 
It is possible for the IP address being assigned by a customerís DSL 
modem to match
the default settings of their router. For instance, the DSL modem might 
assign an 
IP address of 192.168.1.2 and their routerís LAN IP address is 
192.168.1.1. This will 
not work because of routing issues. To fix this, simply change the IP 
schema of the router 
to something else - like 192.168.2.1 
</p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to 
Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">PPPoE is needed to connect (not done on modem)</a></h3>
<br>

<ul>
<li>On the setup screen, it should be set to PPPoE, and then username 
and password should be entered correctly.
<li>Go to the status screen.
<li>Here, if everything is working properly, it should show that it is 
connected, and should be assigned a real/public IP address.
<li>If it says disconnected, and has the option to connect, then hit 
connect.
<li>If it won't connect, have them go to the setup screen and retype 
their username and password
	<ul><li>If it still won't connect, try their username and password in 
telnet, if it works, then their password probably just needs to be 
reset.
	<li>If it doesn't work, something could be wrong with their account, 
and they would want to contact customer service about this</ul>
<li>If it says connected and is assigning a good IP address, but the 
internet still isn't working, this is a computer issue, and you would 
want to check for firewall/spyware programs.
</ul>
<p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to 
Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="3">A static IP address needs to be specified</a></h3>
<br>

</ul>
<li>On the setup screen, it should be set to Static IP, and the IP 
address, subnet, gateway, and DNS numbers should be specified  
<li>Go to the status screen
<li>Verify that the all the addresses were entered correctly
<li>If the internet still isn't working, you can determine where the 
signal is being lost by pinging
	<ul><li>Start by pinging the Gateway address that is specified on the 
router, and if that responds, then ping external IPs and domains
	<li>If everything pings successfully, but the internet is still not 
working, then this is a computer issue, and you would want to check for 
firewall/spyware programs.</ul>
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to 
Top"></a>
</p>
<!-- Back to Top button -->



<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to 
Top"></a>
</p>
<!-- Back to Top button -->

