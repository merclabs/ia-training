<h1>SpeedStream 5200</h1>
<p>
<table class="list_table">
<tr><td class="list_header">What lights are on?</td></tr>
<tr><td class="content_link"><a href="?content=./troubleshooting/first_level/main.php">Back to First Level Tips</a></td></tr>
<tr><td class="content_link"><a href="#1">If the DSL light is out</a></td></tr>
<tr><td class="content_link"><a href="#2">If Power, DSL and USB or ENET are on solid</a></td></tr>
<tr><td class="content_link"><a href="#3">If neither USB nor ENET are on solid</a></td></tr>
<tr><td class="content_link"><a href="#4">If no lights are on</a></td></tr>
</table>
</p>
<p class="note">
<b>NOTE:</b>  If DSL light blinks steadily, then it is training, but if it just blinks randomly, then that indicates internet traffic flowing.  If USB or ENET lights are blinking, that just indicates internet traffic flowing.
There is NO ACT LIGHT on this modem.
</p>

<p>
<h3><a name="1">If the DSL light is out</a></a></h3>
<br>
If a customer is doing a NEW INSTALL, please check when the install date is 
(and put it in your ticket). The customer can expect service 3 business days from 
the date they signed up.  If it has not been that long yet, please ask the customer 
to wait until then, and then call back if it still doesn�t work.  DO NOT escalate 
to 2nd level if you tell them that.  
</p>
<p>
If they have been called and told that it should be up by a certain time, or it 
has been more than 3 days since they got their DSL service, or it has worked before, 
then continue troubleshooting:
</p>
<ul>
 <li>Check physical connections.
 <li>Make sure filters are hooked up properly (they should have all phones going through filters).  
 <li>If the dsl line is hooked up to a filter, it must be hooked up on the dsl side (which means it isn�t really being filtered). 
 <li>Try bypassing splitters and/or surge protectors if applicable.
 <li>If dsl light is still out, then <a href="#escalate">escalate</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="2"><a name="2">If Power, DSL and USB or ENET are on solid</a></a></h3>
<br>
Have the customer do the following: 
<ol>
 <li>Power cycle the DSL modem.  Wait for Power, DSL and USB or ENET lights to come back on solid.
 <li>Power cycle the router (if they have one).  Wait for lights to come back on like they were.
 <li>Restart the computer.
 <li>Try the internet browser again.
</ol>
</p>

<p class="note">
If it still isn�t working, make sure in their network settings that it is set to 
obtain IP/DNS/Gateway automatically. Customers with Speedstream modems cannot have a
static IP.
</p>

<p>
<ul>
 <li>If they are going through a router, then <a href="#router">click here</a>.
 <li>If the computer is connected directly to the DSL modem, <a href="#no_router">click here</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->



<p>
<h3><a name="3"><a name="3">If neither USB nor ENET are on solid</a></a></h3>
<br>
Do the following:
<p>
<ul>
 <li>Check the physical connection from the DSL modem to the computer (or router if there is one).  
 <li>Make sure the computer/router is turned on. 
 <li>Power cycle the DSL modem and the computer/router. 
 <li>If they are trying to connect through USB, make sure the drivers have been installed correctly. 
 <li>If they have different Ethernet/USB cable, have them try that next. 
 <li>If they are connecting through USB, have them try a different USB port in the computer. 
 <li>If none of this helps then <a href="#escalate">escalate</a>.  
</ul>
 <p class="note">
  It could be a bad modem, bad Ethernet card/USB port or bad Ethernet/USB cable.
 </p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->



<p>
<h3><a name="4"><a name="4">If no lights are on</a></a></h3>
<p>
Check all physical connections (power cord(s), power strip(s), and for any light 
switches that may control the power outlet that the modem is on.).
</p>
<p>
<i>If still not working</i> <a href="#escalate">follow escalation procedure</a>.
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="#router">If they are going through a router</a></h3>
<br>
What IP address is being assigned?
<ul>
<li>If the customer is getting a 169.x.x.x ip address, <a href="#169">click here</a>.
<li>If the customer is getting a 192.x.x.x ip address, <a href="#192">click here</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="#no_router">If the computer is connected directly to the DSL modem</a></h3>
<br>
What IP address is being assigned?
<ul>
 <li>If it is a <a href="#PRIVATE">PRIVATE IP</a> of 169.x.x.x, <a href="#169-private">click here</a>.
 <li>If it is a <a href="#PRIVATE">PRIVATE IP</a> of 192.x.x.x, <a href="#192-private">click here</a>.
 <li>If it is a <a href="#PUBLIC">PUBLIC IP</a>, then <a href="#public">click here</a>.
 <li>If it they are getting an IP address of 0.0.0.0, <a href="#zero-ip">click here</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="#3">If neither USB nor ENET are on solid</a></h3>
<br>
Do the following:
<ul>
   <li>Check the physical connection from the DSL modem to the computer (or router if there is one).  
   <li>Make sure the computer/router is turned on. 
   <li>Power cycle the DSL modem and the computer/router. 
   <li>If they are trying to connect through USB, make sure the drivers have been installed correctly. 
   <li>If they have different Ethernet/USB cable, have them try that next. 
   <li>If they are connecting through USB, have them try a different USB port in the computer. 
   <li>If none of this helps then escalate.  It could be a bad modem, bad Ethernet card/USB port or bad Ethernet/USB cable. 
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="#169">Router - 169.x.x.x IP Address</a></h3>
<br>
Depending on what lights are on on the router, this could indicate there is no 
connectivity between the computer and the router, or it could mean there just isn�t 
a good connection between them.

<p>
If there are other computers hooked up to it that can communicate with it, then it 
is a problem with either the port on the router, the Ethernet cable, or the Ethernet card.  
</p>
<p>
If this is the only computer hooked up to the router, try bypassing it. 
</p>
<ul>
 <li>If the internet works now, it was a problem with the router. 
 <li>If not, it is a problem with either the Ethernet cable or Ethernet card. 
</ul>
<p class="note">
See an LT for approval on what to do about this.
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="#192">Router - 192.x.x.x IP Address</a></h3>
<br>
This means that there is communication between the computer and the router.  
<ul>
<li>See what the gateway address is, and see if you can open that address in Internet 
Explorer.  This may take you to the router configuration page. 
<li>If this has a �Status� area, you may be able to determine what is wrong with 
the connection by what it says there.   
</ul>
<p>
<i>See an LT for further troubleshooting info.</i>
</p>

<p class="note">
<b>Sharing an Internet connection that has a private IP address </b><br>
It is possible for the IP address being assigned by a customer�s DSL modem to match
the default settings of their router.  For instance, the DSL modem might assign an 
IP address of 192.168.254.x and their router�s LAN IP address is 192.168.254.1. 
This will not work because of routing issues. To fix this, simply change the IP schema 
of the router to something else - like 192.168.100.1
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="#169-private">If it is a PRIVATE IP of 169.x.x.x</a></h3>
<br>
Make sure they aren�t trying to hook up a different computer to the DSL modem without releasing 
the IP address from the old machine. If this is the case, it could take 4 hours for it 
to auto-release on our end (this does not apply for Gulftel), and then it will work. 
<p>
If they are using the same computer they always have been, then go ahead and 
<a href="#escalate">escalate</a>. Although their physical connection is good at this point, 
it is not a good connection. 
</p>
 
<p class="warning">
<b>NOTE:</b> Release and renew will NOT work, and you won�t be able to pull the 
config page, so don�t try those things.
</p>
</p><br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="#192-private">If it is a PRIVATE IP of 192.x.x.x</a></h3>
<br>
Try to go to the config page (192.168.254.254). Then follow instructions on training site for the <a href="javascript:open('http://www.iatraining.net/newdesign/broadband/gulftel/speedstream5200_webinterface.php','bbw','width=550, height=450');this.href;">web interface</a>.
<ul>
 <li>If they can�t get to the config page, or if it shows connected on the page, but the internet still isn�t working:
    <ul>
	  <li>Check to make sure nothing is checked in LAN settings (i.e. proxy settings). 
	  <li>Check for firewall programs and suggest they disable/remove them.
	  <li>Check for spyware programs and suggest they have them removed.
	  <li>Otherwise, see lead tech for approval to refer them to the computer manufacturer
    </ul>
	</li>
 <li>If they can get to the config page, but they are unable to get it to connect 
 on that page, then document the error they get and <a href="#escalate">escalate</a>.
</ul>
<p class="warning">
<b>NOTE:</b> Do <u><b>NOT</b></u> try to release and renew � it doesn�t accomplish anything.
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="public">If it is a PUBLIC IP</a></h3>
<br>
Do the following:
<ul>
<li>Try to release and renew (or repair in XP).
  <p>
	<ul>
    <li>When you release, the IP address should go to 0.0.0.0.  
    <li>When you renew, it should go back to a <a href="#public">PUBLIC</a> IP. 
  </ul>
	</p>
<li>
<p>
If the release and renew was successful and the internet still isn�t working, 
then this is a problem on the customer�s machine. Try <a href="#ping">pinging</a>. 
</p>
<li>If renewing failed, try power cycling the DSL modem and restarting the computer again.  
    <p>
		<ul>
		 <li>If it has a 169.x.x.x IP address now, then <a href="#escalate">escalate</a>. 
     <li>If it got back the <a href="#public">PUBLIC</a> IP, try the internet again, if it still isn�t working, then try <a href="#ping">pinging</a>. 
    </ul>
		</p>
</ul>
<p class="note">
NOTE:  Do <u><b>NOT</b></u> try going to the config page.
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="escalate">Escalation Procedure</a></h3>
<br>
The following things should be in your ticket:
<ul>
 <li>ALL the troubleshooting steps you took in detail.
 <li>Has the DSL worked before?
 <li>The Serial Number for Lucent DSL Pipes or the MAC address for Speedstream modems
 <li>Customer�s Name
 <li>Contact Number
 <li>Alternate Contact Number (such as cell phone or work number)
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="#PRIVATE">About PRIVATE IP</a></h3>
<br>
The 169.254 subnet range is for APIPA - Automatic Private IP Addressing. Microsoft 
uses APIPA in instances where a DHCP server is not present. When a host cannot find 
a DHCP server, it will assign itself an address in the 169.254 subnet range, thus 
allowing communications between itself and other PCs who are using APIPA.  In short, 
if no DHCP server exists, and both machines use APIPA to get an IP address, YES, you 
can ping from one to the other. It is a valid IP address, and the IP stack is still 
functioning. 
</p> 
<p>
The Internet Assigned Numbers Authority (IANA) has reserved the following three 
blocks of the IP address space for private internets (local networks): 
<ul>
 <li>10.x.x.x 
 <li>172.16.x.x � 172.31.x.x 
 <li>192.168.x.x 
</ul>
The 192.168 subnet range is generally used by routers or DSL modems in router mode 
to allow computers to share one public IP address, and still be able to distinguish 
between the computers sharing the network connnection. 
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3>About PUBLIC IP</h3>
<br>
<p>
A public IP address is recognized throughout the internet.  Common public IP addresses
for DSL connections are (but are not limited to) 66.x.x.x and 207.x.x.x.
</p>
<p>
Here is a list of addresses that they shouldn�t get if they are supposed to have 
a public IP:
</p>
<pre>
Address Block             Present Use                       Reference
   ---------------------------------------------------------------------
   0.0.0.0/8            "This" Network                 [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1700" target="_blank">RFC1700</a>, page 4]
   10.0.0.0/8           Private-Use Networks                   [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1918" target="_blank">RFC1918</a>]
   14.0.0.0/8           Public-Data Networks         [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1700" target="_blank">RFC1700</a>, page 181]
   24.0.0.0/8           Cable Television Networks                    --
   39.0.0.0/8           Reserved but subject to allocation    [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1797" target="_blank">RFC1797</a>]
   127.0.0.0/8          Loopback                       [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1700" target="_blank">RFC1700</a>, page 5]
   128.0.0.0/16         Reserved but subject to allocation           --
   169.254.0.0/16       Link Local                                   --
   172.16.0.0/12        Private-Use Networks                   [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1918" target="_blank">RFC1918</a>]
   191.255.0.0/16       Reserved but subject to allocation           --
   192.0.0.0/24         Reserved but subject to allocation           --
   192.0.2.0/24         Test-Net
   192.88.99.0/24       6to4 Relay Anycast                     [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?3068" target="_blank">RFC3068</a>]
   192.168.0.0/16       Private-Use Networks                   [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1918" target="_blank">RFC1918</a>]
   198.18.0.0/15        Network Interconnect Device Benchmark Testing      [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?2544" target="_blank">RFC2544</a>]
   223.255.255.0/24     Reserved but subject to allocation            --
   224.0.0.0/4          Multicast                              [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?3171" target="_blank">RFC3171</a>]
   240.0.0.0/4          Reserved for Future Use        [<a href="http://www.freesoft.org/CIE/RFC/bynum.cgi?1700" target="_blank">RFC1700</a>, page 4]
</pre>
</p>
Information from <a href="http://www.freesoft.org" target="_blank">Connected</a>: An Internet Encyclopedia
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="#zero-ip">IP Address of 0.0.0.0</a></h3>
<br>
<p>
<b>Windows NT/2000/XP:</b> Make sure the local area connection is enabled.<br />
<b>Windows 95/98/ME/:</b> <i>Pending</i> 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="#ping">Ping</a></h3>
<br>
Start by trying to ping the Gateway. 
<p>
When getting 4 replies (0% loss):<br />
<ol>
<li>Try to ping an outside IP to determine if the DSL connection is up and working. 
<li>If that doesn�t work, then <a href="#escalate"><u>escalate</u></a>.
<li>If that does work, try to ping a domain.  If that doesn�t work, then escalate.
<li>If that does work, but they are not pulling web pages:
  <ul>
   <li>Check to make sure nothing is checked in LAN settings (i.e. proxy settings). 
   <li>Check for firewall programs and suggest they disable/remove them.
   <li>Check for spyware programs and suggest they have them removed.
   <li>Otherwise, see lead tech for approval to refer them to the computer manufacturer.
  </ul>
</ol>
<p>
If you got no response (100% loss):<br />
<ul>
<li>Try to ping the IP address for the network card. 
<li>If that doesn�t respond, refer to manufacturer. 
<li>If it does respond, then escalate. 
</ul>
</p>
<p class="note">
This will determine if the connection from DSL to PC is secure and/or if the static IP settings(if any) are correct.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
