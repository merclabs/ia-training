<h1>Lucent DSL Pipe</h1>
 
<p>
<table class="list_table">
<tr><td class="list_header">What lights are on?</td></tr>
<tr><td class="content_link"><a href="?content=./troubleshooting/first_level/main.php">Back to First Level Tips</a></td></tr>
<tr><td class="content_link"><a href="#1">If WAN(or BRI) light is flashing</a></td></tr>
<tr><td class="content_link"><a href="#2">If PWR, LNK(or COL), and WAN(or BRI) are all on solid</a></td></tr>
<tr><td class="content_link"><a href="#3">If PWR and WAN(or BRI) are on solid, but LNK(or COL) is not</a></td></tr>
<tr><td class="content_link"><a href="#4">If no lights are on</a></td></tr>
</table>
</p>

<p>
<h3><a name="1">If WAN(or BRI) light is flashing</a></h3>
<br>
Do the following:
<ul>
<li>Check the physical connection of the phone cord from the DSL pipe to the phone jack on the wall. 
<li>If a surge protector or splitter is hooked up to it, try bypassing it. 
<li>Power cycle the DSL pipe. 
</ul>
If the WAN light is still blinking, then <a href="#escalate"><u>escalate</u></a>.
</p>
<p class="note">
If bypassing surge protector or splitters corrects problem, have devices replaced. If surge protector was provided by us, have it replaced at local office.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">If PWR, LNK(or COL), and WAN(or BRI) are all on solid</a></h3>
<br>
Have the customer do the following:
<ol>
<li>Power cycle the DSL pipe.  Wait for Power, LNK (or COL) and WAN (or BRI) lights to come back on solid.
<li>Power cycle the router (if they have one).  Wait for lights to come back on like they were.
<li>Restart the computer.
<li>Try the internet browser again.
</ol>
<p>
If it still does not pull a page, then: 
</p>
<ul>
<li><a href="#router">If they have a router.</a>
<li><a href="#no_router">If they do NOT have a router</a>
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="3">If PWR and WAN(or BRI) are on solid, but LNK(or COL) is not</a></h3>
<br>
Do the following:
</ul>
<li>Check the physical connection from the DSL pipe to the computer (or router if there is one).  
<li>Make sure the computer/router is turned on. 
<li>Power cycle the modem and restart the computer/router. 
<li>If light is still off, try a different port in back of DSL pipe. 
<li>If they have different Ethernet cord, have them try that next. 
<li>If none of this helps then <a href="#escalate"><u>escalate</u></a>.
<p class="note">
It could be a bad modem, bad Ethernet card or bad Ethernet cable. 
</p>
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="4">If no lights are on</a></h3>
<br>
<p>
Check all physical connections (power cord(s), power strip(s), and for any light 
switches that may control the power outlet that the modem is on.).
</p>
<p>
<i>If still not working</i> <a href="#escalate">follow escalation procedure</a>.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 

<p>
<h3><a name="4"><a name="router">If they have a router</a></a></h3>
<br>
The router needs to be setup with a static IP, and the computer should be setup to obtain everything automatically.
<ul>
<li>See what IP address and gateway they are being assigned on the computer. 
<li>Type in the gateway address in their browser and it should bring up the router configuration screen. 
<li>If it is not set to Static IP on the router, have them set it to that.  Then enter the IP/subnet/gateway/DNS numbers. 
 <ul>
  <li>If they don�t have real static IP, then have them enter in the same settings you would for a PC (192.168.1.2, etc�). 
  <li>If they do have a real static IP, then enter in those settings. 
 </ul>
<li>If all settings are specified correctly, and they still can�t get web pages. Then try to ping. 
</ul>
</p>
<p class="note">
<b>Sharing an Internet connection that has a private IP address</b><br> 
It is possible for the IP address being assigned by a customer�s DSL modem to match
the default settings of their router. For instance, the DSL modem might assign an 
IP address of 192.168.1.2 and their router�s LAN IP address is 192.168.1.1. This will 
not work because of routing issues. To fix this, simply change the IP schema of the router 
to something else - like 192.168.100.1 
</p>
<p class="note">
See a lead tech for further troubleshooting steps or for permission to <a href="#escalate">escalate</a>. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 

<p>
<h3><a name="no_router">If they do <u>NOT</u> have a router</a></h3>
<br>
Do the following:
<ul>
<li>Check network settings to make sure that the IP/subnet/gateway/DNS have been specified.  For most, the IP address should be a local address of 192.168.1.x (x being a number between 2 and 254).  
<li>If they have a real IP address specified, such as 66.x.x.x or 207.x.x.x, leave the settings as they are, unless they were for an old provider or business that they know longer subscribe to. 
<li>If all settings are specified correctly, and they still can�t get web pages. Then try to ping. 
</ul>
<p class="note">
<b>NOTE:</b> <b>DO NOT ASSUME</b> that IP/subnet/gateway/DNS numbers are specified 
if they show up correctly in <b>IPCONFIG</b> or in the support tab in the connection 
status window for the local area connection. Sometimes windows will pick up the right 
IP by DCHP, however this may not be a constant, so it's always best to specify.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 

<p>
<h3>Ping</h3>
<br>
Start by trying to ping the Gateway. 
<p>
When getting 4 replies (0% loss):<br />
<ol>
<li>Try to ping an outside IP to determine if the DSL connection is up and working. 
<li>If that doesn�t work, then <a href="#escalate"><u>escalate</u></a>.
<li>If that does work, try to ping a domain.  If that doesn�t work, then escalate.
<li>If that does work, but they are not pulling web pages:
  <ul>
   <li>Check to make sure nothing is checked in LAN settings (i.e. proxy settings). 
   <li>Check for firewall programs and suggest they disable/remove them.
   <li>Check for spyware programs and suggest they have them removed.
   <li>Otherwise, see lead tech for approval to refer them to the computer manufacturer.
  </ul>
</ol>
<p>
If you got no response (100% loss):<br />
<ul>
<li>Try to ping the IP address for the network card. 
<li>If that doesn�t respond, refer to manufacturer. 
<li>If it does respond, then escalate. 
</ul>
</p>
<p class="note">
This will determine if the connection from DSL to PC is secure and/or if the static IP settings(if any) are correct.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 

<p>
<h3><a name="escalate">Escalation Procedure - Create a Ticket</a></h3>
<br>
<i>The following things should be in your ticket:</i>
<ul>
<li>ALL the troubleshooting steps you took in detail.
<li>Has the DSL worked before?
<li>The Serial Number for Lucent DSL Pipes or the MAC address for Speedstream modems
<li>Customer�s Name
<li>Contact Number
<li>Alternate Contact Number (such as cell phone or work number)
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button --> 
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
