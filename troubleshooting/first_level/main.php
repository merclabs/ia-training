<h1>First Level Tips</h1>
<p>
<b>The following is a walkthrough of troubleshooting steps you should take on 
every 1st level Madison River call.  </b>
</p>
<p>
Here are things you should check before starting a ticket:
<ul>
<li>Check to see if a customer has a ticket open already. If so, just add to it, 
do not start another one. 
<li>If they have an open ticket that has already been escalated, do not just transfer 
it to 2nd level without adding an entry to the log. There has to be a record of their 
call into 1st level.  
<li>If a call has already been escalated to 3rd level, do not transfer the customer 
back to 2nd level. They do not have anymore information than 1st level. You can read 
what has been entered in to the ticket by 3rd level. Give the customer any pertinent 
information; otherwise, let them know that 3rd level will be contacting them ASAP.  
DO NOT transfer them to customer service to ask to speak to 3rd level! 
<li>If a customer calls wanting to add another computer to their service we refer
people to the business office if they attempt to add additional computers so they 
can be <u>charged</u>. 
</ul>
</p>
<p>
Before starting to troubleshoot the customerís equipment, you should ask them the 
following questions:
<ul>
<li>Has it worked before? 
<li>When did it stop working? 
<li>Did anything else happen around the time it stopped working (such as their 
phone service being cut off or a lightning storm)? 
<li>Did they make any changes to the computer right before the problem started? 
</ul>
</p>
<p>
Determine what kind of DSL modem the customer has.  Click on the modem they have 
for more info: 
<ul>
<li><a href="?content=./troubleshooting/first_level/lucent_dslpipe/main.php">Lucent DSL Pipe</a> 
<li><a href="?content=./troubleshooting/first_level/speedstream5200/main.php">Speedstream 5200</a>
<li><a href="?content=./troubleshooting/first_level/speedstream5667/main.php">Speedstream 5667</a> (this one is very rare).
</ul>
<p class="note"><b>NOTE:</b> Remember to <b>CLOSE ALL</b> 1st level TICKETS. Also, 
we support only Microsoft Outlook and Outlook express, if they ask, we do not help 
a customer wishing to use anything else, we can only give them the correct mail 
server settings. 
</p>
</p>
