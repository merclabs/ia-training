<h1>SpeedStream 5667</h1>
<p>
<table class="list_table">
<tr><td class="list_header">What lights are on?</td></tr>
<tr><td class="content_link"><a href="?content=./troubleshooting/first_level/main.php">Back to First Level Tips</a></td></tr>
<tr><td class="content_link"><a href="#1">If the ADSL light is out or blinking</a></td></tr>
<tr><td class="content_link"><a href="#2">If Power, ADSL and USB or ETH are on solid</a></td></tr>
<tr><td class="content_link"><a href="#3">If USB and ETH are both off</a></td></tr>
<tr><td class="content_link"><a href="#4">If no lights are on</a></td></tr>
</table>
</p>

<p class="note">
<b>NOTE:</b> ACT, ETH, and USB will flash when there is internet traffic flowing.  ADSL 
light should always be on solid.
</p>

<p>
<h3><a name="1">If the ADSL light is out or blinking</a></h3>
<br>
If a customer is doing a NEW INSTALL, please check when the install date is 
(and put it in your ticket). The customer can expect service 3 business days from 
the date they signed up.  If it has not been that long yet, please ask the customer 
to wait until then, and then call back if it still doesn�t work.  DO NOT escalate 
to 2nd level if you tell them that.  
</p>
<p>
If they have been called and told that it should be up by a certain time, or it 
has been more than 3 days since they got their DSL service, or it has worked before, 
then continue troubleshooting:
<ul>
 <li>Check physical connections.
 <li>Make sure filters are hooked up properly (they should have all phones going through filters).  
 <li>If the dsl line is hooked up to a filter, it must be hooked up on the dsl side (which means it isn�t really being filtered). 
 <li>Try bypassing splitters and/or surge protectors if applicable.
 <li>If dsl light is still out, then <a href="#escalate">escalate</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">If Power, ADSL and USB or ETH are on solid</a></h3>
<br>
Have the customer do the following:
</p>
<ol>
 <li>Power cycle the DSL modem.  Wait for Power, ADSL and USB or ETH lights to come back on solid.
 <li>Power cycle the router (if they have one).  Wait for lights to come back on like they were.
 <li>Restart the computer.
</ol>
</p>
Have them try the internet again:
</p>
</ul>
 <li>If they have a router <a href="#router">click here</a>. 
 <li>If they have the DSL Modem directly connected to the Computer <a href="#no_router">click here</a>.
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="3">If USB and ETH are both off</a></h3>
<br>
Do the following:
<ul>
  <li>Check the physical connection from the DSL modem to the computer (or router if there is one).  
  <li>Make sure the computer/router is turned on. 
  <li>Power cycle the DSL modem and the computer/router. 
  <li>If they are trying to connect through USB, make sure the drivers have been installed correctly. 
  <li>If they have different Ethernet/USB cable, have them try that next. 
  <li>If they are connecting through USB, have them try a different USB port in the computer. 
  <li>If none of this helps then <a href="#escalate">escalate</a>.  It could be a bad modem, bad Ethernet card/USB port or bad Ethernet/USB cable. 
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="4">If no lights are on</a></h3>
<br>
<p>
Check all physical connections (power cord(s), power strip(s), and for any light 
switches that may control the power outlet that the modem is on.).
</p>
<p>
<i>If still not working</i> <a href="#escalate">follow escalation procedure</a>.
</p>

</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="router">If they have a router</a></h3>
<br>
<p>
If they have a router, then the router needs to be setup to do PPPoE, and the 
computer should be setup to obtain everything automatically.
</p>
<ul>
 <li>See what IP address and gateway they are being assigned. 
 <li>Type in the gateway address in their browser and it should bring up the router configuration screen. 
 <li>Go to the Status screen and see if they are getting connected. 
 <li>If it says disconnected, and has an option to connect, have them hit connect. 
 <ul>
  <li>If  it won�t connect due to authentication error, have them go to setup screen and retype username/password. 
  <li>If  it is not set to PPPoE, go to the setup screen and set it to that. 
 </ul>
 <li>If they are connected with the PPPoE connection on the router, but they can�t pull web pages, have them try to <a href="#ping">ping</a>. 
</ul> 
<p class="note">
<b>See:</b> a lead tech for further troubleshooting steps or for permission to <a href="#escalate">escalate</a>. 
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

</p>

<p>
<h3><a name="no_router">If they have the DSL Modem directly connected to the Computer</a></h3>
<br>
The computer should be setup to make a PPPoE connection.  
<p>
Older versions of windows will need Tango manager, but windows XP doesn�t have 
to use it (they can make a broadband connection)
</p>
<p>
Have them try to connect:
<ul>
  <li>If they are unable to connect, make sure their Ethernet card (or USB adapter) is responding properly.  
    <p>
		<ul>
		 <li>If the Ethernet card is not responding correctly, refer them to manufacturer.   
     <li>If the USB software won�t install for them, then <a href="#escalate">escalate</a>. 
    </ul>
		</p>
	<li>If their Ethernet/USB connection is good, but they are unable to connect with 
	the PPPoE connection, put the error they are getting in the ticket and <a href="#escalate">escalate</a>. 
  <li>If they are connected with the PPPoE connection, but they can�t pull web pages, 
	have them try to <a href="#ping">ping</a>. 
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="ping">Pinging</a></h3>
<br>
Start by trying to ping the Gateway. 
<p>
<p>
If you got 4 replies (0% loss), then click here.
<ol>
 <li>Try to ping an outside IP to determine if the DSL connection is up and working. 
 <li>If that doesn�t work, then <a href="#escalate">escalate</a>.
 <li>If that does work, try to ping a domain.  If that doesn�t work, then <a href="#escalate">escalate</a>.
 <li>If that does work, but they are not pulling web pages:
   <p>
   <ul>
     <li>Check to make sure nothing is checked in LAN settings (i.e. proxy settings). 
     <li>Check for firewall programs and suggest they disable/remove them.
     <li>Check for spyware programs and suggest they have them removed.
     <li>Otherwise, see lead tech for approval to refer them to the computer manufacturer.
  </ul>
	</p>
</ol>
</p>

<p>
If you got no response (100% loss)
</p>
<p>
 <ul>
  <li>Try to ping the IP address for the network card. 
  <li>If that doesn�t respond, refer to manufacturer. 
  <li>If it does respond, then <a href="#escalate">escalate</a>. 
 </ul>
</p>
</p>
<p class="note">
This will determine if the connection from DSL to PC is secure and/or if the static IP settings(if any) are correct.
</p>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="escalate">Escalation Procedure</a></h3>
<br>
The following things should be in your ticket:
<ul>
 <li>ALL the troubleshooting steps you took in detail.
 <li>Has the DSL worked before?
 <li>The Serial Number for Lucent DSL Pipes or the MAC address for Speedstream modems
 <li>Customer�s Name
 <li>Contact Number
 <li>Alternate Contact Number (such as cell phone or work number)
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

