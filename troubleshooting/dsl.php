<h1>Basic DSL Troubleshooting</h1>
<p>
  <b>Non-DSL Specific Troubleshooting Steps</b><br>
	When troubleshooting DSL no matter what modem you are using, there are several steps that 		 should be taken to determine the problem. Follow these steps in order, and you should cover 
	everything you need for DSL. 
</p>
<p>
<ol type="A">
<li>
 <b>Current State of the Modem</b>
    <ol>
		 <li><b>Modem Type:</b> ADSL, DSL, etc - What type of connection does the customer have?
		 <li><b>Modem Name & Model</b> - Document the Modem name & model, many Modem 			    distributors have several models under a single modem name, ie. Speedstream 5200, 			    5300, 5600, 5660
		 <li><b>Other Devices:</b> Routers, Switches, Hubs, Additional PCs, etc. - Check for additional PC's on 		    the network or additional devices, this can help eliminate the faulty device. 
			<ol type="a">
			 <li>Be sure to document other device type names and model numbers.
			 <li>Provided by the ISP?
			</ol>
		 <li><b>Current state of LEDs:</b>
		  <ol type="a">
			 <li>What lights are lit, flashing (periodically or constantly), and off.
			 <li>Document this information in your log.
     </ol>
	 </ol>
<li>
 <b>PowerCycle (the ultimate fix for broadband)</b>
    <ol>
		 <li><b>Steps to follow:</b>
			<ol type="a"> 
			 <li>PowerDown PC & DSL
			 <li>Wait 1-3 minutes
			 <li>PowerOn DSL, wait for the signal light(ie. WAN) to return solid, then PowerOn PC
		  </ol>
		 <li><b>When dealing with multiple devices:</b>
		  <ol type="a"> 
			 <li>PowerDown PC, DSL, other Devices (Router, Switch, etc.)
			 <li>Wait 1-3 minutes
			 <li>PowerOn the DSL, wait for the signal light(ie. WAN) to return solid, then PowerOn 
			 the next device in line until you reach the PC
				Ex. DSL > Router > PC
		  </ol>
		 <li><b>Note:</b> <i>You do not need to powercycle all PCs when doing these steps, work with one PC
		 at a time, if the problem is corrected by this then let the customer reboot the other PCs if necessary. 
		 Also, do not let these extra devices intimidate you.</i>
   </ol>
<li>
 <b>Physical Connections</b>
    <ol>
		 <li><b>PowerUnit</b> - plugged into the correct port and secure?
		 <li><b>Phoneline from walljack to DSL</b> - plugged into the correct port and secure?
		 <li><b>Surge Protector</b> - connected, May be faulty, try bypassing.
		 <li><b>Line filters</b> - connected to the phonelines and not the DSL? 
			<ol type="a">
			 <li>2 types: phoneline only or splitter type which has an input for both DSL & Phone				b.  If the filter is on the DSL line it can cause slow connections, disconnects/dropoffs, 			     and in some cases can cause the DSL to not receive a signal. (ie. DSL light off)
			 <li>Do not remove line filters from the phone lines, these must stay.
			 <li>The splitter filters given out with the Speedstreams have 2 inputs on one side, 1 for 			     the DSL and 1 for the phone, then an output on the other side for the line to the 				     walljack. There is no need to remove this one either.
		  </ol>
		 <li><b>Ethernet/USB from DSL to PC</b> - plugged into the correct port and secure?
	  </ol>
<li>
 <b>Network</b>
    <ol>
		 <li><b>Does the network card or USB adapter respond?</b>
			<ol type="a">
			 <li>In Win9x check the winipcfg for ethernet adapter.
			 <li>In WinXP/2000 check the Local Area Connection and make sure it is enabled.
		  </ol>
		 <li><b>What IP Address is being assigned?</b>
			<ol type="a">
			 <li>If it is a 169.xxx.xxx.xxx address, communication between PC and modem/router 			     was not successful.
				<ol>
				 <li>Do not try to Release/Renew, it won't help.
				 <li>You will not be able to access web configuration page for modem/router
			  </ol>
			 <li>If it is a 192.xxx.xxx.xxx address, there is communication between PC and 					     modem/router. (unless the address was specified)
				<ol> 
				 <li>If it was assigned this address by DHCP, you should be able to access  the 				    web configuration page.
				 <li>If the address was specified, ping the gateway to determine connectivity.
			  </ol>
			 <li>If you receive a real IP address (ie. 66.xxx.xxx.xxx), then there is communication all 			     the way through. (unless the address was specified)
				<ol> 
				 <li>If still not receiving a connection try to Release/Renew the IP.
		    </ol>
			</ol>
		 <li><b>Check TCP/IP Settings</b>
			<ol type="a">
			 <li>a. If the IP|GWY|DNS are specified, and you make changes document exactly 				     what changes you made, include IP|GWY|DNS before and after.
      </ol>
	 </ol>
<li>
 <b>Ping/TraceRoute</b>
   <ol>
		<li><b>Pinging an IP or Domain</b><br>
		  <ol type="a">
			 <li>Start by trying to ping the Gateway IP, this will determine if the connection from 				     DSL to PC is secure and/or if the static IP settings(if any) are correct.
			 <li>Next, try to ping an outside IP to determine if the Broadband connection is  up 				     and working.
			 <li>Document the IP/Domain and the results:  Request Timed Out, Reply From, 				     Destination Host Unreachable.
			 <li>Remember to test both because if domain fails then IP's may work and the issue 				     could be with DNS.
		 </ol>
		<li><b>Traceroute</b> is useful when trying to determine if there is a problem reaching a particular 			     site and determining where the connection drops. (tracert xxx.xxx.xxx.xxx)
   </ol>
</ol>
</p>
