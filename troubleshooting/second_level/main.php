<h1>2nd Level Tips</h1>
 
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="#1">Gallatin River Escalations</a></td></tr>
<tr><td class="content_link"><a href="#2">Gallatin River Speedtest Sites</a></td></tr>
<tr><td class="content_link"><a href="#3">E-mail</a></td></tr>
<tr><td class="content_link"><a href="#4">Static IP</a></td></tr>
<tr><td class="content_link"><a href="#5">General Procedures</a></td></tr>
</table>
</p>

<p>
<h3><a name="1">Gallatin River Escalations</a></h3>
<br> 
<ul>
<li>The Repair bureau (I&R) takes most tickets where the customer is out of service like a flashing wan or slow speed.  We do a warm transfer to Repair (800-238-3705) for this so that they can schedule a time for a tech to come out. 
<li>ISP would take transfers for e-mail, IP addresses, and configuration type problems. 
<li>If the customer�s issue is that it is dropping off, you can call Ty and get him to check to see if their DSL modem has been dropping off 
<ul>	<li>If it has, you will escalate to repair (I&R) and warm transfer 
	<li>If it hasn�t, this is most likely a computer issue.  They would want to check computer for firewall, spy ware, and file sharing programs.  It also could be a bad Ethernet card/cable. </ul></ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Gallatin River Speedtest Sites</a></h3>
Here are the speed test sites for Gallatin. We should not use from outside the area as it will not give an accurate result. 
<ul>
<li>Dixon-  <a href="http://speedtest.dx.gallatinriver.net/">http://speedtest.dx.gallatinriver.net</a>
<li>Pekin-  <a href="http://speedtest.pk.gallatinriver.net/">http://speedtest.pk.gallatinriver.net</a> 
<li>Galesburg-  <a href="http://speedtest.gb.gallatinriver.net/">http://speedtest.gb.gallatinriver.net</a> 
</ul>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">E-mail</a></h3>
The max size of an e-mail someone can send is 10mb.
</p>
<p class="note">
The only way a customer would be able to send mail while
they are connected through another ISP is to use that ISP's
outgoing mail server settings.  We do not allow them to
authenticate and then send through our server.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="4">Static IP</a></h3>
If a customer has a static IP, and their subnet ends in 252, then their gateway is not necessarily going to end in 1.  It will be one number less than the IP.  For example, for an IP of 64.40.77.46 and a subnet of 255.255.255.252, the gateway would be 64.40.77.45. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="5">General Procedures</a></h3>
<br>
<ul>
<li>Make sure the 1st level tech followed all the Escalation Procedures, and get any information they missed. 
<li>We have one person working on open tickets on a daily basis per shift. When the others that are taking the calls are not on a call they need to check for open tickets at first level. If a ticket has been open for 48 hours and the customer has not called back please close these tickets. The lead techs can help with this at night near the end of the shift.
<li>If a ticket is blue/red, this means a call back time was assigned.  Please check what time it was assigned for, and if it is on your shift, then make sure the customer gets called back then. 
<li>If a ticket is assigned to someone in 2nd level, this does not mean that you don�t have to work on it.  All it means is that we have at least attempted to call the customer, and that it wasn�t complete enough to where we felt we could just escalate it.  It is still good to check the tickets that aren�t assigned to someone first, because those are ones where nothing has been done with it yet.
</ul>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
