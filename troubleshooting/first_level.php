<h1>First Level Tips</h1>
<p align="justify">
<b>
Those of us in 2nd level have gotten a list together of things that need to be done 
when troubleshooting dsl connections.  All of these things must be documented 
thoroughly.  This is not a list of all troubleshooting steps that need to be done,
but all these things should be done before escalating to us. They are as follows:
</b>
</p>
<p>
<i>First of all, check to see if a customer has a ticket open already.  If so, just 
add to it, do not start another one.  Also, if they have an open ticket that has 
already been escalated, do not just transfer it to us without adding an entry to 
the log.  There has to be a record of their call into 1st level.</i>
<ol>
<li><b>Please note whether or not the dsl connection has ever worked before.</b>

<ul>
<li>If a customer is doing a new install and the DSL light is not lit, or it is lit and 
still unable to connect, please check when his install date is. Grics will give 
him a time by when the DSL will be ready, typically by the end of the business day. 
Gulftel and others enforce a 3-day rule: The customer can expect service 3 days from 
the date they signed up; Signed up on Monday, service is activated Thursday. Also, 
check physical connections.  Make sure filters are hooked up properly (they should 
have all phones going through filters).
<li>If it has worked before, and WAN light is flashing or DSL light is out, then check 
physical connections and try bypassing surge protector.
<li>If DSL/WAN light is solid, then do the following:
</ul>
<li><b>POWERCYCLE DSL and ROUTER (if applicable) and REBOOT PC.</b>
<li><b>(SPEEDSTREAM modems ONLY):</b>If all the correct lights are on (specify which ones), the network card is responding, and internet still doesn�t work, then next CHECK for IP ADDRESS being assigned.  Also, make sure in their network settings that it is set to obtain ip address automatically.  Customers with speedstream modems cannot have a static ip.
<ul>
  <li>If they are getting a 169 ip address, check to make sure they aren�t trying 
	to hook up a different computer to the dsl without releasing the ip address from 
	the old machine. If this is the case, it could take 4 hours for it to auto-release 
	on our end, and then it will work.  If they are using the same computer they always 
	have been, then go ahead and escalate, because this means there is no connection. 
	Release and renew will not work, and you won�t be able to pull the config page, so 
	don�t try those things.
  <li>If they are getting a 192 ip address, do not try to release and renew � it 
	doesn�t accomplish anything.  Do try to go to the config page (This would be the 
	Gateway address).  Then follow instructions on training site.
  <li>If they are getting a real ip, like 209 or 66, try to release and renew (or 
	repair in XP).  Do NOT try going to the config page.
</ul>
<li><b>(LUCENT modems ONLY):</b> Make sure that that the correct ip address, subnet, 
gateway and DNS numbers are being specified in the network settings.  DO NOT ASSUME 
that they are if it shows that that�s what they have in the support tab in the connection 
status window for the local are connection.  Sometimes XP will pick up the right ip by 
dchp, but it doesn�t work consistently that way, so always specify. Customers can 
have a static ip with these modems, if they have it set in there, do not have them 
change it to the 192 addresses.
<br>
<li><b>(SPEEDSTREAM modems ONLY):</b> If the dsl is connected (it is getting a real ip 
and release and renew was successful, or it has a 192 ip and the config page shows 
it�s connected), and the internet still isn�t working, then this is a problem on the 
customer�s machine.  Try pinging, check for firewalls and spyware, and check to make 
sure nothing is checked in LAN settings (i.e. proxy settings).
</p>
<b>Other Important Info:</b>
</ol>
<p>
When escalating a call, ask the customer for their NAME and a CONTACT NUMBER and 
read it back to them to make sure you have gotten it correctly.  Do not just assume 
it is what automatically shows up when it brings up the customer�s info.
If a call has already been escalated to 3rd level, do not transfer the customer 
to us to find out what is going on with it.  We don�t have anymore information than 
1st level.  You can read what has been entered in in the ticket by 3rd level and give 
the customer any information in there that may be relevant, otherwise, let them know 
that 3rd level will be contacting them ASAP.
</p>