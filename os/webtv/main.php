<h1>Web TV Platform</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/main.php">Main Menu</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Always validate the <b>username</b> and <b>password</b> before sending customers to<br>
<b>1-800-GO-WEBTV</b>.
</td></tr>
</table>
</p>
<p>
<h3>Main Menu</h3>
Select <b>Setup</b> from the sidebar. This will take you to the <b>Setup</b> screen. 
</p>
<p>
<img src="./os/webtv/images/webtv1.jpg" alt="">
</p>
<p>
<h3>Setup</h3>
In <b>Setup</b>, select <b>Dialing</b>. This will take you to the <b>Dialing options</b> screen.  
</p>
<p>
<img src="./os/webtv/images/webtv2.jpg" alt="">
</p>
<p>
<h3>Dailing options</h3>
In <b>Dialing Options</b>, choose <b>Use Your ISP</b> .  
</p>
<p>
<img src="./os/webtv/images/webtv3.jpg" alt="">
</p>
<p>
<h3>Using your own ISP</h3>
This screen explains <b>Using your own ISP</b>, choose <b>Continue</b>. 
</p>
<p>
<img src="./os/webtv/images/webtv4.jpg" alt="">
</p>
<p>
<h3>Is using yourown ISP right for you?</h3>
Then next screen is explains what to expect if you choose to continue with
the <b>Using your own ISP</b> option. choose <b>Continue</b>. 
</p>
<p>
<img src="./os/webtv/images/webtv5.jpg" alt="">
</p>
<p>
<h3>Should you use WebTV to connect instead?</h3>
This screen explains the <i>pros</i> and <i>cons</i> of <b>Using your own ISP</b>, select <b>Continue</b>.  
</p>
<p>
<img src="./os/webtv/images/webtv6.jpg" alt="">
</p>
<p>
<h3>Use your own ISP</h3>
If the customer has their <b>Account Information Card</b>, have them check the 
checkbox at the top and enter the <b>required</b> account information on this screen.
Then choose <b>Done</b>.
</p>
<p>
<img src="./os/webtv/images/webtv7.jpg" alt="">
</p>
<p class="note">
Customer may have only one <b>Modem dial-in number</b>, if they have more than one number for their area 
have them enter a second number as the <b>Backup dail-in number</b>.
</p>
<p>
<h3>Reconnecting...</h3>
The <b>WebTV Internet Terminal</b> need to reconnect for setting to take effect. Choose <b>Continue</b>.
</p>
<p>
<img src="./os/webtv/images/webtv8.jpg" alt="">
</p>
<p>
<h3></h3>
The <b>Internet terminal</b> connects to the <b>WebTV Network</b>. A confirmation
screen will summarize your choice and provide you with the option to <b>Use your own 
ISP</b> or use the <b>WebTV Network</b>. Choose <b>Use your ISP</b>, this will cause 
the <b>WebTV Internet Terminal</b> to redial using your own ISP's settings.
</p>
<p>
<img src="./os/webtv/images/webtv9.jpg" alt="">
</p>
<p class="note">
If a customer has only one <b>phoneline</b>, let them go to try to connect.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
 
