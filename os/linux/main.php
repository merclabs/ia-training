<h1>Linux Platforms</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="https://www.redhat.com/docs/manuals/linux/" target="_blank">Red Hat Linux</a></td></tr>
<tr><td class="content_link"><a href="http://www.debian.org/doc/" target="_blank">Debian  Linux</a></td></tr>
<tr><td class="content_link"><a href="http://doc.mandrakelinux.com/MandrakeLinux/90c/en/EverydayApplications.html/" target="_blank">Mandrake  Linux</a></td></tr>
<tr><td class="content_link"><a href="http://www.slackware.com/book" target="_blank">Slackware  Linux</a></td></tr>
<tr><td class="content_link"><a href="http://www.yellowdoglinux.com/support/configuration/" target="_blank">Yellow Dog Linux</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Most users of Linux are should first consult the their respective Linux documentation. We mostly support
settings internet settings and not System Administration tasks such as <b><i>"Network Configurations"</i></b>, <b><i>"Firewall Setup"</i></b>, 
<b><i>"Server Configurations"</i></b>,<b><i>"Uninstalling Packages"</i></b> and any other tasks that go beyond our level of service.
Check with an <b>LT</b> if you are unsure of how far to go.

</td></tr>
</table>
</p>