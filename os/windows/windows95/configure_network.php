<h1>Configure TCP/IP</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/main.php">Windows 95</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in the <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>. Click on the <b>Network</b> icon, the <b>Network</b> window should display.
</td></tr>
</table>
</p>
<h3>Network</h3>
To configure  highlight <b>TCP/IP</b> select <b>TCP/IP -&gt; Dial-Up Adapter</b> then click
the <b>Properties</b> button.
</p>
<p>
<img src="./os/windows/windows95/images/configure_network.gif" alt="Network">
</p>
</p>
<h3>TCP/IP Properties Information</h3>
You may receive a message that you have asked to change <b>TCP/IP Properties</b> for
<b>Dail-Up Adapter</b>, click <b>OK</b>
on this message.
</p>
<p>
<img src="./os/windows/windows95/images/you_have_asked.gif" alt="TCP/IP Properties Information">
</p>
<p>
<h3>IP Address Tab</h3>
This will bring you to the <b>IP Address Tab</b>, there you should have a dot bedside
<b>Obtain an IP address automaticlly</b>.
</p>
<p>
<img src="./os/windows/windows95/images/ipaddress_tab.gif" alt="IP Address Tab">
</p>
<p class="note">
If the customer is using DSL and needs to enter an <b>IP address</b>, select <b>
Specify an IP address</b> so they can enter their <b>IP address</b> and <b>Subnet Mask</b>.
</p>
<p>
<h3>WINS Configuration Tab</h3>
On the <b>WINS Configuration Tab</b> there should be a dot beside <b>Disable WINS Resolution</b>.
</p>
<p>
<img src="./os/windows/windows95/images/winsconfiguration_tab.gif" alt="WINS Configurations Tab">
</p>
<p class="note">
If a customer is using a <b>Cable Modem</b> you may need to select <b>Use DHCP for WINS Resolutions</b>
at the bottom of the tab. 
</p>
<p>
<p>
<h3>Gateway Tab</h3>
No settings are needed on this screen unless a customer is a using <b>DSL</b> or <b>Cable Modem</b>.
</p>
<p>
<img src="./os/windows/windows95/images/gateway_tab.gif" alt="Gateway Tab">
</p>
<p class="note">
If a <b>Gateway address</b> needed, enter the gateway in the <b>New gateway</b> box and
click the <b>Add</b> button. This will put the new gateway in the <b>Installed gateways</b> box.
</p>
<p>
<h3>DNS Configuration Tab</h3>
On the <b>DNS Configurations Tab</b>, <b>Disable DNS</b> should be doted.
</p>
<p>
<img src="./os/windows/windows95/images/dnsconfiguration_tab.gif" alt="DNS Configurations Tab">
</p>
<p>
<h3>Advanced Tab</h3>
On the <b>Advanced Tab</b>, the only setting that you should be concerned about is at the bottom. 
Make sure a check is in <b>Set this protocol to be the default protocol</b>.
</p>
<p>
<img src="./os/windows/windows95/images/advanced_tab.gif" alt="Advanced Tab">
</p>
<p class="note">
<b>Memo:</b> To check this tab if the customer has a network card installed and using a dial-up
modem to connect. The network card sometimes gets set as default. 
</p>
<p>
<h3>Bindings Tab</h3>
<b>Client for Microsoft Networks</b> should be check on this screen, if <b>Client for Microsoft Networks</b> is installed.
</p>
<p>
<img src="./os/windows/windows95/images/bindings_tab.gif" alt="Bindings Tab">
</p>
<p>
<h3>NetBIOS Tab</h3>
In most cases, default the settings on this screen should be ok.
</p>
<p>
<img src="./os/windows/windows95/images/netbios_tab.gif" alt="NetBIOS Tab">
</p>
<p>
<h3>Primary Network Logon</h3>
Once you have configured the settings on all tabs, click <b>OK</b>, this should
take you back to the <b>Network Configurations Tab</b>. Here under <b>Primary
Network Logon</b>, select <b>Windows Logon</b> from the drop-down menu, then click 
<b>OK</b>. 
</p>
<p>
<img src="./os/windows/windows95/images/windows_logon.gif" alt="Windows Logon">
</p>
<p>
<h3>Restart</h3>
<b>Windows</b> should prompt you to restart, if so, click <b>Yes</b> to restart.
</p>
<p>
<img src="./os/windows/windows95/images/restart_prompt.gif" alt="Windows Logon">
</p>
<p class="note">
If you receive a <b>Copy files from</b> window, click <b>OK</b> then point to the 
correct location for <b>Cab files(c:\windows\options\cabs)</b> then click <b>OK</b>. 
After it copies files, click <b>Yes</b> to restart when prompted. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->