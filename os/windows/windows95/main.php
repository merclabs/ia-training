<h1>Windows 95</h1>
<p align="center">
<img src="./os/windows/windows95/images/splash.jpg" alt="Windows 95">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/create_dialer.php">Create a Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/configure_dialer.php">Configure Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/create_shortcut.php">Create a Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/configure_modem.php">Configure Modem</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/configure_network.php">Configure TCP/IP</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/add_components.php">Adding Network Components</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/randr.php">Remove & Reinstall Dail-Up Networking</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<h3>Cab File Information</h3>
<p>
<b>Windows 95 OSR1 (7/11/95 4.00.950)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 42KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 65KB
<li><b>Cabs:</b> 2-18<br>
<li><b>Copyright:</b> 81-95<br>
</ul>
</p>
<p>
<b>Windows 95 OSR2 (8/24/96 4.00.95B)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 42KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 65KB
<li><b>Cabs:</b> 2-28<br>
<li><b>Copyright:</b> 81-96<br>
</ul>
</p>
<p>
<b>Windows 95 OSR2.5 (5/1/97 4.00.95C)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 42KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 65KB
<li><b>Cabs:</b> 2-28<br>
<li><b>Copyright:</b> 81-97<br>
</ul>
</p>
<p>
<b>Windows 95 with Winsock 2 update (03/20/98)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 22KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 21KB
</ul>
</p>
<p>
<b>Location:</b> c:\windows\system</br>
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->







 




