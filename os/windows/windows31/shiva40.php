<h1>Shiva Dialer 4.0</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/main.php">Windows 3.1</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/shiva30.php">Shiva Dialer 3.0</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/trumpetwinsock.php">Trumpet Winsock</a></td></tr>
</td></tr>
</table>
</p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If the Internet Toolkit has been successfully installed, you should have a Internet 
Toolkit folder like the one below.
</td></tr>
</table>

<p>
<h3>Prgram Folder</h3>
Double click on the icon like the one titled <b>Dialer</b>, and you
should get a window like the one below.
</p>
<p>
<img src="./os/windows/windows31/images/40_program_folder.gif" alt="Program Folder"><br>
</p>
<p>
<h3>Dialer Prompt</h3>
If your software has been properly
configured, your account name/user id should be found in the <b>User Name</b> field.
Similarly, your password, should be present, although starred out in the <b>Password</b>
field. The <b>User Name</b> and <b>Password</b> fields are the only fields in this window
for which you normally should adjust the contents (if necessary).
</p>
<p>
<img src="./os/windows/windows31/images/40_dialer_prompt.gif" alt="Dailer Prompt"></p>
</p>
<p>
Click on the <b>Properties</b> button to see the window below.
</p>
<p>
(<i><b>Please note</b></i>: ShivaRemote 4 will often open to the last <b>Properties</b> tab accessed,
and some tabs may not be immediately visible. If you cannot see the tab you need, try
clicking on the left and right arrow buttons in the top right hand corner of the <b>ShivaRemote
Properties</b> window.)
</p>
<p>
<h3>Phone Number Tab</h3>
On the <b>Phone #</b> tab, you can make adjustments with regards to your dialing preferences. If you wish
to change the number to be dialed, you may specify a new number in the <b>Number to Dial</b>
field. If you have call waiting, you may either manually insert <i>70#,</i> in front of
the phone number in the <b>Number to Dial</b> field, or you may choose to activate the <b>Build
Number to Dial</b> option. If you choose the latter, click on the white box next to <b>Disable
call waiting with:</b> to put a check mark in the box, and then select <b>70#,</b> from
the drop down menu to the right.
</p>
<p>
If you are calling from an office, hotel, etc., where you need to enter a
number or code to access an outside line, you also may manually insert the the required
number in the <b>Number to Dial</b> field. If you choose to activate the <b>Build Number
to Dial</b> option, you may click on the white box next <b>Access outside line with:</b>
and select the appropriate number from the drop down menu to the right (<i>before using a
line in an office or hotel, always confirm whether it is an analogue or digital line,
since using a digital line with a standard analogue modem will likely damage the modem</i>).</p>
<p>
<img src="./os/windows/windows31/images/40_properties_phonenumber_tab.gif" alt="Phone Number">
</p>
<p align="center">
Click on the <b>Modem</b> tab to see the window below.
</p>
<p>
<h3>Modem Tab</h3>
If you need to add a modem for use with the <b>Dialer</b>,
then click on the <b>New...</b> button, to go to the <b>Modem Wizard</b>.
</p>
<p>
<i><i><b>Please note:</b></i> The version 4 software can only detect modems
for which it already has records, and it only has records for up to 56 kbps modems
available at the time the dialer was released. This does not mean you cannot use an
unlisted 56 kbps modem, but you may have difficulties doing so and/or have to make manual
modifications to the initialization string entry which can be modified by clicking on the <b>Advanced</b>
button. Although entries for 14.4 kbps modems can be found in the version 4 records, such
modems are no longer offically supported. If you have a 14.4 kbps modem and you can
successfully use it, you are welcome to do so, but no technical support is available for
14.4 kbps modems if they do not work.
</i>
</p>
<p>
The other settings normally are fine as found, but you can adjust the <b>Port</b>
depending on which port your modem is installed on. You can check on some settings
relating to the port and/or manually specify the initialization string to be used by
clicking on the <b>Advanced</b> button. As well, the maximum speed can be adjusted to
reflect optimal port speed for communication via your modem.</p>
</p>
<p>
<img src="./os/windows/windows31/images/40_properties_modem_tab.gif" alt="Modem"></p>
</p>
<p align="center">
Click on the <b>Service Provider</b> tab to see the window below.
</p>
<p>
<h3>Service Provider Tab</h3>
The <b>Primary DNS Server</b> and <b>Secondary DNS Server</b>
fields should contain the infoave.net entries of <b>206.74.254.2</b> and <b>204.116.57.2</b>.
In the <b>Domain Name</b> field, you should make sure it contains <b>Your Provider</b>.</p>
</p>
<p>
<img src="./os/windows/windows31/images/40_properties_serviceprovider_tab.gif" alt="Service Provider">
</p>
<p align="center">
Click on the <b>Protocols</b> tab to see the window below.</p>
</p>
<p>
<h3>Protocols Tab</h3>
No settings should need to be changed on this tab, if they are as shown above.</p>
</p>
<p>
<img src="./os/windows/windows31/images/40_properties_protocols_tab.gif" alt="Protocols"></p>
</p>
<p align="center">
Click on the <b>Dialing</b> tab to see the window below.
</p>
<p>
<h3>Dailing Tab</h3>
If you wish to have the <b>Dialer</b> start automatically
when you double click on the Netscape Communicator or Navigator icon, then leave <b>Dial
on Demand</b> enabled. The redial setting is optional, and is not generally required. All
other settings can be left as they are found.
</p>
<p>
<img src="./os/windows/windows31/images/40_properties_dailing_tab.gif" alt="Dailing">
</p>
<p align="center">
Click on the <b>Login</b> tab to see the window below.
</p>
<p>
<h3>Login Tab</h3>
You normally do <u>not</u> need to adjust any of the <b>Login</b>
options, and normally this window should be set to <b>Standard login</b>.
</p>
<p>
<img src="./os/windows/windows31/images/40_properties_login_tab.gif" alt="Login">
</p>
<p align="center">
Click on the <b>Connection</b> tab to see the window below.
</p>
<p>
<h3>Connection Tab</h3>
<p>
<img src="./os/windows/windows31/images/40_properties_connection tab.gif" alt="Connection">
</p>
<p>
If you want a particular Internet
application (e.g.- a web browser) to automatically start when you begin your connection by
clicking on the Dialer's <b>Connect</b> button, you will want to click on the white box
next to <b>Automatically start</b> to place an <b>X</b> in it. You will then need to click
on the <b>Browse...</b> button to select the .exe file for the Internet application you
wish to have start. It is recommended you leave the automatic <b>Disconnect after</b>
option selected, but you are free to adjust the time period before this setting kicks in.
</p>