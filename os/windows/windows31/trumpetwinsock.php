<h1>Trumpet Winsock</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/main.php">Windows 3.1</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/shiva30.php">Shiva Dialer 3.0</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/shiva40.php">Shiva Dialer 4.0</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Trumpet Winsock</b> is a program used to connect a machines running <b>Windows</b> to the Internet. 
Trumpet Winsock uses <b>PPP</b>(<i>Point to Point Protocol</i>), which is what makes it possible 
for graphical Internet programs to communicate with the Internet. 
In most case if a customer is running <b>Windows 3.1</b>, they need to have <b>Trumpet Winsock</b>
installed to connect to the Internet. <a href="http://www.trumpet.com.au/products.html" target="_blank">Learn more...</a> 
</p>
</td></tr>
</table>
</p>

<p>
<h3 align="center">Setup Screen</h3>
Pull down the <strong>File</strong> menu and choose <strong>Setup</strong>.
<p align="center">
<img src="./os/windows/windows31/images/trumpet_winsock.jpg" alt="Trumpet Winsock Screen" border="0">
</p>
<ul>
<li>Make sure the <strong>IP Address </strong>is <b>0.0.0.0</b> and if it is anything else, set it back to 0s.
<p><b>NOTE</b>: This number is the <b>IP address</b> you get assigned when online, and so each time you dial in, it
is a different number, which then resets back to 0s when you log
out. If you open Trumpet <strong>while online </strong>the IP address field will &quot;hold&quot; the current address, which is
a <b>bad</b> thing, because then next time you log in, Trumpet will try to
find that exact IP address instead of taking the first available
one. So make sure this is set back to <b>0.0.0.0 if it is anything else</b>.</p>
<li>Sometimes we change the <strong>MTU, TCP RWIN and TCP MSS </strong>numbers. Below are the settings we change them to. (This can greatly
help when pages on the web are not loading completely, since this
makes the packets of informaton smaller)
Some people may have a setting of 1460 rather than 1450 for the MSS. Change that number back to 1450 if this is the case.
</ul>
</p>
<p>
As a rule of thumb, 28.8 modems should get the first settings,problem
14.4 modems should get the 2nd set of numbers below. Real probl
cases should use the lowest set of numbers.
</p>
<p align="center">
<table border="0" cellpadding="5" cellspacing="5">
<tbody>
<tr bgcolor="#dedede">
<td><b>MTU</b></td>
<td><b>TCP RWIN</b></td>
<td><b>TCP MSS</b></td>
</tr>
<tr bgcolor="#dedede">
<td>1490</td>
<td>4096</td>
<td>1450</td>
</tr>
<tr bgcolor="#dedede">
<td>1006</td>
<td>2048</td>
<td>966</td>
</tr>
<tr bgcolor="#dedede">
<td>552</td>
<td>2048</td>
<td>512</td>
</tr>
</tbody>
</table>
</p>
<p>
<ul>
<li>Almost all of our users should have <b>PPP</b> protocol and have <strong>Internal PPP</strong> checked here.
<li><strong>Slip port</strong> should be the Com port the modem is on.
<li><strong>Baud rate</strong> should be correct for the modem speed.
<li><strong>Hardware Handshaking </strong>and <strong>V-J Compression </strong>should be checked for all modems EXCEPT for 2400 modems. A 2400
baud modem does not support hardware Flow Control.
<li><strong>Online status Detect</strong> is always <b>None</b>
<li>Close the <b>Setup</b> window. <strong>NOTE:</strong> You will need to close Trumpet and open it again for any changed
settings to take effect.
</ul>
</p>

<p>
<h3>Trace Menu</h3>
<b>Nothing</b> should be checked in the Trace Menu. If anything is checked, <b>uncheck</b> it. 
 <p align="center">
  <img src="./os/windows/windows31/images/trace_window.gif" alt="Trace Window" border="0">
 </p> 
</p>
<p>
<h3>Dialer Menu</h3>
Finally, under the <strong>Dialler menu,</strong> click on <strong>1.Setup.cmd</strong>. There are 4 fields that come up, in this order: 
 <p align="center">
  <img src="./os/windows/windows31/images/cmd_phone.jpg" alt="Phone Number" border="0">
 </p>
</p>

<p>
<b>Phone number</b> is the number to dial into Teleport.
 <p align="center">
  <img src="./os/windows/windows31/images/cmd_name.jpg" alt="User Name" border="0">
 </p>
</p>

<p>
<b>User Name</b> is your user name, in lower case.
 <p align="center">
  <img src="./os/windows/windows31/images/cmd_pwd.jpg" alt="Password" border="0">
 </p>
</p>

<p>
<b>Password,</b> your password should be entered here. There should be 8 *s there.</A>.
 <p align="center">
  <img src="./os/windows/windows31/images/cmd_init.jpg" alt="Modem" border="0">
 </p>
</p>

<br> 