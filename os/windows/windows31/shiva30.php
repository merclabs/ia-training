<h1>Shiva Dialer 3.0</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/main.php">Windows 3.1</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/shiva40.php">Shiva Dialer 4.0</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows31/trumpetwinsock.php">Trumpet Winsock</a></td></tr>
</td></tr>
</table>
</p>

<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If the Netscape Personal edition software has been successfully installed, and the
Account Setup Wizard already run, whether in regular mode or manual mode, you should 
have a Info Avenue program folder like the one below.
</td></tr>
</table>
<p>
<h3>Program Folder</h3>
Double click on the icon titled <b>Info Avenue Internet Dialer </b>, and you should
get a window like the one below.
</p>	 
<p>
 <img src="./os/windows/windows31/images/program_folder.gif" alt="Program Folder">		
</p>	
<p>
<h3>Dialer</h3>
If your software has been properly configured, your account name/user id should be
found in the <b>User Name</b> field. Similarly, your
password, should be present, although starred out in the <b>Password</b> field. The <b>User
Name</b> and <b>Password</b> fields are the only fields in this window for which you
normally should adjust the contents (if necessary). Regarding the <b>Current Location</b>
field, you can choose a different location (that would appear in the drop down menu) once
you have configured more than one location in the <b>Properties</b>.
</p>
<p>
<img src="./os/windows/windows31/images/dialer_prompt.gif" alt="Dialer Prompt">
</p>
<p align="center">
Click on the <b>Properties</b> button to see the window below.
</p>
<p>
<h3>Location Tab</h3>
On the <b>Location</b> tab, you can make adjustments
with regards to your dialing preferences. If you wish to change the number to be dialed,
you may specify a new number in the <b>Provider phone number</b> field. If you have call
waiting, click on the white box next to <b>disable call waiting</b> to put a check mark in
the box, and then select <b>70#,</b> from the drop down menu to the right.
</p>
<p>
If you are calling from an office, hotel, etc., where you need to enter
a number or code to access an outside line, enter the required number in the appropriate
box (<i>before using a line in an office or hotel, always confirm whether it is an
analogue or digital line, since using a digital line with a standard analogue modem will
likely damage the modem</i>). Most phone lines are now tone lines, so this setting should
normally be fine as the default.
</p>
<p>
If you are using a portable computer, and wish to specify different
calling requirements (e.g., call waiting, outside line, number to be dialed) depending on
where you are, you may do so by clicking on the <b>New</b> button and defining a new
location.
</p>
<p>
<img src="./os/windows/windows31/images/properties_location_tab.gif" alt="Location Tab">
</p>
<p align="center">
Click on the <b>General</b> tab to see the window below.
</p>
<p>
<h3>General Tab</h3>
The <b>General</b> tab is shown with its default settings. If you wish to have the <b>Dialer</b> start
automatically when you double click on the Netscape Navigator icon, then leave <b>Dial on
Demand</b> selected. Similarly, if you want Netscape Navigator to automatically start when
you begin your connection by clicking on the Dialer's <b>Dial</b> button, leave <b>Automatically
start Netscape Navigator</b> selected.
</p>
<p>
If you deselect <b>Minimize connect-time window</b>, a small window
showing that you are connected will be visible when you are connected. And, it is
recommended you leave the automatic <b>Disconnect after</b> option selected, but you are
free to adjust the time period before this setting kicks in.
</p>
<p>
You normally do <u>not</u> need to select any of the <b>Logging in</b> options.
</p>
<p>
<img src="./os/windows/windows31/images/properties_general_tab.gif" alt="General Tab">
</p> 
<p align="center">
Click on the <b>Modem</b> tab to see the window below.
</p>
<p>
<h3>Modem Tab</h3>
If you wish to change the modem selected for use with
the <b>Dialer</b>, then click on the <b>Change Modem...</b> button, to go to the Modem Wizard.
</p>
<p>
<i><b>Please note:</b></i> The version 3 software can only detect modems
for which it already has records, and it only has records for up to 33.6 kbps modems. This
does not mean you cannot use a 56 kbps modem, but you may have difficulties doing so
and/or have to make manual modifications to the initialization string entry which can be
modified by clicking on the <b>Advanced</b> button.
</p>
<p>
Although entries for 14.4 kbps modems can be found in the version 3 records, such
modems are no longer offically supported. If you have a 14.4 kbps modem and you can
successfully use it, you are welcome to do so, but no technical support is available for
14.4 kbps modems if they do not work.
</p>
<p>
The other settings normally are fine as found, but you can adjust the <b>Port</b>
depending on which port your modem is installed on. You can check on some settings
relating to the port and/or manually specify the initialization string to be used by
clicking on the <b>Advanced</b> button. As well, the
maximum speed can be adjusted to reflect optimal port speed for communication via your modem.
</p>
<p>
<img src="./os/windows/windows31/images/properties_modem_tab.gif" alt="General Tab">
</p>
<p align="center">
Click on the <b>Service Provider</b> tab to see the window below.
</p>
<p>
<h3>Service Provider Tab</h3>
The <b>Primary DNS Server</b> and <b>Secondary DNS
Server</b> fields should contain the entries of <b>206.74.254.2</b> and <b>204.116.57.2</b>.
In the <b>Domain Name</b> field, you should make sure it contains <b>Your Provider </b>.
</p>
<p>
<img src="./os/windows/windows31/images/properties_serviceprovider_tab.gif" alt="General Tab">
</p>
<p align="center">
The <b>About</b> tab does not contain any settings.
</p>