<h1>Create Shortcut</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Control Panel</b> then click on
<b>Network Connections</b>.  
</td></tr>
</table>
</p>
<p>
<h3>Network Connections</h3>
Once inside of <b>Network Connections</b>, located the dialer you wish to create a shortcut of. 
</p>
<p>
<img src="./os/windows/windowsxp/images/network_connections_dial_up.gif" alt="Network Connections -  Created Shortcut">
</p>
<p class="note">
In most cases when you create a new connection, if you check <b>Add a shortcut to this connection to my desktop</b>
a shortcut will be placed on the desktop for you after clicking <b>Finish</b>.
<br><br>
<img src="./os/windows/windowsxp/images/ncw_8.gif" alt="">
<br>
</p>
<p>
<h3>Network Connections - Create Shortcut</h3>
Right click on the icon of the dialer, a pop-up menu should display. Select <b>Create Shortcut</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/create_shortcut.gif" alt="Network Connections -  Created Shortcut">
</p>
<p>
<h3>Desktop Shortcut - Shortcut Created on Desktop</h3>
This will place a shortcut icon on the desktop. 
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_shortcut_icon.gif" alt="Shortcut Icon">
</p>
<p class="note">
This shortcut can be used to both connect and disconnect.
</p>
<p>
<h3>Desktop Shortcut - Connect to...</h3>
When a customer clicks on this icon they should see a
window like below. Clicking the <b>Dail</b> button will start the connection process.
</p>
<p>
<img src="./os/windows/windowsxp/images/connect_to.gif" alt="Connect to...">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

