<h1>Switching to Classic View</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
In most cases you want to switch the <b>Control Panel</b> view to <b>Classic View</b>
because it is easier to navigate, provides a common interface that we all use and is
easier for new users to understand. 
</td></tr>
</table>
</p>
<p>
<h3>Control Panel</h3>
If you are not already in <b>Control Panel</b>, click <b>Start</b> then <b>Control Panel</b>. 
</p>
<p>
<img src="./os/windows/windowsxp/images/start_control_panel.gif" alt="Control Panel - Category View">
</p>
<p>
<h3>Control Panel - Category View</h3>
If <b>Windows XP</b> is in <b>Category View</b>, you should see a link that says <b>Switch to Classic View</b>
on the left menu. If so, have the customer double click on <b>Switch to Classic View</b> to change
the <b>Look and Feel</b> of the <b>Control Panel</b> window.
</p>
<p>
<img src="./os/windows/windowsxp/images/control_panel_category_view.gif" alt="Control Panel - Category View">
</p>
<h3>Control Panel - Classic View</h3>
Once you are in <b>Classic View</b> this link should now read <b>Switch to Category View</b> and the
<b>Control Panel</b> window should not dispaly <b>icons</b> instead of <b>categories</b>. 
</p>
<p>
<img src="./os/windows/windowsxp/images/control_panel_classic_view.gif" alt="Control Panel - Classic View">
</p>
<p class="note">
If the customer has called before and the previous tech has already switched the <b>Control Panel's</b> view to
<b>Classic View</b>, then it should remain in that view untill its changed by you or the customer. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->