<h1>Windows XP</h1>
<p align="center">
<img src="./os/windows/windowsxp/images/splash.jpg" alt="Windows XP">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/switch_to_classic_view.php">Switching to Classic View</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/create_connection.php">Create New Connection</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/configure_connection.php">Configure a Connection</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/create_shortcut.php">Create a Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/configure_network_settings.php">Configure Network Settings</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/configure_modem.php">Configure Modem</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Windows XP</b>
<ul>
<li><b>File:</b> Winsock.dll <b>Size:</b> 3KB - (8/23/2001) 
<li><b>File:</b> Wsock32.dll <b>Size:</b> 21KB - (8/23/2001) 
</ul>
</p>
<p>
<b>Location:</b> c:\windows\system32
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->