<h1>Create a New Connection</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Control Panel</b>. 
</td></tr>
</table>
</p>
<p>
<h3>Control Panel - Classic View</h3>
Then click on the <b>Network Connections</b> icon. 
</p>
<p>
<img src="./os/windows/windowsxp/images/control_panel_classic_view.gif" alt="Control Panel - Classic View">
</p>
<p>
<h3>Network Connections - No Dialer</h3>
The <b>Network Connections</b> window should display with only a <b>Local Area Connection</b> icon,
if no previous connections have been created. To created a <u>new</u> connection click on the
<b>Create New Connection</b> link on the left. 
</p>
<p>
<img src="./os/windows/windowsxp/images/network_connections_classic_view.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Start</h3>
This should start the <b>New Connection Wizard</b>. Click <b>Next</b> to continue...
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_1.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Network COnnection Type</h3>
On the this screen select <b>Connect to the Internet</b>, then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_2.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Getting Ready</h3>
Then select <b>Setup my connection manually</b>, then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_3.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Internet Connection</h3>
Then select <b>Connect using a dial-up modem</b>, then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_4.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Connection Name</h3>
This screen prompts you for a descriptive name to give your new connection. You can
name a connection anything you want, for <b>example:</b> <i>Hargray, Horry, My Connection 
or Blomand</i>. After entering a name, click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_5.gif" alt="Network Connections">
</p>
<p>
<h3>New Connection Wizard - Phone Number to Dial</h3>
Next enter the <b>Local Modem Access Number</b> this information should be on the 
<b>Account Information Card</b> that the customer received from the <b>Provider</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_6.gif" alt="Network Connections">
</p>
<p class="note">
If the customer does not have their <b>Account Information Card</b> or their <b>Local
Modem Access Number</b>. Notify an <b>LT</b> to get permission to send them to customer
service to get that information. Then click <b>Next</b>.
</p>
<p>
<h3>New Connection Wizard - Internet Account Information</h3>
Enter the <b>User Name</b> and <b>Password</b> that was issued by the <b>Provider</b> and
uncheck <b>Turn on Internet Connection Firewall for this connection</b>. Then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_7.gif" alt="Network Connections">
</p>
<p>
<h3>Completing the New connection Wizard</h3>
This completes the <b>New Connection Wizard</b>, put a check beside <b>Add a shortcut to this 
connection to my desktop</b>, then click <b>Finish</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/ncw_8.gif" alt="Completing the New Connection Wizard">
</p>
<p>
<h3>Shortcut on Desktop</h3>
After clicking <b>Finish</b>, you should have an icon on the <b>Desktop</b> to connect.
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_shortcut_icon.gif" alt="My Connection icon">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->