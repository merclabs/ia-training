<h1>Configure Network Settings</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Network Connections</b>, click on <b>Start</b>, <b>Control Panel</b> then 
<b>Network Connections</b>.  
</td></tr>
</table>
</p>
<p>
<h3>Network Connections - New Dialer</h3>
The <b>Network Connection</b> window should display.
</p>
<p>
<img src="./os/windows/windowsxp/images/network_connections_dial_up.gif" alt="Network Connections -  Dial up">
</p>
<p>
<h3>My Connection Properties</h3>
Right click on the new connection and select <b>Properties</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_properties.gif" alt="New Connection Properties">
</p>
<p>
<h3>My Connection - General Tab</h3>
The <b>General Tab</b> is the default tab to display. 
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_general_tab.gif" alt="New Connection Properties - General">
</p>
<p>
<h3>My Connection - Networking Tab</h3>
Click on the <b>Networking Tab</b>. The <b>type of dail-up server</b> should be set to <b>PPP:Windows 95/98/NT 4/2000,Internet</b>.
Both <b>Internet Protocol</b> and <b>QoS Packet Scheduler</b> should be checked. 
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_network_tab.gif" alt="Networking Tab">
</p>
<p>
<h3>Advanced TCP/IP Settings - Internet Protocol (TCP/IP) Properties</h3>
For additional settings select <b>TCP/IP</b> and click the <b>Properties</b> button. A <b>General Tab</b>
should display which allows you to specify an <b>IP Address</b> and <b>DNS Numbers</b>. In most cases these
settings are not needed. 	
</p>
<p>
<img src="./os/windows/windowsxp/images/internet_protocol_tcpip.gif" alt="Internet Protocol Properties">
</p>
<p>
<h3>Advanced TCP/IP Settings - General Tab</h3>
For additional <b>TCP/IP</b> settings, click the <b>Advanced</b> button, an <b>Advanced TCP/IP Settings</b> window
should display. The two check boxes on the <b>General Tab</b> should be check. 
</p>
<p>
<img src="./os/windows/windowsxp/images/advanced_tcpip_general_tab.gif" alt="General Tab">
</p>
<p>
<h3>Advanced TCP/IP Settings - DNS Tab</h3>
Click on the <b>DNS Tab</b>, this screen allows you to enter <b>DNS Servers Addresses</b>. In most cases DNS Number are not
needed.
</p>
<p>
<img src="./os/windows/windowsxp/images/advanced_tcpip_dns_tab.gif" alt="DNS Tab">
</p>
<p>
<h3>Advanced TCP/IP Settings - Add DNS Server...</h3>
But if <b>DNS Numbers</b> are needed, click on the <b>Add</b> button to display the <b>TCP/IP DNS Server</b>
window to enter <b>DNS Numbers</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/add_dns_server.gif" alt="Add DNS Server...">
</p>
<p class="note">
After entering <b>DNS Numbers</b>, click the <b>Add</b> button. This should add the <b>DNS Numbers</b>
you entered to the <b>DNS Server</b> window.
</p>
<p>
<h3>Advanced TCP/IP Settings - WINS Tab</h3>
The <b>WINS Tab</b> allows you to enter <b>WINS Server Addresses</b>, click on the <b>Add</b> button to display the
<b>TCP/IP WINS Server</b> window.
</p>
<p>
<img src="./os/windows/windowsxp/images/advanced_tcpip_wins_tab.gif" alt="WINS Tab">
</p>

<p>
<h3>Advanced TCP/IP Settings - Add WINS Address</h3>
In most cases <b>WINS Addresses</b> are not needed, but if they are, click the <b>Add</b> button and enter them in the
<b>TCP/IP WINS Server</b> window, click the <b>Add</b> to commit this setting.
</p>
<p>
<img src="./os/windows/windowsxp/images/add_wins.gif" alt="Add WINS Address">
</p>
<p class="note">
After entering <b>WINS Addresses</b>, click the <b>Add</b> button. This should add the <b>WINS Addresses</b>
you entered to the <b>WINS Server</b> window.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->