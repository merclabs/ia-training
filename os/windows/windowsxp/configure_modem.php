<h1>Configure Modem</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Phone and Modem Options</b>, click on <b>Start</b>, <b>Control Panel</b>.  
</td>
</tr>
</table>
</p>
<p>
<h3>Control Panel - Phone and Modem Options</h3>
Once in <b>Control Panel</b> click on <b>Phone and Modem Options</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/control_panel_classic_view.gif" alt="Control Panel">
</p>
<p>
<h3>Phone and Modem Options Window</h3>
The <b>Phone and Modem Options</b> window should display. The default tab to display is the 
<b>Dailing Rules</b> tab. This tab displays the location settings for your modem, in most cases
there should be only one location.
</p>
<p>
<img src="./os/windows/windowsxp/images/phone_modem_dialing_rules_tab.gif" alt="Dailing Rules">
</p>
<p>
<h3>Modems Tab</h3>
The second tab on this window is the <b>Modems Tab</b>, this tab displays the current modem(s) installed on
this PC. To configure setting for the modem installed, select it, then click the <b>Properties</b> button.
</p>
<p>
<img src="./os/windows/windowsxp/images/phone_modem_modem_tab.gif" alt="Modems Tab(Phone and Modems Options)">
</p>

<p>
<h3>Modem Properties - General Tab</h3>
Another <b>Modem Properties</b> window should display. The <b>General Tab</b> is the the default tab to show, this
tab displays general information about the modem installed and shows if it is working properly.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_general_tab.gif" alt="General Tab">
</p>
<p class="note">
The <b>Device Status</b> displays the current working status of the modem, if the modem is
not communicating with the computer properly, its status should display here. If the modem is having 
driver issues, then the status may still indicate that the modem is working properly. In this case
you must run diagnostics to determin if the modem is indeed working properly.
</p>
<p>
<h3>Modem Properties - Modem Tab</h3>
The <b>Modems Tab</b> displays the modems <b>speaker volume</b>, <b>maximum port speed</b>, and <b>dial control</b>. The only setting
that may need to be changed is the <b>Maximum Port Speed</b>. Set the <b>Maximum Port Speed</b> at or below the max speed of 
the modem. All other settings should remain unchanged.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_modem_tab.gif" alt="Modems Tab">
</p>
<p>
<h3>Modem Properties - Diagnostics Tab</h3>
The <b>Diagnostics Tab</b> allows you to run diagnostics on the modem installed. To run diagnostics
click the <b>Query Modem</b> button. If the modem communicating with the computer properly, you should 
see response information in the <b>Command</b> and <b>Response</b> section.  
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_diagnostic_tab.gif" alt="Diagnostic Tab">
</p>
<p>
<h3>Modem Properties - Modem Error</h3>
If the modem is not working properly, <b>Windows</b> will display an <b>Error</b> message indicating that the modem failed to respond.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_error.gif" alt="Response Error">
</p>
<p>
<h3>Modem Properties - Advanced Tab</h3>
The <b>Advanced Tab</b> allows you to enter an <a href="http://www.modemhelp.org/inits/" target="_blank">init string</a>, if one is needed. To change additional settings
click the <b>Change Default Preferences</b> button.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_advanced_tab.gif" alt="">
</p>
<p class="note">
Make sure you line test the modem to see if it responds after adding an init string. 
</p>
<p>
<h3>Modem Default Preferences - General Tab</h3>
The <b>General Tab</b> allows you to change default general settings, in most cases these settings should remain
unchanged.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_advanced_general_tab.gif" alt="">
</p>
<p>
<h3>Modem Default Preferences - Advanceced Tab</h3>
The <b>Advanced Tab</b> allows you to change default advanced settings, in most cases these settings should remain
unchanged.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_advanced_advanced_tab.gif" alt="">
</p>
<p>
<h3>Modem Properties - Driver Tab</h3>
The <b>Driver Tab</b> displays the informatin about the driver maker and the provides buttons to 
update, rollback or uninstall the modem driver.
</p>
<p>
<img src="./os/windows/windowsxp/images/modem_driver_tab.gif" alt="">
</p>
<p class="note">
This is the last tab on this widnow, clicking <b>OK</b> will take you back to the <b>Phone and Modems Options</b>
window. 
</p>
<p>
<h3>Advanced Tab</h3>
The <b>Advanced Tab</b> is the last tab on the <b>Phone and Modems Options</b> window, no settings or changes are needed here unless
otherwise stated by an <b>LT</b>.
</p>
<p>
<img src="./os/windows/windowsxp/images/phone_modem_advanced_tab.gif" alt="">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->