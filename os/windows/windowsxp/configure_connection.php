<h1>Configure a Connection</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsxp/main.php">Windows XP</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Control Panel</b> then click on
<b>Network Connections</b>.  
</td></tr>
</table>
</p>
<p>
<h3>Control Panel - Classic View</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/control_panel_classic_view.gif" alt="Control Panel - Classic View">
</p>
<p>
<h3>Network Connections - New Dialer</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/network_connections_dial_up.gif" alt="Network Connections -  Dial up">
</p>
<p>
<h3>My Connection Properties</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_properties.gif" alt="New Connection Properties">
</p>
<p>
<h3>My Connection - General Tab</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_general_tab.gif" alt="New Connection Properties - General">
</p>
<p>
<h3>My Connection - General Tab/Configure Button</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/configure_button_modem_configuration.gif" alt="New Connection Properties - General">
</p>
<p>
<h3>My Connection - General Tab/Alternate Button</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/alternat_number.gif" alt="New Connection Properties - General">
</p>
<p>
<h3>My Connection - Options Tab</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_options_tab.gif" alt="New Connection Properties - Options">
</p>
<p>
<h3>My Connection - X.25 Logon Settings</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/x25_window.gif" alt="X.25 Window">
</p>
<p>
<h3>My Connection - Security Tab</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_security_tab.gif" alt="Security Tab">
</p>
<p>
<h3>My Connection - Networking Tab</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_network_tab.gif" alt="Networking Tab">
</p>
<h3>My Connection - Networking Tab/Settings Button</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/internet_protocol_tcpip.gif" alt="Settings Button">
</p>
<p>
<h3>My Connection - Advanced Tab</h3>
</p>
<p>
<img src="./os/windows/windowsxp/images/my_connection_advanced_tab.gif" alt="Advanced Tab">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->