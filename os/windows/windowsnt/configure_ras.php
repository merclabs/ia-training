<h1>Configure RAS</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Network Icon</b>. 
<br>
<img src="./os/windows/windowsnt/images/network_icon.gif" alt="Network Icon">
</td></tr>
</table>
</p>
<p>
<h3>Network - Services Tab</h3>
Then click on the <b>Services Tab</b>. If <b>Remote Access Service</b> is listed, click on it once so its highlighted.
Then click on the <b>Properties...</b> button. 
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_services_selected.gif" alt="Network Service">
</p>
<p>
<h3>Remote Access Setup</h3>
Click on the <b>Network</b> button.
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_setup2.gif" alt="">
</p>
<p>
<h3>Remote Configurations</h3>
Only <b>TCP/IP</b> should be checked, click <b>OK</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/network_configuration.gif" alt="">
</p>
<p>
<h3>Remote Access Setup</h3>
Click on the <b>Configure</b> buton.
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_setup.gif" alt="">
</p>
<p>
<h3>Configure Port Usage</h3>
Make sure that you select <b>Dial out only</b>, then click <b>OK</b>. 
</p>
<p>
<img src="./os/windows/windowsnt/images/configure_port_usage.gif" alt="">
</p>
<p>
<h3>Connect to...</h3>
Type the <b>User Name</b> and <b>Password</b>, the <b>Domain</b> box should be 
lefted blank. Click <b>OK</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/connect_to.gif" alt="">
</p>
<p>
This completes configuring <b>RAS</b>. 
</p>
<p>
<b>Next see:</b> <a href="?content=./os/windows/windowsnt/install_ras.php">Reinstall RAS</a>.
</p>