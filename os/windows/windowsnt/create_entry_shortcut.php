<h1>Create Phonebook Entry</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Network and Internet Connections</b> icon. 
</td></tr>
</table>
</p>
<p>
<h3>Create New Entry</h3>
To create a shortcut to a <b>Phonebook Entry</b>, click on the <b>More</b> button,
then select <b>Create shortcut to entry...</b>. This should place a shortcut icon to 
the selected <b>Phonebook Entry</b> on the <b>Desktop</b>. 
</p>
<p>
<img src="./os/windows/windowsnt/images/more_create_shortcut.gif" alt="Create Entry Shortcut">
</p>
<p>
<p class="note">
<b>Windows NT</b> will not confirm that a shortcut
was indeed created. You will have to check the <b>Desktop</b> to see if a shortcut was created for
you. 
</p>
