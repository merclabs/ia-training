<h1>Windows NT</h1>
<p align="center">
<img src="./os/windows/windowsnt/images/splash.jpg" alt="Windows NT">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/create_phonebook_entry.php">Create Phonebook Entry</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/edit_phonebook_entry.php">Edit Phonebook Entry</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/create_entry_shortcut.php">Create Entry Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/configure_ras.php">Configure RAS</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/install_ras.php">Reinstall RAS</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Windows NT (Workstation & Server)</b>
<ul>
<li><b>File:</b> Winsock.dll <b>Size:</b> 3KB
<li><b>File:</b> Wsock32.dll <b>Size:</b> 20KB
</ul>
</p>
<p>
<b>Location:</b> c:\windows\system
</p>
</td></tr>
</table>
</p>

