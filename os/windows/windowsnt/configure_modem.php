<h1>Configure Modem</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Modems icon</b>. 
<br>
<img src="./os/windows/windowsnt/images/modem_icon.gif" alt="Modem Icon">
</td></tr>
</table>
</p>
<p>
<h3>Modems Properties</h3>
After clicking on the <b>Modems Icon</b> the <b>Modems Properties</b> window should
display the current modem thats installed on this PC. To confiure settings for this modem
select the modem that displays and click the <b>Properties</b> button.
</p>
<p>
<img src="./os/windows/windowsnt/images/modem_properties.gif" alt="Modems Properties">
</p>
<p>
<h3>General Tab</h3>
The properties window for the installed modem should display. The only settings you should
be concerned about is the <b>Maximum speed</b>, which should be set at or below the speed
of the modem.  
</p>
<p>
<img src="./os/windows/windowsnt/images/modem_general_tab.gif" alt="Modems General Tab">
</p>
<p>
<h3>Connection Tab</h3>
The <b>Connection Tab</b> displays the <b>Connection</b> and <b>Call Preferences</b>. In most
cases the default settings are fine. To view additional settings click on the <b>Advanced</b>
button. 
</p>
<p>
<img src="./os/windows/windowsnt/images/modem_connection_tab.gif" alt="Modems Connection Tab">
</p>
<p>
<h3>Advanced Settings Window</h3>
On the <b>Advanced Settins Window</b> <b>Use error control</b>, <b>Compress data</b> 
and <b>Use flow control</b> should be checked. <b>Modulation type</b> should be set to 
<b>Standard</b> and <b>Extra settings</b> blank, unless you need to enter an <b><a href="http://www.modemhelp.org/inits/" target="_blank">Init string</a></b>.   
</p>
<p>
<img src="./os/windows/windowsnt/images/modem_advanced_settings.gif" alt="Advanced Setting Window">
</p>
<p>
This completes configuring the modem. 
</p>
<p><b>Next see:</b> <a href="?content=./os/windows/windowsnt/insatall_ras.php">Installing RAS</a></p>
