<h1>Create Phonebook Entry</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Network and Internet Connections</b> icon. 
</td></tr>
</table>
</p>
<p class="note">
<img src="./os/windows/windowsnt/images/empty.gif" alt="Dail-Up Networking">
<br><br>
If this is the <u>first time</u> creating a <b>Phonebook entry</b>, you should see a message indicating that the
<b>Phonebook entry is empty</b>. If so, just click <b>OK</b> to continue to the <b>Dial-Up Networking</b>
window.
</p>
<p>
<h3>Dial-Up Networking</h3>
To create a new <b>Phonebook Entry</b> click on the <b>Make New Connection</b> icon, then the <b>New</b> button.
</p>
<p>
<img src="./os/windows/windowsnt/images/dialup_networking.gif" alt="Dail-Up Networking">
</p>
<p>
<h3>Create New Entry</h3>
The next page allows you to enter a descriptive name for the <b>Phonebook Entry</b>. Enter a
name then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/new_entry.gif" alt="New Entry">
</p>
<p>
<h3>Conntection Method</h3>
Next put a check beside the <b>I am calling the Internet box</b>, then click <b>Next</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/server_info.gif" alt="How to Connect">
</p>
<p>
<h3>Phone Number</h3>
The <b>Phone Number</b> page allows you to enter the phone number of the computer you are dailing 
into. Have the customer enter their <b>Local Modem Access Number</b> from their <b>Account Information 
Card</b>. Make sure <b>Telephony dialing properties</b> is not checked.
</p>
<p>
<img src="./os/windows/windowsnt/images/phone_number.gif" alt="Phone Number">
</p>
<p class="note">
If the customer does not have their <b>Local Modem Access Number</b> or their <b>Account Information 
Card</b> notify an <b>LT</b> to get permission to send them to <b>Customer Service</b>. 
</p>
<p>
<h3>Finish</h3>
This completes creation of a new <b>Phonebook Entry</b>.
</p>
<p>
<b>Next see:</b> <a href="?content=./os/windows/windowsnt/edit_phonebook_entry.php">Edit Phonebook Entry</a>.
</p>
<p>
<img src="./os/windows/windowsnt/images/dialup_finish.gif" alt="Finish">
</p>
