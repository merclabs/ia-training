<h1>Edit Phonebook Entry</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Network and Internet Connections</b> icon. 
</td></tr>
</table>
</p>
<p>
<h3>Dial-Up Networking</h3>
To edit a <b>Phonebook Entry</b> after one has been created, click on the <b>More</b>
button on the <b>Dial-Up Networking</b> window. 
</p>
<p>
<img src="./os/windows/windowsnt/images/dialup_networking2.gif" alt="Dail-Up Networking">
</p>
<p>
<h3>More Button</h3>
Then select <b>Edit entry and modem properties</b>, an <b>Edit Phonebook Entry</b> window should display.
</p>
<p>
<img src="./os/windows/windowsnt/images/more_button.gif" alt="More Button">
</p>
<p>
<h3>Basic Tab</h3>
The first tab on this window is the <b>Basic Tab</b>, which displays the setting of the
<b>Phonebook Entry</b>. The <b>Entry name box</b> displays the name of this 
<b>Phonebook entry</b> and the <b>Phone number</b> box displays the <b>Local Modem Access Number</b>
enter when creating this <b>Phonebook entry</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/basic_tab.gif" alt="Basic Tab">
</p>
<p>
<h3>Server Tab</h3>
The second tab is the <b>Servers Tab</b>, which allows you to select the <b>Dial-Up server type</b>, in
most all cases is set to <b>PPP:Windows NT,Windows 95 Plus,Internet</b>. Under <b>Network protocols</b>,
<b>TCP/IP</b> should be the only item checked.
</p>
<p>
<img src="./os/windows/windowsnt/images/server_tab.gif" alt="Server Tab">
</p>
<p>
<h3>PPP TCP/IP Settings</h3>
To see additional <b>TCP/IP</b> settings, click on the <b>TCP/IP Settings</b> button. This
should bring up the <b>TCP/IP Settings</b> window. Both sections should be set to <b>Server
assigned</b>, unless you were instructed to put in <b>DNS</b> numbers. Everything else 
should reamin checked.
</p>
<p>
<img src="./os/windows/windowsnt/images/ppp_tcpip_settings.gif" alt="PPP TCP/IP Settings">
</p>
<p>
<h3>Script Tab</h3>
The <b>Scripting Tab</b> allows you to select either <b>Pop up a terminal window</b> or 
to <b>Run a script</b>. In most cases you would leave this set to <b>None</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/script_tab.gif" alt="Script Tab">
</p>
<p>
<h3>Security Tab</h3>
On the <b>Security Tab</b> make sure that <b>Accept any authentication including clear text</b> is
selected. 
</p>
<p>
<img src="./os/windows/windowsnt/images/security_tab.gif" alt="Security Tab">
</p>
<p>
<h3>X25 Tab</h3>
On the <b>X.25 Tab</b>, make sure that all boxes are blank, and <b>Network</b> is
set to <b>None</b>. 
</p>
<p>
This completes <b>Editing a Phonebook Entry</b>. <b>Next see:</b> <a href="?content=./os/windows/windowsnt/configure_modem.php">Configure Modem</a>
</p>
<p>
<img src="./os/windows/windowsnt/images/x25_tab.gif" alt="X25 Tab">
</p>