<h1>Installing RAS</h1>
<p>
<table class="list_table">
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsnt/main.php">Windows NT</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b>, click <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>. Then click on the <b>Network Icon</b>. 
<br>
<img src="./os/windows/windowsnt/images/network_icon.gif" alt="Network Icon">
</td></tr>
</table>
</p>
<p>
<h3>Network - Services Tab without RAS</h3>
If <b>RAS(Remote Access Services)</b> is <u>not</u> installed, click on the <b>Services Tab</b> then the <b>Add</b> button.
</p>
<p>
<img src="./os/windows/windowsnt/images/network_services_tab.gif" alt="Service Tab without RAS">
</p>
<p>
<h3>Network - Services Tab without RAS</h3>
A <b>Select Network Service</b> window should display. Select <b>Remote Access Service</b> and click <b>OK</b>. 
</p>
<p>
<img src="./os/windows/windowsnt/images/select_network_service.gif" alt="">
</p>
<p>
<h3>Windows NT Setup</h3>
A <b>Windows NT Setup</b> window should display, enter the path to your <b>Windows NT 4.0
Instalation</b> and press <b>Continue</b>. 
</p>
<p>
<img src="./os/windows/windowsnt/images/nt_setup.gif" alt="Windows NT Setup">
</p>
<p>
<h3>Remote Access Service Setup</h3>
If everything goes well, a progress bar should display while <b>Windows NT</b> copies files
to your harddrive.
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_service_setup.gif" alt="Remote Access Service Setup">
</p>
<p>
<h3>Add RAS Device</h3>
An <b>Add RAS Device</b> window should display after copying files is complete, select the 
modem you wish to use with <b>RAS</b> and press <b>OK</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/add_ras_device.gif" alt="Add RAS Device">
</p>
<p>
<h3>Remote Access Service Setup</h3>
Highlight the modem you wish to use and press <b>Configure</b> button. 
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_setup.gif" alt="Remote Access Service Setup">
</p>
<p>
<h3>Configure Port Usage</h3>
Select <b>Dial out Only</b> and press the <b>OK</b> button. 
</p>
<p>
<img src="./os/windows/windowsnt/images/configure_port_usage.gif" alt="Configure port Usage">
</p>
<p>
<h3>Remote Access Service Setup - Network</h3>
Next, click on the b>Network</b> button. 
</p>
<p>
<img src="./os/windows/windowsnt/images/remote_access_setup2.gif" alt="Click on the Network button">
</p>
<p>
<h3>Network Configuration</h3>
Put a check beside <b>TCP/IP</b> and press <b>OK</b>.
</p>
<p>
<img src="./os/windows/windowsnt/images/network_configuration.gif" alt="Network Configuration">
</p>
<p>
<h3>Windows NT Network Installation</h3>
It may take a couple of minute while <b>RAS</b> is being sets up.
</p>
<p>
<img src="./os/windows/windowsnt/images/network_installation.gif" alt="Network Installation">
</p>
<p>
<h3>Network - Services Tab with RAS Installed</h3>
Once the installation of <b>RAS</b> is complete, you should see <b>Remote Access Service</b> listed
in the Network Services window. Click the <b>Close</b> button.
</p>
<p>
<img src="./os/windows/windowsnt/images/network_services_tab2.gif" alt="Network - Services Tab with RAS Intalled">
</p>
<p>
<h3>Binding Configuration</h3>
<b>Windows NT</b> will then reconfigure your network settings which may also take a couple of minutes... 
</p>
<p>
<img src="./os/windows/windowsnt/images/binding_configuration.gif" alt="Binding Configuration">
</p>
<p>
<h3>Restart Computer</h3>
Once complete, you should receive a <b>Windows NT</b> prompt to <b>restart</b>, click <b>Yes</b> to restart your computer.
</p>
<p class="note">
When windows reboots, your network settings should be reconfigured and <b>RAS(Remote
Access Services)</b> should be installed. 
</p>
<p>
<img src="./os/windows/windowsnt/images/restart_prompt.gif" alt="Restart Prompt">
</p>