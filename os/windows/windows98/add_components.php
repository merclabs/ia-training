<h1>Adding  Network Components</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/main.php">Windows 98</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in the <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>.
</td></tr>
</table>
</p>
<h3>Network</h3>
Click on the <b>Network</b> icon, the <b>Network</b> window should display. 
</p>
<p>
Three components should be listed here.
<b>
<ul>
<li>Client for Microsoft Networks</li>
<li>Dail-Up Adapter</li>
<li>TCP/IP</li>
</ul>
</b>
and <b>Primary Network Logon</b> should be set to <b>Windows Logon</b>.
</p>
<p>
<img src="./os/windows/windows98/images/network.gif" alt="Network">
</p>
<p class="note">
If any of these components are not installed, you may install them by clicking on the 
<b>Add</b> button. 
<br><br>
Installation of the basic three components needed for <b>Dail-Up</b> 
are listed below.
</p>
<p>
<h3>Adding a Client</h3>
If <b>Client for Microsoft Networks</b> is not installed, Click the <b>Add</b> button 
on the <b>Network Configuration Tab</b>, then select <b>Client</b>, then click <b>Add</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_components_client.gif" alt="Add Client">
</p>
<p>
The <b>Select Network Client</b> window should display. Under <b>Manufacturers</b> select 
<b>Microsoft</b>, under <b>Network Clients</b> select <b>Client for Microsoft Networks</b>,
then click <b>OK</b>. This should place <b>Client for Microsoft Networks</b> in the list of
installed components on the <b>Network Configuration Tab</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_network_client.gif" alt="Select Network Client">
</p>
<p>
<h3>Adding an Adapter</h3>
If <b>Dial-Up Adapter</b> is not installed, Click the <b>Add</b> button 
on the <b>Network Configuration Tab</b>, then select <b>Adapter</b>, then click <b>Add</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_components_adapter.gif" alt="Add Adapter">
</p>
<p>
The <b>Select Network Adapter</b> window should display. Under <b>Manufacturers</b> select 
<b>Microsoft</b>, under <b>Network Adapters</b> select <b>Dail-Up Adapter</b>,
then click <b>OK</b>. This should place <b>Dail-Up Adapter</b> in the list of
installed components on the <b>Network Configuration Tab</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_network_adapter.gif" alt="Select Network Adapter">
</p>
<p>
<h3>Adding a Protocol</h3>
If <b>TCP/IP</b> is not installed, Click the <b>Add</b> button 
on the <b>Network Configuration Tab</b>, then select <b>Protocol</b>, then click <b>Add</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_components_protocol.gif" alt="Add Protocol">
</p>
<p>
The <b>Select Network Protocol</b> window should display. Under <b>Manufacturers</b> select 
<b>Microsoft</b>, under <b>Network Protocols</b> select <b>TCP/IP</b>,
then click <b>OK</b>. This should place <b>TCP/IP</b> in the list of
installed components on the <b>Network Configuration Tab</b>.
</p>
<p>
<img src="./os/windows/windows98/images/select_network_tcpip.gif" alt="Select Network Protocol">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->