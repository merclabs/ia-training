<h1>Configure Modem</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows95/main.php">Windows 95</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in the <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>.
</td></tr>
</table>
</p>
<p>
<h3>Control Panel</h3>
Inside of the <b>Control Panel</b>, click on <b>Modems</b>.
</p>
<p>
<img src="./os/windows/windows98/images/control_panel_modem.gif" alt="Control Panel">
</p>
<p>
<h3>Modems Properties</h3>
The <b>Modems Properties</b> page should display. This page displays the current modem that
is installed on the PC. To view the properties of this modem, highlight the modem, then click the <b>Properties</b> button.
</p>
<p>
<img src="./os/windows/windows98/images/modem_properties.gif" alt="Modems Properties">
</p>
<p>
<h3>General Tab</h3>
Another properties page should appear, with three tabs. The first tab is the <b>General Tab</b>.
It displays the current modem and port being used and the current maximum speed. In most cases the
port setting should not need to be changed but the <b>Maximum speed</b> should be adjusted at or below
the speed of the modem. 
</p>
<p>
<img src="./os/windows/windows98/images/properties_page_02.gif" alt="Dialer Properties">
</p>
<p>
<h3>Connection Tab</h3>
The next tab is the <b>Connection Tab</b>. This tab displays Connection and Call preferences, setting on
this screen in most cases do not need to be changed.  
</p>
<p>
<img src="./os/windows/windows98/images/properties_page_03.gif" alt="Dialer Properties">
</p>
<p>
<h3>Advanced Port Settings</h3>
The <b>Port Settings...</b> button displays the modems transmit and receive buffer settings. These
settings should only be changes if an <b>LT</b> advises you to do so, otherwise they should remain unchanged.
</p>
<p>
<img src="./os/windows/windows98/images/properties_fifo.gif" alt="Advance Port Settings">
</p>
<p>
<h3>Advanced Connection Settings</h3>
The <b>Advanced...</b> button displays connection settings that control how the modem
connects and handles information. In most cases the <b>Use error control</b>,<b>Compress data</b>
,<b>Use flow control</b> and <b>Hardware(RTS/CTS)</b> should be checked. <b>Modulation type</b> should
be standard and <b>Extra settings</b> is where you enter <b>init strings</b>.
</p>
<p>
<img src="./os/windows/windows98/images/properties_advance.gif" alt="Advance Connection Settings">
</p>
<p>
<h3>Options Tab</h3>
The next tab is the <b>Options Tab</b>. This tab displays the Connection, Dial and Status control.
These settings control how dialer behaves when connecting; in most cases only <b>display modem status</b>
should be checked. If you are instructed by an <b>LT</b> to set the customer up with a <b>Terminal Dialer</b>, then
you need to check <b>Bring up terminal window after dialing</b> to force the terminal window to display after the computer dials.
</p>
<p>
<img src="./os/windows/windows98/images/properties_options_tab.gif" alt="Dialer Properties">
</p>
<p class="note">
<b>Note:</b> When connecting with a <b>Terminal Dialer</b> the customer must type <b>PPP</b> when the
terminal window appears, then enter their <i>user name and password</i>, then either click the <b>Continue</b>
button or press <b>F7</b> to exit the terminal window. If a customer is sent to connect via a <b>Terminal Dialer</b>
and continue to have problems connecting, check with an <b>LT</b> to get permission to send customer to get
their <b>modem</b> check by a computer tech.
</p>
<p>
<h3>Diagnostics Tab</h3>
The <b>Diagnostic Tab</b> displays the communications port that the modem is installed on. To 
run diagnostic on your modem to see if it is communicating with your computer properly, select
the <b>COM Port</b> that your modem is on, then click the <b>More Info...</b> button.
</p>
<p>
<img src="./os/windows/windows98/images/diagnostic_tab.gif" alt="Modem Diagnostics">
</p>
<p>
<h3>More Info</h3>
The <b>More Info</b> screen should display after a brief message displays that its <b>Communicating with Modem</b>.
If all is well you should see a window like below.
</p>
<p>
<img src="./os/windows/windows98/images/more_info.gif" alt="More Info...">
</p>
<p class="note">
If you receive a message that the <b>"Modem failed to respond"</b> and there are more than
one modem listed on the <b>General Tab</b>. You may remove this modem from the <b>General Tab</b>
because the <b>Modem Driver</b> may not be the correct one to used for the modem installed, 
check the other modem(s) listed on the <b>General Tab</b> to see if it responds as well. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->