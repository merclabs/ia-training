<h1>Remove & Reinstall Dial-Up Networking</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/main.php">Windows 98</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
Before removing <b>Communication</b>, <b>Dial-Up Networking</b> or any <b>Network Components</b>
be sure to check for the correct number of <b>Cab files</b> that are on the computer. 
</p>
<p>
Click on <b>Start</b>, <b>Find</b> then <b>Files or Folders</b>. In the <b>Look In</b> box
select <b>C Drive</b>, then in the <b>Named</b> box type <b>win*.cab</b>. Click the
<b>Find Now</b> button. This should display all <b>Cab files</b> on the PC.
</p>
<p>
<img src="./os/windows/windows98/images/find.gif" alt="Find">
</p>
</p>
<p>
<b>see:</b> <i>Windows 98 link above for more information about correct number of <b>Cab files</b>.</i>
</p>
</td></tr>
</table>
</p>
<p class="note">
If a customer's PC does not have the correct number of <b>Cabs</b> or <b>Windows 95 CD</b>. 
Notify an LT to get permission to send customer to get a <b>Windows 95 CD</b> and note it in your log.
</p>
<p>
<h3>Windows Setup</h3>
Click on <b>Start</b>, <b>Settings</b> then <b>Control Panel</b>.
There you should see the <b>Add/Remove Programs</b> icon. Double click the <b>Add/Remove Programs</b> icon
then select the <b>Windows Setup Tab</b>. 
</p>
<p>
<img src="./os/windows/windows98/images/windows_setup.gif" alt="Windows Setup">
</p>
<p>
<h3>Removing Communications</h3>
After Windows searches for installed components the following window should
display. Remove the check from the box beside <b>Communications</b> then click <b>Apply</b> at the bottom.
</p>
<p>
<img src="./os/windows/windows98/images/communications.gif" alt="Add/Remove Programs">
</p>
<p>
<h3>Restart</h3>
After a few moments windows should prompt you to <b>Restart</b>. Click <b>Yes</b> to 
restart the computer.
</p>
<p>
<img src="./os/windows/windows98/images/restart_prompt.gif" alt="Restart Prompt">
</p>
<p class="note">
If <b>Windows</b> does not restart on its own, then restart from the <b>Start</b> menu.
</p>
<p>
<h3>Check Network</h3>
Once <b>Windows</b> restarts, return to the <b>Control Panel</b>, then click on the 
<b>Network</b> icon.
</p>
<p>
<img src="./os/windows/windows98/images/network_icon.gif" alt="Network Icon">
</p>
<p>
<h3>Clear Network</h3>
The <b>Network</b> window should display; in most cases this window should be blank
as the screen shot shows, unless the customer has a <b>Network Card</b> installed. If so,
that card should be listed here. 
</p>
<p>
If so, <b style="color:red;">DO NOT REMOVE IT</b>, just click <b>Cancel</b> at the 
bottom. This should return you to the <b>Control Panel</b>. 
</p>
<p>
<img src="./os/windows/windows98/images/network_clear.gif" alt="Clear Network">
</p>
<p class="note">
If components other than a <b>Network Card</b> are still listed here, make sure they
are not being used, then remove them by highlighting each component in the list 
then click the <b>Remove</b> button once. Once this window is cleared, click <b>OK</b>
at the bottom. <b>Windows</b> should ask to restart, click <b>Yes</b> to restart.
</p>
<p class="note">
<b>Note:</b> While booting, <b>Windows</b> may prompt for <b>User/Pass</b>. If so, just
enter their <b>User Name</b>, leaving the password blank, then click <b>OK</b> to continue
booting into <b>Windows</b>. If password prompt returns, just have them enter their internet
account password, then click <b>OK</b> to continue. 
</p>
<p>
<h3>Reinstalling Dial-Up Networking</h3>
In the <b>Control Panel</b> click on the <b>Add/Remove Programs</b> icon, then the
<b>Windows Setup Tab</b>. 
<p>
<p>
Once the <b>Windows Setup Tab</b> displays, put a check
in the box beside <b>Communications</b> then click the <b>Details...</b> button.
</p>
<p>
<img src="./os/windows/windows98/images/reinstall_communications.gif" alt="Add/Remove Programs">
</p>
<p>
<h3>Selecting Components</h3>
After clicking the <b>Details...</b> the window below should display. Uncheck
all components in this list except <b>Dail-Up Networking</b> and <b>Hyper Terminal</b> 
then click <b>OK</b>.
</p>
<p>
<img src="./os/windows/windows98/images/uncheck_components.gif" alt="Select Components">
</p>
<p class="note">
Remember to scroll down the list to make sure everything else is <b>unchekced</b>.
</p>
<p>
<h3>Restart</h3>
Next, at the bottom of the <b>Add/Remove Programs</b> window click <b>Appy</b>. 
Windows should prompt to <b>restart</b>. Click <b>Yes</b> to restart.
</p>
<p>
<img src="./os/windows/windows98/images/restart_prompt.gif" alt="Restart">
</p>
<p>
<p class="note">
After <b>Windows</b> reboots, <b>Dail-Up Networking</b> should be reinstalled. The next
step is to create a dialer if one does not already exist. You must delete and recreate any
dialer that already exists. 
<br><br>
 <b style="color:red;">Note:</b> Remember to get the access number from the dialer before 
 deleting it. 
</p>
<b>Next:</b> <i>see: <a href="?content=./os/windows/windows98/configure_network.php">Configure Network</a></i>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->