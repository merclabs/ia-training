<h1>Windows 98</h1>
<p align="center">
<img src="./os/windows/windows98/images/splash.jpg" alt="Windows 98">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/create_dialer.php">Create a Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/configure_dialer.php">Configure Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/create_shortcut.php">Create a Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/configure_modem.php">Configure Modem</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/configure_network.php">Configure TCP/IP</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/add_components.php">Adding Network Components</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/randr.php">Remove & Reinstall Dail-Up Networking</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Windows 98 (5/11/98 4.10.1998)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 21KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 40KB
<li><b>Cabs:</b> 22-69 
</ul>
</p>
<p>
<b>Windows 98 SE (4/23/99 4.10.2222A)</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 21KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 40KB
<li><b>Cabs:</b> 21-74
</ul>
</p>
<p>
<b>Location:</b> c:\windows\system</br>
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->