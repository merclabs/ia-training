<h1>Create a new Dialer</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/main.php">Windows 98</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Before creating a new dialer, you should make sure the customer has the following:<br>
<ul>
<li>An active internet account.</li>
<li>All account information(on Account Information Card obtained from Provider.)</li>
<li>Local modem access number for area.</li>
<li>Dial-Up Networking installed.</li>
</ul>
</td></tr>
</table>
</p>
<p>
<h3>My Computer</h3>
From the Desktop, double click on <b>My Computer</b> then double click on <b>Dail-up Networking</b>.
</p>
<p>
<img src="./os/windows/windows98/images/my_computer.gif" border=0 alt="My Computer">
</p>
<p class="note">
If you click on <b>My Computer</b> and do not see <b>Dial-Up Networking</b>, first, check
to make sure they are indeed using <b>Windows 98</b>. If they are, then <b>Dail-Up Networking</b> is not
installed on their PC. The next step is to install <b>Dail-Up Networking</b> if the computer has
the correct number of <b>Cab Files</b> or the original and correct full version of the <b>Windows 98</b>.
<b>See:</b> <a href="?content=./os/windows/windows98/randr.php" alt="R & R">Remove & Reinstall Dial Up Networking</a>
</p>
<!--
<p>
<h3></h3>
When you open <b>Dial Up Networking</b>, the <b>New Connection Wizard</b> for <b>Dial Up Networking</b> will start.
Click <b>next</b> to start the Wizard.
</p>
<p>
<img src="./os/windows/windows98/images/dail_up_networing_greeting.gif" alt="New Connection Wizard">
</p>
<p class="note">
You may wish to click <b>cancel</b> to exit the wizard, to create a new connection manually.
</p>
-->
<p>
<h3>Dail-up Networking</h3>
Inside of the <b>Dail-Up Networking</b> window you will see a <b>Make New Connection</b> icon. To create a 
new dialer double click on the <b>Make New Connection</b> icon. This should start the <b>Make New Connection Wizard</b>. 
</p>
<p>
<img src="./os/windows/windows98/images/dialup_networking.gif" alt="Dail-up Networking">
</p>
<p>
<h3>Make a New Connection - Name</h3>
The first screen of the <b>Make New Connection Wizard</b> asks for a descriptive name to give
the connection you are creating. Any name can be used, such as the name of the <b>Provider</b> or 
name of the <b>city</b> or <b>location</b> they are dialing into. Once this name is entered, click next. 
</p>
<p>
<img src="./os/windows/windows98/images/make_new_connection_01.gif" alt="Name">
</p>
<p>
<h3>Make New Connection - Access Number</h3>
The next screen requires the <b>Local Modem Access Number</b> that the customer received
from the provider when they originally signed up for service. In most cases you would leave
the area code box blank. If a customer does not have this access number, look for it on their <b>Account
Information Card</b>. 
</p>
<p>
<img src="./os/windows/windows98/images/make_new_connection_02.gif" alt="Access Number">
</p>
<p class="note">
If a customer does not know their <b>Local Modem Access Number</b> or have their <b>Account Information Card</b>, notify
an <b>LT</b> to get permission to send them to <b>Customer Service</b> to get this information.
Once this number is entered, click next.
</p>
<p>
<h3>Make New Conneciton - Finish</h3>
The next screen is the last screen of the <b>Make New Connection Wizard</b>, have the
customer click <b>Finsih</b> to complete the wizard. You should now see a new icon in
<b>Dial-Up Networking</b> labeled according to the name you gave this connection.
</p>
<p>
<img src="./os/windows/windows98/images/make_new_connection_03.gif" alt="Finish">
</p>
<p>
<h3>New Dialer Created</h3>
This completes creating a new dialer on <b>Windows 98</b>, <b>next See:</b>
<a href="?content=./os/windows/windows98/configure_dialer.php">Configure Dialer</a>.
</p>
<p>
<img src="./os/windows/windows98/images/dialer_icon.gif" alt="New Connection Icon">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->