<h1>Configure Dialer</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/main.php">Windows 98</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
To configure a dialer the dialer must first be created. If one has not been created please see
<a href="?content=./os/windows/windows98/create_dialer.php">Create Dialer</a>.
</td></tr>
</table>
</p>
<p>
<h3>Locate Dialer</h3>
After a dialer has been created it must be configured. To configure a dialer, locate
its icon in the <b>Dail-UP Networking</b> window.
</p>
<p>
<img src="./os/windows/windows98/images/dialer_icon.gif" alt="New Connection">
</p>
<p>

</p>
<p>
<h3>Dialer Properties</h3>
Once you locate the icon for the dialer, right click on it to display the pop-up 
memu and select <b>Properties</b> or you can select the icon by clicking on it once,
select <b>File</b> then <b>Properties</b> to open the <b>Properties</b> page.  
</p>
<p>
<img src="./os/windows/windows98/images/right_click_dialer.gif" alt="Right Click on Dialer.">
</p>
<p>
<h3>Dailer Properties</h3>
Once on the <b>Properties</b> page, remove the check from the <b>Use area code and Dialing Properties</b>
checkbox. This should disable the <b>area code</b> box beside the phone number box.
Then click on the <b>Configure...</b> button.
</p>
<p>
<img src="./os/windows/windows98/images/dialer_properties_01.gif" alt="Dialer Properties">
</p>
<p>
<h3>General Tab</h3>
Another properties page should appear, with three tabs. The first tab is the <b>General Tab</b>.
It displays the current modem and port being used and the current maximum speed. In most cases the
port setting should not need to be changed but the <b>Maximum speed</b> should be adjusted at or below
the speed of the modem. 
</p>
<p>
<img src="./os/windows/windows98/images/dialer_properties_02.gif" alt="Dialer Properties">
</p>
<p>
<h3>Connection Tab</h3>
The next tab is the <b>Connection Tab</b>. This tab displays Connection and Call preferences, setting on
this screen in most cases do not need to be changed.  
</p>
<p>
<img src="./os/windows/windows98/images/dialer_properties_03.gif" alt="Dialer Properties">
</p>
<p>
<h3>Advanced Port Settings</h3>
The <b>Port Settings...</b> button displays the modems transmit and receive buffer settings. These
settings should only be changes if an <b>LT</b> advises you to do so, otherwise they should remain unchanged.
</p>
<p>
<img src="./os/windows/windows98/images/properties_fifo.gif" alt="Advance Port Settings">
</p>
<p>
<h3>Advanced Connection Settings</h3>
The <b>Advanced...</b> button displays connection settings that control how the modem
connects and handles information. In most cases the <b>Use error control</b>,<b>Compress data</b>
,<b>Use flow control</b> and <b>Hardware(RTS/CTS)</b> should be checked. <b>Modulation type</b> should
be standard and <b>Extra settings</b> is where you enter <b>init strings</b>.
</p>
<p>
<img src="./os/windows/windows98/images/properties_advance.gif" alt="Advance Connection Settings">
</p>
<p>
<h3>Options Tab</h3>
The next tab is the <b>Options Tab</b>. This tab displays the Connection, Dial and Status control.
These settings control how dialer behaves when connecting; in most cases only <b>display modem status</b>
should be checked. If you are instructed by an <b>LT</b> to set the customer up with a <b>Terminal Dialer</b>, then
you need to check <b>Bring up terminal window after dialing</b> to force the terminal window to display after the computer dials.
</p>
<p>
<img src="./os/windows/windows98/images/properties_options_tab.gif" alt="Dialer Properties">
</p>
<p class="note">
<b>Note:</b> When connecting with a <b>Terminal Dialer</b> the customer must type <b>PPP</b> when the
terminal window appears, then enter their <i>user name and password</i>, then either click the <b>Continue</b>
button or press <b>F7</b> to exit the terminal window. If a customer is sent to connect via a <b>Terminal Dialer</b>
and continue to have problems connecting, check with an <b>LT</b> to get permission to send customer to get
their <b>modem</b> check by a computer tech.
</p>
<p>
<h3>Server Types</h3>
In <b>Windows 95</b> the <b>Server Types</b> page can be access from the <b>General Tab</b>
as a button. Pressing this button displays the <b>Server Types</b> page. On this page 
<b>Type of Dial-Up Server</b> should be set to <b>PPP:Windows 95, Windows NT 3.5, Internet</b>,
<b>Enable software compression</b> should be checked, and under <b>Allowed Newtork Protocols</b>,
only <b>TCP/IP</b> should be checked.</b> 
</p>
<p>
<img src="./os/windows/windows98/images/server_types_tab.gif" alt="Server Types">
</p>
<p class="note">
In <b>Windows 98</b>, <b>Server Types</b> displays as a tab instead of as a button.
</p>
<p>
<h3>TCP/IP Settings</h3>
Clicking on the <b>TCP/IP Settings</b> button displays the <b>TCP/IP Settings</b> page.
This page allows you to specify an <b>IP Address</b>, <b>DNS Servers</b>, select
<b>IP Header Compression</b> and to use the <b>default gateway</b> on a remote network.
In most cases the defualt settings will do.
</p>
<p>
<img src="./os/windows/windows98/images/tcpip_properties.gif" alt="TCP/IP Settings">
</p>
<p>
This completes configuring a dialer, <b>next see:</b> <a href="?content=./os/windows/windows95/create_shortcut.php">Creating a Shortcut</a>
</p>
<p class="note">
If network conditions are normal and a customer is receiving a connection error,with <b>Dail-Up</b>
adding <b>DNS Number</b> in the <b>Specify name server addreses</b> section often resolves
this for some connection issues.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->