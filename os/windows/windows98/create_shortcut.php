<h1>Create Shortcut</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows98/main.php">Windows 98</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already inside of <b>Dial-Up Networking</b>, click on <b>My Computer</b>
then <b>Dial-Up Networking</b>.
</td></tr>
</table>
</p>
<p>
<h3>Dial-Up Networing</h3>
Inside of <b>Dial-Up Networking</b> locate the dialer you wish to create a shortcut
for. 
</p>
<p>
<img src="./os/windows/windows98/images/dialer_icon.gif" alt="Dial-Up Networking">
</p>
<p>
<h3>Create Shortcut</h3>
Right click on the dialers icon, then click <b>Create Shortcut</b>.
</p>
<p>
<img src="./os/windows/windows98/images/create_shortcut.gif" alt="Create Shortcut">
</p>
<p>
<h3>Shortcut Prompt</h3>
Windows should prompt that it can not create a shortcut here, click <b>Yes</b>.
</p>
<p>
<img src="./os/windows/windows98/images/shortcut_prompt.gif" alt="Shortcut Prompt">
</p>
<p class="note">
After clicking <b>Yes</b>, windows will then place a shortcut on the desktop. <b>Note:</b>
Windows does not display a confirmation that this task is complete, you have to check the
desktop to make sure a shortcut has been created for you there. 
</p>
<p>
<h3>Shortcut on Desktop</h3>
Close the <b>Dial-Up Networking</b> windows, so you can see the <b>Desktop</b>. 
Locate the new shortcut and advise the customer to use this icon to get both, 
connected and disconnected. 
</p>
<p>
<img src="./os/windows/windows98/images/dialer_shortcut.gif" alt="Shortcut Icon">
</p>
<p class="note">
If a customer has serveral shortcuts on their desktop, before creating a shortcut to 
the dialer, please delete all unused shortcut so the one that you create can be easly
founded.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->