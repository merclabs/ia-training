<h1>Windows ME</h1>
<p align="center">
<img src="./os/windows/windowsme/images/splash.jpg" alt="Windows ME">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/create_dialer.php">Create a Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/configure_dialer.php">Configure Dialer</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/create_shortcut.php">Create a Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/configure_modem.php">Configure Modem</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/configure_network.php">Configure TCP/IP</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/add_components.php">Adding Network Components</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/randr.php">Remove & Reinstall Dail-Up Networking</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Operating System:</b> Windows ME</b>
<ul>
<li><b>File:</b> Winsock.dll  <b>Size:</b> 22KB
<li><b>File:</b> Wsock32.dll  <b>Size:</b> 37KB
</ul>
</p>
<p>
<b>Location:</b> c:\windows\system
</p>
<p>
<b>Windows ME:</b> - contains all needed cabs
</p>
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->