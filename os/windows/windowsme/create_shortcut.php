<h1>Create Shortcut</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/main.php">Windows ME</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>. Click on the <b>Dail-Up Networing</b> icon.
</td></tr>
</table>
</p>
<p class="note">
<b>Tip:</b>You can also get to the <b>Dail-Up Networking</b> window by clicking on <b>Start</b>, <b>Settings</b>
then <b>Dail-Up Networking</b>.
</p>

<p>
<h3>Dial-Up Networking</h3>
Once <b>Dial-Up Networking</b> opens, locate the dialer of which you want to create a shortcut.  
</p>
<p>
<img src="./os/windows/windowsme/images/control_panel2.gif" alt="Dial-Up Networking">
</p>
<p>
<h3>Create Shortcut</h3>
Right Click once, then select <b>Create Shortcut</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/create_shortcut.gif" alt="Create Shortcut">
</p>
<p>
<h3>Shortcut Message</h3>
Windows will prompt you with a message that <b>Windows can not create a shortcut here...</b>, click
<b>Yes</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/windows_shortcut_message.gif" alt="Windows Shortcut Message">
</p>
<p class="note">
<b>Windows</b> will not confirm that a shortcut has been created on the <b>Desktop</b>. You must
check for yourself that windows indeed created a shortcut there. This is true for all versions of 
<b>Widnows</b>.
</p>
<p>
<h3>Desktop Shortcut Icon</h3>
If everything goes OK, you should see an icon on the <b>Desktop</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/desktop_shortcut.gif" alt="Desktop Shortcut Icon">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->