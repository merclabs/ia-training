<h1>Create a new Dialer</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/main.php">Windows ME</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Dail-Up Networking</b>, click on <b>Start</b>, <b>Settings</b> then
<b>Dail-Up Networking</b>. The <b>Dail-Up Networking</b> window should display.
</td></tr>
</table>
</p>
<p>
<h3>Control Panel</h3>
Once the <b>Control Panel</b> is open, double click on <b>Dial-Up Networking</b>. 
</p>
<p>
<img src="./os/windows/windowsme/images/control_panel.gif" alt="Control Panel">
</p>
<p class="note">
If you click on <b>Control Panel</b> and do not see <b>Dial-Up Networking</b>, first, check
to make sure they are indeed using <b>Windows ME</b>. If they are, then <b>Dail-Up Networking</b> is not
installed on their PC. The next step is to install <b>Dail-Up Networking</b>.
<b>See:</b> <a href="?content=./os/windows/windowsme/randr.php" alt="R & R">Remove & Reinstall Dial Up Networking</a>
</p>
<p>
<h3>Dial-Up Networking</h3>
Inside of <b>Dial-Up Networking</b>, double click on the <b>Make new Connection</b> icon.
</p>
<p>
<img src="./os/windows/windowsme/images/make_new_connection.gif" alt="Dial-Up Networking">
</p>
<p>
<h3>New Connection Wizard</h3>
This should start the <b>Make new Connection</b> wizard. If so, click <b>next</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/new_connection_start.gif" alt="New Connection Wizard">
</p>
<p>
<h3>Make new Connection - Name</h3>
The first screen of the <b>Make New Connection Wizard</b> asks for a descriptive name to 
give the connection you are creating. Any name can be used, such as the name of 
the <b>Provider</b> or name of the <b>city</b> or <b>location</b> they are dialing into. Once this name 
is entered, click next. 
</p>
<p>
<img src="./os/windows/windowsme/images/new_connection_01.gif" alt="">
</p>
<p>
<h3>Make New Connection - Access Number</h3>
The next screen requires the <b>Local Modem Access Number</b> that the customer received
from the provider when they originally signed up for service. In most cases you would leave
the area code box blank. If a customer does not have this access number, look for it on their <b>Account
Information Card</b>. 
</p>
<p>
<img src="./os/windows/windowsme/images/new_connection_02.gif" alt="">
</p>
<p class="note">
If a customer does not know their <b>Local Modem Access Number</b> or have their <b>Account Information Card</b>, notify
an <b>LT</b> to get permission to send them to <b>Customer Service</b> to get this information.
Once this number is entered, click next.
</p>
<p>
<h3>Make New Conneciton - Finish</h3>
The next screen is the last screen of the <b>Make New Connection Wizard</b>, have the
customer click <b>Finsih</b> to complete the wizard. You should now see a new icon in
<b>Dial-Up Networking</b> labeled according to the name you gave this connection.
</p>
<p>
<img src="./os/windows/windowsme/images/new_connection_03.gif" alt="">
</p>
<p>
<h3>New Dialer Created</h3>
This completes creating a new dialer on <b>Windows ME</b>, <b>next See:</b>
<a href="?content=./os/windows/windowsme/configure_dialer.php">Configure Dialer</a>.
</p>
<p>
<img src="./os/windows/windowsme/images/control_panel2.gif" alt="My Connection">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->