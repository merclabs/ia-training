<h1>Configure Network</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/main.php">Windows ME</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>.
</td></tr>
</table>
</p>
<p>
<h3>Modems Icon</h3>
Double click on the <b>Modems</b> icon.
</p>
<p>
<img src="./os/windows/windowsme/images/modems_icon.gif" alt="Modems Icon">
</p>
<p>
<h3>Modems Properties Window</h3>
The <b>Modems Properties</b>  window displays the current modems installed on this
computer. To configure setting for this modem, select it from the list, them click on
the <b>Properties</b> button. 
</p>
<p>
<img src="./os/windows/windowsme/images/modems_general_tab.gif" alt="Modems Properties Window">
</p>
<p>
<h3>Modems Properties - General Tab</h3>
The modem's <b>Properties Widnow</b> should display with 5 tabs. The first tab is
the <b>General Tab</b>. This tab displays the modem's <b>Port</b>, <b>Speaker volume</b>
and <b>Maximum speed</b>. The <b>Port</b>, <b>Speaker volume</b> in most cases should
remain unchanged, but the <b>Maximum speed</b> should be set at or below the true speed
of the modem installed.
</p>
<p>
<img src="./os/windows/windowsme/images/configure_general_tab.gif" alt="Modems Properties - General Tab">
</p>
<p>
<h3>Modems Properties - Connection Tab</h3>
The <b>Connection Tab</b> displays the <b>Connection</b> and <b>Call preferences</b>, these
settings in most cases remain unchanged. For additional setting click on the <b>Port Settings...</b>
button.
</p>
<p>
<img src="./os/windows/windowsme/images/configure_connection_tab.gif" alt="Modems Properties - Connection Tab">
</p>
<p>
<h3>Advanced Port Settings</h3>
Clicking the <b>Port Settings...</b> button displays the <b>Advanced Port Settings</b> window.
This window displays the <b>Receive</b> and <b>Transfer</b> buffers of this modem. These
settings should be changed only if you are told to do so by an <b>LT</b>, otherwise they should 
remain unchanged. 
</p>
<p>
<img src="./os/windows/windowsme/images/advanced_port_settings.gif" alt="Advance Port Settings">
</p>
<p>
<h3>Advanced Connection Settings</h3>
Clicking the <b>Advanced</b> button displays the <b>Advanced Connection Settings</b> window. 
The <b>Use error control</b>, <b>Compress data</b> and <b>Use flow control</b> boxes should be checked.
<b>Modualation type</b> should be set to <b>standard</b> and the <b>Extra settings</b> box is were you should
enter <a href="http://www.modemhelp.org/inits/" target="blank"><b>Init Strings</b></a>. 
</p>
<p>
<img src="./os/windows/windowsme/images/advanced_tab.gif" alt="Advance Connection Settings">
</p>
<p>
<h3>Modems Properties - Options Tab</h3>
The <b>Options Tab</b> displays the <b>Connection control</b> section, the <b>Bring
up terminal window after dialing</b> box is used to set a customer up with a <b>Terminal Dialer</b>.
The <b>Dial control</b> and <b>Status control</b> sections should remain unchanged.  
</p>
<p>
<img src="./os/windows/windowsme/images/configure_options_tab.gif" alt="Modems Properties - Options Tab">
</p>
<p>
<h3>Modems Properties - Destinctive Ring Tab</h3>
The <b>Destinctive Ring Tab</b> is used to enable and configure the <b>Destinctive Ring</b>
service if enabled on this line. This setting is almost never used. 
</p>
<p>
<img src="./os/windows/windowsme/images/configure_destinctive_ring_tab.gif" alt="Modems Properties - Destinctive Ring Tab">
</p>
<p>
<h3>Modems Properties - Forward Tab</h3>
The <b>Forward Tab</b> is used to enable or disable the <b>Call Forwarding</b>
service if enabled on this line. This setting is almost never used. 
</p>
<p>
<img src="./os/windows/windowsme/images/configure_forward_tab.gif" alt="Modem Properties - Forward Tab">
</p>
<p>
<h3>Dialing Properties</h3>
Closing the <b>Properties Window</b> brings you back to the <b>Modems General Tab</b>.
At the bottom of this tab you should see a <b>Dialing Properties</b> button.
Click on the <b>Dialing Properties</b> button displays the <b>Dialing Properties Window</b>,
in most cases the default settings here should be fine. Click <b>OK</b> to get back to the
<b>Modems Properties Window</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/dialing_properties_window.gif" alt="Modem Dialing Properties">
</p>
<p>
<h3>Diagnostic Tab</h3>
On the <b>Modems Properties Window</b> you should see a <b>Diagnostic Tab</b>.
This tab allows you to run a diagnostic on the current modem(s) installed. To start
diagnostic on a modem(s), select the <b>COM</b> port of the modem you wish to test,
then click the <b>More Info...</b> button.
</p>
<p>
<img src="./os/windows/windowsme/images/modems_diagnostic_tab.gif" alt="Modems Dianostic Tab">
</p>
<p>
<h3>More Info...</h3>
If everything goes well and the modem is working properly, you should see the <b>More Info...</b>
window with alot of information in the <b>Command-Response</b> window.
</p>
<p>
<img src="./os/windows/windowsme/images/moreinfo_page.gif" alt="Modems Dianostic Tab">
</p>
<p class="note">
If you get a <b>Port already open</b> or <b>This modem failed to respond</b> message.
The modem you are checking is not installed properly and needs to be <b>upgraded</b>, <b>removed</b>
or <b>reinstalled</b>. If the customer does not have the correct <b>Modem Driver</b>, 
notify an <b>LT</b> to get permission to send the customer to the <b>Computer Manufacturer</b>
or a <b>Computer Tech</b> to get the correct driver for their modem. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->