<h1>Configure Network</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/main.php">Windows ME</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b>
then <b>Control Panel</b>.
</td></tr>
</table>
</p>
<p>
<h3>Network Icon</h3>
Once inside of the <b>Control Panel</b>, click on the <b>Network</b> icon.
</p>
<p>
<img src="./os/windows/windowsme/images/network_icon.gif" alt="Network Icon">
</p>
<p class="note">
<b>Tip: </b>You can also get to the <b>Network Window</b> by right clicking on the
<b>My Network Places</b> icon on the <b>Desktop</b>. 
</p>
<p>
<h3>Network Window</h3>
The <b>Network Window</b> displays the current network components that are installed. There
are three basic components needed for dial-up. <b>Client for Microsoft Nteworks</b>, <b>Dial-Up
Adapter</b> and <b>TCP/IP</b>. To configure TCP/IP, select <b>TCP/IP</b>, then
click <b>Properties</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/network_window.gif" alt="Network Window">
</p>
<p class="note">
If any of these three components are not installed, click the add button to install that
specific component. <b>Windows ME</b> has <b>Cab files</b> pre-installed, no need to check.
</p>
<p>
<h3>DNS Configurations Tab</h3>
On the <b>DNS Configuration Tab</b>, <b>disable DNS</b> should have a dot beside it. 
</p>
<p>
<img src="./os/windows/windowsme/images/tcpip_dns_tab.gif" alt="DNS Configurations">
</p>
<p class="note">
If a customer has upgraded a Windows 98 PC to ME, this screen may still look like
<a href="?content=./os/windows/windows98/configure_network.php">Windows 98</a>.
</p>
<p>
<h3>Bindings Tab</h3>
On the <b>Bindings Tab</b>, <b>Client for Micorsoft Networks</b> should be checked.
</p>
<p>
<img src="./os/windows/windowsme/images/tcpip_bindings_tab.gif" alt="Bindings Tab">
</p>
<p>
<h3>Advanced Tab</h3>
On the <b>Advanced Tab</b>, at the bottom, <b>Set this protocol to be the default protocol</b> should be checked.
Click <b>OK</b>.
</p>
<p>
<img src="./os/windows/windowsme/images/tcpip_advanced_tab.gif" alt="Advanced Tab">
</p>
<p>
<h3>Network Window - Primary Network Logon</h3>
This should take you back to the <b>Network Window</b>, under <b>Primary Network 
Logon</b> select <b>Windows Logon</b>, then click <b>OK</b>. <b>Windows</b> should copy files
for a moment, then ask you to <b>Restart</b>. Click <b>Yes</b> to restart. When windows <b>reboots</b> 
your network should be configure. 
</p>
<p>This completes configuring <b>TCP/IP</b>.  <b>Next see: </b> <a href="?content=./os/windows/windowsme/add_components.php">Adding Components</a></p>
<p>
<img src="./os/windows/windowsme/images/network_window_pnl.gif" alt="Primary Network Logon(Windows Logon)">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->