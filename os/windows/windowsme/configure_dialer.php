<h1>Configure Dialer</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windowsme/main.php">Windows ME</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
To configure a dialer the dialer must first be created. If one has not been created please see
<a href="?content=./os/windows/windowsme/create_dialer.php">Create Dialer</a>.
</td></tr>
</table>
</p>
<p>
<h3>Locate Dialer</h3>
To configure a dialer, locate its icon in the <b>Dail-UP Networking</b> window, 
right click on it to display the pop-up memu and select <b>Properties</b> or you 
can select the icon by clicking on it once, select <b>File</b> then <b>Properties</b>
to open the <b>Properties</b> page. 
</p>
<p>
<img src="./os/windows/windowsme/images/dialup_networing_properties.gif" alt="New Connection">
</p>
<p>
<h3>General Tab</h3>
The <b>General Tab</b> should display, the <b>Telephone number</b> box should contain the
<b>Local Modem Access Phone Number</b> that the customer typed in when creating this 
dialer. The <b>Connect using</b> section should display the current modem that this dialer
is using to connect with. To configure additional settings for this modem click the 
<b>Configure...</b> button.
</p>
<p>
<img src="./os/windows/windowsme/images/general_tab.gif" alt="General Tab.">
</p>
<p class="note">
If the customer does not have their access number or their <b>Account Information Card</b>, notify
an <b>LT</b> to get permission to send them to <b>Customer Service</b> to get that information.
</p>
<p>
<h3>Confiure - General Tab</h3>
The modem's <b>Properties Widnow</b> should display with 5 tabs. The first tab is
the <b>General Tab</b>. This tab displays the modem's <b>Port</b>, <b>Speaker volume</b>
and <b>Maximum speed</b>. The <b>Port</b>, <b>Speaker volume</b> in most cases should
remain unchanged, but the <b>Maximum speed</b> should be set at or below the true speed
of the modem installed.
</p>
<p>
<img src="./os/windows/windowsme/images/configure_general_tab.gif" alt="Confiure - General Tab">
</p>
<p>
<h3>Configure - Connection Tab</h3>
The <b>Connection Tab</b> displays the <b>Connection</b> and <b>Call preferences</b>, these
settings in most cases remain unchanged. For additional setting click on the <b>Port Settings...</b>
button.
</p>
<p>
<img src="./os/windows/windowsme/images/configure_connection_tab.gif" alt="Configure - Connection Tab">
</p>
<p>
<h3>Advanced Port Settings</h3>
Clicking the <b>Port Settings...</b> button displays the <b>Advanced Port Settings</b> window.
This window displays the <b>Receive</b> and <b>Transfer</b> buffers of this modem. These
settings should be changed only if you are told to do so by an <b>LT</b>, otherwise they should 
remain unchanged. 
</p>
<p>
<img src="./os/windows/windowsme/images/advanced_port_settings.gif" alt="Advance Port Settings">
</p>
<p>
<h3>Advanced Connection Settings</h3>
Clicking the <b>Advanced</b> button displays the <b>Advanced Connection Settings</b> window. 
The <b>Use error control</b>, <b>Compress data</b> and <b>Use flow control</b> boxes should be checked.
<b>Modualation type</b> should be set to <b>standard</b> and the <b>Extra settings</b> box is were you should
enter <a href="http://www.modemhelp.org/inits/" target="blank"><b>Init Strings</b></a>. 
</p>
<p>
<img src="./os/windows/windowsme/images/advanced_tab.gif" alt="Advance Connection Settings">
</p>
<p>
<h3>Configure - Options Tab</h3>
The <b>Options Tab</b> displays the <b>Connection control</b> section, the <b>Bring
up terminal window after dialing</b> box is used to set a customer up with a <b>Terminal Dialer</b>.
The <b>Dial control</b> and <b>Status control</b> sections should remain unchanged.  
</p>
<p>
<img src="./os/windows/windowsme/images/configure_options_tab.gif" alt="Configure - Options Tab">
</p>
<p>
<h3>Configure - Destinctive Ring Tab</h3>
The <b>Destinctive Ring Tab</b> is used to enable and configure the <b>Destinctive Ring</b>
service if enabled on this line. This setting is almost never used. 
</p>
<p>
<img src="./os/windows/windowsme/images/configure_destinctive_ring_tab.gif" alt="Configure - Destinctive Ring Tab">
</p>
<p>
<h3>Configure - Forward Tab</h3>
The <b>Forward Tab</b> is used to enable or disable the <b>Call Forwarding</b>
service if enabled on this line. This setting is almost never used. 
</p>
<p>
<img src="./os/windows/windowsme/images/configure_forward_tab.gif" alt="Configure - Forward Tab">
</p>
<p>
<h3>Networking Tab</h3>
The <b>Networking Tab</b> displays the <b>Type of Dail-Up Server</b>, which should be set to
<b>PPP: Internet, Windows 2000,Windows NT,Windows ME</b>. Only <b>Enable software compression</b>
should be check in the <b>Advanced options</b> section. Only one box need to be checked in the 
<b>Allowed network protocols</b> section is <b>TCP/IP</b>. Click the <b>TCP/IP</b> button for addtional
settings.
</p>
<p>
<img src="./os/windows/windowsme/images/networking_tab.gif" alt="Networking Tab">
</p>
<p>
<h3>TCP/IP Properties</h3>
The <b>TCP/IP Properties</b> window should display, this window allows you to specify 
an <b>IP Address</b> and <b>Name Server Addresses</b>, in most cases the 
<b>Server assigned</b> settings for both should be selected. The two boxes at the
bottom are normally checked.
</p>
<p>
<img src="./os/windows/windowsme/images/tcpip_properties.gif" alt="TCP/IP Properties">
</p>
<p>
<h3>Security Tab</h3>
The <b>Security Tab</b> allows you to enter your <b>user name</b> and <b>password</b>
along with other settings. In most cases this screen should remain blank. 
</p>
<p>
<img src="./os/windows/windowsme/images/security_tab.gif" alt="Security Tab">
</p>
<p class="note">
If a customer's computer is dialing automaticly on boot without prompting for <b>user/pass</b>,
check this tab to see if the <b>Connect automaticlly</b> box is check.
</p>
<p>
<h3>Scripting Tab</h3>
The <b>Script file</b> section's <b>File name</b> box should be blank in most all cases,
If this box contains a file name or any text please clear it. The <b>Start terminal window
minimized</b> should be checked.   
</p>
<p>
<img src="./os/windows/windowsme/images/scripting_tab.gif" alt="Scripting Tab">
</p>
<p>
<h3>Multilink Tab</h3>
This tab is almost never used and is almost always blank. 
</p>
<p>
<img src="./os/windows/windowsme/images/multilink_tab.gif" alt="multilink Tab">
</p>
<p>
<h3>Dialing Tab</h3>
This tab controls dailing behavior of the modem. <b>Default</b> settings are fine here. 
</p>
<p>This completes configuring a dialer. <b>Next see:</b> <a href="?content=./os/windows/windowsme/create_shortcut.php">
Create a Shourcut</a> 
</p>
<p>
<img src="./os/windows/windowsme/images/dialing_tab.gif" alt="Dailing Tab">
</p>
<p class="note">If a customer complains about getting disconnected after being online 
for 20 mins while reading e-mail or a webpage, uncheck the <b>Enable idle disconnect</b> box.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

