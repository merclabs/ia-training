<h1>Configure Network</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/main.php">Windows 2000</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
To configure Network settings for a dial-up connection. 
</td></tr>
</table>
</p>

<p>
<h3>Accessing Network Connections and Dial-Up Connecitons</h3>
If you are not already in the <b>Network Connections Window</b>, click on the <b>Start</b> button
then <b>Network and Dial-Up Connections</b>, right click on the dialer and click on <b>Create Shortcut</b>.
</p>
<p>
<img src="./os/windows/windows2000/images/create_shortcut.gif" alt="Create Shortcut">
</p>
<p class="note">
This will create a shortcut on the <b>Desktop</b>. <b>Windows</b> does not comfirm that 
a connection was created. You must check the <b>Desktop</b> to make sure that a connection
was indeed created there for you by <b>Windows</b>. 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
