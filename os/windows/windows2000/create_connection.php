<h1>Create a new Dial-Up Connections</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/main.php">Windows 2000</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
Before creating a new dialer, you should make sure the customer has the following:<br>
<ul>
<li>An active internet account.</li>
<li>All account information(on Account Information Card obtained from Provider.)</li>
<li>Local modem access number for area.</li>
</ul>
</td></tr>
</table>
</p>
<p>
<h3>Accessing Network Connections and Dial-Up Connecitons</h3>
If you are not already in the <b>Network Connections Window</b>, click on the <b>Start</b> button
then select <b>Network and Dial-Up Connections</b>.
</p>
<p>
<img src="./os/windows/windows2000/images/newtwork_connections-menu.gif" alt="Start Menu">
</p>
<p>
<h3>Network and Dial-Up Connections</h3>
The <b>Network and Dial-Up Connections</b> window should display. If a dial-up connection 
has not already been created, you should see two icons, <b>Make New Connection</b> and 
<b>Local Area Connection</b>. Double click on <b>Make New Connection</b>.  
</p>
<p>
<img src="./os/windows/windows2000/images/network_dialup_window_no_dialer.gif" alt="Network and Dial-Up Connections">
</p>
<p>
<h3>Network Connection Wizard</h3>
This should start the <b>Network Connection Wizard</b>, click <b>Next ></b> to Continue...
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_1.gif" alt=" ">
</p>
<p>
<h3>Network Connection Type</h3>
Select <b>Dial-up to the internet</b>, then click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_2.gif" alt=" ">
</p>
<p>
<h3>Network Connection Wizard</h3>
Then select <b>I want to set up my Internet connection manually...</b>, then click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_3.gif" alt=" ">
</p>
<p>
<h3>Set Up your Internet connection</h3>
Select <b>I connect through a phone line and a modem.</b>, then click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_4.gif" alt=" ">
</p>
<p>
<h3>Choose a Modem</h3>
Select the <b>current modem</b> thats installed on this PC, then click <b>Next ></b>. 
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_5.gif" alt=" ">
</p>
<p class="note">
In most cases the <b>current modem</b> should be selected, but if the computer has more than
one modem installed, all modems should be listed here. Select the modem that you plan to
use to connect to the internet. 
</p>
<p>
<h3>Internet account connection information</h3>
The next steps requires the <b>Local Modem Access Number</b>. This number should be
on the <b>Account Information Card</b> that was obtained from the <b>Provider</b>.
If no area code is needed, uncheck the <b>Use area code and dialing rules</b> box, then
click <b>Next ></b>. 
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_6.gif" alt=" ">
</p>
<p class="note">
If the customer does not have their <b>Account Information Card</b> or their 
<b>Local Modem Access Number</b>. Notify an <b>LT</b> to get <u>permission</u> to send
them to <b>Customer Service</b> to get that information.
</p>
<p>
<h3>Advanced Button - Advance</h3>
For addtional settings click on the <b>Advanced...</b> button. In most cases settings
here do not need to be changed.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_ncw.gif" alt=" ">
</p>
<p>
<h3>Advanced Button - Addresses</h3>
In most cases settings here do not need to be changed unless <b>DNS Server</b> settings are
needed.
</p>
<p>
<img src="./os/windows/windows2000/images/addresses_ncw.gif" alt=" ">
</p>
<p>
<h3>Internet account logon information</h3>
Enter the <b>User Name</b> and <b>Password</b>, this information
may be on the <b>Account Information Card</b> that was obtained from the <b>Provider</b>.
Then click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_7.gif" alt=" ">
</p>
<p class="note">
If the customer does not have this information, notify an <b>LT</b> to get permission to 
send to <b>Customer Service</b> to get this information.
</p>
<p>
<h3>Connection Name</h3>
Enter a <b>Name</b> for this connection, this name will server as a <b>label</b> for this dial-up
connection. Then click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_8.gif" alt=" ">
</p>
<p>
<h3>Your Internet Mail Account</h3>
This page asks if you want to set up your <b>Internet Mail Account</b> now. You can select <b>Yes</b>
if you wish to set up <b>Outlook</b> for mail, but if you are just creating a new connection,
select <b>No</b> and click <b>Next ></b>.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_9.gif" alt=" ">
</p>
<p>
<h3>Completing the Internet Connection Wizard</h3>
This should complete the wizard, click <b>Finish</b> to exit the wizard.
</p>
<p>
<img src="./os/windows/windows2000/images/ncw_10.gif" alt=" ">
</p>
<p>
<h3>Network and Dial-Up Connection Window - New Connection</h3>
You should now see a new icon in the <b>Network and Dial-Up Connection Window</b>, labeled
according to the the name you entered as the <b>Connection Name</b>.
</p>
<p>
<b>Next see:</b> <a href="?content=./os/windows/windows2000/configure_connection.php">Configure Connection</a>
</p>
<p>
<img src="./os/windows/windows2000/images/network_and_dialup_connection_window.gif" alt=" ">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->