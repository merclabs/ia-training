<h1>Configure Connection</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/main.php">Windows 2000</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
After a connection has been created, you must configure it. 
</td></tr>
</table>
</p>

<p>
<h3>Accessing Network and Dial-Up Connecitons</h3>
If you are not already in the <b>Network Connections Window</b>, click on the <b>Start</b> button
then <b>Network and Dial-Up Connections</b>, right click on the dialer and click on <b>Properties</b>.
</p>
<p>
<img src="./os/windows/windows2000/images/dialer_properties.gif" alt="Properties">
</p>
<p>
<h3>Connection Properties - General Tab</h3>
The connections properties window should display. The <b>General Tab</b> is the default tab
that shows. This tab shows the <b>current modem</b> being used and the <b>Phone number</b> of the 
conputer being dialed. If <b>Use dialing rules</b> is checked, please uncheck it.
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_general_tab.gif" alt="">
</p>
<p>
<h3>Configure Button</h3>
Clicking on the <b>Configure</b> button opens the <b>Modem Configuration</b> window. This window
displays the <b>Maximum speed</b>, <b>Hardware Features</b> and <b>Initialization</b>.
</p>
<p> 
Set the <b>Maximum speed</b> at or below the speed of the modem, all <b>three</b> boxed should be checked in the 
<b>Hardware Features</b> section. Click <b>OK</b> to close this window.
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_configure.gif" alt="">
</p>
<p>
<h3>Connection Properties - Options Tab</h3>
Next click on the <b>Options Tab</b>, the default settings on this screen do not need 
to be changed. 
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_option_tab.gif" alt="">
</p>
<p>
<h3>Options Tab - X.25 Button</h3>
Clicking on the <b>X.25</b> button at the bottom displays the <b>X.25 Logon Settings</b>
window. The default settings on this screen do not need to be changed. Click <b>OK</b> to 
close this window.
</p>
<p>
<img src="./os/windows/windows2000/images/x25_settings.gif" alt="">
</p>
<p>
<h3>Security Tab</h3>
Click on the <b>Security Tab</b>. This displays the <b>Security options</b>, <b>Typical</b>
should be the only thing selected, all other setting should <u>not</u> be checked. 
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_security_tab.gif" alt="">
</p>
<p>
<h3>Networking Tab</h3>
On the <b>Networking Tab</b>, the first box should be set to
<b>PPP:Windows 95/98/NT4/2000,Internet</b>. Only <b>Internet Protocol(TCP/IP)</b> and <b>Client for
Microsoft Networks</b> should be checked.
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_networking_tab.gif" alt="">
</p>
<p>
<h3>Networking Tab - Settigns</h3>
Clicking on the <b>Settings</b> button displays the <b>PPP Settings</b> window. All boxes
need to be check on this window. 
</p>
<p>
<img src="./os/windows/windows2000/images/ppp_settings.gif" alt="">
</p>
<p>
<h3>Sharing Tab</h3>
Nothing should be check on the <b>Sharing Tab</b>. 
</p>
<p>
This completes configuring a connection <b>Next see:</b> <a href="?content=./os/windows/windows2000/configure_network.php">Configure Network Settings</a>.
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_sharing_tab.gif" alt="">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->