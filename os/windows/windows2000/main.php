<h1>Windows 2000</h1>
<p align="center">
<img src="./os/windows/windows2000/images/splash.jpg" alt="Windows 2000">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/create_connection.php">Create Connection</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/configure_connection.php">Configure Connection</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/configure_network.php">Configure Network Settings</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/create_shortcut.php">Create a Shortcut</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/configure_modem.php">Configure Modem</a></td></tr>

</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
<p>
<b>Windows 2000</b>
<ul>
<li><b>File:</b> Winsock.dll <b>Size:</b> 3KB  
<li><b>File:</b> Wsock32.dll <b>Size:</b> 22KB 
</ul>
</p>
<p>
<b>Location:</b> c:\winnt\system32
</p>
</td></tr>
</table>
</p> 
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->