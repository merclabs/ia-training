<h1>Configure Modem</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/main.php">Windows 2000</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If you are not already in the <b>Control Panel</b> click on <b>Start</b>, <b>Settings</b> then
<b>Control Panel</b>.
</td></tr>
</table>
</p>
<p>
<h3>Phone and Modem Options</h3>
Click on the <b>Phone and Modem Options</b> icon. 
</p>
<p>
<img src="./os/windows/windows2000/images/phone_modem_icon.gif" alt="">
</p>
<p>
<h3>Modem Tab</h3>
The <b>Modem Tab</b> displays the <b>current modem</b> installed on the PC. To
configure this modem, click the <b>Properties</b> button. 
</p>
<p>
<img src="./os/windows/windows2000/images/phone_and_modem_options.gif" alt="">
</p>
<h3>Modems Properies - General Tab</h3>
Another <b>Properties</b> window with three tabs should display. The <b>General Tab</b>
is the default tab to display. <b>Speaker volume</b> can be set to a customer desired level,
<b>Maximum Port Speed</b> should be set at or below the speed of the modem, <b>Dial Control</b>
setting should always be checked.
</p>
<p>
<img src="./os/windows/windows2000/images/properties_general_tab.gif" alt="">
</p>
<p>
<h3>Modems Properies - Modem Diagnostic Tab</h3>
The <b>Diagnostic Tab</b> allows you to run diagnostics on the modem installed to 
see if it functions properly. Click the <b>Query Modem</b> button to run diagnostics.
</p>
<p>
<img src="./os/windows/windows2000/images/properties_diagnostic_tab.gif" alt="">
</p>
<p>
<h3>Modems Properies - Query Modem</h3>
A status window should display for a moment...
</p>
<p>
<img src="./os/windows/windows2000/images/query_modem.gif" alt="">
</p>
<p>
<h3>Modems Properies - Modem Diagnostic Tab</h3>
Once the query is complete, information should display in the <b>Commad | Response</b>
sections, indicating that the modem is functions properly.
</p>
<p>
<img src="./os/windows/windows2000/images/properties_diagnostic2_tab.gif" alt="">
</p>
<p class="note">
If you get any other window or an error message, then the modem may need to be removed or
reinstalled. 
</p>
<p>
<h3>Modems Properies - Modem Advanced Tab</h3>
On the <b>Advanced Tab</b>, the <b>Extra initilization commands</b> box is were
you enter initilization commands(<b><a href="http://www.modemhelp.org/inits/" target="_blank">init strings</a></b>).
</p>
<p>
<img src="./os/windows/windows2000/images/properties_advanced_tab.gif" alt="">
</p>
<p>
<h3>Dialing Rules Tab</h3>
In most cases <b>Dailing Rules</b> do not need to be changed. 
</p>
<p>
<img src="./os/windows/windows2000/images/dialing_rules.gif" alt="">
</p>
<p>
<h3>Advanced Tab</h3>
Nothing on the <b>Advanced Tab</b> should be changed.
</p>
<p>
This completes the configuring the modem.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced.gif" alt="">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->