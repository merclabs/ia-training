<h1>Configure Network Settings</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./os/windows/windows2000/main.php">Windows 2000</a></td></tr>
</td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
To configure Network settings for a dial-up connection it first must exist. 
</td></tr>
</table>
</p>

<p>
<h3>Accessing Network Connections and Dial-Up Connecitons</h3>
If you are not already in the <b>Network Connections Window</b>, click on the <b>Start</b> button
then <b>Network and Dial-Up Connections</b>, right click on the dialer and click on <b>Properties</b>.
</p>
<p>
<img src="./os/windows/windows2000/images/dialer_properties.gif" alt="Properties">
</p>
<p>
<h3>Connection Settings</h3>
The connection settings window should display. Select <b>Internet Protocol(TCP/IP)</b> and click the
<b>Properties</b> button.
</p>
<p>
<img src="./os/windows/windows2000/images/myconnection_networking_tab.gif" alt="">
</p>
<p>
<h3>TCP/IP Settings</h3>
The <b>TCP/IP Settigns</b> window should display. In most cases <b>Obtain an IP addres
automaticlly</b> and <b>Obtain DNS server address automaticlly</b> should be checked, unless
DNS settings are needed. For additional <b>TCP/IP</b> settings click on the <b>Advanced...</b> button.
</p>
<p>
<img src="./os/windows/windows2000/images/internet_protocol.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - General Tab</h3>
The <b>Advanced TCP/IP Settings</b> window should display. The default settings for the 
<b>General Tab</b> are fine.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_tcp_general.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - DNS Tab</h3>
Click on the <b>DNS Tab</b>, default settings are <b>OK</b> here, unless adding <b>DNS</b> addresses.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_tcp_dns.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - Add DNS</h3>
If so, click the <b>Add</b> button to add <b>DNS</b> information, then click <b>Add</b> again.
</p>
<p>
<img src="./os/windows/windows2000/images/add_dns.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - WINS Tab</h3>
Click on the <b>WINS Tab</b>, default settings are <b>OK</b> here, unless adding <b>WINS</b> addresses.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_tcp_wins.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - Add WINS</h3>
If so, click the <b>Add</b> button to add <b>WINS</b> information then click <b>Add</b> again.
</p>
<p>
<img src="./os/windows/windows2000/images/add_wins.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - Options Tab</h3>
The <b>Options Tab</b> displays the <b>IP Security</b> settings for all connections using
<b>TCP/IP</b>.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_tcp_options.gif" alt="">
</p>
<p>
<h3>Advanced TCP/IP Settings - IP Security</h3>
Clicking on the <b>Properties</b> button displays the <b>IP Security Settings</b> window.
This window allow you to select which <b>Security Policy</b> to use for all TCP connections. In most cases, default settings
are <b>OK</b>. 
</p>
<p>
This completes configuring network settings for a <b>Dial-Up Connection</b>, <b>Next see:</b> <a href="?content=./os/windows/windows2000/configure_modem.php">Configure Modem</a>.
</p>
<p>
<img src="./os/windows/windows2000/images/advanced_tcp_properties_ip_security.gif" alt="">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->