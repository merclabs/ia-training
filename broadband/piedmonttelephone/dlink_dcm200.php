<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>D-Link DCM-200</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>D-Link DCM-200</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Powering Up the First Time</b>
<p>
You must allow at least 1 to 3 minutes to power up the first time because the DCM-200 must find and secure a
connection.
</p>
<b>Important Notice:</b>
<ul>
<li>The DCM-200 USB/Ethernet Cable Modem can only work with either the Ethernet port or the USB port at
one time. They <b>CANNOT</b> work simultaneously.</li>
<li>If you connect both the USB port and Ethernet port to different PCs, only the PC connected to the USB port
will be able to get the IP address and establish a link onto the Internet.</li>
<li>If you first connect the DCM-200 to the Ethernet port on your PC and then later connect the USB port, the
Ethernet connection will be dropped and the DCM-200 will begin the process of finding a downstream
channel to lock into by the USB connection. At this juncture the DCM-200 Status LED will start blinking
quickly. Once a channel has been secured, the LED will begin to blink slowly, until the connection has been
established. At that point, the LED will remain lit.</li>
<li>If you have established a connection between the DCM-200 and a USB port on a PC and then later decide to
plug the USB cable into a USB port on another PC, the DCM-200 will restart.</li>
<li>If you shift the Ethernet cable from one PC to another PC, the second PC will not be able to link onto the
Internet until you reboot the DCM-200 by unplugging the power cord and then reinserting it into the
DCM-200.</li>
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_dlink_dcm200.jpg" border="0">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table width=100%>
<tr><td class="infohd">LEDs</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">Power</td><td class="infoline">This LED is lit red when the device is receiving power; otherwise, it is unlit.</td></tr>
<tr><td class="infohd">Link</td><td class="infoline">This LED is lit green to indicate that a valid connection exists between the Ethernet port on the Cable
Modem and your PC. If it is unlit, there is no valid connection.</td></tr>
<tr><td class="infohd">Act (Activity)</td><td class="infoline">A blinking green LED indicates that traffic activity is passing through your cable modem
port.</td></tr>
<tr><td class="infohd">Status</td><td class="infoline">The Status LED will be �ON� solid when the cable modem is properly connected to the cable network
(ISP). *If your status light continues to blink, please contact your cable operator for further instructions.</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_dlink_dcm200.jpg" border="0">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table width="100%">
<tr><td class="infohd">Port</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">AC Power Connector</td><td class="infoline">For the included power adapter, if you use a power adapter other than the one included
with the product, please make sure it has a DC output of 12V/1A.</td></tr>
<tr><td class="infohd">Ethernet 10BASE-T Port</td><td class="infoline">The 10BASE-T Ethernet port accepts Category 5 or better UTP cabling with an RJ-
45 connector used to connect the DCM-100 to a LAN device (hub, switch, PC, etc.).</td></tr>
<tr><td class="infohd">Cable In</td><td class="infoline">This jack is used to connect the DCM-200 to the splitter. This connection is achieved using a length of
cable TV wire supplied by your cable company during installation.</td></tr>
<tr><td class="infohd">USB Port</td><td class="infoline">Connect this port on the DCM-200 directly to your PC.</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/basic_setup_dlink_dcm200.png" border="0">
</p>
</td>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr>
 <td class="infohd">Problem</td>
 <td class="infohd">Possible Solution</td>
</tr>
<tr>
 <td class="infohd" valign="top">Cannot access Internet or e-mail service.</td>
 <td class="infoline">Check all connections.
<ul>
<li>Make sure the cable TV line and the Ethernet (RJ-45) cable is securely connected at all points.</li>
<li>Make sure the AC power adapter is plugged into the device and the power source. Check the LED indicators to see that the Power, Link and Status indicators are steadily lit for an
Ethernet-connected device.</li>
</ul>
Contact your cable service operator to verify that your account is up to date and that you have two-way
service.
There may be a problem with your NIC. Refer to the literature for the NIC to find a possible solution.
The TCP/IP configuration for the computer may be incorrect. Double-check to see that these network
settings comply with the instructions given by the cable service operator. Call your cable company to
make sure they have given you the proper settings.
Check your web browser configuration per ISP instruction.
</td>
</tr>
<tr>
 <td class="infohd" valign="top">Status LED indicator blinks continuously.</td>
 <td class="infoline">
 Check the cable line connection. If this is secure then there may be a weak signal coming from the
central office. Report the problem to your cable operator if the cable connection appears to be OK.
 </td>
</tr>
<tr>
 <td class="infohd" valign="top">LED indicators appear to be normal but Internet access can not be established.</td>
 <td class="infoline">
 Try to establish a new communication with your cable operator�s central office. This can be accomplished
by powering off the computer and turning it back on.
<br><br>
The TCP/IP configuration for the computer may be incorrect.
 </td>
</tr>
<tr>
 <td class="infohd" valign="top">The PC Link light is not green. The light is off.</td>
 <td class="infoline">The modem is not detecting the Network Interface Card. Make sure the cables are firmly attached to the
network card in the back of your computer and the modem as well.
<br><br>
Try replacing the Ethernet cable or the network card with known working components.
Run diagnostic program that came with your network card.
<br><br>
If this is a card that we support, troubleshoot the NIC. Otherwise, contact the organization that installed
or manufacturers the card.</td>
</tr>
<tr>
 <td class="infohd" valign="top">The power light is not on.</td>
 <td class="infoline">
 Check to make sure the power cable is firmly plugged into the cable modem. Also, make sure that the
power converter is plugged into a functional socket.
Change outlets with one you know to have power.
Replace Cable Modem.
 </td>
</tr>
</table>
<br>




</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>