                                                                                                                                                                                     er/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5000 Series DSL Modems</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5000 Series DSL Modems</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Ben Lomand uses Efficient Network SpeedStream 5000 series modems, the models used are
the SpeedStream <b>5200, 5260, 5360, and the 5667B</b>. 
</p>
<p>
They have several modems that they currently sell to customers. 
The <b>5100</b> and <b>5200</b> Efficient modems are Benlomand currently sells to most home users.
These may be upgraded to a router with the purchase of a software key and some of them 
are used in <b>Router</b> mode.
</p>
<p>
They send out these modems pre-configured. Provided the customer has not changed the configuration
or the configuration has been corrupted, otherwise there are no configurations necessary on these modem.
Most customers are connect via <b>Static IP</b>.
</p>
<p>
Tech Support will encounter the following modems/router that are <b>discontinued</b> by Efficient Networks.
<ul>
<li>SpeedStream 5260</li> 
<li>SpeedStream 5360</li>
<li>SpeedStream 5660(router)</li> 
<li>SpeedStream 5667B (bridge)</li>
<li>SpeedStream 5667R (router)</li>
<li>SpeedStream 3060 (internal PCI adapter)</li>
<li>SpeedStream 4060 (USB only)</li>
</ul>
</p>
<p>
With DSL Service, you can talk on your telephone and surf the Internet at the same 
time. The ability to do this requires the use of microfilters. Microfilters filter out 
static, caused by data transmission, so that your phone will be free of unwanted noise during a conversation.
To install the in-line microfilter, simply connect the cord from your telephone, fax , or answering machine to the 
in-line filter jack, then connect the cable from the filter to the telephone wall jack.
</p>
<img src="./images/dslphonefilter.png" border=0>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><A name="section2">USB Software</a></b>
</td>
</tr>
<tr>
<td class="field" valign=top><B>Install Modem Software (USB)</B>
<p>
The only <b>software installation</b> is when customers are trying to use <b>USB</b>. 
The <b>USB drivers</b> for their particular modem must be installed.
<ol>
<li>If you have not already done so, start up your computer.<br> 
<b>Note: </b><font color="red"><b>Do not connect the modem yet!</b></font></li> 
<li>Insert the <b>Speedstream Installation CD</b> into your CD-ROM drive.</li>
<li>Click the "<b>Start</b>" button on the Windows task bar.</li>
<li>Select "<b>Run</b>" from the <b>Start Menu</b></li>
<li>Type "<b>d:\setup.exe</b>" where "<b>d</b>" represents your CD-ROM.</li>
<li>Click the "<b>OK</b>" button</li>
<li>The software installation program will start. Follow the onscreen directions to 
complete the software installation. When prompted to attach the modem.</li>
</ol> 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panels</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_speedstream_series.jpg" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<b>Status Lights on front of DSL Modems:</b><br>
<table>
<tr><td class="infohd">Modem</td><td class="infohd"><div align="center">Lights</div></td></tr>
<tr><td class="infohd">5100</td><td class="infoline">Power, Ethernet, DSL, Activity</td></tr>
<tr><td class="infohd">5200</td><td class="infoline">Power, DSL, USB, Enet</td></tr>
<tr><td class="infohd">5861</td><td class="infoline">Power, Test, Link, Wan, LanT, LanR</td></tr>
</table>

<br><br>
<table>
<b>Older Modems</b>
<tr><td class="infohd">Modem</td><td class="infohd"><div align="center">Lights</div></td></tr>
<tr><td class="infohd">5260</td><td class="infoline">SYS (power), ATM, DSL, Enet</td></tr>
<tr><td class="infohd">5660</td><td class="infoline">SYS (power), ATM, DSL, Enet</td></tr>
<tr><td class="infohd">5360</td><td class="infoline">PWR, DSL, ACT, Enet</td></tr>
<tr><td class="infohd">5667B or R</td><td class="infoline">PWR, DSL, ACT, Enet, USB</td></tr>
</table>
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panels</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_speedstream_series.jpg" border=0>
</center>
</td>
<!--
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
-->
<tr>
 <td class="heading" valign=top><b><A name="section3">USB Setup</a></b></td>
</tr>
<tr>
<td class="field" valign=top>
<b>Dual Ethernet/USB Models:</b><br>
<ul>
<li>5200</li>
<li>5667B</li>
<li>5667R</li>
</li>
</ul>

<ul>
<li>If used in <b>Bridge</b> mode, only one port may be used at any given time.</li>
<li>If used in <b>Router</b> mode, then both ports may be used at the same time.</li> 
<li><b><font color="red">IMPORTANT:</font> Do not install a Microfilter on this line!</b></li>
</ul>
<p>
The Ethernet and dual Ethernet/USB models will work with any operating system that 
will support static IP addressing and a NIC card. The <b>USB</b> modems will only work with 
<b>USB</b> capable systems (Win98 and higher). <b>MAC OS</b>, and others such as <b>Linux, Unix</b> 
will work Ethernet ONLY. <b>THEY NO LONGER SELL ANY USB ONLY MODEMS</b>.<br>
</p>
<p>
</p>
<p>
<ol>
 <li>If you have not completed "<b>Install Modem Software (USB)</b>", please do so now.</li> 
 <li>Plug the power adapter's cord into the PWR port on your modem. Plug the other end 
of the power cord into a power outlet. Push the power switch in, to the <b>ON</b> position.</li>
 <li>Plug one end of the telephone cable into the DSL port of the modem. Plug the other 
end of the cable into the wall jack that is providing your DSL connection.</li>
 <li>Plug the square end of the <b>USB</b> cable(black) into the Modem's USB port. Plug the rectangle 
end of the cable into your computer's USB port.</li>
</ol> 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><A name="section5">Ethernet Setup </a></b></td>
</tr>
<tr>
<td class="field" valign=top>
<b>Ethernet only models:</b>
<ul>
<li>5100</li>
<li>5260</li>
<li>5360</li>
<li>5660</li>
<li>5861</li>
</ul>
<p><ul><li>Connect the modem's power supply, telephone, and ethernet cables.</li>
<li><b><font color="red">IMPORTANT:</font> Do not install a Microfilter on this line!</b></li></ul></p>
<p><ol><li>If you have not completed "<b>Install Modem Software (USB)</b>", please do so now.</li> 
<li>Plug the power adapter's cord into the PWR port on your modem. Plug the other end of the power cord into a power outlet. Push the power switch in, to the <b>ON</b> position.</li>
<li>Plug one end of the telephone cable into the DSL port of the modem. Plug the other end of the cable into the wall jack that is providing your DSL connection.</li>
<li>Plug one end of the gray <b>Ethernet</b> cable into the modem's 10baseT port.</li>
</ol>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top>
<p>
<ul>
<li><b><font color="red">IMPORTANT:</font></b>
Check and configure your <b>TCP/IP Network</b> settings so that you can connect your modem.</i></li>
</ul>
</p>
<p>
<b>Windows 98/Windows ME</b>
<ol>
<li>Click the Windows "<b>Start</b>" button.</li>
				 <ul>
				 <li>Select "<b>Settings</b>".</li>
				 <li>Click "<b>Control Panel</b>".</li>
				 <li>Double click the "<b>Network</b>" icon.</li>
				 </ul>
<li>On the "<b>Configuration</b>" tab, check to see if there is a <b>TCP/IP</b> bound to the <b>Effcient 
Networks USB/Ethernet ADSL Modem adapter</b>.
</li>
<li>Select the <b>TCP/IP</b> entry for the <b>Effcient Networks USB/Ethernet ADSL Modem adapter</b> 
and then click "<b>Properties</b>".
</li>
<li>
On the "<b>IP Address</b>" tab, make sure the button for "<b>Specify IP Address</b>" is selected.<br>
Enter following information provided by <font color="red">BenLomand</font>  
  <ul>
	 <li><b>Static IP Address</b></li>
	 <li><b>Gateway</b> and <b>Subnet Mask</b></li>
  </ul>
</li>
<li>Click the "<b>DNS Configuration</b>" tab. Make sure the button for "<b>Enable DNS</b>" is selected. 
 <ul>
  <li>Enter <b>Host Name</b>(customers user name if known)</li>
  <li><b>Domain</b>(blomand.net)</li>
  <li>Primary DNS <b>64.53.83.131</b></li>
  <li>Secondary DNS <b>206.74.254.2</b></li>
 </ul>
</li>
<li>Click the "<b>WINS Configuration</b>" tab. Make sure the button for "<b>Disable WINS Resolution</b>" is selected.
</li>
<li>Click the "<b>Gateway</b>" tab - enter the Gateway address and click "<b>Add</b>".</li>
<li>Click "OK" to close <b>TCP/IP</b> window.</li>
<li>Click "<b>OK</b>" to close the Network window</li>
<li>Windows may prompt you to <b>restart</b> you computer. If it does click "<b>Yes</b>". If it 
doesn't, close all windows and restart your computer by clicking the Windows "<b>Start</b>" 
button, selecting "<b>Shutdown</b>" and clicking "<b>Restart</b>".
</li>
</ol> 
</p>
<p>
<b>Win2000 & WinXP(Classic View).</b><br>
<ul>
<li>To switch <b>Windows XP</b> to "<b>Classic View</b>", right click on a blank space on the desktop 
and left then click "<b>Properties</b>". Click the "<b>Appearance</b>" tab and in the "<b>Windows and 
Buttons</b>" diaglog box select "<b>Windows Classic Style</b>".
</li>
</ul>
</p>
<p>
<ol>
 <li>Click the Windows "<b>Start</b>" button.</li>
 <li>Select "<b>Settings</b> then <b>Control Panel</b>(Windows 2000)", <b>Control Panel</b> on(Windows XP).</li>
 <li>Click "<b>Network and Dial-up Connections</b>on (Windows 2000)", <b>Network Connections</b> on (Windows XP).</li>
 <li>Look for the "<b>Efficient Networks USB Adapter</b>". The name may be different depending on which modem you have 
 but it will say something about Efficient Networks.</li>
 <li>Right click this <b>Icon</b> and select "<b>Properties</b>".</li>
 <li>Select "<b>TCP/IP</b>" and click the properties button.</li>
 <li>Enter following information provided by <font color="red">BenLomand</font>
    <ul>
		 <li><b>Static IP Sddress</b>
		 <li><b>Gateway</b> and <b>Subnet Mask</b></b></li>
     <li>Preferred DNS <b>64.53.83.131</b></li>
		 <li>Secondary DNS <b>206.74.254.2</b></li>
    </ul>
 <li>Click the "<b>Advanced</b>" button.</li>
 <li>Click the "<b>DNS</b>" tab.</li>
 <li>Look for the diaglog box "<b>Suffix for this domain</b>" and enter in "<b>blomand.net</b>"</li>
 <li>Click "<b>OK</b>" on each window to get back to the "<b>Network and Dial-up Connections</b>" window.</li>
 <li>Close this window and you should be ready to access the Internet.</li>
</ol>
</p>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Additional Troubleshooting</b></td>
</tr>
<tr>
<td class="field" valign=top>
<ol>
<li>
<b>Do you have dial tone on your set?</b><br>
If you do not have dial tone on any of your telephone sets within your house, please
call Ben Lomand Repair.
</li>
<li>
<b>Do you hear static on phone line?</b><br>
If you hear static on the telephones connected to the telephone line providing the Firewire 
DSL, check to make sure you have installed the microfilters correctly.
<br>

<b>Note:</b><i>The inline-micro-filters are directional, the <b>wall</b> side must be</i>
connected to the wall outlet, and the <b>phone</b> side must be the phone.
</li>
<li>
<b>The modem's PWR light does not light but the PWR-SW is pushed in.</b><br>
First, check that the outlet is powered by pluging another electrical device into it.
Next, check to make sure the connection to the modem's PWR power port is tight.
<li>
</li>
<b>The modem's DSL light is out.</b><br>
If the modem's DSL light is out after you have connected the modem to the phone line,
make sure the phone line is not connected to an in-line or wall mount microfilter. If 
the modem's DSL light still fails to light, please call your local Ben Lomand office.
<li>
</li>
<b>The modem's ENET light is out.</b><br>
If you are using the USB method of installation, make sure one end of the Ethernet
cable is securely attached to the modem's 10 BASR-T port and the other end is attached to
your computer's Ethernet port. Make sure you are using a straight through cable and not
a crossover cable. If the modem's ENET light still fails to light, please contact your 
local Ben Lomand office. 
</li>
<li>
<b>The modem's USB light is out.</b><br>
If you are using the Ethernet method of installation, this is normal. If you are using 
the USB method of installation, make sure one end is attached to to the modem's USB port 
the other end is attached to your computer's USB port. If the modem's USB light still 
fails to light, please call your local Ben Lomand office.
</li>
</ol>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>