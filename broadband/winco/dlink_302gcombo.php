<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>D-Link DSL-302G Combo ADSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>D-Link DSL-302G Combo ADSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_302gcombo.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Software Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
In order to install the software driver for the Modem, you first need to install the <b>DSL-300 Configuration Utility</b>,
D-Link�s GUI based management software. This software can later be used to monitor the device or change its
settings. Install the <b>Configuration Utility</b> software on the PC directly connected to the device via either the
<b>Ethernet</b> or <b>USB</b> interface.
<br><br>
<ol>
<li>
To install the Configuration Utilitity place the CD in the CD-ROM Drive. The <b>Setup 
Window</b> will prepare the <b>InstallShield Wizard</b>. Follow the instructions on the screen,
in most cases clicking <b>Next</b> a few times.
<br>
<img src="./images/config1.png" border="0">
<br><br>
</li>
 
<li>
Once complete the <b>Setup Window</b> presents you with the option to launch the <b>Configuration Utility</b>
program. Click <b>Finish</b>, and use the <b>Configuration Utility</b> icon on the desktop to start the program.
<br>
<img src="./images/config7.png" border="0">
<br><br>
</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Configure the Modem</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
With the <b>Configuration Utility</b> software installed, you are ready to configure the Modem. You must enter the
<b>VPI</b> and <b>VCI</b> values that have been given to you by your ADSL service provider to define the path of
connection to the ATM network backbone.
<p align="center">
<img src="./images/config8.png" border="0">
</p>
<font color="red">Click the <b>Configuration Utility</b> icon to initiate the configuration of the Modem. The Configuration Utility will
locate the Modem on your LAN.</font>
<ol>
<li>
If the <b>DSL-302G</b> and corresponding <b>MAC address</b> does not appear in the Device List of the Configuration
Utility window, click the <b>Discover</b> button on the bottom of the window. The <b>DSL-302G</b> should appear in
the <b>Device Name</b> column preceded by the <b>MAC Address</b> of the device. If there are other DSL-300 Family
Modems on the same network, these may also appear in the list.
<p align="center">
<img src="./images/config9.png" border="0">
</p>
</li>
<li>
Double-click on the <b>Modem</b> in the list or highlight it and depress the <b>Enter key</b>. The <b>Main Menu</b> appears,
click on the <b>General ADSL Information</b> tab to configure the Modem.
<p align="center">
<img src="./images/config10.png" border="0">
</p>
</li>
<li>
In the new window you must enter the <b>VPI</b> and <b>VCI</b> numbers (assigned to you by your service provider).
Type in these numbers in the ADSL Setting field and click Finish.
<p align="center">
<img src="./images/config11.png" border="0">
<br><br>
</p>

</li>
<li>
The <b>Save configuration and restart system</b> window will appear. You will be asked if you would like to <b>save</b>
the changes you have made (the VPI and VCI numbers) and <b>restart</b> the Modem. Click <b>Yes</b> if you have
correctly entered the necessary information.
<p align="center">
<img src="./images/config12.png" border="0">
</p>
</li>
<li>
The final window, <b>Configuration saved</b>, confirms that you have made changes to the configuration of the
Modem and restarted it. If you wish to close the Configuration Utility, click <b>Yes</b>.
<p align="center">
<img src="./images/config13.png" border="0">
</p>
</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Modem Diagnostics</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The <b>Configuration Utility</b> can be used to monitor the activity and performance of the Modem. The four status
windows are read-only with the exception of the General ADSL Information. Once you have set the VCI and
VPI values there should not be any need to change them.
</p>
<p>
 To check to see if the <b>DSL-302G</b> is connected
click on the <b>ADSL Status</b> tab to see information about the status of your connection, If the modem is connected
you should see <b>Green</b> dot in the <b>ADSL Link Information</b> section of the <b>ADSL Status</b>.
</p>
<p align="center">
<img src="./images/config14.png" border="0">
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>Pending discovery.</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>