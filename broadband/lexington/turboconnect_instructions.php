<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Lexcom TURBOConnect Instructions</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Lexcom TURBOConnect Instructions</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<h3>Lexcom Cable Modems are called TurboConnect</h3>
<p>
If you are talking to a <span style="color:red"><b>Comfort Suites</b></span> customer the network settings should 
be configured to <b>Obtain an IP Address Automatically</b>, <b>No Gateway</b> and <b>DNS</b> 
should be <b>disabled</b>.</p> For more information on Comfort Suites customers <a href="turboconnect_card.php">Click here</a>.
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Troubleshooting</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<p>
<b>Ask the customer the following questions:</b>
 <ul>
	<li>Are the lights on the cable modem green?</li>
	<li>Are there any amber lights?</li>
 </ul>
If there are <b>amber lights</b> flashing <b>power cycle</b> the modem and give it about <b>three minutes</b> to <b>reboot</b>. 
If there are still <b>amber lights</b> after power cycle, then the trouble is in the <b>cable line</b>. 
<br><br>
Then notify <b>LT</b>, 
get permission to <b>write up</b>.
<p style="color:red">
<b>NOTE:</b> If reported before 4:00 P.M. the report will be repaired
the same day. After 4:00 P.M report will be fixed next day. Repairs on Sunday done only if customer insists. 
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Questions to Ask</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>If all LEDs are Green</b>
<p>
 <ul>
	<li>Have you <b>reinstalled</b> Windows&reg;xx or is this a <b>new computer</b>?
	<br>If yes, they will need to enter their <b>IP Address, Gateway and DNS</b> Information
	obtained from Lexcom.
<br><br>
	<span style="color:red"><b>Note:</b> The <b>IP Address</b> can be found via your <b>LT or Supervisor</b>.</span> 
	<br><br>
	The <b>Gateway</b> is always the <b>IP Address</b> with the last digits replaced with a <b>1</b>.
	<br>
	<b>Example:</b> <i>IP Address 206.74.66.120  Gateway would be 206.74.66.1</i>
	<br><br>
	<li>Do you have the <b>IP Address</b> and  <b>Gateway Address</b> entered in the computer?
 </ul>
<br>
 </td>
<tr>
 <td class="heading" valign=top colspan=2><b>Setting Up the Network(Win95/Win98/Win2000/WinME)</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Enter the IP, Gateway, Subnet and DNS</b>
<ul>
	<li><b>Right click</b> on the <b>Network</b> icon in the desk top.</li>
	<li>Left click on <b>properties</b></li>
	<li><b>Double Left click</b> on <b>TCP/IP</b> for Ethernet card</li>
	<li>Click on, <b>Specify an IP Address</b>, 
	<li>Enter the <b>IP Address</b></li>
	<li>Enter the <b>Subnet mask: 255.255.255.0</b></li>
</ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Gateway Tab</b>
<ul>
 <li>Under <b>New Gateway</b> type the same address, but the last number will be a <b>1</b></li>
	<li>Click on <b>add</b></li>
	<li>Click on <b>DNS Configuration Tab</b></li>
	<li>Enable <b>DNS</b></li>
<li>Enter <b>Host Name</b>, This can be the computer name or just the customer's <b>user name</b></li>
	<li>Enter the <b>Domain Name, Lexcominc.net</b></li>
	<li>Enter the foloowing two <b>IP Addresses:</b><br>
	(these are the mail server addresses at Infoave)</li>
	<li>Type <span style="color:red;"><b>206.74.254.2</b></span></li>
	<li>Click on <b>add</b></li>
	<li>in the same box again type <span style="color:red;"><b>204.116.57.2</b></span></li>
	<li>Click on <b>add</b></li>
	<li>Then <b>OK</b></li>
</ul>	
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Advanced Tab</b>
  <ul>
   <li>Click on the <b>Advanced Tab</b></li>
	 <li>At the bottom, put a check in <b>Set this Protocol  to be the <i><b>Default</b></i>.</li>
	 <li>Then Click <b>OK</b>, then <b>OK</b> again</li>
  </ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Restart</b>
	<ul>
	 <li><b>RESTART COMPUTER</b></li>
  </ul>

<p>This should get them back on line. If not, notify an <b>LT</b>.</p> 
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>