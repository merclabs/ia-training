<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Com21 DOXport 1010 Cable Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Com21 DOXport 1010 Cable Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This modem is discontinued by Manufacturer., no longer being made, still supported 
by ISP's and us, standard Cable modem troubleshooting applies. <a href="turboconnect_instructions.php">
Click here</a> to see Instructions on setting up a Lexcom Cable Modem.
<p>
<b>Note:</b><br>
The Com21 <b>COMport 2000</b> and <b>DOXport 1010</b> Cable Modems as you can see cosmetically
are almost if not identitcal. 
</p>
<p>
The way to tell the if it is a <b>DOXport 1010 (DOCSIS)</b> and not a <b>COMport 2000(non-DOCSIS)</b>
is by the MAC id. If the <b>7th</b> character is a <b>6</b> it is a 
DOCSIS , for example ( 00:a0:73:6x:xx:xx ) then it 
is a DOCSIS. If not it is a <b>COMport 2000(non-DOCSIS)</b>. 
Pay attention to this it will help you dearly in 
determining the difference between the two.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_com21_1010.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The panel lights are light-emitting diodes (LEDs) that display the status of the 
modem. These lights indicate both normal and problematic conditions and are therefore used to diagnose communication problems. 
<br><br>
<!-- Beginning of lights description -->
<table width=100%>
<tr>
 <td class="infohd">LED</td>
 <td class="infohd">Color</td>
 <td class="infohd">State</td>
</tr>
<tr>
 <td class="infohd"  rowspan=2>PWR</td>
 <td class="infoline">Green</td>
 <td class="infoline">Power on.</td>
</tr>
<tr>
<td class="infoline">Off</td>
 <td class="infoline">Power off.</td>
</tr>
<tr>
 <td class="infohd" rowspan=4>ST/RF</td>
 <td class="infoline">Green</td>
 <td class="infoline">Cable Modem is registered / provisioned and ready to transfer data
.</td>
</tr>
<tr>
 <td class="infoline">Green-Amber-Off</td>
 <td class="infoline">Ready for provisioning.</td>
</tr>
<tr>
<td class="infoline">Blinking-Green</td>
 <td class="infoline">Modem blocked.</td>
</tr>
<tr>
<td class="infoline">Off</td>
 <td class="infoline">No downstream RF carrier present or power off.</td>
</tr>
<tr>
 <td class="infohd">TD</td>
 <td class="infoline">Blinking Green</td>
 <td class="infoline">Transmit data flickers when data is transmitted to the computer.</td>
</tr>
<tr>
 <td class="infohd">RD</td>
 <td class="infoline">Blinking Green</td> 
 <td class="infoline">Receive data flickers when data is received from the computer.</td>
</tr>
<tr>
 <td class="infohd" rowspan=2>CD/LNK</td>
 <td class="infoline">Green</td>
 <td class="infoline">Connectivity of ethernet connected to computer.</td>
</tr>
<tr>
 <td class="infoline">Off</td>
 <td class="infoline">Ethernet not connected to computer.</td>
</tr>
</table>
<!-- end of lights description -->
<br>
<!-- beginning of second lights description -->
<table width=100%>
<tr>
 <td class="infohd">LED</td>
 <td class="infohd">Stands for</td>
 <td class="infohd">Description</td>
</tr>
<tr>
 <td class="infohd">PWR</td>
 <td class="infoline">Power</td>
 <td class="infoline">Indicates that power is supplied to the DOXport 1010 modem</td>
</tr>
<tr>
 <td class="infohd">ST/RF</td>
 <td class="infoline">Radio Frequency</td>
 <td class="infoline">Indicates the status of the modem�s RF connection to the cable company�s data network. In normal operation, this light is solid green. This light is important in troubleshooting problems during modem operation.</td>
</tr>
<tr>
 <td class="infohd">TD</td>
 <td class="infoline">Transmit Data</td>
 <td class="infoline">Flickers to indicate that the modem is transmitting data from your computer�s Ethernet connection to the cable network. When large amounts of data are being transmitted, the light may appear to be solid green.</td>
</tr>
<tr>
 <td class="infohd">RD</td>
 <td class="infoline">Receive Data</td> 
 <td class="infoline">Flickers to indicate that the modem is receiving data from the network and transmitting it to your computer�s Ethernet connection. When large 
amounts of data are being transmitted, the light may appear to be solid green.</td>
</tr>
<tr>
 <td class="infohd">CD/LNK</td>
 <td class="infoline">Link Indicates</td>
 <td class="infoline">Indicates that the modem has a working connection with the Ethernet card in your computer.
The normal state for this light is solid green, which indicates that the connection is working. If your 
computer is off or the Ethernet cable is disconnected, the LNK light will be off.</td>
</tr>
</table>
<!-- end of second lights description -->
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_com21_1010.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>NOTE:</b>  The difference between the DOXport 1010 and the DOXport 1020 models is the presence of the Application Interface Module (AIM) expansion port on the DOXport 1020.
<br><br>
<table>
<tr>
 <td class="infohd">Port</td>
 <td class="infohd">Description</td>
</tr>
<tr>
 <td class="infohd">DC</td>
 <td class="infoline">The socket for the external, 15 VDC power supply.  The socket polarity has positive center and negative shield.</td>
</tr>
<tr>
 <td class="infohd">Reset Switch</td>
 <td class="infoline">The very small, white, unlabeled button to the right of the DC power connection.When you push the reset switch, it causes the modem to perform a hardware reset and a software boot that re-initializes your cable modem.<td> 
</tr>
<tr> 
 <td class="infohd">10BT</td>
 <td class="infoline">The socket used to connect a standard RJ-45, 10 Base-T Ethernet interface cable.
</tr>
<tr>
 <td class="infohd">Maintenance Port</td> 
 <td class="infoline">This port is normally covered. It allows service technicians to access the cable modem electronics and is not meant for customer use(applies to the DOXport 1020).
</tr>
<tr> 
 <td class="infohd">Factory Setting Switches</td> 
 <td class="infoline">These switches are normally covered to prevent accidental changes to the settings. They are used during product manufacture and are not meant for customer use.
 Your cable modem is shipped with all switches set to the OFF (down) position.</td>
<tr>
 <td class="infohd">CATV</td>
 <td class="infoline">The socket used to connect a standard coaxial cable from your cable outlet to your modem.</td>
</tr>
</table>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This what a COMport 1000/2000 and DOCport 1010 looks like when the issue is <font color="red">RF</font>.
<br>
<br>
<img src="./images/animation_com21.gif" border=0>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>