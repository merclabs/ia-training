<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Turbo Connect Comfort Suites Inn Card</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Turbo Connect Comfort Suites Inn Card</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>High Speed Internet Access</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Applies only for Comfort Suites customers.</b><br>
[Last Updated: Wednesday, February 04, 2004 11:18:31 AM]
<div align="center">
<h2>High Speed Internet Access</h2>
<h2>Powered By</h2>
<h2>Lexcom Communications</h2>
</div>
<div align="center">
<img align="center" src="./images/turboconnect_logo.png" border="0"><br>
</div>

</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>How to Connect to the Cable Modem</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>
<i>
Note: LAN Connection ONLY - Requires an Ethernet Card 
(This connection is NOT a Telephone Dial-Up Connection)
</i>
</b>
<ul>
<li><b>Step #1:</b> Locate the <b>Cable Modem</b> Unit <u>Mounted</u> to the <i>Wall</i> at the <i>Desk</i>. 
<li><b>Step #2:</b> Connect the cord provided with the <b>Modem</b> to the <b>Ethernet Port</b> on <u>your</u> computer.
<li><b>Step #3:</b> <b>Setup the Computer</b> for the <u>Appropriate</u> <i>Windows</i> Version Below.
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Technical Support</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<i>Technical Support is  available 24 hours a day, 7 days a week.</i>
  Call <b>224-2222</b> and Inform the <b>Technical Support Representative</b> of Your <b>Room Number</b>.
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Windows 95/98/ME</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ul>
<li>On the Desk top <b>RIGHT</b> click on <b>Network Neighborhood</b> Icon</li>
<li>Click on <b>Properties</b></li>
<li>Double click on <b>TCP/IP</b> (Ethernet card name)</li>
<li>You will be at the <b>IP Address Tab</b></li>
<li>Click on <b>Obtain an IP Address Automatically</b></li>
<li>Click <b>OK</b></li>
<li>Click <b>OK</b></li>
<li><b>Restart</b> Computer</li>
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Windows NT</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ul>
<li>On the desk top <b>RIGHT</b> click on <b>Network Neighborhood</b> Icon</li>
<li>Click on <b>Properties</b></li>
<li>Click on <b>Protocol</b></li>
<li>Double click on <b>TCP/IP</b></li>
<li>You will be at the <b>IP Address Tab</b></li>
<li>Click on <b>Obtain an IP Address Automatically</b></li>
<li>Click <b>OK</b></li>
<li>Click <b>OK</b></li>
<li><b>Restart</b> Computer</li>
<ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Windows 2000</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ul>
<li>On the desk top <b>right</b> click on <b>MY Network Places</b> Icon</li>
<li>Double click on <b>TCP/IP</b> (Ethernet card name)</li>
<li>You will be at the <b>IP Address Tab</b></li>
<li>Click on <b>Obtain an IP Address Automatically</b></li>
<li>Click <b>OK</b></li>
<li>Click <b>OK</b></li>
<li><b>Restart</b> Computer</li>
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Windows XP</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ul>
<li>Click on <b>Start</b></li>
<li>Click on <b>Control Panel</b></li>
<li>Click on <b>Switch to Classic View</b></li>
<li>Click on <b>Network Connections</b></li>
<li>Right click on <b>LAN Icon</b></li>
<li>Click on <b>Properties</b></li>
<li>Click select <b>TCP/IP Protocols</b></li>
<li>Click on <b>Properties</b></li>
<li>Set to <b>obtain IP Address</b></li>
<li>Click <b>OK</b></li>
</ul>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>