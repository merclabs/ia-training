<?

$modemname[] = "MadisonRiver Remedy";
$modelnumber[] = "ARSystem 5.0";
$type[] = "Webpage";
$description[] = "None";
$documentation[] = "https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave";

$modemname[] = "Lucent";
$modelnumber[] = "DSLpipe HST";
$type[] = "SDSL";
$description[] = "None";
$documentation[] = "../".$id."/lucent_dslpipe.php";

$modemname[] = "SpeedStream";
$modelnumber[] = "5200";
$type[] = "ADSL";
$description[] = "None";
$documentation[] = "../".$id."/speedstream_5200.php";

$modemname[] = "SpeedStream";
$modelnumber[] = "5200 ";
$type[] = "Web Interface";
$description[] = "Web Interface for 5200 ADSL Modem";
$documentation[] = "../".$id."/speedstream5200_webinterface.php";

$modemname[] = "SpeedStream";
$modelnumber[] = "5667";
$type[] = "ADSL";
$description[] = "None";
$documentation[] = "../".$id."/speedstream_5667.php";

$modemname[] = "Efficient Networks";
$modelnumber[] = "Tango Manager";
$type[] = "Software";
$description[] = "DSL Connection Software for 5667 DSL Modem";
$documentation[] = "../".$id."/tango_home.php";

?>
