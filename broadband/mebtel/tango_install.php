<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Tango Manager</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Tango Manager</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This screen appears when installing the <b>Tango Manager</b> software, to start installation
of the software, click <b>Installation</b>.
<p align="center">
<img src="./images/tangomanager/install.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following screen will appear after clicking <b>Installation</b>, it displays an <b>Installation
Overview</b> of the step followed during installation. To continue installation
click <b>Next</b>.
<p align="center">
<img src="./images/tangomanager/install_step1.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After clicking next, you will be asked to <b>choose your connection method</b>. The two
choices that you have are <b>Ethernet</b> or <b>USB</b>. Make sure you choose the correct
connection method for the customers type of set up. The installation steps from this point
on are the same for both connection methods, the difference is the network adapter for
the selected connection method is installed. 
<p align="center">
<img src="./images/tangomanager/ethernet_usb.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After selecting a connection type, the next window displays the <b>Software License 
Agreement</b>, to continue with the installation click <b>Accept</b>.
<p align="center">
<img src="./images/tangomanager/install_step2.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After accepting the Software License Agreement, this next screen gives the user a
chance to view a <b>video</b> to make sure they have all of their everything needed
for installation, in most cases click <b>Next</b> to continue installation.
<p align="center">
<img src="./images/tangomanager/install_step3.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After clicking <b>Next</b>, the software checks your computer to see if it meets
system requirments. If you should see a red <font color="red"><b>X</b></font> beside any icon, this means 
that a their system did not pass the system requirements test to 
continue installation, click <b>Full Report</b> to get more details about why.
If all goes well, you should see instruction on the screen to click <b>Next</b> 
to continue installation.      
<p align="center">
<img src="./images/tangomanager/install_step4.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
At this point the customer is instructed to install the <b>Line Filters</b> on 
all devices(<b>Telephones, Fax Machines, Answering Machines and Caller ID boxes</b>)
that share the same <b>phone number</b> as the DSL Modem. <font color="red"><b>Note:</b></font> 
Do not install a line filter on the same phone line the <b>DSL</b> in using unless the filter used 
is a <b>Dual Line Filter</b>, accepting both <b>DSL and Pnone</b> connections.
<p align="center">
<img src="./images/tangomanager/install_step5.jpg" border=0>
<br>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Next, the customer is instructed to connect the <b>DSL Modem</b> if they have not already
done so, once they have the <b>DSL Modem</b> connected, click <b>Next</b>.
<p align="center">
<img src="./images/tangomanager/install_step6.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Ater clicking next, the software checks both the <b>DSL Modem</b> and <b>Network Connection</b>
to see if everything is working correctly. If everything goes well, the next steps should finish
installation. 
<p align="center">
<img src="./images/tangomanager/install_step7.jpg" border=0>
<br>
</p>
<b>See Next: </b><a href="tango_home.php"><b>Home</b></a> <b>|</b> <a href="tango_access.php"><b>Access</b></a> <b>|</b> <a href="tango_support.php"><b>Support</b></a>  <b>|</b> <a href="tango_properties.php"><b>Properties</b></a>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>