<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5660 Series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5660 DSL Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SpeedStream 5660 Ethernet to DSL router provides ATM-based
network access for home users, telecommuters, or remote offices over
existing telephone lines.
</p>
<p>
Through its Ethernet and DSL ports located on the back of the router, the
5600 provides local area network (LAN) and wide area network (WAN)
connectivity. The router connects to the LAN through its Ethernet port and
connects to the WAN through its DSL port.</p>
<p>
<i>The 5660 can be used as either a router or a bridge.</i> 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_ss5600.png" border=0></td>
</center>
</tr>
<tr>
<td class="field" valign=top colspan=2>

The router is equipped with four lights on the front panel:<br>
the System light(<b>sys</b>), ATM light (<b>atm</b>), DSL light (<b>dsl</b>), Ethernet light (<b>enet</b>).
<ol>
<li>All four lights flash red for a moment as the hardware initializes.
Then, the <b>sys</b> light turns yellow while the router performs a self-test.
The <b>sys</b> light turns green when the unit has finished passing
diagnostic tests.</li>
<li>The <b>dsl</b> light will flash green while the router is attempting to obtain
the optimum transmission rate over the DSL line. If a DSL link
cannot be established, the light will flash green and yellow. When a
link is established, the <b>dsl</b> light turns solid green.</li>
<li>The <b>enet</b> light will turn solid green when there is a link detected, then
begin to flash green when Ethernet traffic is flowing.</li>
<li>The <b>atm</b> light will flash green indicating traffic flow on the DSL line.</li>
</ol>
<center>
<img src="./images/lightstatus_ss5600.png" border=0>
</center>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><b>NOTE: The labels for the back panel of the router are located on
the underside of the unit.</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><img src="./images/rearpanel_ss5600.png" border=0></td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/setup_ss5600.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Be sure the power switch is in the OFF position.</li>
<li>Insert the power cable jack into the power connector on the back of
the router. Connect the power cable to a power outlet.</li>
<li>Connect the router to the LAN using one of the following methods:

<ul type="disc">
<li>Connect directly to the Ethernet port on a PC, workstation or laptop
using the gray RJ45 cable, which is a straight-through cable.</li>
<li>Connect to an Ethernet hub or switch using the crossover cable
labeled �XOVER� on one end.</li>
</ul>

NOTE: Some Ethernet hubs and switches have an �Uplink� port that
allows you to connect to the router using the gray RJ45
straight-through cable.</li>
<li>Connect one end of an RJ11 or RJ45 telephone cable to the router�s
DSL port and the other end to the DSL service port (wall jack).</li>
<li>Press the router�s power button to turn ON the unit.</li>
</ol>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Using DHCP to Assign an IP Address</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Before you can use your computer over a network, it must have an IP
address. The router contains a dynamic host configuration protocol
(DHCP) server that will automatically assign an IP address to the PC if the
PC is set to obtain an IP address from a DHCP server. If you have set up
your router and computer according to the installation instructions, follow
these steps to get an IP address via DHCP if you are running a PC with
</p>
<b>Windows 95/98:</b>
<ol>
<li>Select <b>Start</b> > <b>Settings</b> > <b>Control Panel</b></li>
<li>Double click the <b>Network</b> icon</li>
<li>Select <b>TCP/IP</b> from the list of network components. Select <b>Properties</b>.</li>
<li>Select the <b>IP Address</b> tab and verify that <b>Obtain an IP address
automatically</b> is selected. If not, then select it.</li>
<li>Also check the <b>Advanced</b> tab, make sure <b>TCP/IP</b> is set as default protocol at the bottom.</li>
<li>Click <b>OK</b> twice.</li>
<li><b>Restart</b> if prompted.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Router Mode</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Configuring the Router</b><br>
Once your PC has an IP address, you can configure the router using either
the web interface or the command line interface.
Configuring the Router through the Web Interface
</p>
<p>
<b>NOTE:</b> Consult your service provider for your encapsulation type.
Creating a Virtual Circuit using PPP Encapsulation (default):
</p>
<ol>
<li>Start your web browser</li>
<li>Enter the router�s IP address: <b>10.0.0.1</b></li>
<li>The browser displays the router�s management interface.</li>
<li>On the main page, click on <b>Simple Setup</b>.</li>
<li>If your service provider has specified a <b>VPI/VCI number</b> to use, enter
the numbers in the <b>VPI</b> and <b>VCI</b> fields.</li>
<li>Enter the PPP Username and Password issued by your service
provider.</li>
<li>Click the <b>Set</b> button. This will set the new <b>VPI/VCI</b> parameters on the
router.</li>
</ol>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Bridge Mode</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Because the 5660 is configured by default in router mode, it must be set to
<b>bridge mode</b> in order to function as a bridge.</p>
<b>Changing to Bridge Mode through the Web Interface</b>
<ol>
<li>Start your web browser</li>
<li>Enter the router�s IP address: <b>10.0.0.1</b></li>
<li>On the first page, click on the link, �<b>Change to Bridge Mode</b>�.</li>
<li>When the display appears, click on the radio button labeled <b>Bridge</b>.</li>
<li>Click on the button, <b>Change Mode</b></li>
<li>When the response appears at the bottom of the screen, it will include
link that says �<b>REBOOT REQUIRED</b>�. Click on that link and choose
<b>yes</b> to reboot the router. <br>Select <b>Refresh</b> after a minute to see the new
configuration.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

