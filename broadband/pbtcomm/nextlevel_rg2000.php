<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Next Level VDSL Residental Gateway</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Next Level VDSL Residental Gateway</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Residential Gateway customers using Windows 98, ME, 
2000, need to use the WINPOET Software, Windows XP users need to the builtin PPPoE connection.
</p>
<p>
The <b>VDSL Residential Gateway</b>(<i>often called a "Residential Gateway Dolby AC-3 Box" because of the Surround Sound feature</i>)
functions as a single access device to the home. Using VDSL technology that delivers up to 26 
Mbps of throughput to the home, the  VDSL Residential Gateway provides the functionality of two or three 
high-quality set-top boxes. Given the prevalence of multiple TV households and concurrent channel
usage in North America. 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/nextlevel_rg2000.png">
</td>
<!--
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_rg2000.png">
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Features of the VDSL Residential Gateway</b><br>
<ul>
<li>3 concurrent MPEG digital quality video streams</li>
<li>Dual-mode UHF (ultra high frequency) and IR (infrared) remote controls</li>
<li>Separate remote controls for each video stream</li>
<li>High quality video and audio output, composite video, S-Video, stereo
audio, and digital (AC-3) stereo audio</li>
<li>10BaseT Ethernet for data communications</li>
<li>Caller ID and Voice Message Waiting features</li>
<li>Video IPG (Interactive Program Guide)</li>
<li>PPV (Pay-Per-View) provided for special programs</li>
<li>Automatic transparent downloading of software feature updates</li>
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Home Wiring</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/homewiring_rg2000.png">
</center>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
VDSL Residential Gateway customers using Windows 98, ME, 2000, need to use the <b>WINPOET Software</b>,
Windows XP users need to the builtin PPPoE connection. If getting WINPOET error messages, 
check the number of the error message number, and check status of the lights on the VDSL 
Residential Gateway. <br>
</p>
The following lights should be on.<ul>
<li><b>Power</b> should be on Red.</li>
<li><b>Network</b> light should be Green.</li>
<li><b>Online</b> should be Green.</li>
</ul>
<p>
If all lights are on have customer check the video signal on TV in home, if TV has video, check lights 
on modem. If network light is amber, this means that the ethernet connection between the VDSL Residential Gateway 
and PC is established but the VDSL Residential Gateway is not provisioned for data. Escalate to have 
VDSL Gateway reprogrammed. 
</p>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>