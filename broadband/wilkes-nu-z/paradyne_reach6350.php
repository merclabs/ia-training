<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Hotwire 6350 ReachDSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Hotwire 6350 ReachDSL<sup>&trade;</sup> Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_p6350.jpg" border=0>
<br>
<img src="./images/frontpanel_drawing.gif" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<table width="100%">
<tr><td class="infohd">LED</td><td class="infohd">Status</td><td class="infohd">Description<td></tr>
<tr><td class="infohd">PWR</td><td class="infoline">ON</td><td class="infoline">ReachDSL modem has power.<td></tr>
<tr><td class="infohd" rowspan=2>ALM</td><td class="infoline">OFF</td><td class="infoline">No active alarms.<td></tr>
<tr><td class="infoline">ON</td><td class="infoline">An alarm condition exists.<td></tr>
<tr><td class="infohd" rowspan=2>TST</td><td class="infoline">OFF</td><td class="infoline">No active tests.
<tr><td class="infoline">ON</td><td class="infoline">The TST LED is on during the power-on self-test and
during a test initiated by the service provider.
<tr><td class="infohd" rowspan=2>LINE</td><td class="infoline">ON</td><td class="infoline">The DSL link is active and ready to transmit and receive
data.<td></tr>
<tr><td class="infoline">OFF</td><td class="infoline">The DSL link has not been established.<td></tr>
<tr><td class="infohd" rowspan=2>TX/RX</td><td class="infoline">ON</td><td class="infoline">Data transmission is in progress on the DSL line.<td></tr>
<tr><td class="infoline">OFF</td><td class="infoline">The modem is not transmitting or receiving data.<td></tr>
<tr><td class="infohd" rowspan=2>ETHERNET</td><td class="infoline">ON</td><td class="infoline">The Ethernet connection to the Ethernet hub or PC is
active.<td></tr>
<tr><td class="infoline">OFF</td><td class="infoline">No Ethernet 10BaseT device is detected.<td></tr>
</table>
</div>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_p6350.png" border="0">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd" align="center">Number</td><td class="infohd" align="center">Port</td><td class="infohd" align="center">Description</td></tr>
<tr><td class="infohd" align="center">1</td><td class="infoline">Power</td><td class="infoline">connects the power supply to unit.</td></tr>
<tr><td class="infohd" align="center">2</td><td class="infoline">Ethernet</td><td class="infoline">connects CAT5 ethernet cable to unit.</td></tr>
<tr><td class="infohd" align="center">3</td><td class="infoline">Phone</td><td class="infoline">connects standard phoneline, also serves as a filter.</td></tr>
<tr><td class="infohd" align="center">4</td><td class="infoline">Line</td><td class="infoline">connects standard phoneline from wall jack.</td></tr>
</table>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/basicsetup.gif" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table width="100%">
<tr><td class="infohd">LED Symptom</td><td class="infohd">Action</td></tr>

<tr><td class="infoline">All LEDs are on.</td>
<td class="infoline">If LEDs remain on after ten minutes, the modem is not functional. Contact the service provider.</td></tr>

<tr><td class="infoline">ALM LED remains on.</td>
<td class="infoline">The power-on self-test may have failed. Unplug the unit and reapply power. If the alarm LED is still on, contact the service provider.</td></tr>

<tr><td class="infoline">ALM and TST LEDs are blinking.</td>
<td class="infoline">Firmware download may be in progress. If firmware download is not in progress or the LEDs continue blinking after ten
minutes, contact the service provider.</td></tr>

<tr><td class="infoline" rowspan=2>Ethernet LED is off.</td><td class="infoline">Verify that the Ethernet cable is securely installed at both ends,
and at least one PC is connected and powered on.</td></tr>
<tr><td class="infoline">Verify that the correct straight-through or crossover cable is
installed. Refer to Installing the Hotwire 6350 ReachDSL
Modem on page 5.</td></tr>

<tr><td class="infoline" rowspan=2>LINE LED is off.</td><td class="infoline">Verify that the DSL LINE cable is securely installed on both
ends. If the problem continues, contact the service provider.</td></tr>
<tr><td class="infoline">Verify that the line has dial tone. If there is no dial tone, contact
the service provider.</td></tr>

<tr><td class="infoline">LINE LED is on and there is no data transmission.</td><td class="infoline">The DSL link has been established but there is no data
transmission. Verify the Ethernet connection. If the problem
persists, contact the service provider.</td></tr>

<tr><td class="infoline">LINE and Ethernet LEDs are on and there is no data transmission.</td><td class="infoline">The DSL and Ethernet links have been established but there is
no data transmission. If the problem continues, contact the
service provider.</td></tr>

<tr><td class="infoline" rowspan="3">PWR LED is off.</td><td class="infoline">Check that the power cord is securely installed on both ends.</td></tr>
<tr><td class="infoline">If no LEDs are on, the power supply may be defective. Test the
outlet to verify power. If the problem persists, contact the
service provider.</td></tr>
<tr><td class="infoline">If other LEDs are on, the PWR LED may be burned out. Unplug
the unit and reapply power; watch all LEDs during the
power-on self-test to verify if the PWR LED is functioning.</td></tr>

<tr><td class="infoline">TST LED is on.</td><td class="infoline">A test initiated by the service provider may be active. Wait five
minutes. If the TST LED does not go off, contact the service.
provider.</td></tr>
</table>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>