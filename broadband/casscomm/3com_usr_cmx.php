<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>3Com U.S. Robotics CMX</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>3Com U.S. Robotics CMX</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>3Com U.S. Robotics CMX</b> is a 2 way cable modem, no software is requried to set up modem. To configure
your computer to connect with the modem you need to configure <b>TCP/IP</b> to obtain an IP address automaticlly.
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_3com_cmx.png">
<br>
<img src="./images/frontpanel_leds_3com_cmx.png">
</p>
</td>
</tr>
<tr>
<td class="field">
<p align="center">
<br>
<table>
<tr><td class="infohd">LED</td><td class="infohd">Status</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">1.</td><td class="infoline">Power</td><td class="infoline">Indicates power is applied to the cable modem. This light is solid green when the modem is on.</td></tr>
<tr><td class="infohd">2.</td><td class="infoline">Cable Modem Status</td><td class="infoline">When this LED is solid green, the modem has completed initial communications with the cable company
server and is functional. If it is blinking, the modem is still starting up. If this LED never stops blinking, you may be using the
wrong power supply or the modem may require service.</td></tr>
<tr><td class="infohd">3.</td><td class="infoline">PC Link Status</td><td class="infoline">Indicates that the cable modem is connected to the Ethernet card in your computer. This light is solid green when
this link is established.</td></tr>
<tr><td class="infohd">4.</td><td class="infoline">PC Link Activity</td><td class="infoline">Indicates that data is being transmitted to or from your PC over the Ethernet port. Flashing orange indicates
traffic. This LED should blink when data is being transmitted or received over the Ethernet port.</td></tr>
<tr><td class="infohd">5.</td><td class="infoline">Cable Activity</td><td class="infoline">Indicates that data is being transmitted to or from your cable company over the RF (cable) port. Flashing orange
indicates traffic.</td></tr>
<tr><td class="infohd">6.</td><td class="infoline">FCN</td><td class="infoline">Individual cable operators determine the function of this LED.</td></tr>
</table>
<br><br>
</p>
</td>
<tr>
 <td class="heading" valign=top colspan=2><b>Rear Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_3com_cmx.png"></td>
</p>

</td>
</tr>
 <td class="heading" valign=top colspan=2><b><a name="ts">Troubleshooting Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
 
Standard Cable Modem Troubleshooting applies.
</td>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
