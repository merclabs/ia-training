<?
// Created 09/24/03 by Clayton Barnette
// cbarnette@infoave.net
// Script that lists all of the modems used on each provider, on the 
// listings page. pulls information from the modeminfo.php file in each 
// providers directory. 

function linker($link){
// includes linking to correct modeminfo file per provider.
include_once("$link/modeminfo.php");
include_once("./provider/$link/company.php");
//echo("<table class=\"list_table\">");
echo("<tr><td class=\"content_link\">");
echo("<a href=\"?content=./broadband/viewer/index.php&id=$link\">$officialname</a>");
//echo("<b style=\"color:#999999\">Modems and Software Used:</b> ");
//echo("<span style=\"color:rgb(0,53,126)\">");
//for($i = 0;$i < count($modemname);$i++){
//echo($modemname[$i]." ".$modelnumber[$i]);
// Eliminates the trailing "|" at the end of the line.
//if(($i + 1) == count($modemname)){
// echo("");
// }else{
// echo("<b> | </b>");
// }
//}
// echo("</span>");
echo("</td></tr>");
//echo("</table>");
//echo("<br>");
unset($officialname);
unset($modemname);
unset($modelnumber);
}
?>
