<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5100 Series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5100 DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
SpeedStream� DSL Modems provide DSL access with
connectivity choice. Their exclusive SpeedStream technology
ensures enhanced ADSL performance.
<br>
<i><b>see:</b>Troubleshooting Information below.</i>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_ss5100.png" border=0></td>
</center>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<table>
<tr>
<td class="infotxt" colspan=2></td>The following light are on the front panel of the modem:</tr>
</tr>
<tr>
<td class="infohd">Sys</td> <td class="infoline">Indicates power on to modem(Should always be on).</td></tr>
</tr>
<tr>
<td class="infohd">DSL</td><td class="infoline">Indicated DSL has synched up Should be steady green</td></tr>
</tr>
<tr>
<td class="infohd">Act</td><td class="infoline">ATM Should blink green when trans/recv</td></tr>
</tr>
<tr>
<td class="infohd">Eth</td><td class="infoline">Ethernet indicated modem is communicating with computer's Ethernet card.</td></tr>
</tr>
</table>

<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_ss5100.png" border=0>
</center>
</td>
</tr>
<!--
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infotxt" colspan=2>From left to right:</td></tr>
</tr>
<tr>
<td class="infohd">Power</td> <td class="infoline">The bridge uses a standard AC power cord and has anON/OFF switch</td></tr>
</tr>
<tr>
<td class="infohd">DSL Port</td><td class="infoline">DSL connectivity is through this 8-pin, RJ-45 port.</td></tr>
</tr>
<tr>
<td class="infohd">Ethernet Ports</td><td class="infoline">One Ethernet 10Base-T port (8 pin, RJ-45)</td></tr>
</tr>
<tr>
<td class="infohd">Console Port</td><td class="infoline">Asynchronous RS232 connectivity is through this 8 pin,RJ-45 port (support only).</td></tr>
</tr>
</table>
<br>
</td>
</tr>
--
<tr>
 <td class="heading" valign=top colspan=2><b>Bridge Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Place your bridge in a location where it will be well ventilated. Do not stack
it with other devices or place it on carpet.</li>
<li>Connect the bridge to your PC, using the red cable between the bridge�s
Ethernet port and the Ethernet port on your PC.</li>
<li>Connect the bridge to your DSL wall jack, using the purple label cable
between the bridge�s DSL port and the DSL wall jack.</li>
<li>Connect the bridge to an AC power outlet using the supplied power cord.</li>
<li>Switch the bridge ON.</li>
</ol>
The following illustration shows the SpeedStream 5200 Series Bridge when
connected:
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/basicsetup_ss5200.png" border=0>
<img src="./images/frontlights_ss5200.png" border=0>
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Before you can use your computer over a network, it must have an IP
address. The modem contains a dynamic host configuration protocol
(DHCP) server that will automatically assign an IP address to the PC if the
PC is set to obtain an IP address from a DHCP server. If you have set up
your modem and computer according to the installation instructions, follow
these steps to get an IP address via DHCP if you are running a PC with
</p>
<b>Windows 95/98:</b>
<ol>
<li>Select <b>Start</b> > <b>Settings</b> > <b>Control Panel</b></li>
<li>Double click the <b>Network</b> icon</li>
<li>Select <b>TCP/IP</b> from the list of network components. Select <b>Properties</b>.</li>
<li>Select the <b>IP Address</b> tab and verify that <b>Obtain an IP address
automatically</b> is selected. If not, then select it.</li>
<li>Also check the <b>Advanced</b> tab, make sure <b>TCP/IP</b> is set as default protocol at the bottom.</li>
<li>Click <b>OK</b> twice.</li>
<li><b>Restart</b> if prompted.</li>
</ol>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
