<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/content.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>ActiontTec R1520SU</title>
</head>
<body class="document">
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2>ActiontTec R1520SU</td>
</tr>
<tr>
<td class="list_header2" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The Actiontec Wireless-Ready DSL Gateway integrated 4-port Ethernet switch allows 
users to share files and resources at speeds up to 100 Mbps. The advanced
built-in router supports a variety of networking protocols and
provides easy configuration, maximum security and full network
compatibility. The Gateway also features a firewall,
Internet service blocking, Web site blocking and full logging
support.
</td>
</tr>
<tr>
 <td class="list_header2" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_R1520SU.png" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This Modem has a total of seven lights. 
<ul>
<li>three(3) status lights</li>
<li>four(4) lights, one for each ethernet connection</li>
</ul>
<ul>
<li><b>Power:</b> power is on</li>
<li><b>Internet:</b> modem has obtained a network connection.</li>
<li><b>PC Card:</b> ethernet link is connected to PC.</li>
<li><b>Ethernet Network:</b> receiving data.</li>
</ul>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->
<tr>
 <td class="list_header2" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Not much is known about this modem, for now, try standard power cycle and basic troubleshooting.<br>
<i>More information to come.</i>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>