<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/content.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>ActiontTec R4500U</title>
</head>
<body class="doc">
<p>
<table class="doc_table">
<tr><td class="doc_header">Table of Content</td></tr>
<tr><td class="doc_link"><a href="#general">General Information</a></td></tr>
<tr><td class="doc_link"><a href="#front-panel">Front Panel</a></td></tr>
<!-- <tr><td class="doc_link"><a href="#back-panel">Back Panel</a></td></tr> -->
<tr><td class="doc_link"><a href="#troubleshooting">Troubleshooting</a></td></tr>
</table>
</p>
<p>
<table class="doc_table" cellspacing=1>
<tr>
 <td class="doc_header" valign=top colspan=2>ActiontTec R4500U</td>
</tr>
<tr>
<td class="doc_header2" valign=top colspan=2><b><a name="general">General Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The USB/Ethernet DSL Modem is capable of data performance
speeds up to 8 Mbps downstream and up to 860 Kbps upstream.
It was tested with all major DSLAMs, each with multiple line
cards, and is compliant with all major ADSL standards including
T1.413i2 and G.dmt. The Actiontec USB/Ethernet DSL
Modem has been carefully designed to maximize performance
and loop reach while guaranteeing compatibility.
</td>
</tr>
<tr>
 <td class="doc_header2" valign=top colspan=2><b><a name="front-panel">Front Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_R4500U.png" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This Modem has four lights on the side panel:
<ul>
<li><b>Power:</b> power is on</li>
<li><b>Ready:</b> modem has obtained a network connection.</li>
<li><b>Link:</b> ethernet link is connected to PC.</li>
<li><b>Activity:</b> receiving data.</li>
</ul>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->
<tr>
 <td class="doc_header2" valign=top colspan=2><b><a name="troubleshooting">Troubleshooting Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Not much is known about this modem, for now, try standard power cycle and basic troubleshooting.<br>
<i>More information to come.</i>
</td>
</tr>
</table>
</p>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>