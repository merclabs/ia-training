<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Toshiba PCX-1100U Cable Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Toshiba PCX-1100U Cable Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
At this time we <font color="red"><b>DO NOT</b></font> provide technical support for <b>DSL</b> or <b>Cable 
Modem</b> customers with <b>Comporium(Fort Mill, Rock Hill and Lancaster)</b>. If you 
have a <b>DSL</b> or <b>Cable Modem</b> customer on the phone, they need to call the following number for technical
support.
</p>
<h3>1-866-843-9444</h3>
<p>
The information below is here as a visual aid only to help customer locate and/or identify what type 
of connection they are have. <b>(DSL, Cable, or Dial-Up)</b>.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_pcx1100u.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The Cable Modem has five status LEDs for diagnostics. You can monitor the LEDs
during installation and during normal operations .
</p>
<p align="center">
<table>
<tr>
<td class="infohd">Function</td>
<td class="infohd"><p align="center">Color</p></td>
<td class="infohd">Definition</td>
</tr>
<tr><td class="infohd" rowspan=2>Power</td>
<td class="infoline" rowspan=2 width="30">Green</td>
<td class="infoline">Dark for power off</td></tr>
<tr>
<td class="infoline">Solid for power on</td>
</tr>

<tr>
<td class="infohd" rowspan=4>Cable</td>
<td class="infoline" rowspan=4 width="50">Green</td>
<td class="infoline">Dark for no downstream RF carrier present or power off.</td></tr>
<tr>
<td class="infoline">Flashing slowly (*1) for downstream RF carrier present and initial ranging in progress.</td>
</tr>
<tr>
<td class="infoline">Flashing fast (*2) for registration in progress.</td>
</tr>
<tr>
<td class="infoline">Solid for the Cable Modem registered and ready to transfer data.</td>
</tr>

<tr><td class="infohd" rowspan=2>PC</td>
<td class="infoline" rowspan=2 width="50">Green</td>
<td class="infoline">Dark for no carrier to/from PC present or power off.</td></tr>
<tr>
<td class="infoline">Solid for carrier to/from PC present.</td>
</tr>

<tr><td class="infohd" rowspan=2>Data</td>
<td class="infoline" rowspan=2 width="50">Green</td>
<td class="infoline">Dark for no user data going through the Cable Modem to/from PC present or power off.</td></tr>
<tr>
<td class="infoline">Flashing for user data going through the Cable Modem to/from PC present.</td>
</tr>

<tr>
<td class="infohd" rowspan=3>Test</td>
<td class="infoline" rowspan=3 width="50">Amber</td>
<td class="infoline">Dark for initial self-test of the Cable Modem OK or power off.</td></tr>
<tr>
<td class="infoline">Flashing for initial self-test of the Cable Modem in progress or 
software down loading of the Cable Modem in progress</td>
</tr>
<tr>
<td class="infoline">Solid for self-test failure of the Cable Modem</td>
</tr>
</table>
</p>
<p>
<b>
(*1) flashing slowly : flashing every 2-seconds (approximately)<br>
(*2) flashing fast : flashing every 1-second (approximately)
</b>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_pcx1100u.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<table>
<tr><td class="infohd">Port</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">10BASE-T</td><td class="infoline">10Mbps; half-duplex Ethernet Port</td></tr>
<tr><td class="infohd">USB</td><td class="infoline">12Mbps USB Port</td></tr>
<tr><td class="infohd">DC~IN</td><td class="infoline">Power</td></tr>
<tr><td class="infohd">F Connector(RF)</td><td class="infoline">Connects the modem to the Cable System.</td></tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<h3><font color="red">No Technical Support for Comporium DSL/Cable Modems</font></h3>
Give customer <b>1.866.543.9444</b> for High Speed Internet.
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>