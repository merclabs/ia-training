<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Best Data Smart One DSL800EU Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Best Data Smart One DSL800EU Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Chesnee</b> issues only two types of <b>DSL Modems</b>.
<ol>
<li><b>Best Data-</b> there are only a few of these left in their system as 
they are going away from this type of dsl modem. After a surge or lightning 
strike, these modems make take up to three minutes to sync up, so 
Chesnee recommends a 3-4 minute power cycle with them. Also they have requested 
that we inquire about new phones added to the line, and/or surge 
protectors. *This may not be a bad idea to look out for on any dsl 
service.

<li><b>Westel- Westel</b> modems are dhcp with no software or config pages. 
Some business customers will use static ips. However, in a typical residential 
setting there should be no ip info specified. The westel modems do not 
take longer than a minute to sync up. 
</p>
<!-- commented out on 13-Feb-2004 07:30 AM //
<p>Not much information is known about this particular DSL, however Chesnee is 
moving away from these DSL's and over to the Westell's, so we should not see too 
many calls with these. Although if you get one of these DSL's try the following 
troubleshooting steps below and if nothing works follow escalation procedures. 
If this is a business customer check for <b>Static IP</b> but residential customers 
are <b>DHCP</b>.</p>
-->
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_bd800eu.jpg" border=0>
</p>
</td>
</tr>
<!--
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>

<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><ol><li>If you have not completed "Install Modem Software (USB)", please do so now.</li> 
<li>Plug the power adapter's cord into the PWR port on your modem. Plug the other end of the power cord into a power outlet. Push the power switch in, to the <b>ON</b> position.</li>
<li>Plug one end of the telephone cable into the DSL port of the modem. Plug the other end of the cable into the wall jack that is providing your DSL connection.</li>
<li>Plug one end of the gray Ethernet cable into the modem's 10baseT port.</li></ol></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>This modem has no software setup or network settings other than DNS.</p><p><B>Check and configure TCP/IP (Ethernet)</B></p>
<p>
<ul>
<li>This step checks for and configures your TCP/IP networking so that you can connect your modem.</li>
<li><font color="red"><b>IMPORTANT:</b></font>If TCP/IP networking settings are incorrect, you will not be able to utilize your connection to DSL! NOTE: There are a few slight differences between the various Windows Operating Systems (Win98, WinME, etc).</li>
</ul>

<b>These instructions are for Win98 & WinME</b>
<ul>
<p>
    <ol>
         <li>Click the Windows "Start" button.</li>
				 <ul><li>Select "Settings".</li>
				 <li>Click "Control Panel".</li>
				 <li>Double click the "Network" icon.</li></ul>
<li>On the "Configuration" tab, check to see if there is a TCP/IP bound to the Ethernet</li>
<li>Select the TCP/IP entry for the Ethernet adapter and then click "Properties".</li>
<li>On the "IP Address" tab, make sure the button for "Enable DNS" is selected. Enter IP address, and subnet mask provided.</li>
<li>Click the "DNS Configuration" tab. Make sure the button for "Enable DNS" is selected. Enter host name (any three characters), domain (chesnet.net),  and DNS servers (Primary DNS 206.74.254.2, Secondary DNS 204.116.57.2).</li>
<li>Click the "WINS Configuration" tab. ake sure the button for "Disable WINS resolution" is selected.</li>
<li>Click the "Gateway" tab - enter the Gateway address and click "Add".</li>
<li>Click "OK" to close TCP/IP window.</li>
<li>Click "OK" to close the Network window</li>
<li>Windows may prompt you to restart you computer. If it does click "Yes". If it doesn't, close all windows and restart your computer by clicking the Windows "Start" button, selecting "Shutdown" and clicking "Restart".</li></ol> 
</ul>
</p>

<b>These instructions are for Win2000 & WinXP(Classic View).</b><br>
To switch XP to "Classic View", right click on a blank space on the desktop 
and left click "properties". Click the "Appearance" tab and in the "Windows and Buttons" diaglog
box select "Windows Classic Style".
</p>
<p><ol>
<li>Click the Windows "Start" button.</li>
<li>Select "Settings".</li>
<li>Click "Network and Dial-up Connections".</li>
<li>Look for the "Ethernet Adapter". The name may be different depending on which modem you have but it will say something about Efficient Networks.</li>
<li>Right click this Icon and select "Properties".</li>
<li>Select "TCP/IP" and click the properties button.</li>
<li>Enter in your IP address, subnet mask, and gateway in the boxes provided.</li>
<li>Enter in the Preferred DNS 206.74.254.2 and Secondary DNS 204.116.57.2.</li>
<li>Click the "Advanced" button.</li>
<li>Click the "DNS" tab.</li>
<li>Look for the diaglog box "Suffix for this domain" and enter in "chesnet.net"</li>
<li>Click "OK" on each window to get back to the "Network and Dial-up Connections" window.</li>
<li>Close this window and you should be ready to access the Internet.</li></ol></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>Only a few troubleshooting tips on this modem.</p>
<p>
<ul>
<li>After a surge, lightning strike, or power outage these modems make take up to
three minutes to sync up, so Chesnee recommends a 3-4 minute power cycle with them.</li>
<li>Verify DNS settings in cases where you can ping but not pull webpages.</li>
</ul>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>