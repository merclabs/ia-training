<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Stormport 400</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Stormport 400 DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Hardy customers use static IP addresses, the 400 does not use USB, no software is 
required for set up. 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_stormport_400.jpg" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Begin with a power cycle. If all the correct lights come on, (remember link light 
will start out yellow/orange for a few minutes then "<b>train</b>" to a faint blinking 
green.) then check network settings in the tcp/ip for the ethernet card for Win 9x
or the LAN connection for WinXP/2000. Customers should have static IP information 
filled in. They use our <b>DNS</b>.
			 <ul>
			 <li><b>PRIMARY</b> - 206.74.254.2 
			 <li><b>Secondary</b> - 204.116.57.2 
       </ul>
</p> 
<b>Reminders</b><br>
   <ul>
	 <li>There will be no need for making a broadband connection or for trying to install any connective software such as winpoet. 
   <li>Releasing/renewing ip will not do anything as these settings are static. 
	 <li>If pinging the gateway does not work it may indicate problems with the ethernet drivers or cat 5 connection.
	 <li><b>WE ESCALATE DSL FOR HARDY, PLEASE SEE AN LT<b>. 
   </ul>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
