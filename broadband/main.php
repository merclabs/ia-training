<div align="center">
<h1>Broadband Support</h1>

<?include_once("listinginfo.php")?>
<p style="color:red;font-size:8pt;">
PROPRIETARY AND CONFIDENTIAL<br>
INFORMATION OF SPIRIT TELECOM/INFO AVENUE<br>
PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
<p class="note" align="left">
<b style="color:red">Memo:</b> 
Please remember to check the phone line for each device(Phone, Fax, Answering Machine) to make sure a  
<b>Line Filter</b> is installed. The only phone line that should not have a 
filter is the line that the <b>DSL Modem</b> is on, unless the filter being used
is a <b>Dual Line Filter</b> that accepts the phone line on one side and the
<b>DSL Line</b> on the other. 
</p>
<p class="note" align="left">
<b style="color:red;">Memo:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
<p align="center">
<a href="#a">A</a>&nbsp;
<a href="#b">B</a>&nbsp;
<a href="#c">C</a>&nbsp;
<a href="#d">D</a>&nbsp;
<a href="#e">E</a>&nbsp;
<a href="#f">F</a>&nbsp;
<a href="#g">G</a>&nbsp;
<a href="#h">H</a>&nbsp;
<a href="#i">I</a>&nbsp;
<a href="#j">J</a>&nbsp;
<a href="#k">K</a>&nbsp;
<a href="#l">L</a>&nbsp;
<a href="#m">M</a>&nbsp;
<a href="#n">N</a>&nbsp;
<a href="#o">O</a>&nbsp;
<a href="#p">P</a>&nbsp;
<a href="#q">Q</a>&nbsp;
<a href="#r">R</a>&nbsp;
<a href="#s">S</a>&nbsp;
<a href="#t">T</a>&nbsp;
<a href="#u">U</a>&nbsp;
<a href="#v">V</a>&nbsp;
<a href="#w">W</a>&nbsp;
<a href="#x">X</a>&nbsp;
<a href="#y">Y</a>
<a href="#z">Z</a>
</p>
<table class="list_table" cellspacing=1 width="550">
<tr>
 <td class="list_header" valign=top colspan=2>Highspeed Support Listing</td>
</tr>
<!-- A -->
<tr><td class="list_header2" valign=top><b><a name="A">A</a></b></td></tr>
<?linker("accesspoint");?>
<?linker("atlantic");?>
<!-- Back to Top button -->


<!-- B -->
<tr><td class="list_header2" valign=top><b><a name="B">B</a></b></td></tr>
<?linker("benlomand");?> 
<?linker("blue_mountain");?>
<?linker("bulloch");?>

<!-- C -->
<tr><td class="list_header2" valign=top><b><a name="C">C</a></b></td></tr>
<?linker("casscomm");?>
<?linker("chesnee");?>
<?linker("chestertel");?>
<?linker("citizensnc");?>
<?linker("coastalnow");?>
<?linker("fortmill");?>
<?linker("lancaster");?>
<?linker("rockhill");?>

<!-- D -->
<tr><td class="list_header2" valign=top><b><a name="D">D</a></b></td></tr>

<!-- E -->
<tr><td class="list_header2" valign=top><b><a name="E">E</a></b></td></tr>
<?linker("ellerbe");?>

<!-- F -->
<tr><td class="list_header2" valign=top><b><a name="F">F</a></b></td></tr>

 
<!-- G -->
<tr><td class="list_header2" valign=top><b><a name="G">G</a></b></td></tr>
<?linker("cyberlink");?>
<?linker("grics");?>
<?linker("graceba");?>
<?linker("gulftel");?>
 
<!-- H -->
<tr><td class="list_header2" valign=top><b><a name="H">H</a></b></td></tr>
<?linker("hardy");?>
<?linker("hayneville_telephone");?>
<?linker("hayneville_fiber");?>
<?linker("hargray");?>
<?linker("home");?>
<?linker("horry");?>
 
<!-- I -->
<tr><td class="list_header2" valign=top><b><a name="I">I</a></b></td></tr>

<!-- J -->
<tr><td class="list_header2" valign=top><b><a name="J">J</a></b></td></tr>
<?linker("jefferson");?>

<!-- K -->
<tr><td class="list_header2" valign=top><b><a name="K">K</a></b></td></tr>
<?linker("klm");?>
 
<!-- L -->
<tr><td class="list_header2" valign=top><b><a name="L">L</a></b></td></tr>

<?linker("lexington");?>

<!-- M -->
<tr><td class="list_header2" valign=top><b><a name="M">M</a></b></td></tr>
<?linker("mebtel");?>

<!-- N -->
<tr><td class="list_header2" valign=top><b><a name="N">N</a></b></td></tr>
<?linker("northpenn");?>

<!-- O -->
<tr><td class="list_header2" valign=top><b><a name="O">O</a></b></td></tr>

<!-- P --> 
<tr><td class="list_header2" valign=top><b><a name="P">P</a></b></td></tr>
<?linker("palmettorural");?>
<?linker("parksbrothers");?>
<?linker("piedmonttelephone");?>
<?linker("plant");?>
<?linker("planters");?>
<?linker("pbtcomm");?>

<!-- Q -->
<tr><td class="list_header2" valign=top><b><a name="Q">Q</a></b></td></tr>

<!-- R -->
<tr><td class="list_header2" valign=top><b><a name="R">R</a></b></td></tr>


<!-- S -->
<tr><td class="list_header2" valign=top><b><a name="S">S</a></b></td></tr>
<?linker("surry");?>
<!-- T -->
<tr><td class="list_header2" valign=top><b><a name="T">T</a></b></td></tr>
<?linker("tricounty");?>
<?linker("twinlakes");?>

<!-- U -->
<tr><td class="list_header2" valign=top><b><a name="U">U</a></b></td></tr>
<?linker("united");?>

<!-- V -->
<tr><td class="list_header2" valign=top><b><a name="V">V</a></b></td></tr>

<!-- W -->
<tr><td class="list_header2" valign=top><b><a name="W">W</a></b></td></tr>
<?linker("westcarolina");?>
<?linker("winco");?>
<?linker("wilkes");?>
<?linker("wilkes-nu-z");?>

<!-- X -->
<tr><td class="list_header2" valign=top><b><a name="X">X</a></b></td></tr>

<!-- Y -->
<tr><td class="list_header2" valign=top><b><a name="Y">Y</a></b></td></tr>


<!-- Z -->
<tr><td class="list_header2" valign=top><b><a name="Z">Z</a></b></td></tr>
</table>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->