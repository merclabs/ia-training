<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Next Level N3 ETHERset ADLS</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Next Level N3 ETHERset ADLS</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Home Telephone DSL uses PPPoE</b>
<p>
We use <b>ADSL filters</b> outside the customers home, therefore the <b>WireSpeed</b> modem 
must be plugged into the jack in which it was installed in.
Some installations require a telephone at the same location as the <b>WireSpeed</b> modem, 
we accomplish this with a <b>Y adapter</b> and a <b>microfilter</b>. The telephone plugs into the 
microfilter and the modem plugs into the other side of the Y adapter.
A microfilter filters keeps the <b>ADSL frequency</b> from <b>interfering</b> with the telephone.
</p>
<p align="center">
<img src="./images/linefilters_wt36R51X.jpg" border=0>
<p>
<br>
</p>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_N3.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<table width="500">
<tr><td class="infohd">LED</td><td class="infohd">State</td><td class="infohd">Function</td></tr>

<tr><td class="infohd" rowspan="2">Power</td><td class="infoline">OFF 
</td><td class="infoline">indicates no power to the ADSL ETHERset.</td></tr>
 <tr><td class="infoline">GREEN</td><td class="infoline">indicates that the ADSL ETHERset is receiving power.</td></tr>

<tr><td class="infohd"rowspan="2">Fail or System</td>
 <td class="infoline">OFF</td><td class="infoline">indicates that the ADSL ETHERset is operating normally.</td></tr>
 <tr><td class="infoline">RED</td><td class="infoline">indicates that the ADSL ETHERset is booting up.</td></tr>

<tr><td class="infohd" rowspan="3">Line</td>
 <td class="infoline">OFF</td><td class="infoline">indicates no DSL signal present on the line.</td></tr>
 <tr><td class="infoline">AMBER</td><td class="infoline">indicates the presence of DSL signal on the line but no PPPoE authentication.</td></tr>
 <tr><td class="infoline">GREEN</td><td class="infoline">indicates the presence of DSL signal on the line and PPPoE authentication succsessful. </td></tr>

<tr><td class="infohd" rowspan="2">LAN</td>
<td class="infoline">OFF</td><td class="infoline">indicates no Ethernet connection to the ADSL ETHERset. 
</td></tr>
 <tr><td class="infoline">GREEN</td><td class="infoline">indicates an Ethernet connection to the ADSL ETHERset.</td></tr>

<tr><td class="infohd">DATA</td><td class="infoline">Flashing AMBER</td><td class="infoline">indicates data activity between the PC, ADSL ETHERset and the DSL network.</td></tr>
</table> 
<br>
</p> 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Standard ADSL troubleshooting applies.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>