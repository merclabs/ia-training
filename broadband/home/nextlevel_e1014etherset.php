<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Next Level 1013 and 1014 ETHERset ADSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Next Level 1013 and 1014 ETHERset ADSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Home Telephone DSL uses PPPoE</b>
<p>
Troubleshoot connection error codes the same as DialUp
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_E1014_ETHERset.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<div align="center">
<table width=100%>
<tr><td class="infohd">LED</td><td class="infohd">State</td><td class="infohd">Function</td></tr> 

<tr><td class="infohd" rowspan=2>PWR</td>
<td class="infoline">OFF</td><td class="infoline">indicates no power to the ADSL ETHERset.</td></tr> 
<tr>
<td class="infoline">GREEN</td><td class="infoline">indicates that the ADSL ETHERset is receiving power.</td></tr> 
 
<tr><td class="infohd" rowspan=2>DIAG</td>
<td class="infoline">OFF</td><td class="infoline">indicates that the ADSL ETHERset is operating normally.</td></tr> 
<tr>
<td class="infoline">AMBER</td><td class="infoline">indicates that the ADSL ETHERset is booting up.</td></tr>
 

<tr><td class="infohd" rowspan=2>LAN LINK</td>
<td class="infoline">OFF</td><td class="infoline">indicates no Ethernet connection to the ADSL ETHERset.</td></tr> 
<tr>
<td class="infoline">GREEN</td><td class="infoline">indicates an Ethernet connection to the ADSL ETHERset.</td></tr> 
 
<tr><td class="infohd">LAN ACT</td>
<td class="infoline">Flashing AMBER</td><td class="infoline">indicates data activity between the PC and the ADSL ETHERset.</td></tr> 
<tr>
<td class="infohd" rowspan="3">WAN LINK</td>
<td class="infoline">OFF</td><td class="infoline"> indicates no DSL signal present on the line.</td></tr> 
<tr>
<td class="infoline">Flashing GREEN</td><td class="infoline">indicates that the ADSL ETHERset is trying to synch with the DSL signal.</td></tr> 
<tr>
<td class="infoline">GREEN</td><td class="infoline">indicates the presence of DSL signal on the line and a succsessful synch.</td></tr> 
 
<tr><td class="infohd">DATA</td><td class="infoline">Flashing AMBER</td><td class="infoline">indicates data activity between the ADSL ETHERset and the DSL network.</td></tr> 
</table>
</div>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p> 
<b>No Power</b> Make sure the AC Adapter is plugged into the ADSL ETHERset and the power outlet. 
Try Power cycling the ADSL ETHERset. 
</p>
<p> 
<b>No WAN LINK</b>
or is flashing Make sure the phone cable is plugged into the ADSL ETHERset and the correct phone outlet. 
Try Power cycling the ADSL ETHERset. 
</p>
<p> 
<b>No LAN LINK</b> Make sure the Ethernet cable is plugged into the ADSL ETHERset and the Network device. 
Restart the computer.
</p> 
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>