<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Tango Manager</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Tango Manager</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Access</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This window allows the customers to create a profile. If you come to this screen and there is no
profile here for the customer, you should create one by clicking on the <b>Create New Profile</b>. 
<p align="center">
<img src="./images/tangomanager/tangoaccess.jpg" border=0>
</p>
If the customer already has a profile created here, then right click on the profile icon
and click on <a href="tango_properties.php"><b>Properties</b></a>.
<br>
</td>
</tr>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Create a New Profile</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Once you click on the Create New Profile icon, the following window appears. This window allows
you to enter a name for the new profile. After entering a name for the profile, click <b>Next</b>.
<p align="center">
<img src="./images/tangomanager/profile1.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The next step is to enter the customers <b>username</b> and <b>password</b>, after entering their username, password
and password again to confirm, click <b>Next</b>.
<p align="center">
<img src="./images/tangomanager/profile2.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After clicking next, the wizard will try to locate and connect to the server. Click <b>Next</b> to start this process.
<p align="center">
<img src="./images/tangomanager/profile3.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
During this point if the wizard can not locate the server, you will see the following window. 
This will give you the chance to either click <b>Back</b> to try to locate the server again or
click <b>Next</b> to finish creating the new profile.
<p align="center">
<img src="./images/tangomanager/profile4.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If next is clicked, the wizard prompts you to save the new profile, to complete and save your new created profile
click <b>Finish</b>.
<p align="center">
<img src="./images/tangomanager/profile5.jpg" border=0>
</p>
<b>See Next: </b><a href="tango_home.php"><b>Home</b></a> <b>|</b> <a href="tango_install.php"><b>Install</b></a> <b>|</b> <a href="tango_support.php"><b>Support</b></a> <b>|</b> <a href="tango_properties.php"><b>Properties</b></a>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>