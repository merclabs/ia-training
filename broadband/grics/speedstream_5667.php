<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5667 USB/Ethernet Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5667 USB/Ethernet Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Speedstream 5667 series uses the <a href="tango_home.php"><b>Tango Software</b></a>, has power button on back, requires a router or hub/switch for multiple machines. 
</p>
</tr>
<!--
<tr>
<td class="heading" valign=top colspan=2><b>DSL Settings</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<br>
<table width=200>
 <tr><td class="infohd">IP</td><td class="infoline">192.168.1.2</td></tr>
 <tr><td class="infohd">Subnet</td><td class="infoline">255.255.255.0</td></tr>
 <tr><td class="infohd">Gateway</td><td class="infoline">192.168.1.1</td></tr>
 <tr><td class="infohd">DNS1</td><td class="infoline">64.40.75.20</td></tr>
 <tr><td class="infohd">DNS2</td><td class="infoline">64.40.72.25</td></tr>
</table>
<br>
</p>
</td>
</tr>
-->
<tr>
<td class="heading" valign=top colspan=2><b>Phone Numbers</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<br>
<table>
 <tr><td class="infohd">Customer Service</td><td class="infoline">1-800-223-1851</td></tr>
  <tr><td class="infohd">Second Level</td><td class="infoline">1-800-771-4926</td></tr>
</table>
</br>
</p>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Hours</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Please <font color="red"><b>DO NOT</b></font> tranfer customers to <b>2nd Level</b> before <b>9am</b>, if you have a customer that
needs to be transfered to <b>2nd Level</b>, get an LT's approval, create a ticket, give customer 
ticket number and advise customer to call back to be transfered to 2nd level when they are open.
<p align="center">
<table>
 <tr><td class="infohd">Monday - Firday</td><td class="infoline"> 9am - 11pm</td></tr>
  <tr><td class="infohd">Saturday</td><td class="infoline">9am - 6pm</td></tr>
	  <tr><td class="infohd">Sunday</td><td class="infoline">Closed</td></tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Things to remember</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<ul>
<li>Customer service transfers should be limited to billing issues(signing up for accounts, closing accounts), most everything 
else will be handled by second level.</li>
<li>When entering a username in to the <b>PTS</b>, you will want to follow it with a <b>]</b> symbol; this symbol serves as a wild-card
which will cause <b>PTS</b> to take a few moments longer but will find and display a <b>Madision</b> customer that would 
not normally pull up in <b>PTS</b>. If they are a new customer and not coming up in <b>PTS</b>, try looking them up in 
<a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave"  target="_blank"><b>Remedy</b></a> first.
Then if the user's information comes up there, verify with customer and add them to <b>PTS</b> with that information, make sure you also verify their correct phone number, since most of the time 
the nubmer in <a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave" target="_blank"><b>Remedy</b></a>
is often their DSL line number.</li>
<li></li>
</ul>
<br>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Software</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Tango Manager Software</b> is required for this modem to connect, for more information on
the Tango Manager Software, <a href="tango_home.php"><b>click here</b></a>.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_ss5667.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td>
<td class="infohd">Power</td>
<td class="infohd">ADSL</td>
<td class="infohd">Act</td>
<td class="infohd">Enet</td>
<td class="infohd">USB</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/unit.png" border=0></center></td>
<td class="infoline">Power Off</td>
<td class="infoline">N/A</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port not connected; check Ethernet cable connection if using Ethernet interface</td>
<td class="infoline">USB port not connected; check USB cable connection if using USB interface</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/solid.png" border=0></center></td>
<td class="infoline">Power On</td>
<td class="infoline">Ready for Data Traffic</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port connected to LAN</td>
<td class="infoline">USB port connected to host</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/blinking.png" border=0></center></td>
<td class="infoline">N/A</td>
<td class="infoline">Searching for signal</td>
<td class="infoline">DSL traffic flow</td>
<td class="infoline">Ethernet traffic flow</td>
<td class="infoline">Traffic flow on the USB interface</td>
</tr>
<tr>
<td class="infohd">
<center>All Blinking</center></td>
<td class="infoline" colspan=5>Post failure</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following illustration shows the connectors on the rear panel of the SpeedStream router.
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_ss5667.png" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Devices such as fax machines, caller ID boxes, or phones that share the same phone number as the DSL
require a line filter, which prevents modem noise from disrupting the DSL signal on the phone line.
A line filter is not needed for the line the DSL modem is on unless the line filter used is a 
<b>Dual Line Filter</b> that connects both DSL and Phone on the same line.                  
</p>
<p align="center">
<img src="./images/dslphonefilter.png" border=0>
</p>
</p>
<p>
The DSL may also be connected to a surge protector call a <b>MasterCube</b>(made by Belkin, model:F5C594).
This surge protector plugs into the outlet, has two jacks on the bottom, one for <b>INPUT</b> and one
for <b>OUTPUT</b>. The DSL line coming from the wall plugs into the <b>INPUT</b> and the line coming from
the DSL modem plugs into the <b>OUTPUT</b>. If a customer calls and their <b>DSL</b> light is flashing and 
continues to flash even after a power cycle, try bypassing the <b>MasterCube</b>, plugging the line coming
from the DSL modem directly into the DSL wall jack. 
<br><br>
<b>NOTE:</b> If a DSL connection starts to work after bypassing the <b>MasterCube</b>, create
a ticket for customer, give customer <b>ticket number</b> then transfer them to 2nd 
level for them to be issued them another <b>MasterCube</b>.
<br><br>
<div align="center">
<img src="./images/masterqube_f5c594.jpg" border=0>
</div>
</p>
<p>
<b>NOTE:</b> The new model of the <b>MasterCube</b> has both ports located on the side instead of the bottom,
and has <b>SurgeCube</b> printed on the side instead of MasterCube. Be aware of both names.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/ethernetsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/usbsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
 
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

