<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Pairgain Megabit 701f ADSL Bridge/Router Web Interface</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Pairgain Megabit 701f ADSL Bridge/Router Web Interface</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
To access your modem Web Interface, connect your PC to the same LAN IP subnet as as the ADSL Modem.
The default LAN IP subnet for the Modem is <b>10.0.0.1</b> with a subnet mask of <b>255.255.255.0</b>.
In addition, the modem uses <b>10.0.0.1</b> as its default LAN IP address.
</p>
<p>
<img src="./images/note_icon.gif" border=0><br>
<b>If your PC cannot connect to the modem, set your PC Ethernet NIC Card for 10Mbps half-duplex transmission(not auto-detect)</b>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Set-Up PC</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>The Following applies to Windows 98:</b>
<ol>
<li>
From the Windows Desktop click <b>Start</b>, <b>Settings</b>, <b>Control Panel</b>.
</li>
<li>
In the <b>Control Panel</b>, double-click the <b>Network</b> icon.
</li>
<li>
On the <b>Configuration</b> tab, double-click <b>TCP/IP</b> for the <b>NIC Card</b>.
</li>
<li>
<b>Note:</b> About DHCP Server:
<ul>
<li>If <b>DHCP Server</b> has <b>NOT</b> been enabled on the modem(default), <b>Specify and IP address</b>.</li>
<li>If <b>DHCP Server</b> has been enabled, on the modem, select <b>Obtain an IP Address Automaticlly</b>.
and skip to <b>Step 6</b>.</li>
</ul>
</li>
<li>Enter <b>IP Address</b> and <b>Subnet Mask</b>. The default LAN IP addres is <b>10.0.0.1</b>
with a subnet mask of <b>255.255.255.0</b>. Use an IP address for your PC between the range of <b>10.0.0.2
to <b>10.0.0.254</b>.
</b>
</li>
<li>Click <b>OK</b> to close <b>TCP/IP Properties</b>.</li>
<li>Click <b>OK</b> to close <b>Network</b>.</li>
<li>When prompted to restart click <b>Yes</b>.</li>
</ol>

<tr>
 <td class="heading" valign=top colspan=2><b>Configure the Web Browser</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
To view the modem's Web page properly, your Web Browser must have the proxies disabled and cache settings
enabled to compare the cached document against the network document everytime it is accessed.
</p>
<p>
<b>Internet Explorer(5.5)</b><br>
<ol>
<li>Open <b>Internet Explorer</b>.</li>
<li>Click, <b>Tools</b>, <b>Internet Options</b>.</li>
<li>In the <b>Temporary Internet Files</b> section, click the <b>Settings</b> button.</li>
<li>Seclect <b>Every visit to the page</b>, then click <b>OK.</b></li>
<li>Click the <b>Connections</b> tab, then the <b>LAN Settings</b> button.</li>
<li>In the <b>Automatic Configuration</b> section uncheck <b>Automaticlly detect settings</b>.</li>
<li>In the <b>Proxy Server</b> section clear all text boxes and uncheck <b>Use a Proxy Server</b>.</li>
<li>Then click <b>OK</b>.</li>
<li>Then <b>OK</b> again to close <b>Internet Options</b>.</li>
</ol>
</p>
<p>
<b>Netscape(4.0)</b><br>
<ol>
<li>Open <b>Netscape</b>.</li>
<li>Click <b>Edit</b>, <b>Preferences</b>.</li>
<li>In the <b>Category</b> box, select <b>Advanced</b>, click <b>Cache</b>, then
select <b>Everytime</b> for <b>Document in cache is compared to Document on Network</b>. 
</li>
<li>Again, in Category section, select <b>Advanced</b>, click <b>Proxies</b> then select
<b>Direct connection to the internet</b>.</li>
<li>Then click <b>OK</b>.</li>
</ol>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Accessing the Modem Web Pages</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Open <b>Internet Explorer</b>, in the address bar type <b>http//10.0.0.1</b> then press enter.
</p>
<p align="center">
<img src="./images/address_bar.png" border=0>
</p>
<br>
The following window should display.<br>
<p align="center">
<img src="./images/webpage_step1.png" border=0>
</p>
This login is for the system administrator responsible for operating the modem.
<ul>
<li>Enter the default user name(<b>admin</b>).</li>
<li>Enter the default password(<b>password</b>).</li>
<li>Click <b>Login</b>.</li>
</ul>
<b>Managing the System:</b><br>
The System pages are designed so that you can manage, update and troubleshoot the modem as
a whole. From these pages you can.
<ul>
<li>View the overall cofiguration of the modem.</li>
<li>Enable or Disable spanning tree.</li>
<li>Change the login name and password.</li>
<li>Update the modem software and configuration files.</li>
<li>revert back to the default factory settings.</li>
</ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>View Modem Status:</b></p>
The <b>System Status</b> page is a <b>read-only</b> summary of the current modem configuration. It
includes information about the modem software, DSL configuration values, WAN session, and 
LAN parameters. Use it as an overview of the modem's status.<br>
<b><i>You cannot change the Device Name</i></b>
<p align="center">
<img src="./images/webpage_step2.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Set Spanning Tree:</b><br>
</p>
Spanning Tree eliminates loops in a LAN topology, ensuring that there is only one path(or link)
between any two nodes on a network. Use Spanning Tree only when you have already selected a Bridge 
session and when your LAN has more than one device(a PC Only) on your LAN and those devices have
more that one physical path connected to them.
<ol>
<li>Select <b>System</b> on the menu bar then click <b>Configuration</b> to access the <b>System Configuration</b> page.</li>
<li>Select <b>Enable</b> to activate the Spanning Tree protocol for all bridging sessions.</li>
</ol>
<p align="center">
<img src="./images/webpage_step3.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Set Login Name and Password:</b><br>
</p>
You can change Login parameters for the system administrator. The default login name is 
<b>admin</b> and the default password is <b>password</b>.
<ol>
<li>Select <b>System</b> on the menu bar then click <b>Password</b> to access the <b>System Password</b>.</li>
<li>Enter the <b>Current Login Name</b> then enter the <b>Current Password</b>.</li>
<li>Enter the <b>New Login Name</b> the enter the <b>New Password</b>.</li>
<li>Enter the new password again to <b>Confirm New Password</b>.</li>
<li>Click <b>Submit</b>.</li>
</ol>
use the new Login Name and Passowrd the next time you login to the modem.
<p align="center">
<img src="./images/webpage_step4.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Update System Software:</b>
</p>
You can upgrade the software on your modem, to upgrade you must specify the <b>IP address</b>
of the server where the <b><u>new software</u></b> is sotred. The Modem uses <b>TFTP</b> to download
the software which comprises a configuration file(Must be named <b>"celsiancfg"</b>) and 
a image file(must be named <b>"image"</b>).
<ol>
<li>Click <b>System</b> on the menu bar, then <b>System Update</b> to access the <b>System Update</b> page.</li>
<li>Enter the <b>IP Address</b> of the server where the firmware image or configuration file is located.</li>
<li>From <b>Seletct file to Update</b>, do one of the following:
  <ul>
	<li>select <b>Configuration</b> to download the configuration file <b>"celsiancfg"</b>.</li>
	<li>select <b>Image</b> to download the image file <b>"image"</b>.</li>
	</ul> 
</li>
<li>Click <b>Download</b> to start the file download.</li>
</ol>
<p align="center">
<img src="./images/webpage_step5.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Set Factory Defaults:</b>
</p>
When you configure the Modem, you change the factory default settings to new values.
You can return these parameters to their default values to provide a know starting
point if you are troubleshooting or simply want to configure new parameters. 
<br>
<b>active links are lost when rest to factory default values.</b>
<br>
<ol>
<li>Click <b>System</b> on the menu bar, then <b>Factory Defaults</b> to access <b>System Factory Defaults</b> page.</li>
<li>Click <b>Proceed</b> if you want to return all values to their original factory values.</li>
<li>Click <b>Cancel</b> if you do not want to perform this action.</li>
</ol>
<p align="center">
<img src="./images/webpage_step6.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Set Up WAN Sessions:</b></p>
<ol>
<li>Select <b>WAN</b> on the menu bar to access the <b>Session Configuration</b> page.</li>
<li>Select a session from the <b>[Session]</b> list. The configuration option for that
session to appear in the <b>Session X</b> table(where <b>X</b> equals the Session number).</li>
<li>After making changes to any of the settings on this screen, click <b>Apply</b> then save changes.</li>
<li>If you want to undo the changes you made, click <b>Reset</b>.</li>
<li>If you eant to remove a session, then click <b>Delete</b>, then save changes.</li>
</ol>
<p align="center">
<img src="./images/webpage_step7.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Configure the LAN:</b></p>
This configuration sets up the communication between your LAN and the Modem.
<ol>
<li>Select <b>LAN</b> on the menu bar, to access the <b>LAN Settings</b> page.</li>
<li>Make any changes that are neeeded.</li>
<li>Click <b>Apply</b>.</li>
<li>Then save changes.</li>
<li>If you have <b>DHCP</b> enabled, power cycle the modem.</li>
</ol>
<p align="center">
<img src="./images/webpage_step8.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Managing DSL:</b></p>
You can configure the Modem to comply with different standards for DSL transmission.
For the quick configuration, you only select the transmission type. This transmission type
must match the tramssion type specified by the <b>Service Provider</b>.
<ol>
<li>select <b>DSL</b> on the menu bar, then select <b>ADSL Quick Config</b> under <b>[ADSL Options]</b>.</li>
<li>Select the option that applies to you <b>Service Providers</b> transmission type.</li>
<li>Then click <b>Apply</b>.</li>
</ol>
<p align="center">
<img src="./images/webpage_step9.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>DSL Advanced Configuration:</b></p>
You can configure the Modem to comply with different standards for ADSL transmission
as well as parameters.
<ol>
<li>select <b>DSL</b> on the menu bar, then select <b>DSL Advanced Config</b> under <b>[ADSL Options]</b></li>
<li>Configure the settings suppied by your <b>Service Provider</b>.</li>
<li>Then click <b>Apply</b>.</li>
</ol>
<p align="center">
<img src="./images/webpage_step10.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Test DSL:</b></p>
The modem has embedded diagnostics for used to detect line problems or as an aid in
troubleshooting line related techinal problems. These diagnostic programs have a 
significance only to technical support only.
<ol>
<li>
Select <b>DSL</b> on the menu bar, then select <b>Spectrum Tests</b>.
</li>
<li>Slelect the type of test you would like to do. Then click <b>Start</b></li>
<li>To stop the test, click <b>Stop</b>.</li>
</ol>
<p align="center">
<img src="./images/webpage_step11.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>View Link Statistics:</b></p>
Use the ATM stistics on the <b>DSL Link Staticstics</b> page for troubleshooting and monitoring ATM traffic.
<ol>
<li>Select <b>DSL</b> on the menu bar, then select <b>Link Statistics</b> to access the <b>DSL Link Statistics</b> page.</li>
<li>You can view real-time statistics on this page.</li>
</ol>
<p align="center">
<img src="./images/webpage_step12.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>View Error Counters:</b></p>
Use the ATM statistics on the <b>DSL Error Counters</b> page for troubleshooting ATM traffic.
<ol>
<li>Select <b>DSL</b> on the menu bar, then select <b>ADSL Counters</b> to access the <b>ADSL Counters</b> page.</li>
</ol>
<p align="center">
<img src="./images/webpage_step13.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Saving Changes:</b></p>
Use the <b>Save Changes</b> page for saving your current configuration to flash memory. THis option
immediately writes all current sysem configurations to permanent memory(<b>NVRAM</b>). You
cannot selectively write configuration to NVRAM. When you issue the save command, all
current configuration is written to NVRAM.
<ol>
<li>
Select <b>Save Changes</b> on the menu bar.
</li>
<li>You can:<br>
 <ul>
  <li>Click <b>Save</b> to write the configuration to flash memory.</li>
	<li>Click <b>Cancel</b> to exit.</li>
 </ul> 
</li>
</ol>
<p align="center">
<img src="./images/webpage_step14.png" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Rebooting the Modem:</b></p>
Before you reboot the modem, make sure you have saved any configuration changes.<br>
<i><b>see: Save Changes</b></i>
<p align="center">
<img src="./images/webpage_step15.png" border=0>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value=" << Back  " onclick="history.back()">
&nbsp;
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>