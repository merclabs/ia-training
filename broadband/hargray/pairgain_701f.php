<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Pairgain Megabit 701f ADSL Bridge/Router</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Pairgain Megabit 701f ADSL Bridge/Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The Pairgain Megabit 701f ADSL Bridge/Router is a versitile, high speed modem that connects your LAN to one or more
service providers. It employs ADSL technology for asymetric rates up to 7.552
Mbps over a sigle-pair witing and allows for multiple management options including
an easy to use Web-based interface(web-based).
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_pg701f.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<b>The Parigain DSL Modem has 8 LEDs across the front:</b>
<table>
<tr><td class="infohd">LED</td class="infohd"><td class="infohd">State</td><td class="infohd">Description</td></tr>
<tr><td class="infoline" rowspan=2>POWER</td>
<td class="infoline">On Green</td><td class="infoline">Modem has power</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">Modem has no power</td></tr>

<tr><td class="infohd" colspan=3><center>Ethernet</center></td></tr>

<tr><td class="infoline">LINK</td>
<td class="infoline">On Green</td>
<td class="infoline">A PC, hub or other networkdevice is connected to the Modem's 10Base-T interface.</td></tr>

<tr><td class="infoline" rowspan=2>TX</td>
<td class="infoline">Flashing Green</td><td class="infoline">Modem is transmitting data to devices on the LAN.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">Modem is not transmitting data to devices on the LAN.</td></tr>

<tr><td class="infoline" rowspan=2>RX</td>
<td class="infoline">Flashing Green</td><td class="infoline">Modem is receiving data from devices on the LAN.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">Modem is not receiving data from devices on the LAN.</td></tr>

<tr><td class="infoline" rowspan=2>COL</td>
<td class="infoline">Flashing Green</td><td class="infoline">Ethernet packet collisions are occurring on the LAN.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">No Ethernet collisions are occurring.</td></tr>

<tr><td class="infohd" colspan=3><center>DSL</center></td></tr>

<tr><td class="infoline" rowspan=3>SYNC</td>
<td class="infoline">On Green</td><td class="infoline">DSL tranceiver is synchronizied(connected) and in normal operation mode.</td></tr>
<tr><td class="infoline">Flashing Green</td><td class="infoline">Slow flashing green indicated that the DSL transceiver is in a start-up
or handshaking sequence. Fast flashing green indicates the DSL transceiver is in training sequence.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">DSL transceiver is not detecting a transceiver at the far end and is not connected.</td></tr>

<tr><td class="infoline" rowspan=2>TX</td>
<td class="infoline">Flashing Green</td><td class="infoline">modem is transmitting data over the DSL connection.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">modem is not transmitting data over the DSL connection.</td></tr>

<tr><td class="infoline" rowspan=2>RX</td>
<td class="infoline">Flashing Green</td><td class="infoline">modem is receiving data over the DSL connection.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">modem is not receiving data over the DSL connection.</td></tr>

<tr><td class="infoline" rowspan=2>MAR</td>
<td class="infoline">On Green</td><td class="infoline">DSL margin is above the preset margin value.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">DSL margin is below the preset margin value.</td></tr>

</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/backpanel_mb701f.gif" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The Pairgain Megabit 701f ADSL Bridge/Router has 4 ports on the back panel.
<p align="center">
<ul>
<li><b>DSL</b></li>
<li><b>CONSOLE</b></li>
<li><b>10-BASE-T</b></li>
<li><b>POWER</b></li>
</ul>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_mb701f.jpg" border=0>
</center>
</td>
</tr>
 <td class="heading" valign=top colspan=2><b>Web Interface</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
For information on how to configure the Pairgain Megabit 701f ADSL Modem via a 
web-browser, click the link below.<br>
<a href="webinterface_701f.php"><b>Pairgain Megabit 701f ADSL Bridge/Router Web Interface</b></a>
</td>
</tr>
</tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
standard DSL/Cable Modem Troubleshooting applies.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>