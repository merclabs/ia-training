<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Paradyne StormPort</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Paradyne StormPort</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Hargray distributes the <b>Paradyne Stormport</b> modems, these modems have two service types, <b>DSL</b> and 
<b>Cable</b>. Please be sure to check what brand of modem as well as what type service type
you are woking with before doing any troubleshooting. Customer are given a <b>Linksys Router</b> along with 
their <b>Paradyne Stormport</b> modem. Two type of routers are distributed, the 1 port and 4 port Linksys Routers. Customers
that received a DSL/Cable modem before Hargray started distributing routers with DSL/Cable, need to contact
Hargray customer service if they find they need a router.(there is a charge that will apply for the router
in addition to the DSl/Cable service.). 
 
</p>
<p>
<font color="red"><b>Important:</b></font> <b>Do not</b> attempt to use <b>any</b> software CD that may be included 
in the high speed modem or router boxes. No software installation is necessary to complete the high speed
installation.
</p>
<p>
<img src="./images/paradyne_stop.gif" border=0><br><br>
<b>Check Ethernet Port:</b><br> 
Be sure customer has a working Ethernet card on their computer. If customer does not have an Ethernet
card on their system, have them contact a computer dealer to purchase a <b>Network Card(NIC)</b>
or <b>Universal Serial Bus(USB) Ethernet adapter</b>.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_stormport.jpg" border=0>
</p>
</td>
</tr>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>DSL Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>DSL-For Customers who receive HIGH Speed Internet Service over telephone line</b>
<ol>
<li>Connect the <b>Line</b> jack of the StormPort modem to your telephone wall jack
using the included gray cable.</li>
<li>
Plug the power supply cable into the modem and a standrd electrical outlet. Modem
<b>Power</b> light should be solid green. <b>Xfer</b> light should initially be orange,
the eventually rapidly flash green. If the <b>Xfer</b> light does not flash green after several minutes, call
Hargray support at 611, option 2
</li>
<li>
Connect an Ethernet cable from the modem's <b>Ethernet</b> port to the <b>WAN</b>
port on your <b>Linksys Router</b>.
</li>
</ol>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_dsl_hargray.jpg" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Cable Set-UP</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>HFC-For Customers who receive High sped Internet Service over Cable TV line.</b>
<ol>
<li>Connect an Ethernet cable to the <b>WAN</b> on the back of the <b>Linksys Router</b>
to the special network wall jack installed by a technicians.</li>
</lo>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_cable_hargray.jpg" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Set-Up Router</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Plug the Linksys power supply cable to the router and a standard electical outlet.
Confirm that <b>Power</b> light glows solid green. If <b>Link</b> light or the <b>WAN</b>
column of the router, indicating a connection to the <b>StormPort</b> modem or your network
wall jack, does not glow green, call Hargray support at 611, option 2 for assistance.
</li>
<li>
Connect another Ethernet cable from your computer's Ethernet port to the Router's
<b>LAN</b> port. (The <b>LAN</b> ports are numbered on multi-port routers.) When your 
computer is on, confirm that the <b>Link/ACT</b> light in the <b>LAN</b> or numbered column of the router
glows green.
</li>
<li>
<b>Reboot</b> you computer before continuing to router configuration.
</li>
</ol>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="./images/linksys_routers.jpg" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Router Configuration</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<ol>
<li>
On your computer's desktop, locate <b>Internet Explorer</b> icon and click, using
your right mouse botton. With the left mouse button, click <b>"Properties"</b> then
<b>"Connections".</b> Select <b>"Never Dial a Connection"</b>. Click <b>OK</b> to save
the settings.
</li>
<li>
Lanuch <b>Internet Explorer</b>, ignore the error message as it attempts to try to display
your homepage. Click in the white address bar, clear out anything that appears in the address bar.
and enter <font color="red"><b>192.168.1.1</b></font> then press <b>Enter</b> to display the <b>Router
Administration</b> screen. 
</li>
<li>
Press <b>Tab</b> to skip the username field, and enter <font color="red"><b>admin</b></font><b>(in lowercase),</b>
then click <b>OK</b> to display the <b>Set Up</b> screen.
</li>
<li>
Follow the diagram below, change the <b>WAN</b> connection type to <b>PPPoE</b>. Enter your <b>Hargray</b>
username and password and click the button next to the <b>Keep Alive</b> field. Click <b>Apply</b> to save your
settings. The router should respond with a message <b>"Settings are Successful"</b>.
</li>
<li>
Click continue, then Click the <b>Status</b> tab at the top of the page. The status should change from <b>"Connecting..."</b>
to <b>"Connected"</b>.
</li>
<li>
<b>That's it!</b>, The customer should be connected. Click the <b>Home</b> button at the top of <b>Internet Explorer</b>
to go to your preset <b>Homepage</b>. The router shouldn't have to be configured again.
</li>
</ol>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/linksys_setup.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>