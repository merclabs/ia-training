<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Troubleshooting MegaWire</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Troubleshooting MegaWire</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
ChesterTel uses PPPoE software called <b>MegaWire</b>.<br><br>
<font color="red"><b>NOTE TO TECH SUPPORT:</b></font><br><br>
<b>If the LINE light is not on:</b>
<ul>
<li>Always instruct the customer to bypass their surge protector.</li>
<li>verify that the modem is not connected to a filter and that it is connected to the correct jack.</li>
<li>Power cycle the modem, if it still not on after 1 minute, escalate.</li> 
</ul>
If you click on <b>MegaWire</b> and the display shows <u>FAILED ON CREATE DEVICE</u>, you need 
to uninstall and then reinstall <b>MegaWire</b>.<br>This occurs if you load MegaWire without 
loading your network adapter drivers first.
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>FAIL TO LOAD TAP</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Go to <b>Start</b>, Then to <b>Programs</b>, <b>Chester Long Distance Services Inc. MegaWire</b>,
click on <b>MegaWire</b>.
<p align="center">
<img src="./images/ms_troubleshoot1.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Go to <b>Connections</b>, and click on <b>Settings</b>.
<p align="center">
<img src="./images/ms_troubleshoot2.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The an "<b>Application Settings</b>" window should appear, click on <b>Advanced</b>.
<p align="center">
<img src="./images/ms_troubleshoot3.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
In the <b>Network Access</b> section, select <b>Protocol Driver</b>, then click <b>OK</b>.
<p align="center">
<img src="./images/ms_troubleshoot4.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>FAILED ON CREATE DEVICE</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If you see the following screen when you double click <b>MegaWire</b>, you need to
check your <b>Network Adapter Properties</b>.
<p align="center">
<img src="./images/ms_troubleshoot5.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click <b>OK</b> and the following screen will display. 
<p align="center">
<img src="./images/ms_troubleshoot6.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click <b>OK</b> and the following screen will display. 
<p align="center">
<img src="./images/ms_troubleshoot7.jpg" border=0>
</p>
<br><br>
Click <b>OK</b>, then <b>Exit</b> to exit the <b>MegaWire application</b>.<br>
Now you can check or <a href="ms_updatedriver.php"> modify</a> the network adapter properties.
<br><br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Also see:</b> <b><a href="ms_uninstall.php">Uninstalling MagaWire</a></b>,<b>
<a href="ms_install.php">Installing MagaWire</a></b>.
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>LOGIN FAILURE</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
When <b>Megawire</b> is executed, if you receive an error message saying <b>Login Failed</b>,
make sure that the customer has entered the <font color="red">correct</font> <b>User Name</b> 
and <b>Password</b>.
</p>
<b>Retype: <font color="red">username and password, use only lowercase letters.</font></b>
<p align="center">
<img src="./images/ms_using8.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>