<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Zoom 5041 CableModem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Zoom 5041 CableModem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The <b>Zoom CableModem Model 5041</b> has both <b>10/100Base-T Ethernet</b>
and <b>USB</b> interfaces to support a wide range of operating systems, <b>Chestertel</b> only 
configures using <b>Ethernet</b>... This is the Primary
Modem issued by <b>Chestertel</b> and like the ADSL modem the <b>SYNC LED</b> should be the first thing
to check. The Network Adapter should be set to obtain an IP Address automaticlly.<br>
<br>
To access <b>Chestertel's</b> website <b><a href="http://207.144.13.2" target="_blank">click here</a></b>.
</p>
<p>
<b>NOTE:</b><br>
<font color="red"><b>In "Internet Options", do not set it to connect via a "LAN",
 the correct setting is "Always dial my default connection". 
</b></font>
</p>
<p>
For more information see:<b> <a href="#CYB">Configuring Your Browser</a> below.</b>
</p>
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
<p>
When a customer is issues a Cable Modem they are also given a Linksys NIC Card if needed.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_zoom5041.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr><td class="infohd">LED</td><td class="infohd">MODE</td><td class="infohd">STATUS</td></tr>
<tr><td class="infohd">PWR</td><td class="infoline">Lit</td><td class="infoline">Power On.</td></tr>
<tr>
<td class="infohd" rowspan=3>SYNC</td>
<td class="infoline">Blinking Fast</td><td class="infoline">Searching for �data� channel from cable provider.</td></tr>
<td class="infoline">Blinking Slow</td>
<td class="infoline">
Ranging (synchronizing the signal
for optimum performance) and
Registering (signing on to the cable
company�s network).</td></tr>
<td class="infoline">Steady(Lit)</td><td class="infoline">The modem is synchronized to the cable system.</td></tr>
<tr><td class="infohd">ACT</td><td class="infoline">Blinking</td>
<td class="infoline">Data activity is present on the cable.
The cable modem is communicating
with the cable system. It may be
downloading data or uploading data
to the cable service provider and the
Internet.</td></tr>
<tr><td class="infohd">10/100</td><td class="infoline">Lit</td>
<td class="infoline">The Ethernet (10/100BaseT) computer
interface is connected.</td></tr>
<tr><td class="infohd">USB</td><td class="infoline">Lit</td>
<td class="infoline">The USB computer interface is connected.</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_zoom5041.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<table>
<tr><td class="infohd"></td class="infohd"><td></td></tr>
<tr><td class="infohd">POWER</td><td class="infoline">Off/On switch for Cable Modem</td></tr>
<tr><td class="infohd">CABLE</td><td class="infoline">Connects the Cable Mode to Cable Network</td></tr>
<tr><td class="infohd">ETHERNET</td><td class="infoline">Connects the Cable Modem to the PC's NIC Card</td></tr>
<tr><td class="infohd">USB</td><td class="infoline">Allows the Computer to communicate with Cable Modem via USB.</td></tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_zoom5041.gif" border=0>
</p>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b><a name="CYB">Configuring Your Browser</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
An Internet browser is a program used to find and display Web
pages. To find a page, the browser must connect to the Internet,
either via phone lines or a LAN (Local Area Network). If
you are using a cable modem with Chestertel, your browser needs to be set to <b>
"Always dial my default connect"</b>.
</p>
To configure your bowser do the following:
<ol>
<li>Start <b>Internet Explorer</b>.</li>
<li>Click the <b>Tools</b> menu, and then <b>Internet Options</b>.</li>
<li>In the <b>Internet Properties</b> dialog box, click the <b>Connections</b> tab.</li>
<li>On the <b>Connections</b> tab, select <b>"Always dial my default c<u>o</u>nnection."</b>.</li>
<li>Then click the <b>LAN Settings</b> button.</li>
<li>Deselect <b>"Automatically detect settings"</b> and click <b>OK</b>.</li>
</ol>
<p align="center">
<img src="./images/internet_options_zoom5041.gif" border=0>
</p>
<b>LAN Settings:</b>
<p align="center">
<img src="./images/lan_settings_zoom5041.gif" border=0>
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Power-Cycle Process:</b></p>
As your cable modem powers itself up, the lights on the front
panel indicate the different stages. When the modem initially powers
itself up, the <b>PWR</b> light comes on. After completing a <b>self-test</b>,
the cable modem registers with the cable system. While the modem
is searching for and registering with the cable system, the <b>SYNC</b>
light blinks: At first the light blinks fast; then it blinks slowly. After
the process is complete, the <b>SYNC</b> light stays on steady.
After the cable modem is connected, the <b>10/100</b> or <b>USB</b> light (depending
on the interface you are using) comes on. This indicates
that the cable modem and the computer�s data interface are linked.
If you do not see the <b>10/100</b> or <b>USB</b> light on, double-check to see
that the Ethernet cable or USB cable is securely inserted in the
proper jacks on the cable modem and computer. The <b>PWR</b> light
must be on before the <b>10/100</b> or <b>USB</b> light will come on.
<br><br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>