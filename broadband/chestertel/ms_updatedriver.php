<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Updating Network Drivers - MegaWire Software</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Updating Network Drivers - MegaWire Software</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If you receive the followiing error message, check or <a href="#fix">
update</a> your the network adapter.
<p align="center">
<img src="./images/ms_troubleshoot7.jpg" border=0>
</p>
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b><a name="fix">Updating Network Driver</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Right click on <b>My Computer</b> then <b>Properties</b>.
<p align="center">
<img src="./images/ms_updatedriver1.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The <b>System Properties</b> screen should display.
<p align="center">
<img src="./images/ms_updatedriver2.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click the <b>Device Manager</b> tab.<br>
<b><font color="red">NOTICE:</font> If a <img src="./images/ms_updatedriver10.jpg" border=0>
displays beside the PCI Ethernet Controller, this means the driver is not installed.</b>
<p align="center">
<img src="./images/ms_updatedriver3.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Double click the <b>PCI Ethernet Controller</b>.
<p align="center">
<img src="./images/ms_updatedriver4.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following window should appear, click <b>Reinstall Driver</b> button. 
<p align="center">
<img src="./images/ms_updatedriver5.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following window should appear, click <b>Next</b>.
<p align="center">
<img src="./images/ms_updatedriver6.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Now if you have the <b>CD</b> that we provided, it has the drivers for 
the network card. Insert the <b>CD</b> into your <b>CD-ROM</b> drive.
Select "<b>Specify a location:</b>" and type your <b>CD-ROM</b> drive letter and <b>:\Win98</b>.
<p align="center">
<img src="./images/ms_updatedriver7.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click <b>Next</b>.
<p align="center">
<img src="./images/ms_updatedriver8.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click <b>Next</b>, then <b>Finish</b>. Your <b>Network Driver</b> should now be Updated.
<p align="center">
<img src="./images/ms_updatedriver9.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>