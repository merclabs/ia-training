<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Uninstalling MegaWire Software</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Uninstall MegaWire</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
ChesterTel uses PPPoE software called <b>MegaWire</b>.<br><br>
<font color="red"><b>NOTE TO TECH SUPPORT:</b></font><br><br>
<b>If the LINE light is not on:</b>
<ul>
<li>Always instruct the customer to bypass their surge protector.</li>
<li>verify that the modem is not connected to a filter and that it is connected to the correct jack.</li>
<li>Power cycle the modem, if it still not on after 1 minute, escalate.</li> 
</ul>
If you click on <b>MegaWire</b> and the display shows <u>FAILED ON CREATE DEVICE</u>, you need 
to uninstall and then reinstall <b>MegaWire</b>.<br>This occurs if you load MegaWire 
without loading your network adapter drivers first.
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Starting the uninstallShield</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
From <b>Start</b> select <b>Programs</b>, then <b>Chester Long Distance Service 
Inc., MegaWire</b> then <b>Uninstall MegaWire</b>.
<p align="center">
<img src="./images/ms_uninstall1.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Comfirm File Deletion</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
A "<b>Confirm file Deletion</b>" window will appear, Click <b>Yes</b>.
<p align="center">
<img src="./images/ms_uninstall2.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Removal of Shared Files</b></td>
</tr>
<td class="field" valign=top colspan=2>
You will be prompted with a window to remove shared files, in most cases its <b>OK</b> to click
"<b>Yes to All</b>" and will not cause any problems for other applications because these files are not
being used by any other programs.
<p align="center">
<img src="./images/ms_uninstall3.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Removing Network TeleSystem PPPoE Adapter(NTSP3)</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Please wait while the uninstallShield removes the <b>Network TeleSystems PPPoE Adapter(NTSP3)</b>
<p align="center">
<img src="./images/ms_uninstall4.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Shared Files Prompt</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
You may be prompted again about shared files, click <b>Yes</b>. 
<p align="center">
<img src="./images/ms_uninstall5.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Uninstall Progress</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The uninstallShield should progress through the rest of the removal process.
<p align="center">
<img src="./images/ms_uninstall6.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Next, you will be prompted to <b>Restart</b>, click "<b>Yes</b>" to restart and finalize removal of <b>MegaWire</b>. 
<p align="center">
<img src="./images/ms_uninstall7.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>