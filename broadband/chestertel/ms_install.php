<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Installing MegaWire Software</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>MegaWire Software Set-Up</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
ChesterTel uses PPPoE software called <b>MegaWire</b>.<br><br>
<font color="red"><b>NOTE TO TECH SUPPORT:</b></font><br><br>
<b>If the LINE light is not on:</b>
<ul>
<li>Always instruct the customer to bypass their surge protector.</li>
<li>verify that the modem is not connected to a filter and that it is connected to the correct jack.</li>
<li>Power cycle the modem, if it still not on after 1 minute, escalate.</li> 
</ul>
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Starting Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
When you insert the MegaWire CD into your CD-ROM, the following screen should be displayed.
<p align="center">
<img src="./images/ms_install1.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Starting CD with Run</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If you do not have auto run enabled on your PC, do the following:
<ul>
 <li>Click on <b>Start</b></li>
 <li>Click on <b>Run</b></li>
 <li>Type your <b>CD</b> drive letter and <b>Setup.exe</b> as shown in the following screen.</li>
 <li>Click <b>OK</b></li>
</ul>
<p align="center">
<img src="./images/ms_install2.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Quick Install</b></td>
</tr>
<td class="field" valign=top colspan=2>
The following screen will be displayed, leave <b>"Quick Install"</b>selected, then Click <b>Next</b>.
<p align="center">
<img src="./images/ms_install3.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>User/Password</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Type your <b>username</b> and <b>password</b> and Click <b>Next</b>.<br>
<font color="red"><i>Your username and password must be lower case.</i></font>
<p align="center">
<img src="./images/ms_install4.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Installing files...</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following screens will be displayed as the application software is loading.
<p align="center">
<img src="./images/ms_install5.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Please wait...</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following screens will continue to display while the software sets up registry information.
<p align="center">
<img src="./images/ms_install6.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Installing PPPoE Adapter(NTSP3)</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following screens will be display as the Network TeleSystems PPPoE Adapter is installed.
<p align="center">
<img src="./images/ms_install7.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Finished...</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click <b>Finish</b> to restart your computer after installation is complete.
<p align="center">
<img src="./images/ms_install8.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>