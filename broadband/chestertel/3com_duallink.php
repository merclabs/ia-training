<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>3Com HomeConnect ADSL Modem Dual Link</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>3Com HomeConnect ADSL Modem Dual Link</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>The <b>3Com Dual Link ADSL</b> modem can be connected via <b>Ethernet</b> or a <b>USB</b> port. 
We <b>DO NOT</b> recommend <b>USB</b> because customers have so much trouble with it 
associated with the <b>Power Saver</b> functions of their Computer. <b><i>see:<a href="#ts">Troubleshooting Information</a></i></b>
</p>
<p>
We use <b>ADSL filters</b> outside the customers home, therefore the <b>WireSpeed</b> modem 
must be plugged into the jack in which it was installed in.
Some installations require a telephone at the same location as the <b>WireSpeed</b> modem, 
we accomplish this with a <b>Y adapter</b> and a <b>microfilter</b>. The telephone plugs into the 
microfilter and the modem plugs into the other side of the Y adapter.
A microfilter filters keeps the <b>ADSL frequency</b> from <b>interfering</b> with the telephone.
</p>
<p align="center">
<img src="./images/linefilters_wt36R51X.jpg" border=0>
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_3CP4130.jpg"></td>
</p>
</tr>
<tr>
<td class="field">
<p align="center">
<table> 
 <tr>
 <td class="infohd" valign=top width=10%><b>LED</b></td>
 <td class="infohd" valign=top width=30%><b>State</b></td>
 <td class="infohd" valign=top width=60%><b>Description</b></td>
</tr>
<tr>
 <td class="infohd" valign=middle rowSpan=6>Alert</TD>
 <td class="infoline" valign=top>Off</TD>
 <td class="infoline" valign=top>The modem is operating properly</TD></TR>
<tr>
 <td class="infoline" valign=top>Flashing Orange</TD>
 <td class="infoline" valign=top>During boot, the modem is checking for the download of new software. 
      This lasts for about 4 seconds.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Green slowly</TD>
 <td class="infoline" valign=top>While the modem is initializing. This lasts for 30 to 40 seconds after 
      booting the modem.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Green rapidly</TD>
 <td class="infoline" valign=top>The reset button is being held in and the modem has just been powered 
      on. This lets you know that the modem has recognized the reset request. It 
      lasts for about 4 seconds.</TD>
</tr>
 <tr>
 <td class="infoline" valign=top>Orange</TD>
 <td class="infoline" valign=top>An unexpected error has occurred, but it does not affect the operation 
      of the modem.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Red</TD>
 <td class="infoline" valign=top>A non-recoverable error has occurred. If this happens, the modem will automatically reboot.</TD>
</tr>
<tr>
 <td class="infohd" valign=top>Power</TD>
 <td class="infoline" valign=top>Green</TD>
 <td class="infoline" valign=top>Power is on.</TD>
</tr>
<tr>
 <td class="infohd" valign=middle rowSpan=3>LAN Status</TD>
 <td class="infoline" valign=top>Off</TD>
 <td class="infoline" valign=top>No Ethernet signal has been detected.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Green</TD>
 <td class="infoline" valign=top>A PC or hub is properly connected to the Ethernet port.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Green</TD>
 <td class="infoline" valign=top>A proper connection exists and data is present on the Ethernet 
  port.</TD>
</tr>
<tr>
 <td class="infohd" valign=middle rowSpan=5>USB Status</TD>
 <td class="infoline" valign=top>Off</TD>
<td class="infoline" valign=top>No USB signal has been detected.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Orange</TD>
 <td class="infoline" valign=top>A PC is properly connected to the USB port but the modem has not "enumerated." (When you plug in the modem, the PC senses voltage differences in the USB network and proceeds to query (enumerate) the modem for type, vendor, functionality and bandwidth required.)</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Orange</TD>
 <td class="infoline" valign=top>The modem has enumerated with the PC. This means that the operating system running on the PC has recognized the modem.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Green</TD>
 <td class="infoline" valign=top>The modem's software has established communications with the 3Com USB driver software running on the PC.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Green</TD>
 <td class="infoline" valign=top>The modem is working and data is present on the USB port.</TD>
</tr>
<tr>
 <td class="infohd" valign=middle rowSpan=4>ADSL Status</TD>
 <td class="infoline" valign=top>Off</TD>
<td class="infoline" valign=top>No ADSL signal has been detected</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Orange</TD>
 <td class="infoline" valign=top>The modem is attempting to synchronize with the DSL service provider's equipment.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Green</TD>
 <td class="infoline" valign=top>The link has been established between your modem and the DSL service provider's equipment.</TD>
</tr>
<tr>
 <td class="infoline" valign=top>Flashing Green</TD>
 <td class="infoline" valign=top>The link is up and there is data on the ADSL port</TD>
</tr>
</table>
<br><br>
</p>
</td>
</tr>
 <td class="heading" valign=top colspan=2><b><a name="ts">Troubleshooting Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
We program the <b>3Com Dual Link ADSL</b> modem by pointing a browser to <b>192.168.1.100</b> 
when we install it. No additional programming is required by the customer or support. 
<br><br>
<b>NOTE TO INSITE:</b> Always instruct the customer to bypass their surge protector.<br><br>
<b>If the Ready Light is flashing:</b>
<ul>
<li>Verify that the modem is not connected to a filter and that it is connected to 
the correct jack.</li>
<li>Power Cycle the DSL Modem</li>
<li>If it still flashes after 1 minute, send us an E-Mail.</li>
</ul>
</td>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
