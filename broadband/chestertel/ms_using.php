<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Using MegaWire Software</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>MegaWire Software</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
ChesterTel uses PPPoE software called <b>MegaWire</b>.<br><br>
<font color="red"><b>NOTE TO TECH SUPPORT:</b></font><br><br>
<b>If the LINE light is not on:</b>
<ul>
<li>Always instruct the customer to bypass their surge protector.</li>
<li>verify that the modem is not connected to a filter and that it is connected to the correct jack.</li>
<li>Power cycle the modem, if it still not on after 1 minute, escalate.</li> 
</ul>
<p>
<b>NOTE:</b><br>
<font color="red"><b>If a customer continues to have "user/pass" errors even after 
we have determined that all user information is vaild, please see an LT,&nbsp;&nbsp;get permission 
to write-up for escalation to Chestertel.
</b></font>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Start MegaWare</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Locate the <b>MegaWire</b> icon on your desktop, and <b>double</b> click it.
<p align="center">
<img src="./images/ms_using1.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>From Start</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If you do not see a <b>MegaWire</b> icon, use the <b>Start Menu</b> to access the program.
<p align="center">
<img src="./images/ms_using2.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Connecting...</b></td>
</tr>
<td class="field" valign=top colspan=2>
Once the program is executed the following screen will appear, the screen below shows 
the Authentication process. If you entered the correct username or password, the following 
screen will appear.
<p align="center">
<img src="./images/ms_using3.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Incorrect username and/or password</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If you entered a incorrect username or password, the following screen will appear.
<p align="center">
<img src="./images/ms_using8.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Connected</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Once connected, two computers will be displayed in the systray, in the lower right hand corner.
<p align="center">
<img src="./images/ms_using4.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Status</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Double click on the computers in the  systray to see the following.
<p align="center">
<img src="./images/ms_using5.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Details</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click Details to see the following.
<p align="center">
<img src="./images/ms_using6.jpg" border=0>
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Disconnecting</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Click Disconnect and the following screen will appear.
<p align="center">
<img src="./images/ms_using7.jpg" border=0>
<br><br>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>