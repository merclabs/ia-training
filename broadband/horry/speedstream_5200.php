<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5200 Series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5200 DSL Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SpeedStream for businesses can be set up for Router mode or Bridge mode. No WinPoET software required.
Check the Network setting and set the TCP/IP for NIC Card obtain IP Address automaticlly and as default on Advanced tab.</p>
<p>
Devices such as fax machines, caller ID boxes, or phones that share the same phone number as your DSL
account require a line filter, which prevents modem noise from disrupting the DSL signal on the phone line.</p>
<br>
<img src="./images/dslphonefilter.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><img src="./images/frontpanel_ss5200.png" border=0></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td><td class="infohd">Power</td><td class="infohd">DSL</td><td class="infohd">Act</td><td class="infohd">Enet</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/unit.png" border=0></center></td><td class="infoline">Power Off</td><td class="infoline">DSL link not detected</td><td class="infoline">No Network activity</td><td class="infoline">Ethernet port not connected: check Ethernet cable connection</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/solid.png" border=0></center></td><td class="infoline">Power On</td><td class="infoline">Ready for Data Traffic</td><td class="infoline">N/A</td><td class="infoline">Ethernet port connected to LAN</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/blinking.png" border=0></center></td><td class="infoline">N/A</td><td class="infoline">Searching for Data</td><td class="infoline">DSL traffic flow</td><td class="infoline">Ethernet traffic flow</td></td>
</tr>
</table>

<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/rearpanel_ss5200.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infotxt" colspan=2>From left to right:</td></tr>
</tr>
<tr>
<td class="infohd">Power</td> <td class="infoline">The bridge uses a standard AC power cord and has anON/OFF switch</td></tr>
</tr>
<tr>
<td class="infohd">DSL Port</td><td class="infoline">DSL connectivity is through this 8-pin, RJ-45 port.</td></tr>
</tr>
<tr>
<td class="infohd">Ethernet Ports</td><td class="infoline">One Ethernet 10Base-T port (8 pin, RJ-45)</td></tr>
</tr>
<tr>
<td class="infohd">Console Port</td><td class="infoline">Asynchronous RS232 connectivity is through this 8 pin,RJ-45 port (support only).</td></tr>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Bridge Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Place your bridge in a location where it will be well ventilated. Do not stack
it with other devices or place it on carpet.</li>
<li>Connect the bridge to your PC, using the red cable between the bridge�s
Ethernet port and the Ethernet port on your PC.</li>
<li>Connect the bridge to your DSL wall jack, using the purple label cable
between the bridge�s DSL port and the DSL wall jack.</li>
<li>Connect the bridge to an AC power outlet using the supplied power cord.</li>
<li>Switch the bridge ON.</li>
</ol>
The following illustration shows the SpeedStream 5200 Series Bridge when
connected:
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/basicsetup_ss5200.png" border=0>
<img src="./images/frontlights_ss5200.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Reminder:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
<p>
<b>A.</b> If the Speed Stream is set up in Router mode you will be able to open
your web browser and pull up the configuration page of the modem:<b>192.168.100.1</b>.
Here you will be able to see if the modem has a WAN IP of <b>66.153.*.*</b>.
</p>
<p>
If all light are on modem but can not browse or WAN IP address is 0.0.0.0, click on <b>Simple Setup</b> on the left
of screen and go to username/password. Have customer type their username/password and reset modem. If all lights
are on and their username/password is not working and modem not getting a <b>66.153.*.*</b> address, escalate.
</p>
<p>
Check the status on the modem and power cycle modem. If the DSL light is flashing,this means there is a problem with the DSL line.
Make sure line is plugged into modem properly. If so and DSL light still flashing after power cycling modem, escalate.
</p>
<p>
If the <b>Enet</b> light is not on, then the Ethernet connection to modem and PC are not linked. Check network cables. 
</p>
<p>
<b>B.</b> If business has router such as Linksys router and the SpeedStream modem should be in bridge mode. If the modem is in bridge mode,
 you will not be able to access router config page. The Linksys router will need to be set up for <b>PPPoE</b> connection. the WAN address on
router should have the <b>66.153.*.*</b> address.
</p>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>