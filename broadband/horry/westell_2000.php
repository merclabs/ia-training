<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Westell ADSL 2000</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Westell ADSL 2000</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Westell�s ADSL Modem uses an existing phone line to provide reliable, high-speed, Internet
access for home or office. The ADSL Modem converts an existing phone line into a
dedicated, high-speed connection ending the inconvenience of dial-up modems and busy
signals.
<br><br>
<b>Note:</b>You may see some modems that connect via PPPOE. 
<!-- posted 10/29/03, prompted by travis wilson working on a call.-->
</p>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Note:</b> Please check case of all <b>User/Pass</b> information that the
customer is entering on configuration pages. Horry customer <b>User Names</b> are all <b>lowercase</b> and <b>Passwords</b>
are both <b>upper</b> and <b>lowercase</b>. 
</p>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Reminder:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_wt2000.jpg" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<b>Power:</b>
<table>
<tr>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Solid Green</td><td class="infoline">Power ON</td>
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No power</td>
</tr>
</table>
<br>
<b>Ready:</b>
<table>
<tr>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Slow Flashing Green<td class="infoline">Power ON and passed power-up diagnostics (1 flash/sec)</td>
</tr>
<tr>
<td class="infohd">Moderate Flashing Green<td class="infoline"> Power ON and attempting synchronization (2 flashes/sec)</td>
</tr>
<tr>
<td class="infohd">Solid Green<td class="infoline"> Power ON and synchronized with the ADSL line card</td>
</tr>
<tr>
<td class="infohd">Solid Red (less than 20 sec.)<td class="infoline"> Hardware power-up in progress</td>
</tr>
<tr>
<td class="infohd">Blinking Red<td class="infoline"> Unit failed power-up diagnostic</td>
</tr>
<tr>
<td class="infohd">Alternating Red/Green<td class="infoline"> Modem diagnostic failed</td>
</tr>
<tr>
<td class="infohd">No Light<td class="infoline"> No power</td>
</tr>
</table>
<br>
<b>Link:</b>
<table>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Solid</td><td class="infoline">GreenLink established</td>
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No 10BaseT link</td> 
</tr>
</table>
<br>
<b>Activity:</b>
<table>
<tr>
<td class="infohd">State</td> <td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Pulsing Green</td> <td class="infoline">Data being transmitted. Pulses should match the reception or transmission of Ethernet data.
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No data on Ethernet interface</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_wt2000.jpg" border=0>
</center>
<p>
The following items are located on the rear panel of the ADSL Modem.
</p>
<table>
<tr>
<td class="infohd">Symbol&nbsp;&nbsp;&nbsp;</td><td class="infohd">Name</td><td class="infohd">Type</td><td class="infohd">Function</td>
</tr>
<tr>
<td class="infohd"><center><img src="./images/symbol1_wt2000.png" border=0></center></td><td class="infohd">LINE</td><td class="infoline">6-pin (RJ11) modular jack</td><td class="infoline">POTS/DSL is used to connect the telephone jack with ADSL</td>
<tr>
<tr>
<td class="infohd"><center><img src="./images/symbol2_wt2000.png" border=0></center></td><td class="infohd">POWER</td><td class="infoline">3-pin mini-DIN connector</td><td class="infoline">Power source from plug-top transformer</td>
</tr>
<tr>
<td class="infohd"><center><img src="./images/symbol3_wt2000.png" border=0></center></td><td class="infohd">ETHERNET</td><td class="infoline">8-pin (RJ45) jack</td><td class="infoline">Connects to PC or 10/100 BaseT hub</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<ol>
<li>Verify that you have an available Ethernet card. Note: See the above caution box.</li>
<li>Connect the Ethernet cable to the Ethernet port on your computer and to the Ethernet jack marked <img src="./images/symbol3_wt2000.png" border=0>
on the rear panel of the modem.</li>
<li>Connect one end of the power supply cord to the connector marked <img src="./images/symbol2_wt2000.png" border=0> on the rear panel of the modem. Plug the
other end of the power supply cord into an AC wall socket.</li>
<li>Connect the DSL phone cable from the rear panel of the modem marked <img src="./images/symbol1_wt2000.png" border=0> to the DSL-equipped telephone line jack
on the wall.<b>IMPORTANT</b>: Do not use a DSL filter on this connection. Note: You must use the modem phone cord
that was provided with your modem kit.</li>
<li>Wait for the Ready, and Link LEDs to light steady green.</li>
</ol>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/setup_wt2000.png" border=0>
</center>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>System Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Windows 95/98/NT</b>
<ol>
<li>Ask whether your service provider uses static or dynamic (DHCP) IP addressing.</li>
<li>Start your Windows environment.</li>
<li>Click <b>Start</b> > <b>Settings</b> > <b>Control Panel</b>, then double-click the <b>Network</b> icon.</li>
<li>Click the <b>Configuration</b> or <b>Protocols</b> tab.</li>
<li>Select <b>TCP/IP</b>.</li>
<li>Click the <b>Properties</b> or <b>Protocol</b> button.</li>
<li>When the <b>TCP/IP Properties</b> window displays, click the <b>IP Address</b> tab.</li>
<li>If your ISP uses <b>DHCP</b>, select <b>Obtain an IP address automatically</b>. If your service provider uses <b>Static
IP Addressing</b>, select the <b>IP address box</b> and enter the <b>IP</b> address and <b>subnet mask</b> assigned by your ISP.</li>
<li>Close all Windows and <b>restart</b> your computer (if required).</li>
</ol>
<b>Macintosh</b>
<ol>
<li>Ask whether your service provider uses static or dynamic (DHCP) IP addressing.</li>
<li>Start your Mac.</li>
<li>From the <b>Apple</b> icon, select <b>Control Panels</b>, then <b>TCP/IP</b>.</li>
<li>Under the <b>Setup box</b>, select <b>Connected by</b> and choose <b>�Ethernet�</b> from the drop-down list box.</li>
<li>From <b>Configure</b>, choose <b>�Manually�</b> or <b>�DHCP�</b>.</li>
<li>Close all windows and <b>restart</b> your computer.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Standard DSL Troubleshooting(Power Cycle, check Network, LAN settings, physical connections, Status lights.)
</p>
<b>WinPoET PPPoE</b> software is required to connect DSL Modem for Windows 95/98/ME users. If customer is using <b>Windows XP</b>, 
they do not need the <b>WinPoET PPPoE</b> software. They need to use the built in <b>PPPoE</b> connection(Broadband Connection with User/Pass).
<p> 
If getting errors when connecting with software,check the status of the lights on the modem. Power Cycle.
</p>
<p>
The top 3 lights should be on(<b>Power, Ready and Link</b>). If the <b>Ready</b> light is flashing red or green then there is a problem
wiht the DSL connection, escalate. 
</p>
<p>
If the <b>Link</b> light is not on the check the ethernet link from modem to pc. 
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

