<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5667 USB/Ethernet Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5667 USB/Ethernet DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SpeedStream router may be connected to either an existing USB port or an Ethernet port. Both
connection methods are shown below. This Modem runs in router mode only.
</p>
<p>
To access the Internet through the SpeedStream Dual USB/Ethernet Router, the TCP/IP protocol must be
installed on your computer and configured with the same IP address and subnet as the router.
</p>
</p>
<p>
</td>
</tr>
<tr>
<td class="field">
Devices such as fax machines, caller ID boxes, or phones that share the same phone number as your DSL
account require a line filter, which prevents modem noise from disrupting the DSL signal on the phone line.
</p>
<br>
<img src="./images/dslphonefilter.png" border=0>

</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Software Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<img src="./images/cdrom_icon.png" border=0><br>
<ol>
<li>After hardware is installed, power on your PC. Power on modem.</li>
<li>Plug and play will detect the modem and ask you to install drivers.</li>
<li>Insert the Installation CD.</li>
<li>Direct the driver search to the CD-ROM drive.</li>
<li>Follow on-screen directions to complete the installation.</li>
</ol>
<br>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/frontpanel_ss5667.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td>
<td class="infohd">Power</td>
<td class="infohd">ADSL</td>
<td class="infohd">Act</td>
<td class="infohd">Enet</td>
<td class="infohd">USB</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/unit.png" border=0></center></td>
<td class="infoline">Power Off</td>
<td class="infoline">N/A</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port not connected; check Ethernet cable connection if using Ethernet interface</td>
<td class="infoline">USB port not connected; check USB cable connection if using USB interface</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/solid.png" border=0></center></td>
<td class="infoline">Power On</td>
<td class="infoline">Ready for Data Traffic</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port connected to LAN</td>
<td class="infoline">USB port connected to host</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/blinking.png" border=0></center></td>
<td class="infoline">N/A</td>
<td class="infoline">Searching for signal</td>
<td class="infoline">DSL traffic flow</td>
<td class="infoline">Ethernet traffic flow</td>
<td class="infoline">Traffic flow on the USB interface</td>
</tr>
<tr>
<td class="infohd">
<center>All Blinking</center></td>
<td class="infoline" colspan=5>Post failure</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following illustration shows the connectors on the rear panel of the SpeedStream router.
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/rearpanel_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/ethernetsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/usbsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Reminder:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
<p>
<b>A.</b> If the Speed Stream is set up in Router mode you will be able to open
your web browser and pull up the configuration page of the modem:<b>192.168.100.1</b>.
Here you will be able to see if the modem has a WAN IP of <b>66.153.*.*</b>.
</p>
<p>
If all light are on modem but can not browse or WAN IP address is 0.0.0.0, click on <b>Simple Setup</b> on the left
of screen and go to username/password. Have customer type their username/password and reset modem. If all lights
are on and their username/password is not working and modem not getting a <b>66.153.*.*</b> address, escalate.
</p>
<p>
Check the status on the modem and power cycle modem. If the DSL light is flashing,this means there is a problem with the DSL line.
Make sure line is plugged into modem properly. If so and DSL light still flashing after power cycling modem, escalate.
</p>
<p>
If the <b>Enet</b> light is not on, then the Ethernet connection to modem and PC are not linked. Check network cables. 
</p>
<p>
<b>B.</b> If business has router such as Linksys router and the SpeedStream modem should be in bridge mode. If the modem is in bridge mode,
 you will not be able to access router config page. The Linksys router will need to be set up for <b>PPPoE</b> connection. the WAN address on
router should have the <b>66.153.*.*</b> address.
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>