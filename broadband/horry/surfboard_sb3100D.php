<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Surfboard 3100D 1-Way Cable Modem</title>
</head>
<body>

 <!-- start -->
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Surfboard 3100D 1-Way Cable Modem</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>General Informaiton</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SB3100D contains all necessary software. You don�t need to configure the
SB3100D but you must configure the computer's Network settings by setting TCP/IP
for the Network Card to obtain an IP address automatically.
</p>
<p>
You must call your service provider to activate your service. You need to
provide the media access control (MAC) address. This address is found on
the barcode label marked HFC MAC ID on the rear panel. The address
format is 00:20:40:xx:xx:xx.</p>
<p>
The installation of the SB3100D can be completed in a matter of minutes.
After you attach the three cables, you must configure your computer.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>

<tr>
<!-- Image here --> 
<td class="field" valign=top><img src="./images/frontpanel_sb3100D.png" border=0></td>
<td class="field" valign=top>
<!-- FrontPanel Information -->
<table>
<tr><td class="infotxt" colspan=4>
The five front-panel lights provide information about power, communications, and errors.
</td></tr>
<tr>
<td class="infohd" valign="top">&nbsp;</td>
<td class="infohd" valign="top">Light</td>
<td class="infohd" valign="top">Color</td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td class="infohd" valign="top">1</td>
<td class="infohd" valign="top">Power</td>
<td class="infohd" valign="top">Green</td>
<td class="infoline" valign="top">
When the light is flashing, startup diagnostics are being
performed. A solid light indicates the SB3100D is on.
</td>
</tr>
<tr>
<td class="infohd" valign="top">2</td>
<td class="infohd" valign="top">Receive</td>
<td class="infohd" valign="top">Green</td> 
<td class="infoline" valign="top">When the light is flashing, the SB3100D is scanning for
the downstream frequency. A solid light indicates the
downstream channel is acquired.</td>
</tr>
<tr>
<td class="infohd" valign="top">3</td>
<td class="infohd" valign="top">Send</td>
<td class="infohd" valign="top">Green</td>
<td class="infoline" valign="top">
When the light is flashing, the SB3100D is scanning for
the upstream frequency. A solid light indicates the
upstream channel is acquired.</td>
</tr>
<td class="infohd" valign="top">4</td>
<td class="infohd" valign="top">Online</td>
<td class="infohd" valign="top">Green</td>
<td class="infoline" valign="top">When the light is flashing, the SB3100D is scanning for
the network connection. A solid light indicates the
network connection is acquired.</td>
</tr>
<tr>
<td class="infohd" valign="top">5</td>
<td class="infohd" valign="top">Activity</td>
<td class="infohd" valign="top">Amber</td>
<td class="infoline" valign="top">When the light is flashing, the SB3100D is transmitting
or receiving data. When the light is off, the SB3100D is
not transferring data.</td>
</tr>
<tr><td class="infotxt" colspan=4>
If an error occurs, the lights provide a quick way of detecting the problem.
See <i>Troubleshooting</i> for more information.
</td></tr>
</table>
<!-- end of FrontPanel Information -->
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Rear Panel</b></td>
</tr>
<tr>
<!-- Image here --> 
<td class="field" valign=top><img src="./images/rearpanel_sb3100D.png" border=0></td>
<td class="field" valign=top>
<!-- Start of Rear panel Information -->
<table>
<tr>
<td class="infoline" valign="top" colspan=3>
The rear panel provides Ethernet activity and link status lights, the reset
button, and all the connectors.</td>
</tr>
<tr>
<td class="infohd" valign="top">&nbsp;</td>
<td class="infohd" valign="top">Item </td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td class="infohd" valign="top">1</td>
<td class="infohd" valign="top"><img src="./images/phoneicon_sb3100D.png" border=0></td>
<td class="infoline" valign="top">This connector is for the internal telephone modem.</td>
</tr>
<tr>
<td class="infohd" valign="top">2</td>
<td class="infohd" valign="top">ACT</td>
<td class="infoline" valign="top">When the light flashes, the Ethernet connection is
transferring data.</td>
</tr>
<tr>
<td class="infohd" valign="top">3</td>
<td class="infohd" valign="top">ENET</td>
<td class="infoline" valign="top">
This port transfers data to and from your computer.</td>
</tr>
<tr>
<td class="infohd" valign="top">4</td>
<td class="infohd" valign="top">LINK</td>
<td class="infoline" valign="top">
When the light is on, the Ethernet connection is available.</td>
</tr>
<tr>
<td class="infohd" valign="top">5</td>
<td class="infohd" valign="top"><img src="./images/reseticon_sb3100D.png" border=0></td>
<td class="infoline" valign="top">This is the recessed reset button.
</tr>
<tr>
<td class="infohd" valign="top">6</td>
<td class="infohd" valign="top">CABLE</td>
<td class="infoline" valign="top">
This port transfers data to and from the service provider.</td>
</tr>
<tr>
<td class="infohd" valign="top">7</td>
<td class="infohd" valign="top">POWER</td> 
<td class="infoline" valign="top">
This connector provides power to the SB3100D.</td>
</tr>
</table>
<br>
<!-- end of Rear panel Information -->
</td>
</tr>
<!-- Start of power cycle information -->
<tr>
<td class="heading" valign=top colspan=2><b>Power Cycle</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
You must allow <b>5 to 30 minutes</b> to power up the first time because the
SB3100D must find and lock on the appropriate channels for communications.
</p>
<ol>
<li>Be sure that your computer is off and SB3100D is unplugged.</li>
<li>Plug the SB3100D in and notice that the lights on the front panel cycle
through this sequence:</li>
 <ul>
 <li>Power flashes during a self-test. When the self-test is successfully
 complete, the light is solid green.
 <li>Receive flashes while the SB3100D scans for the downstream
 channel. When the downstream channel is locked, the light is
 solid green.
 <li>Send flashes while the SB3100D scans for the upstream channel.
 When the upstream channel is locked, the LED is solid green.
 <li>Online flashes while the SB3100D is obtaining configuration
 information. When the configuration information is obtained, the
 LED is solid green.
 </ul>
 <li>Turn on your computer.</li>
</ol>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Full Reset(For One-Way Cable Modems)</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2 style="background-color:#ffffcc;">
<br>
<ul>
<li>Goto <b>Config Page</b> and reset all defaults.</li>
<li>Goto  <b>Phone Page</b>, do reset all defaults and click <b>Yes</b> when prompted.</li>
<li>Click <b>Reset Phone Defaults</b> and save changes.</li>
<li>Sign into <b>Phone Setup</b>(located 2 inches below)</li>
<li>Then enter <b>User Name as "guest"</b> and <b>Password: (leave blank)</b></li>
<li>Then click <b>access</b> then <b>restart</b> at the bottom of the page to restart the cable modem. </li>
</ul>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Check the status lights on the front of the cable modem.</b>
<ol>
<li><b>If all lights are on the modem: The modem is online.</b></li> 
<!-- Start of inside list table -->
   <ol type="a"> 
   <li>If the customer is unable to browse, check the IP address on 
       the PC's network card that is connected to the cable modem. 
       The customer's IP address on their network card should be 
        66.153.xxx.xxx.</li>
   

   <li>If it is 192.168.100.11, have the customer to release and renew 
       their IP address of their network card bound to cable modem.</li>
       

   <li>If it is a 169.xxx.xxx.xxx default windows IP address, make sure
       the customer has not changed PCs or NIC cards since installation 
       of modem. If so, the new Network card will need to be provisioned 
       by HTC. No further troubleshooting should be done if the customer 
       has a new PC or network card. Refer the customer to Tier II at: 
       843-369-8796.</li>

   <li>For Win9x and ME, have the customer release and renew the IP
       address bound to the Network Card by clicking on the Release and 
       Renew buttons on the WINIPCFG window. Do not click Renew and Release all, just
       Renew and Release. It should give them a 66.153.xxx.xxx. address back, and then 
       have the customer try to browse.</li>

   <li>If they still cannot browse, check Internet Options in Internet Explorer. 
       Make sure that no checks are in any of the boxes under LAN settings. 
       After you have checked all of the above, have the customer browse.</li>

   <li>Check their TCP/IP properties for the Network Adapter. Make sure that 
        the network adapter is setup to obtain an IP address automatically. Also, make 
        sure that DNS is disabled and TCP/IP is setup as the default protocol.</li>

   <li>If the customer is getting the proper IP address, but still cannot browse, do 
        a ping test to see if they can ping IP addresses and not domain name. If they 
        can ping IP addresses, but no domain names, try this. For Win 9x and ME uninstall
        Client for Microsoft Networks and Dialup Networking. Restart the computer 
        and then reinstall. ***Make sure the customer has CABS or CD 
        before attempting these steps.</li>

   <li>Make sure any firewall software has been disabled.</li>
   </ol>
<!-- end of inside listing -->
<li><b>If the Receive light is flashing: Modem has not locked on the downstream channel....signal issues.</b></li> 
<!-- Start of inside list table -->        
        <ol type="a"> 
        <li>Check to make sure the CATV is working on the TV. This will verify the 
            CATV is active in the house.</li> 

        <li>Check CATV connection to modem and make sure coax cable connector 
            is screwed in tightly.</li>

        <li>Reset and restart the cable modem. <font color="green"><b>Go to:</b> </font><font color="red"><b>http://192.168.100.1/config.html</b></font>
             to bring up the modem configuration page. Click on reset all defaults and 
             restart the cable modem.</li>

        <li>If after reset the receive light does not stay solid, there is a signal issue. 
             Makes sure there is no splitters. 
             No further troubleshooting should be done. Refer to Tier II support.</li>
				</ol>
<!-- end of inside listing -->
<li><b>If the Send light is flashing the Telco modem is trying to make the upstream connection via the phone line.</b></li> 
<!-- Start of inside list table -->  
        <ol type="a"> 
        <li>If the customer only has one line, then the modem will not be able to dial 
            out while on the phone.  The customer will need to hang up and let the
            modem dial out.</li> 

        <li>If the customer has a separate line and the send light is flashing on the 
            modem, and the modem does not dial out, reset and restart the cable modem.</li>

        <li>If the modem will still not dial out on its own after you restart the cable modem,
            check the phone line for a dial tone. Have the customer plug a phone into the 
            line that is running into the cable modem to see if there is a dial tone on the line. 
            If there is a dial tone on the line, but the modem will not dial out, refer the 
            customer to Tier II support.</li>
				</ol> 
<!-- end of inside listing -->
<li><b>If the Online light is flashing: Cable modem did not connect to service.</b></li> 
<!-- Start of inside list table -->  
        <ol type="a">      
        <li>Modem is not provisioned in our system.</li>

        <li>Possible network problems.</li> 

        <li>Try resetting all defaults and restart modem. Let the modem redial and 
            attempt the connection again.  If the online light is still flashing, please 
            report these to Tier II and our Network Control.</li>
				</ol>
<!-- end of inside listing -->
<li><b>If the Send and Online lights are flashing together:</b></li>
<!-- Start of inside list table -->  <!-- end of inside listing -->
        <ol type="a">
				<li>Cable modem is attempting to connect but could not dial up and establish
            a connection.</li>

        <li>If the customer only has one line, the modem will not dial out while on the 
             phone. You will need to hang up and let the modem dial out.</li> 

        <li>If the customer has a separate line and the send light is flashing and the
             modem does not dial out, reset and restart cable modem.</li> 

        <li>If the modem will still not dial out on its own after you restart the cable modem,
            check the phone line for a dial tone. Have the customer plug a phone into the 
            line that is running into the cable modem to see if there is a dial tone on the line. 
            If there is a dial tone on the line, but the modem will not dial out, refer the 
            customer to Tier II support.</li>
				</ol>
<!-- end of inside listing -->
</ol>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
