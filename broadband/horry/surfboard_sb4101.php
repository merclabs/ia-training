<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href=
"../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Surfboard 4101 2-Way Cable Modem</title>
</head>
<body>
<!-- start -->
<table class="main" cellspacing="1">
<tr>
<td class="master" valign="top" colspan="2">Surfboard 4101 2-Way
Cable Modem</td>
</tr>
<tr>
<td class="heading" valign="top" colspan="2"><b>General
Informaiton</b></td>
</tr>
<tr>
<td class="field" valign="top" colspan="2">
<p>The Surfboard 4101 is a 2-way Cable Modem. You must configure
the computers Network Settings to make sure the TPC/IP for the
Network Card is set to obtain an IP address automaticlly, DNS is
disabled, and TCP/IP is set as the default protocol.</p>
<i><b>see: Troubleshooting</b> below for more information</i> 
<p>Customer must call service provider to activate their service.
They need to provide the media access control (MAC) address. This
address is found on the bar code label marked HFC MAC ID on the
rear panel.</p>
<p>The address format is 00:20:40:xx:xx:xx.</p>
</td>
</tr>
<tr>
<td class="heading" valign="top" colspan="2"><b>Front
Panel</b></td>
</tr>
<tr><!-- Image here -->
<td class="field" valign="top"><img src=
"./images/frontpanel_sb4101.png" border="0"></td>
<td class="field" valign="top"><!-- FrontPanel Information -->
<table>
<tr>
<td class="infotxt" colspan="4">The six front-panel lights
provide information about power, communications, and errors.</td>
</tr>
<tr>
<td class="infohd" valign="top"> </td>
<td class="infohd" valign="top">Light</td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td class="infohd" valign="top">1</td>
<td class="infohd" valign="top">Standby Switch</td>
<td class="infoline" valign="top">Press this switch to disable
the Ethernet and USB ports on the SB4101. No data is transmitted
or received. Press this switch again to transmit and receive
data. The Standby button offers added Internet security. When the
standby switch is activated, all other lights turn off.</td>
</tr>
<tr>
<td class="infohd" valign="top">2</td>
<td class="infohd" valign="top">Power LED</td>
<td class="infoline" valign="top">LED When the light is flashing,
startup diagnostics are being performed. A solid light indicates
the SB4101 is powered on.</td>
</tr>
<tr>
<td class="infohd" valign="top">3</td>
<td class="infohd" valign="top">Receive LED</td>
<td class="infoline" valign="top">When the light is flashing, the
SB4101 is scanning for a receive channel connection. A solid
light indicates the channel connection is acquired.</td>
</tr>
<tr>
<td class="infohd" valign="top">4</td>
<td class="infohd" valign="top">Send LED</td>
<td class="infoline" valign="top">LED When the light is flashing,
the SB4101 is scanning for the send channel connection. A solid
light indicates the channel connection is acquired.</td>
</tr>
<tr>
<td class="infohd" valign="top">5</td>
<td class="infohd" valign="top">Online LED</td>
<td class="infoline" valign="top">When the light is flashing, the
SB4101 is scanning for the network configuration server
connection. A solid light indicates the network connection is
acquired.</td>
</tr>
<tr>
<td class="infohd" valign="top">6</td>
<td class="infohd" valign="top">Activity</td>
<td class="infoline" colspan="4">When the light is flashing, the
SB4101 is transmitting or receiving data. When the light is off,
the SB4101 is not transmitting or receiving data.</td>
</tr>
<tr>
<td class="infohd" valign="top">7</td>
<td class="infohd" valign="top">Standby</td>
<td class="infoline" colspan="4">After pressing the Standby
button on the top of the SB4101, the Standby light turns on
indicating the Ethernet and USB ports are disabled (not receiving
or transmitting data). The cable connection to the service
provider remains active. Internet service is interrupted until
the Standby button is pressed again. The Standby button offers
added Internet security.</td>
</tr>
</table>
<!-- end of FrontPanel Information -->
</td>
</tr>
<tr>
<td class="heading" valign="top" colspan="2"><b>Rear
Panel</b></td>
</tr>
<tr><!-- Image here -->
<td class="field" valign="top"><img src=
"./images/rearpanel_sb4101.png" border="0"></td>
<td class="field" valign="top">
<!-- Start of Rear panel Information -->
<table>
<tr>
<td class="infotxt" colspan="4"></td>
</tr>
<tr>
<td class="infohd" valign="top"> </td>
<td class="infohd" valign="top">Item</td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td class="infohd" valign="top">1</td>
<td class="infohd" valign="top">Link/Act</td>
<td class="infoline" valign="top">When the light is on, the
Ethernet connection is available. A blinking light indicates data
is being transferred.</td>
</tr>
<tr>
<td class="infohd" valign="top">2</td>
<td class="infohd" valign="top"><img src=
"./images/symbol1_sb4100.png" border="0"></td>
<td class="infoline" valign="top">This port is the Ethernet
connector that transfers data to and from your computer.</td>
</tr>
<tr>
<td class="infohd" valign="top">3</td>
<td class="infohd" valign="top">100</td>
<td class="infoline" valign="top">The light is on when a
100Base-T link is established and off when a 10Base-T link is
established.</td>
</tr>
<tr>
<td class="infohd" valign="top">4</td>
<td class="infohd" valign="top">USB</td>
<td class="infoline" valign="top">This port provides a direct
connection to USB equipped computers.</td>
</tr>
<tr>
<td class="infohd" valign="top">5</td>
<td class="infohd" valign="top"></td>
<td class="infoline" valign="top">This is the recessed reset
button.</td>
</tr>
<tr>
<td class="infohd" valign="top">6</td>
<td class="infohd" valign="top">Cable</td>
<td class="infoline" colspan="4">This port transfers data to and
from the service provider.</td>
</tr>
<tr>
<td class="infohd" valign="top">7</td>
<td class="infohd" valign="top">Power</td>
<td class="infoline" colspan="4">This connector provides power to
the SB4101.</td>
</tr>
</table>
<br>
<!-- end of Rear panel Information -->
</td>
</tr>
<tr>
<td class="heading" valign="top" colspan="2"><b>Basic
Set-Up</b></td>
</tr>
<tr>
<td class="field" valign="top" colspan="2">You must allow 5 to 30
minutes to power up the first time because the SB4101 must find
and lock on the appropriate channels for communications. 
<ol>
<li>Be sure that your computer is on and the SB4101 is
unplugged.</li>
<li>Connect the coaxial TV cable to the cable outlet or splitter
and the other end to the SB4101 connector marked CABLE.
Hand-tighten the connectors to avoid damaging them. You may need
a 5-900 MHz splitter and an additional 75-ohm coaxial cable if
you have a TV connected to the cable outlet.</li>
<li>Insert the SB4101 CD-ROM into your CD-ROM drive (<i>for USB
Driver</i>).</li>
<li>Plug the power cord into the SB4101 connector marked POWER
and the other end to the electrical outlet.</li>
</ol>
<i><b>Note:</b> Do not connect both Ethernet and USB cables to
the same computer.</i></td>
</tr>
<tr>
<td class="field" valign="top" colspan="2">
<center><img src="./images/setup_sb4101.png" border="0"></center>
</td>
</tr>
<!-- Start of truobleshooting cycle information -->
<tr>
<td class="heading" valign="top" colspan="2">
<b>Troubleshooting</b></td>
</tr>
<tr>
<td class="field" valign="top" colspan="2"><!-- Start of Text -->
<p>Check the status lights on the front of the cable modem.</p>
<ol type="1">
<li>
<p>If all lights are on: the modem is online. Try to browse.</p>
<ol type="a">
<li>
<p>If the customer is unable to browse, check the IP address of
the Network Card, the address for the network card should be a
66.153.*.* address.</p>
</li>
<li>
<p>If you get an IP address of 192.168.100.11, have the customer
release and renew the IP address for the network card.</p>
</li>
<li>
<p>If you get an IP address of 169.*.*.*(default windows IP
address), make sure customer has not changes PC's or NIC Cards
since installation of Cable Modem. If so, the new Network Card
will need to be provisioned by HTC. <i>No further troubleshooting
should be done</i>. Escalate to have MAC Address provisioned.</p>
</li>
<li>
<p>For Win9x and ME have the customer to release and renew the IP
address bound to the NIC Card. Go to Start &gt; Run, and type in
winipcfg in the white box, click OK, the IP Configuration window
should appear, select the NIC Card Driver from the drop down
menu, click the release button, then the renew button. the window
should return an IP address of 66.153.*.* If so, have the
customer try to browse.</p>
</li>
<li>
<p>If they still can not browse, check Internet Options in the
Control Panel. make sure that their is not a check in any boxes
under the LAN Settings button, then have them try to browse.</p>
</li>
<li>
<p>Check their Network Settings to make sure the TPC/IP for the
Network Card is set to obtain an IP address automaticlly, DNS is
disabled, and TCP/IP is set as the default protocol.</p>
</li>
<li>
<p>If the customer is getting proper IP address but still can not
browse, do a PING test to see if they can ping the IP addresses
and domains. If they can ping IP address but not domains, for
Windows 9x and ME machines, checked for correct number and
version of CAB files, and do a standard Remove and Reinstall
Communications</p>
</li>
<li>
<p>Check for any Firewall such as "Zero Knowledge Freedom",
"Norton" or "Zone Alarm", if any of these are running, shut them
down or disable it. then try to browse.</p>
</li>
</ol>
</li>
<li>
<p>If the Receive light is flashing: Modem has not locked onto
downstream channel... this is a signal issue.</p>
<ol type="a">
<li>
<p>Check to make sure that customers has active Cable Service in
home, and that Cable service is working on all cable connected
TVs.</p>
</li>
<li>
<p>Check Cable TV connections to modem, by making sure coax cable
is connected to modem and screwed in tightly.</p>
</li>
<li>
<p>Reset and Restart the Cable Modem. Bring up Modem
Configuration manager page http://192.168.100.1/config.html.
Click reset all defaults and restart cable modem.</p>
</li>
<li>
<p>If after resetting the receive light does not stay solid,
there is a signal issue, No further troubleshooting is necessary.
Escalate.</p>
</li>
</ol>
</li>
<li>
<p>Send Light Flashing: This is a return path/signal issue, no
further troubleshooting should be done, Escalate.</p>
</li>
<li>
<p>Online Light Flashing: Modem not provisioned, this is a
network problem, Escalate.</p>
</li>
</ol>
</td>
</tr>
</table>
<p align="right"><input type="button" value="Close Window"
onclick="window.close()"></p>
</body>
</html>
