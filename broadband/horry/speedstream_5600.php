<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/content.css">

<title>Speedstream 5600/5660 Series</title>
</head>
<body class="document">

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2>Speedstream 5600/5660 DSL Router</td>
</tr>
<tr><td class="content_link"><a href="#1">General Information</a></td></tr>
<tr><td class="content_link"><a href="#2">Front Panel</a></td></tr>
<tr><td class="content_link"><a href="#3">Back Panel</a></td></tr>
<tr><td class="content_link"><a href="#4">Basic Setup</a></td></tr>
<tr><td class="content_link"><a href="#5">Switching to Bridge Mode</a></td></tr>
<!--  <tr><td class="content_link"><a href="#6">Escalation Procedure</a></td></tr> -->
<!-- <tr><td class="content_link"><a href="#7">Troubleshooting Information</a></td></tr> -->
</table>
 
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="7">Troubleshooting Information</a></b></td>
</tr>
<tr><td class="content_link"><a href="#8">If the DSL light is out or blinking</a></td></tr>
<tr><td class="content_link"><a href="#9">If Sys, DSL and Enet are on solid</a></td></tr>
<tr><td class="content_link"><a href="#10">If Enet light is off</a></td></tr>
<tr><td class="content_link"><a href="#11">If no lights are on</a></td></tr>
</table>

<p class="note">
<b>NOTE:</b> ATM and ENET will flash when there is internet traffic flowing.  DSL and SYS lights should always be on solid.
</p>
 
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top colspan=2><b><a name="1">General Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SpeedStream 5600 and 5660 Ethernet to DSL router provides ATM-based
network access for home users, telecommuters, or remote offices over
existing telephone lines.
</p>
<p>
Through its Ethernet and DSL ports located on the back of the router, the
5600 provides local area network (LAN) and wide area network (WAN)
connectivity. The router connects to the LAN through its Ethernet port and
connects to the WAN through its DSL port.</p>
<p>The 5600 modem can be set it router or bridge mode.  By default, it is set to Router Mode.  For info on how to change to Bridge Mode, click <a href="#5">here</a>.</p>
<p>
<b><i>The 5600 can be used as either a router or a bridge.</i></b> 
</p>
<br>
</td>
</tr>
</table>
 
<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="2">Front Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_ss5600.png" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The router is equipped with four lights on the front panel:<br>
the System light(<b>sys</b>), ATM light (<b>atm</b>), DSL light (<b>dsl</b>), Ethernet light (<b>enet</b>).
<ol>
<li>All four lights flash red for a moment as the hardware initializes.
Then, the <b>sys</b> light turns yellow while the router performs a self-test.
The <b>sys</b> light turns green when the unit has finished passing
diagnostic tests.</li>
<li>The <b>dsl</b> light will flash green while the router is attempting to obtain
the optimum transmission rate over the DSL line. If a DSL link
cannot be established, the light will flash green and yellow. When a
link is established, the <b>dsl</b> light turns solid green.</li>
<li>The <b>enet</b> light will turn solid green when there is a link detected, then
begin to flash green when Ethernet traffic is flowing.</li>
<li>The <b>atm</b> light will flash green indicating traffic flow on the DSL line.</li>
</ol>
<center>
<img src="./images/lightstatus_ss5600.png" border=0>
</center>
<br>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="3">Back Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><b>NOTE: The labels for the back panel of the router are located on
the underside of the unit.</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/rearpanel_ss5600.png" border=0>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="4">Basic Setup</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/setup_ss5600.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Be sure the power switch is in the OFF position.</li>
<li>Insert the power cable jack into the power connector on the back of
the router. Connect the power cable to a power outlet.</li>
<li>Connect the router to the LAN using one of the following methods:
<ul type="disc">
<li>Connect directly to the Ethernet port on a PC, workstation or laptop
using the gray RJ45 cable, which is a straight-through cable.</li>
<li>Connect to an Ethernet hub or switch using the crossover cable
labeled �XOVER� on one end.</li>
</ul>
NOTE: Some Ethernet hubs and switches have an �Uplink� port that
allows you to connect to the router using the gray RJ45
straight-through cable.</li>
<li>Connect one end of an RJ11 or RJ45 telephone cable to the router�s
DSL port and the other end to the DSL service port (wall jack).</li>
<li>Press the router�s power button to turn ON the unit.</li>
</ol>
</td>
</tr>
</table>

<br />
<br />


<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="5">Switching to Bridge Mode</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Start your web browser 
<li>Enter the router's IP address: 192.168.100.1 
<li>On the first page, click on the link, "Change to Bridge Mode". 
<li>When the display appears, click on the radio button labeled Bridge. 
<li>Click on the button, Change Mode 
<li>When the response appears at the bottom of the screen, it will include link that says "REBOOT REQUIRED". Click on that link and choose yes to reboot the router. 
<li>Select Refresh after a minute to see the new configuration.
</ol> 
</td>
</tr>
</table>

<br />
<br />
<!--
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="6">Escalation Procedure</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Troubleshoot according to steps for specific DSL/Cable Modem on <b>IA Training Broadband</b> section.
<li>Get permission to write up from Lead Tech.
<li>Complete a "Cable and DSL Write Up Sheet".
<li>Gather all necessary infornation:
 <ul type="square">
  <li>User Name</li>
  <li>Customer's Full Name</li>
  <li>Street Address(include city)</li>
  <li>Contact Number</li>
  <li>PTS Log ID</li>
  <li>DSL Line Number or Account Number</li>
  <li>Operating System</li>
  <li>Connection Type<br>( DSL, ADSL, 1 Way Cable, 2 Way Cable )</li>
  <li>Description of problem</li>
  <li>Can they Ping(Domain Name, IP Address, NIC)</li>
  <li>What you have checked<br>( Network Settings, Power Cycle, PPoE )</li>
 </ul>
<li>Check with Lead Tech to make sure you have all information from customer before you let
customer go.
<li>Give "Cable and DSL Write Up Sheet" to Lead Tech or DSL Tech.
<li>Give customer Log ID.
</ol>
</td>
</tr>
</table>
<br />
<br />
-->

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="8">If the DSL light is out or blinking</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br> 
<ul>
 <li>Check physical connections.
 <li>If they were given filters for their phones, make sure they are hooked up properly. 
 <li>If the dsl line is hooked up to a splitter filter, it must be hooked up on the dsl side (which means it isn�t really being filtered). 
 <li>Try bypassing splitters and/or surge protectors if applicable.
 <li>If dsl light is still out, then <!--<a href="#6"> -->escalate <!-- </a> -->.
</ul>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="9">If Sys, DSL and Enet are on solid</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<br>
Have the customer do the following:
</p>
<ol>
 <li>Power cycle the DSL modem.  Wait for Sys, DSL and Enet lights to come back on solid.
 <li>Power cycle the router (if they have one).  Wait for lights to come back on like they were.
 <li>Restart the computer.
</ol>
</p>
Have them try the internet again:
</p>
</ul>
 <li>If they have a router <a href="#router">click here</a>. 
 <li>If they have the DSL Modem directly connected to the Computer <a href="#no_router">click here</a>.
</ul>
</p>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="10">If Enet light is off</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<br>
Do the following:
<ul>
  <li>Check the physical connection from the DSL modem to the computer (or router if there is one).  
  <li>Make sure the computer/router is turned on. 
  <li>Power cycle the DSL modem and the computer/router. 
  <li>If they have different Ethernet cable, have them try that next. 
  <li>If none of this helps then escalate.  It could be a bad modem, bad Ethernet card or bad Ethernet cable. 
</ul>
</p>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="11">If no lights are on</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p>
Check all physical connections (power cord(s), power strip(s), and for any light 
switches that may control the power outlet that the modem is on.).
</p>
<p>
<i>If still not working</i>  <b>follow escalation procedure</b>.
</p>
<br>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="router">If they have a router</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p>
The modem would need to be in <a href="#5">Bridge mode</a> before hooking up the router to it.  After hooking up the router, troubleshoot it as a Router connection setup for PPPoE (see Router section on training site for more info).  
</p>
<br>
</td>
</tr>
</table>

<br />
<br />

<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2><b><a name="no_router">If they have the DSL Modem directly connected to the Computer</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The modem should be in Router mode by default.  
<ul>
<li>Check the IP address they are getting assigned.
<li>If it is a 192 IP address, then the modem is in router mode, and you should be able to access the configuration page (192.168.100.1).  
<ul>	<li>Here you will be able to see if the modem has a WAN IP of 66.153.*.*.
	<li>If all light are on modem but can not browse or WAN IP address is 0.0.0.0, 
	<ul>	<li>click on Simple Setup on the left of screen and go to username/password. 
		<li>Have customer type their username/password and reset modem.</ul>
	<li>If all lights are on and their username/password is not working and modem not getting a 66.153.*.* address, escalate.</ul>
<li>If it is a 169 IP address, then either the modem is not in router mode, or the connection is not good.
<ul>	<li>Try resetting the modem.  
	<li>Then restart if they have Windows 9x.
	<li>Check the IP address again now.  
	<li>If it is still a 169 IP address, then escalate.
	</ul>
</ul>
</p>
</td>
</tr>
</table>

<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

