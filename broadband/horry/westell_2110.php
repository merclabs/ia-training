<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Westell ADSL 2110</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Westell ADSL 2110</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Westell�s WireSpeed� Dual Connect Modem adds reliable, high-speed, Internet access to your existing home or
office phone line. Your ADSL connection is �always-on� ending the hassles of dial-up modems and busy signals.
Installation is easy ... no tools ... no headaches. Simply plug the modem into the 10/100 Base-T or USB port of your
PC, apply power, perform the simple software configuration, and connect your ADSL phone line to the modem.
</p>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Note:</b> Please check case of all <b>User/Pass</b> information that the
customer is entering on configuration pages. Horry customer <b>User Names</b> are all <b>lowercase</b> and <b>Passwords</b>
are both <b>upper</b> and <b>lowercase</b>. 
</p>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Reminder:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_wt2110.jpg" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<b>Power:</b>
<table>
<tr>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Solid Green</td><td class="infoline">Power ON</td>
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No power</td>
</tr>
</table>
<br>
<b>Ready:</b>
<table>
<tr>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Slow Flashing Green<td class="infoline">Power ON and passed power-up diagnostics (1 flash/sec)</td>
</tr>
<tr>
<td class="infohd">Moderate Flashing Green<td class="infoline"> Power ON and attempting synchronization (2 flashes/sec)</td>
</tr>
<tr>
<td class="infohd">Steady Green<td class="infoline"> Power ON and synchronized with the ADSL line card</td>
</tr>
<tr>
<td class="infohd">Steady Red (less than 20 sec.)<td class="infoline"> Hardware power-up in progress</td>
</tr>
<tr>
<td class="infohd">Blinking Red<td class="infoline"> Unit failed power-up diagnostic</td>
</tr>
<tr>
<td class="infohd">Alternating Red/Green<td class="infoline"> Modem diagnostic failed</td>
</tr>
<tr>
<td class="infohd">No Light<td class="infoline"> No power</td>
</tr>
</table>
<br>
<b>Ethernet:</b>
<table>
<td class="infohd">State</td><td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Solid Green</td><td class="infoline">Ethernet link established</td>
</tr>
<tr>
<td class="infohd">Flashing Green</td><td class="infoline">Transmit or Receive Activity</td> 
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No link established</td> 
</tr>
</table>
<br>
<b>USB:</b>
<table>
<tr>
<td class="infohd">State</td> <td class="infohd">Description</td>
</tr>
<tr>
<td class="infohd">Solid Green</td> <td class="infoline">USB link established</td>
</tr>
<tr>
<td class="infohd">Flashing Green</td><td class="infoline">Transmit or Receive Activity</td>
</tr>
<tr>
<td class="infohd">No Light</td><td class="infoline">No USB Activity</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_wt2110.jpg" border=0>
</center>
<p>
The following items are located on the rear panel of the ADSL Modem.
</p>
<table>
<tr>
<td class="infohd">Symbol&nbsp;&nbsp;&nbsp;</td><td class="infohd">Name</td><td class="infohd">Type</td><td class="infohd">Function</td>
</tr>
<tr>
<td class="infohd"><center><img src="./images/symbol1_wt2000.png" border=0></center></td><td class="infohd">DSL</td><td class="infoline">6-pin (RJ11) modular jack</td><td class="infoline">Connects to an ADSL-equipped telephone jack or DSL connection of a POTS splitter.</td>
<tr>
<tr>
<td class="infohd"><center><img src="./images/symbol4_wt2110.png" border=0></center></td><td class="infohd">USB</td><td class="infoline">4-pin USB Series B connector</td><td class="infoline">Connects the USB device to the PC.</td>
</tr>
<tr>
<td class="infohd"><center><img src="./images/symbol2_wt2000.png" border=0></center></td><td class="infohd">POWER</td><td class="infoline">Barrel connector</td><td class="infoline">Power source.</td>
</tr>
<tr>
<td class="infohd"><center><img src="./images/symbol3_wt2000.png" border=0></center></td><td class="infohd">ETHERNET</td><td class="infoline">8-pin (RJ-45) modular jack</td><td class="infoline">Connects the Ethernet device to the PC.</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
If you choose to connect via <b>USB</b> you must first install <b>USB Drivers</b>. 
<ol>
<li>After you have connected the Westell Dual Connect Modem to your PC, the <b>Found New Hardware</b> window
appears. Then in a few moments, the <b>Add New Hardware Wizard</b> window will open. Click
<b>Next</b>.<br>
<img src="./images/newhardware.jpg" border=0></li>
<li>Click the <b>option</b> button for Search for the best driver for your device. <b>(Recommended)</b>. Click <b>Next</b>.</li>
<li>Select <b>CD-ROM drive</b> option. Click <b>Next</b>. Windows will search for the driver.</li>
<li>Select option button The updated driver <b>(Recommended)</b> <b>Westell T1AR5 ADSL USB Modem</b>. Click <b>Next</b>.</li>
<li>Windows will display the location of the driver. Click <b>Next</b>. <b>Note</b>: <i>The drive �letter� may vary</i>.</li>
<li>Remove the <b>Westell CD</b> from the <b>CD-ROM Drive</b>. Next, insert the <b>Windows operating system CD</b> into the CD-ROM Drive if prompted for it. Click <b>OK</b>.</li>
<li>The system will begin copying files</li>
<li>Windows may or may not have <b>CAB files</b> stored on the hard disk, please check for <b>CAB files</b> before trying to install the <b>USB Drivers</b>.</li>
<li>If prompted for a location, point to the location that the <b>CAB files</b> are stored.</li>
<li>When the <b>System Settings Change</b> screen appears, the <b>USB drivers</b> are installed properly. Click <b>Yes</b>.<br>
<img src="./images/restart.jpg" border=0></li>
</ol>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/setupusb_wt2110.jpg" border=0>
</center>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Note:</b> Before you connect the modem via 10/100 Base-T, you must have an available Ethernet card installed in your computer. If your
Ethernet card does not auto-negotiate, you must set it to half duplex. Refer to the Ethernet card manufacturer�s instructions for
installing and configuring your Ethernet card.
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/setupethernet_wt2110.jpg" border=0>
</center>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>System Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Windows 95/98/NT</b>
<ol>
<li>Ask whether your service provider uses static or dynamic (DHCP) IP addressing.</li>
<li>Start your Windows environment.</li>
<li>Click <b>Start</b> > <b>Settings</b> > <b>Control Panel</b>, then double-click the <b>Network</b> icon.</li>
<li>Click the <b>Configuration</b> or <b>Protocols</b> tab.</li>
<li>Select <b>TCP/IP</b>.</li>
<li>Click the <b>Properties</b> or <b>Protocol</b> button.</li>
<li>When the <b>TCP/IP Properties</b> window displays, click the <b>IP Address</b> tab.</li>
<li>If your ISP uses <b>DHCP</b>, select <b>Obtain an IP address automatically</b>. If your service provider uses <b>Static
IP Addressing</b>, select the <b>IP address box</b> and enter the <b>IP</b> address and <b>subnet mask</b> assigned by your ISP.</li>
<li>Close all Windows and <b>restart</b> your computer (if required).</li>
</ol>
<b>Macintosh</b>
<ol>
<li>Ask whether your service provider uses static or dynamic (DHCP) IP addressing.</li>
<li>Start your Mac.</li>
<li>From the <b>Apple</b> icon, select <b>Control Panels</b>, then <b>TCP/IP</b>.</li>
<li>Under the <b>Setup box</b>, select <b>Connected by</b> and choose <b>�Ethernet�</b> from the drop-down list box.</li>
<li>From <b>Configure</b>, choose <b>�Manually�</b> or <b>�DHCP�</b>.</li>
<li>Close all windows and <b>restart</b> your computer.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Standard DSL Troubleshooting(Power Cycle, check Network, LAN settings, physical connections, Status lights.)
</p>
<b>WinPoET PPPoE</b> software is required to connect DSL Modem for Windows 95/98/ME users. If customer is using <b>Windows XP</b>, 
they do not need the <b>WinPoET PPPoE</b> software. They need to use the built in <b>PPPoE</b> connection(Broadband Connection with User/Pass).
<p> 
If getting errors when connecting with software,check the status of the lights on the modem. Power Cycle.
</p>
<p>
The top 3 lights should be on(<b>Power, Ready and Link</b>). If the <b>Ready</b> light is flashing red or green then there is a problem
wiht the DSL connection, escalate. 
</p>
<p>
If the <b>Link</b> light is not on the check the ethernet link from modem to pc. 
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>


