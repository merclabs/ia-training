<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>U.S. Robotics SureConnect 9300</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>U.S. Robotics SureConnect 9300</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Windows 95 & NT 4.0, Macintosh and Linux Users</b><br>
If you�re installing the U.S. Robotics SureConnect ADSL Ethernet/USB
Router on a system running Windows 95, NT 4.0, Macintosh or Linux,
you must install the router using the Ethernet option. Network adapter
must be set to automaticlly obtain an IP Address.
</p>
<p>
<b>Connect Microfilters to Telephone Devices</b><br>
To install the microfilter, plug the phone into the microfilter, and then plug the
microfilter into the telephone wall jack. Do not install a microfilter on the cable
that connects your router to the telephone jack unless your microfilter has a
connection for both the telephone and the DSL device.
</p>
<p style="background-color:#ffff99;border:1px solid #000000;padding:4px;">
<b style="color:red;">Reminder:</b> Make sure you check to see if the customer is using
a router or if the modem is directly connected to the PC.  
</p>
</td>
</tr>

<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_sc9300.png" border="0">
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<!-- start of text -->
<p>
The front of the U.S. Robotics SureConnect ADSL Ethernet/USB Router has five
LEDs. The first from the left is the �PWR� LED. The second and third are the data
transfer �ENET1� and �ENET2� LEDs. The fourth is the �USB� LED. The fifth is the
�ADSL� LED. LED conditions below indicate the router�s operational status.
</p>
<table>
<tr>
<td class="infohd" valign="top">LED</td>
<td class="infohd" valign="top">Status</td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td rowspan=2 style="text-align:center;" class="infohd"><b>PWR</b></td>
<td class="infoline"><p>Geen On</p></td>
<td class="infoline"><p>Receiving power from the wall jack power supply.</p></td>
</tr>
<tr>
<td class="infoline"><p>Off</p></td>
<td class="infoline"><p>Detected no power.</p></td>
</tr>
<tr>
<td rowspan=3 style="text-align:center;" class="infohd"><b>ENET1/<br>ENET2</b></td>
<td class="infoline">On Green/10 Mbps On Orange/100 Mbps.</td>
<td class="infoline">Established and detected a physical connection through the Ethernet cable between router and computer</td>
</tr>
<tr>
<td class="infoline">Flashing Green or Orange</td>
<td class="infoline">Flowing data traffic.</td>
</tr>
<tr>
<td class="infoline">Off</td>
<td class="infoline">Did not establish a physical connection between router and computer.</td>
</tr>
<tr >
<td rowspan=3 style="text-align:center;" class="infohd"><b>USB</b></td>
<td class="infoline">On Green</td>
<td class="infoline">On Green Established and detected a physical connection through the USB cable between router and computer.</td>
</tr>
<tr>
<td class="infoline">Flashing Green</td>
<td class="infoline">Flowing data traffic.</td>
</tr>
<tr>
<td class="infoline">Off</td>
<td class="infoline">Did not establish a physical connection between router and computer.</td>
</tr>
<tr >
<td rowspan=3 style="text-align:center;" class="infohd"><b>ADSL</b></td>
<td class="infoline">On Green</td>
<td class="infoline">Established a DSL link.</td>
</tr>
<tr>
<td class="infoline">Flashing Green</td>
<td class="infoline">Negotiating a DSL link.</td>
</tr>
<tr>
<td class="infoline">Off</td>
<td class="infoline">The DSL link failed.</td>
</tr>
</table>
<br>
<!-- end of text section -->
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/rearpanel_sc9300.png" border="0">
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infotxt" colspan="4"></td>
</tr>
<tr>
<td class="infohd" valign="top">Item</td>
<td class="infohd" valign="top">Description</td>
</tr>
<tr>
<td class="infohd" valign="top">I/O</td>
<td class="infoline" valign="top">
Pushbutton switch that turns the U.S. Robotics SureConnect ADSL USB/
Ethernet Router on and off.</td>
</tr>
<tr>
<td class="infohd" valign="top">Power</td>
<td class="infoline" valign="top">Input jack that accepts cable from wall power supply.</td>
</tr>
<tr>
<td class="infohd" valign="top">Console</td>
<td class="infoline" valign="top">
Connects an RS-232 cable (not included) to the router. You can use the
cable to communicate to the router through the Terminal User Interface.
The Terminal User Interface (TUI) is another way to configure the router
or get diagnostic info. The TUI substitutes for the Web User Interface.
</td>
</tr>
<tr>
<td class="infohd" valign="top">USB</td>
<td class="infoline" valign="top">Universal serial bus port on the back of the router.</td>
</tr>
<tr>
<td class="infohd" valign="top">ENET1</td>
<td class="infoline" valign="top">Ethernet Port 1 on the back of the router.</td>
</tr>
<tr>
<td class="infohd" valign="top">ENET2</td>
<td class="infoline" colspan="4">Ethernet Port 2 on the back of the router.</td>
</tr>
<tr>
<td class="infohd" valign="top">ADSL</td>
<td class="infoline" colspan="4">Digital subscriber line RJ-11 service jack on the back of the router.</td>
</tr>
</table>
<br>
</td>
</tr>


<tr>
 <td class="heading" valign=top colspan=2><b>Software Installation(Windows 98, 2000, Me & XP)</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
To install the U.S. Robotics SureConnect ADSL Ethernet/USB Router, insert the
U.S. Robotics SureConnect Installation CD-ROM into the CD-ROM drive of your
computer. If the installation does not start automatically, go to your desktop and
double-click My Computer, double-click the drive letter associated with your CDROM
drive, and then double-click Setup.
</p>
<ol>
<li>The U.S. Robotics SureConnect ADSL Ethernet/USB Router Installer Welcome window 
will display. Click Next to continue installing the U.S. Robotics SureConnect ADSL 
Ethernet/USB Router.
<br>
<center>
<img src="./images/software1_sc9300.png" border="0">
</center>
<br>
</li>

<li>
Select the connection type that you will use to connect
the router to your computer. Click Next.
<br>
<center>
<img src="./images/software2_sc9300.png" border="0">
</center>
<br>
</li>

<li>
A qualification test will run to verify that your system meets the minimum installation
requirements. The Results screen will display those items in your configuration that
passed with a green flag and the ones that failed with a red flag. If your system
passed the qualification, click Next.
<br>
<center>
<img src="./images/software3_sc9300.png" border="0">
</center>
<br>
</li>

<li>
<p>
You�ve completed installation of the U.S. Robotics SureConnect ADSL Ethernet/
USB Router. Click Finish. The installer will automatically launch the Internet
browser. The Internet browser will point to IP address 192.168.1.1.
<center>
<img src="./images/software4_sc9300.png" border="0">
</center>
</p>
<p>
At this point, the software prompts you for a username and password. The
default username is <b>�root�.</b> The default password is <b>�12345�</b>. Enter these values
(without periods or quotation marks).

</p>
<p>
If your browser doesn�t auto-launch...
Begin the Quick Setup by launching your Internet browser and entering <b>http://
192.168.1.1</b>. The <b>SureConnect ADSL Utility Quick Setup</b> screen will display.
</p>

<br>
<center>
<img src="./images/quicksetup1_sc9300.png" border="0">
</center> 

</li>
</ol>
<i><b>see next step: </b>Quick Setup Menu below.</i>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Quick Setup Menu</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The U.S. Robotics SureConnect ADSL Ethernet/USB Router comes equipped
with the SureConnect ADSL Web Utility. This utility helps you get the router set
up in three easy steps...
<ul>
<li>Select ADSL Standard.</li>
<li>Configure service provider settings.</li>
<li>Save and restart.</li>
</ul>
</p>
<b>Select ADSL Mode</b>
<ol>
<li>Click the ADSL Standard radio button. The ADSL Standard window opens.
This window allows you to select the ADSL standard that you�ll use.
<br>
<center>
<img src="./images/quicksetup1_sc9300.png" border="0">
</center>
<br>
</li>
<li> From the ADSL Standard drop-down list, select G.dmt, G.lite, T1.413, or
Multi-Mode. In most cases, the default setting of Multi-Mode is sufficient.
You may want to check with your ISP to confirm the correct settings. Once
you have made your selection, click Apply.</li>
<li>Click the Next button in the lower right corner of the screen to move to the
WAN Setup page.
<br>
<center>
<img src="./images/quicksetup2_sc9300.png" border="0">
</center>
</li>
</ol>
<p>
<b>Configure Service Provider Settings</b><br>
In the Service Provider Settings/WAN Setup screen, enter the values obtained
from your ISP. If you do not have these settings, please contact your ISP for these
settings.
</p>
<ol>
<li>See the Current ATM PVC List at the bottom of the screen. On this list,
delete any connection type that you don�t need.</li>
<li>Select the connection type PPPoE, and enter username
and password.</li>
<li>The following values should fill in after choosing <b>PPoE</b>.
<ul>
<li><b>VPI</b> should be "0"</li>
<li><b>VCI</b> should be 35</li>
<li><b>LLC Snap</b> should be dotted</li>
<li><b>Enable NAT and Enable DHCP</b> should be checked</li>
</ul>
</li>
<li>Click the Add button.</li>
<li><b>NOTE:</b> check the <b>"Current PVC/ATM List"</b> at the bottom of the page making 
sure there is only one connection listed, if more than one connection exist, you 
must remove the previous connection before creating a new connection.</li>
<li>To continue, click the Next button in the lower right corner of the screen.</li>
</ol>
</p>
<center>
<img src="./images/quicksetup3_sc9300.png" border="0">
</center>
<br>
<p>
<b>Save and Restart</b><br>
Once you�ve filled out the ADSL Standard and WAN Setup screens, save your
settings and restart your router.
</p>
<ol>
<li>On the Save & Restart screen, click Save.</li>
<li>Once the save is complete, click Restart.</li>
</ol>
</p>
<center>
<img src="./images/quicksetup4_sc9300.png" border="0">
</center>
<br>
</li>
<br>
</td>
</tr>


<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

To help diagnose the problem, use the checklist below.
<ul>
<li>Confirm that you have secured the power adapter to the router and to
an active wall outlet. The �PWR� LED should illuminate.</li>
<li>Confirm that you have secured the telephone cable to the telephone
wall jack and to the router. The �ADSL� LED should illuminate.</li>
<li>Confirm that you have secured the Ethernet cable to the �ENET1� and/
or �ENET2� port on the router and to the computer�s network interface
card. The �ENET1� or �ENET2� LED, or both, should illuminate.</li>
<li>If you�re using the USB cable: Confirm that you�ve secured the USB
cable to the �USB� port on the router and computer. The �USB� LED
should illuminate.</li>
</ul>

</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>