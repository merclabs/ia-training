<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Netopia Cayman Gateway 3300 series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Netopia Cayman Gateway 3341 DSL modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
No General Information, please see <b>Troubleshooting</b> below. 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<img src="./images/frontpanel_netopia_3341.jpg" border=0>
</div>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The lights should be (from left to right):
<ol>
<li>Ethernet Link - should be <b>SOLID GREEN</b> unless they're using USB 
<li>Ethernet Traffic - should be <b>FLASHING GREEN </b>when there is activity between DSL and PC  
<li>DSL Traffic - should be <b>FLASHING GREEN</b> when there is activity between DSL and ISP 
<li>DSL Sync - should be <b>SOLID GREEN </b>- blinks green when no line attached or training
<li>USB Active - should be <b>OFF</b> unless they're connecting through USB
<li>Power - should be on <b>SOLID GREEN </b>
</ol>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<img src="./images/backpanel_netopia_3341.gif" border=0>
</div>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b><a name="new_setup">Initial Setup of Netopia modem</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
If this is the <i>inital</i> setup or the modem has been <b>reset</b>, open a web browser such as <b>Netscape</b> or 
<b>Internet Explorer</b> and go to <b>http://192.168.1.254</b>, you should see the following
Cayman Welcome page. The first screen, prompts for <b>New Password</b> and <b>Confirm Password</b>. Have them
enter <b>htc</b> for both passwords, then click the <b>submit</b> button.
</p>
<p>
<div align="center">
<img src="./images/admin_password.jpg" border=0>
</div>

<p>
After clicking submit, it will bring up another screen prompting them to login. Enter 
<b>admin</b> for the username and <b>htc</b> for the password. It will then display the 
Welcome page, and after it finishes that, it will display the Quickstart page.

<p>
Have them put their username and password they have with us for the <b>ISP Username</b>
and <b>ISP Password</b>, and then click on <b>Connect to the internet</b> button.
</p>
<div align="center">
<img src="./images/admin_prompt.jpg" border=0>
</div>
</p>


<p>
A please wait message should display. 
<div align="center">
<img src="./images/admin_please_wait.jpg" border=0>
</div>
</p>

<p>
If it connects, it will show a <b>Connection Successful</b> screen, and then it will 
either take them to their homepage (whatever is set in internet options), or it 
will take them to the Registration page for their Netopia DSL modem.
<div align="center">
<img src="./images/admin_connection_successful.jpg" border=0>
</div>
</p>

<p>
If it takes them to the Registration page, you do not have to walk them through 
this.  If they haven't registered and they want to, let them go to do that, otherwise, 
have them try their home page or other pages just to show them it's working now.
</p>

<p>
If it won't authenticate, but their username and password works in telnet, you 
may be able to have a lead tech reset the password, and it might fix it, otherwise 
they will need to be escalated.
</p>

<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><a name="reset"><b>How to reset the Netopia modem</b></a></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<img src="./images/reset_netopia_3341.gif" border=0>
</div>
</p>
<p>
After this, go back to <b>http://192.168.1.254</b> and it should be on the initial setup now, so click <a href="#new_setup">here.</a>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><p>
If all lights are on correctly, and the internet connection is not working, and you have already had them <b>POWER CYCLE</b> and <b>REBOOT</b> the computer, then:

<p>
Make sure the Ethernet card is responding, and that you are getting assigned an IP address of <b>192.168.1.x</b>
</p>

<p>
Open Internet Explorer and go to <b>http://192.168.1.254</b>
</p>

<p>
If it brings up a screen where it has 'New Password' and 'Confirm Password' under it, click <a href="#new_setup">here.</a>
</p>

<p>
<b>If it prompts for username and password:</b><br>
<ul>
<li>The username will be <b>admin</b>
<li>The password should be <b>htc</b>
<br>unless they reset the modem, then it will be whatever password they typed in 
(i.e. 'password')
</ul>
</p>

<p>
If <b>htc</b> doesn't work, and they don't know what they typed in when setting up, you can have them reset 
the modem again.  For info on how to do this, click <a href="#reset">here.</a>
</p>

<p>
The next screen that comes up will either be in <b>basic mode</b> or <b>expert mode</b>.  If 
there is a link for <b>basic mode</b> on the left, have them click on it.
<div align="center">
<img src="./images/config_home.jpg" border=0>
</div>
</p>

<p>
Have them read what it says next to  <b>Status of DSL</b>.<br>
<ul>
<li>If it says <b>Up</b>, then the connection is good.  Troubleshoot the computer like 
it was a ghost connection.
<li>If it says <b>Not Connected</b>, you can reenter the username and password by clicking 
on 'Manage My Account' on the left
<ul>
</p>

<p>
On this screen, enter the username, the password in the next 2 lines, and then click on 'Submit'
</p>
<ul>
<li>If it won't authenticate, but their username and password works in telnet, you may be able to have a lead tech reset the password, and it might fix it, otherwise they will need to escalate it.</ul>
<li>If you get a different connection error, see a Lead Tech about escalating them.</ul>
</ul>
</p>

<p class="note"><b>NOTE:</b> Most customers are given the default password of' 'password' and this is what is setup in their Netopia DSL modem.  If they change their password later through member services, their DSL modem can continue to stay connected with the old password for several days until there is an interruption in service.  Once this happens, they are going to need to go to http://192.168.1.254 and go to 'Manage My Account' and reenter the username with the new password.
</p>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>