<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Tango Manager</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Tango Manager</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Support</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This window provides information about <b>The Computer, DSL Modem and Your Internet Connection</b>. 
Click on any of the three icons to view details about that specific area.
<p align="center">
<img src="./images/tangomanager/tangosupport.jpg" border=0>
</p>
<b>See Next: </b><a href="tango_home.php"><b>Home</b></a> <b>|</b> <a href="tango_install.php"><b>Install</b></a> <b>|</b> <a href="tango_access.php"><b>Access</b></a> <b>|</b> <a href="tango_properties.php"><b>Properties</b></a>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>