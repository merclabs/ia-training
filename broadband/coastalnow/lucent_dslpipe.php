<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Lucent DSL Pipe SDSL</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Lucent DSL Pipe SDSL</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The Lucent DSLPipe HST SDSL Modems have 4 ports, on the back, can connect multiple computer, and
does not need any software to connect. The Lucent modems will no longer be distributed, MadisonRiver
is moving to the Speedstream brand of modems. 
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>DSL Settings</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<br>
<table width=200>
 <tr><td class="infohd">IP</td><td class="infoline">192.168.1.2</td></tr>
 <tr><td class="infohd">Subnet</td><td class="infoline">255.255.255.0</td></tr>
 <tr><td class="infohd">Gateway</td><td class="infoline">192.168.1.1</td></tr>
 <tr><td class="infohd">DNS1</td><td class="infoline">216.231.160.2</td></tr>
 <tr><td class="infohd">DNS2</td><td class="infoline">209.102.191.47</td></tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Phone Numbers</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<br>
<table>
 <tr><td class="infohd">Customer Service</td><td class="infoline">1-912-369-9000</td></tr>
  <tr><td class="infohd">Second Level</td><td class="infoline">1-800-773-2415</td></tr>
</table>
</br>
</p>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Hours</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Please <font color="red"><b>DO NOT</b></font> tranfer customers to <b>2nd Level</b> before <b>9am</b>, if you have a customer that
needs to be transfered to <b>2nd Level</b>, get an LT's approval, create a ticket, give customer 
ticket number and advise customer to call back to be transfered to 2nd level when they are open.
<p align="center">
<table>
 <tr><td class="infohd">Monday - Firday</td><td class="infoline"> 9am - 11pm</td></tr>
  <tr><td class="infohd">Saturday</td><td class="infoline">9am - 6pm</td></tr>
	  <tr><td class="infohd">Sunday</td><td class="infoline">Closed</td></tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Things to remember</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<ul>
<li>Customer service transfers should be limited to billing issues(signing up for accounts, closing accounts), most everything 
else will be handled by second level.</li>
<li>When entering a username in to the <b>PTS</b>, you will want to follow it with a <b>]</b> symbol; this symbol serves as a wild-card
which will cause <b>PTS</b> to take a few moments longer but will find and display a <b>Madision</b> customer that would 
not normally pull up in <b>PTS</b>. If they are a new customer and not coming up in <b>PTS</b>, try looking them up in 
<a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave"  target="_blank"><b>Remedy</b></a> first.
Then if the user's information comes up there, verify with customer and add them to <b>PTS</b> with that information, make sure you also verify their correct phone number, since most of the time 
the nubmer in <a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave" target="_blank"><b>Remedy</b></a>
is often their DSL line number.</li>
<li></li>
</ul>
<br>
</td>
</tr>

<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_lucent_dslpipe.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The lucent DSLpipe has 5 LED's on its front panel, they are as follows:
<br><br>
<table>
<tr><td class="infohd">LED</td><td class="infohd">Status</td><td class="infohd">Description</td></tr>
<tr><td rowspan=2 class="infohd" valign="top">pwr</td>
<td class="infoline">On</td><td class="infoline">DSLpipe has power.</td></tr>
<td class="infoline">Off</td><td class="infoline">DSLpipe does not have power.</td></tr>
<tr><td rowspan=2 class="infohd" valign="top">act</td>
<td class="infoline">Blinks</td><td class="infoline">when there is any DSL activity.</td></tr>
<td class="infoline">Off</td><td class="infoline">when there is no DSL activity.</td></tr>
<tr><td class="infohd" valign="top">ink</td>
<td class="infoline">On</td><td class="infoline">when either the DSR(Data Set Ready) or RTS(Request To Send) control port is active.</td></tr>
<tr><td rowspan=2 class="infohd" valign="top">wan</td>
<td class="infoline">On</td><td class="infoline">when there is an active connected WAN.</td></tr>
<td class="infoline">Blinks</td><td class="infoline">when you are not connected to the WAN.</td></tr>
<tr><td rowspan=2 class="infohd" valign="top">con</td>
<td class="infoline">On</td><td class="infoline">initially, then turns off.</td></tr>
<td class="infoline">Blinks</td><td class="infoline">if there are any SDSL errors.</td></tr>
</table>
</br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="./images/rearpanel_lucent_dslpipe.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
From left to right.
<p align="center">
<table>
<tr><td class="infohd">Port</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">Power</td><td class="infoline">connects the modems power supply.</td></tr>
<tr><td class="infohd">Serial Port</td><td class="infoline">allows you to configure the computer via its Serial port, (Adapter provided)</td></tr>
<tr><td class="infohd">Ethernet Ports 1-4</td><td class="infoline">Ethernet Ports to connect multiple computers(each IP must be incremented, starting from <b>192.168.1.2</b> through to <b>192.168.1.5</b>)</td></tr>
<tr><td class="infohd">WAN</td><td class="infoline">port for DSL line form wall jack.</td></tr>
</table>
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Power Cycle:</b>
 <ul>
 <li>Unplug the power supply from the back of the DSL modem.</li>
 <li>Shutdown the computer.</il>
 <li>Plug the power supply back into the DSL modem.</li>
 <li>wait for the <b>WAN</b> light to stop flashing, then power on the computer.</li>
 <li>If <b>WAN</b> light does not stop flashing after power cycle, try bypassing
 the <b>MasterCube</b>.<li>
 </ul>
If the <b>WAN</b> continues to flash after bypassing the <b>MasterCube</b>, notify an <b>LT</b> to 
get permission to transfer, create a ticket with <a href="https://secureisp.madisonriver.net/arsys/apps/en/mrtcsun9/arforms/ISPOSVCreateTicket_ISPOSVCreateWebView.jsp?formalias=ISPOSVCreateTicket&server=mrtcsun9&username=infoave&pwd=infoave&locale=en&cacheId=infoave" target="_blank"><b>Remedy</b></a>, then transfer to 2nd Level. 
<p>
If the customer is wanting to connect another computer to the <b>Lucent DSLpipe</b> they must increment
the second computers IP address up by one. if the first computers IP Address is <b>192.168.1.2</b> then the second 
computers IP Address needs to be <b>192.168.1.3</b>. The rest of the setting(<b>Subnet Mask, Gateway, Primary DNS
and Secondary DNS</b> are the same as on first computer.</i> This also applies to any new computer that are added,
up to 4 computer.
</p>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

