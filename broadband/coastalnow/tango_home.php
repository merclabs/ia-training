<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Tango Manager Home</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Tango Manager Home</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Home</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
This window serves as the main window for <b>Tango Manager</b>. If a customer already has
<b>Tango Manager</b> install they should have an icon on the <b>Desktop</b> to gain access
this window. If not, double check with customer to make sure that they indeed installed the software.
If they have not or have since ran their restore disk, have them re-install the software. If they
do not have the software CD, inform your LT and send then to customer service.
<br><br>
<b>Note:</b> If the customer has upgraded to or gotten a new computer with <b>Windows XP</b>, they do not need the
install the <b>Tango Manager</b> software.
<p align="center">
<img src="./images/tangomanager/tangohome.jpg" border=0>
</p>
<b>See Next: </b><a href="tango_install.php"><b>Install</b></a> <b>|</b> <a href="tango_access.php"><b>Access</b></a> <b>|</b> <a href="tango_support.php"><b>Support</b></a>  <b>|</b> <a href="tango_properties.php"><b>Properties</b></a>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>