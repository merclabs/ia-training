<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Tango Manager</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Tango Manager</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Properties [ Home Tab ]</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
After right clicking ong the profile icon and clicking on <b>Properties</b>, you
should see the following screen. The <b>Home</b> tab controls how and when the <b>Tango Manager</b>
displays. It also controls pop-up windows for different types of errors <b>Warning
, Error and Critical</b>.
<p align="center">
<img src="./images/tangomanager/prefstab1.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Properties [ Support Tab ]</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The <b>Support</b> tab displays the types of protocols and services that are supported by the modem. To add 
or remove any protocol or service simply uncheck what is needed or not needed.(most cases selected defaults are
OK.)
<p align="center">
<img src="./images/tangomanager/prefstab2.jpg" border=0>
</p>
<b>See Next: </b><a href="tango_home.php"><b>Home</b></a> <b>|</b> <a href="tango_install.php"><b>Install</b></a> <b>|</b> <a href="tango_access.php"><b>Access</b></a> <b>|</b> <a href="tango_support.php"><b>Support</b></a>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>