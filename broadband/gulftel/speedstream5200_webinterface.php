<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5200 Web Interface</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5200 Web Interface</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Accessing the Web Interface</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="right">
<input type="button" value=" << Back to 5200 Page" onclick="location.href='./speedstream_5200.php'">
</p>
<p align="center">
<br>
<img src="../images/01.jpg" border=0>
</p>
<ul>
<li>Open Internet Explorer.</li>
<li>Click <b>STOP</b> so that you get a screen like the one below.</li>
<li>Next, type <font color="red"><b>http://192.168.254.254</b></font> in the address box.</li>
</ul>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/02.jpg" border=0>
</p>
<ul>
<li>You should see the <b>"Administrative User Setup"</b> screen.</li>
<li>Enter the username <b>"admin"</b> in the username box.</li>
<li>then enter <b>"admin"</b> in the password box as well.</li>
<li>Then click <b>Save Settings</b>.</li>
</ul>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Login</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/03.jpg" border=0>
</p>
<ul>
<li>On this screen, enter the customers <b>username</b> and <b>passord</b>.</li>
<li><b>Note:</b> Remember that the user name may include <b>".coastalnow.net"</b>.</li>
<li>Check the <b>"Save Settings on Connect"</b> box.</li>
<li>Then select the <b>"Show Options"</b> button.</li>
  

</ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/04.jpg" border=0>
</p>
<ul>
<li>The<b>"PPP OPTIONS"</b> should display.</li>
<li>Check the <b>"Auto-Connect on Disconnect"</b> box.</li>
<li>
This will cause the router to re-connect and authenticate if the modem turned off or loses power.
<br>
<b>Note:</b> Please check this area if customer complains that the DSL was working after
while the DSL Service Tech was there and for a while after he left. With this box unchecked the 
DSL modem will not re-connect or authenticate after the customer restarts his/her computer.
</li>
<li>Then click <b>"Connect"</b>.</li>
</ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/05.jpg" border=0>
</p>
<ul>
<li>If everything went well, you should see the <b>"System Summary"</b> page.</li>
<li>This means you are connect, have the customer try to pull pages.</li>
</ul>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Problems</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/06.jpg" border=0>
</p>  
<ul>
<li>If you see the above screen, the modem is not in sync with the DSLAM.</li>
<li>Check physical connections.</li>
<li>If DSL light is not flashing, there is no data coming through.</li>
<li>notify LT, then Escalate.</li>
</ul>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/07.jpg" border=0>
</p>
<ul>
<li>If you get a Login Error, make certain username and password was entered correctly.</li>
<li>Make sure caps-lock is not on, it is case-sensitive.</li>
<li>Make sure no one else is using this same connection, the system will only allow one user to authenticate at a time.</li>
</ul>
</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The following screens display when a customer has clicked on the <b>SetUp Wizard</b> on the left.
Accept all of the <b>default</b> settings when going through the <b>Setup Wizard</b>, enter the 
customers <b>username</b> and <b>password</b> where required, then click <b>Reboot</b> on the final screen
to <b>Reboot</b> the DSL modem. Running the <b>Setup Wizard</b> is <font color="red"><b>NOT</b></font> required for instalation 
of DSL modem but the <b>Setup Wizard</b> can be use to authenticate.
</p>
<br>
<p align="center">
<img src="../images/08.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/09.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/10.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/11.jpg" border=0>
</p>
<br>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<p align="center">
<img src="../images/12.jpg" border=0>
</p>
<br>
</td>
</tr>

</table>
<p align="right">
<input type="button" value="<< Back" onclick="location.href='./speedstream_5200.php'">&nbsp;
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>