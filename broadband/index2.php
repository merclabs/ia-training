<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<?include("../listinginfo2.php")?>
<link rel="stylesheet" type="text/css" href="../provider/css/main.css">

<div align="center">
<br>
<div class="content">
<p class="warning">
PROPRIETARY AND CONFIDENTIAL INFORMATION OF INFO AVENUE PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.
</p>
</div>
<table class="list_table" cellspacing=1 width="550">
<tr>
 <td class="list_header" valign=top colspan=2>Highspeed Support Listing</td>
</tr>
<!-- A -->
<tr><td class="list_header2" valign=top><b>A</b></td></tr>
<?linker("accesspoint");?>
<?linker("atlantic");?>
<!-- B -->
<tr><td class="list_header2" valign=top><b>B</b></td></tr>
<?linker("benlomand");?> 
<?linker("bulloch");?>

<!-- C -->
<tr><td class="list_header2" valign=top><b>C</b></td></tr>
<?linker("casscomm");?>
<?linker("chesnee");?>
<?linker("chestertel");?>
<?linker("citizensnc");?>
<?linker("coastalnow");?>
<!-- D -->
<tr><td class="list_header2" valign=top><b>D</b></td></tr>

<!-- E -->
<tr><td class="list_header2" valign=top><b>E</b></td></tr>
<?linker("ellerbe");?>

<!-- F -->
<tr><td class="list_header2" valign=top><b>F</b></td></tr>
<?linker("fortmill");?>
 
<!-- G -->
<tr><td class="list_header2" valign=top><b>G</b></td></tr>
<?linker("cyberlink");?>
<?linker("grics");?>
<?linker("graceba");?>
<?linker("gulftel");?>
 
<!-- H -->
<tr><td class="list_header2" valign=top><b>H</b></td></tr>
<?linker("hayneville_telephone");?>
<?linker("hayneville_fiber");?>
<?linker("hargray");?>
<?linker("home");?>
<?linker("horry");?>
 
<!-- I -->
<tr><td class="list_header2" valign=top><b>I</b></td></tr>

<!-- J -->
<tr><td class="list_header2" valign=top><b>J</b></td></tr>
<?linker("jefferson");?>

<!-- K -->
<tr><td class="list_header2" valign=top><b>K</b></td></tr>
<?linker("klm");?>
 
<!-- L -->
<tr><td class="list_header2" valign=top><b>L</b></td></tr>
<?linker("lancaster");?>
<?linker("lexington");?>

<!-- M -->
<tr><td class="list_header2" valign=top><b>M</b></td></tr>
<?linker("mebtel");?>

<!-- N -->
<tr><td class="list_header2" valign=top><b>N</b></td></tr>
<?linker("northpenn");?>

<!-- O -->
<tr><td class="list_header2" valign=top><b>O</b></td></tr>

<!-- P --> 
<tr><td class="list_header2" valign=top><b>P</b></td></tr>
<?linker("palmettorural");?>
<?linker("parksbrothers");?>
<?linker("piedmonttelephone");?>
<?linker("plant");?>
<?linker("planters");?>
<?linker("pbtcomm");?>

<!-- Q -->
<tr><td class="list_header2" valign=top><b>Q</b></td></tr>

<!-- R -->
<tr><td class="list_header2" valign=top><b>R</b></td></tr>
<?linker("rockhill");?>

<!-- S -->
<tr><td class="list_header2" valign=top><b>S</b></td></tr>
<?linker("surry");?>
<!-- T -->
<tr><td class="list_header2" valign=top><b>T</b></td></tr>
<?linker("tricounty");?>
<?linker("twinlakes");?>

<!-- U -->
<tr><td class="list_header2" valign=top><b>U</b></td></tr>
<?linker("united");?>

<!-- V -->
<tr><td class="list_header2" valign=top><b>V</b></td></tr>

<!-- W -->
<tr><td class="list_header2" valign=top><b>W</b></td></tr>
<?linker("westcarolina");?>
<?linker("winco");?>
<?linker("wilkes");?>
<?linker("wilkes-nu-z");?>

<!-- X -->
<tr><td class="list_header2" valign=top><b>X</b></td></tr>

<!-- Y -->
<tr><td class="list_header2" valign=top><b>Y</b></td></tr>


<!-- Z -->
<tr><td class="list_header2" valign=top><b>Z</b></td></tr>
</table>

<br>
<br>
</table>
</div>
