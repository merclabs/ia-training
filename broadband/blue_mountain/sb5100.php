<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Motorola Surfboard 5100</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Motorola Surfboard 5100</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Blue Mountain distributes this modem to all of its customers. The following
applies for this Cable Modem.
<ul>
 <li>USB Connections not support, ethernet only.
 <li>No additional software needed, set up network to obtain IP/DNS automaticlly. 
 <li>Some NIC Cards are issued by Blue Mountain, if so, can help with driver for 
 Nic, otherwise no help with NIC cards.
 <li><i><b>See:</b> <a href="#ts">Troubleshooting below for more info...</a></i>
</ul>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center"><img src="./images/frontpanel_sb5100.jpg" alt="SB5100"></div>
</td>
</tr>
<tr>
<td class="field" valign=top  colspan=2>
<p>
Always check to see if the <b>Standby</b> button has been pressed, the <b>Standby</b> 
button is used to suspend the Internet connection. <u>No data</u> will be transmitted or received from the 
Internet when the Standby light is <u>on</u>. All other front-panel lights stay off until 
the Standby button is pressed again. 
</p>
<p>The lights provide information about power, communications, and errors:</p>
<!-- Start of table -->
<table align="center">
  <tr>
    <td class="infohd">Lights</td>
    <td class="infohd">Flashing </td>
    <td class="infohd">On</td>
  </tr>
  <tr>
    <td class="infohd">Power</td>
    <td class="infoline">Startup diagnostics in progress </td>
    <td class="infoline">The cable modem is powered on </td>
  </tr>
  <tr>
    <td class="infohd"> Receive</td>
    <td class="infoline"> Scanning for a receive (downstream) channel connection</td>
    <td class="infoline">The downstream channel is connected </td>
  </tr>
  <tr>
    <td class="infohd">Send </td>
    <td class="infoline">Scanning for a send (upstream) channel connection</td>
    <td class="infoline">&nbsp; The upstream channel is connected </td>
  </tr>
  <tr>
    <td class="infohd"> Online</td>
    <td class="infoline"> Scanning for a network connection</td>
    <td class="infoline"> The startup process is complete</td>
  </tr>
  <tr>
    <td class="infohd"> PC/Activity</td>
    <td class="infoline">Transmitting or receiving data</td>
    <td class="infoline">A device, such as a computer or hub, is connected to the USB or Ethernet
    connectors on the back panel.</td>
  </tr>
  <tr>
    <td class="infohd">Standby</td>
    <td class="infoline">This light does not flash Internet service is blocked because the Standby
    button was pressed. </td>
    <td class="infoline">If this light is on, all other lights are off. </td>
  </tr>
</table>
<br>
<!-- endo of table -->
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center"><img src="./images/rearpanel_sb5100.jpg" alt="SB5100"></div>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Back Panel Ports:</b><br>
 <ul>
  <li><b>ETHERNET</b> - The Ethernet port provides a connection to Ethernet equipped computers
	using a cable terminated with an RJ-45 connector.
  <li><b>USB(NOT SUPPORT BY THIS ISP)</b> - The USB port provides a connection to USB equipped computers.
  <li><b>CABLE</b> - The CABLE port provides a connection to the coaxial cable (coax) outlet.
  <li><b>+12VDC</b> - This connector provides.
 </ul>
 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b><a name="ts">Troubleshooting Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<br>
<ul>
 <li>Standard Power Cycle should fix most issues.
 <li>Check to see if <b>Cable TV</b> is <u>working</u>.
 <li><b>4</b> Lights, <b>POWER, RECEIVE, SEND, ONLINE,</b> should be solid. 
 <li>If router is connected, and still not working after power cycle, try bypassing router.
 <li>If activity light never flashs, check physical connections, maybe a bad cable or NIC. 
 <li>Check to see if Standby light is on, If so, press it again, then check to see if 
 internet connection has returned and all other lights are on.
 <li>If still not working, see an LT.
</ul>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>