<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Zyxel Prestige 645 Series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Zyxel Prestige 645 Series</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The Zyxel Prestige 645 ADSL Modem requires no software to be installed for both 
Mac and Windows(Windows 98, ME, NT, 2000, XP).
</p>
<p>
Customers are given their own <b>Static IP, Subnet, Gateway,</b> and <b>DNS Numbers</b> when the modem is installed.
These modems connect via the <b>Ethernet</b> and there is no software needed for set up. If a customer does not have
a <b>Network Card(NIC)</b>, advise them to call the <b>Service/Repair Department</b>.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_prestige645.jpg" border="0">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr><td class="infohd">LED</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">PWD</td><td class="infoline">The PWR(power) LED is on when power is applied to the Prestige.</td></tr>
<tr><td class="infohd">SYS</td><td class="infoline">A steady on SYS(system) LED indicates the Prestige is on and functioning properly
while an off SYS LED indicates the system is not ready of has a malfunction. The SYS LED blinks when the system is rebooting.</td></tr>
<tr><td class="infohd">LAN 10M</td><td class="infoline">A steady light indicates a 10mb Ethernet connection. The LED blinks when data is being sent/received.</td></tr>
<tr><td class="infohd">LAN 100M</td><td class="infoline">A steady light indicates a 100mb Ethernet connection. The LED blinks when data is being sent/received.</td></tr>
<tr><td class="infohd">DSL</td><td class="infoline">the ADSL LED is on when the Prestige is connected successfully to a DSLAM. The LED blinks during
ADSL line initialization. The LED is off when the link is down.</td></tr>
<tr><td class="infohd">ACT</td><td class="infoline">The ACT LED blinks during data transfer via the ADSL line. The LED is off when no data
is being transfered on the ADSL line.</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_prestige645.jpg" border="0">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd"></td><td class="infohd"></td></tr>
<tr><td class="infohd">Off/On</td><td class="infoline">Off on switch for unit.</td></tr>
<tr><td class="infohd">Power</td><td class="infoline">connects a power source using the power adapter.</td></tr>
<tr><td class="infohd">LAN 10/100M</td><td class="infoline">connect a computer using crossover ethernet cable.</td></tr>
<tr><td class="infohd">Reset</td><td class="infoline">resotres the unit to factory settings.</td></tr>
<tr><td class="infohd">DSL</td><td class="infoline">connect a telephone jack using a telephone wire.</td></tr>
</table>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Accessing the Prestige's config page.</b><br>
<ul>
<li>Web configurator(<b>not available with all models</b>)</li>
<li>Lanuch your web-browser and go to <b>http://192.168.1.1</b> as a URL.</li>
<li>Type "<b>Admin</b>" as the user name and "<b>1234</b>" as the password(default) and press <b>ENTER</b></li>
<li>You should see the <b>SITE MAP</b> screen.</li>
</ul>
Standard DSL Troubleshooting applies.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>