<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Twin Lakes DSL Modems</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Twin Lakes DSL Modems</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>Twin Lakes</b> issues three types of DSL Modems, <b>Zyxel</b>, <b>Acctel</b> and <b>Broadmax</b>.

<br><br>
<b>see: Troubleshooting Information below.</b>

</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Customers are given their own <b>Static IP, Subnet, Gateway,</b> and <b>DNS Numbers</b> when the modem is installed.
These modems connect via the <b>Ethernet</b> and there is no software needed for set up. If a customer does not have
a <b>Network Card(NIC)</b>, advise them to call the <b>Service/Repair Department</b>.
</p>
<p>
<font color="red"><b>NOTICE:</b></font><br>
Only a few customers buy their own modems, instead of using what Twin Lakes distributes. If this 
is the case, then we do not provide the customer with support, no other troubleshooting should be done.
Advise to contact Service/Repair Department.<br>
</p>

<font color="red"><b>Service/Repair Department Numbers:</b></font>
<ul>
<li><b>Baxter, Cookville South, Chestnut Mound:</b> 931-858-3191</li>
<li><b>Celina, Moss:</b> 931-268-0281</li>
<li><b>Gainesboro, Granville, Highland, North Springs:</b> 931-268-0281</li>
<li><b>Jamestown, Clarkrange:</b> 931-879-5869</li>
<li><b>Livingston, Byrdstown, Crawford, Rickman:</b> 931-823-5566</li>
</ul>

<p style="font-size:12;">
<i>The reason that we do not support customers that supply their own DSL modem
is because most DSL modems are the same and differ only by chipset, some customers buy their DSL modems
online or from friends and the chipset on these modems are set up for a different network and will not work
on Twin Lakes Network, matter what troubleshooting steps you take. Again no troubleshooting should be done 
for customer supplied DSL modems. send to Serivce/Repair.</i>
</p>

</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>