<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>VisionNet 708EU/R Ethernet/USB Combo ADSL Modem/Router</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>VisionNet 708EU/R Ethernet/USB Combo ADSL Modem/Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The VisionNet 708EU/R �Plug-NPlay� Combo USB/Ethernet
modem provide fast Internet access for all of your residential and SOHO ADSL needs. The
708EU/R incorporates both a USB and Ethernet port to support the requirements of the mass market.
</p>
<p>
Surry distributes this modem to most of its customer in the field, but distributes the 
<a href="speedstream_5667.php">SpeedStream 5667</a> to customers that live far out from Telco.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_vn708eur.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The front panel contains lights called LEDs that indicate the status
of the unit.
<p align="center">
<table>
<tr>
 <td class="infohd">Label</td>
 <td class="infohd">Status</td>
</tr>
<tr>
 <td class="infohd">PPP</td>
 <td class="infoline">
 It�s On when VisionNet 708 is connected to PPP which is working.
 </td>
</tr>
<tr>
 <td class="infohd">LAN</td>
 <td class="infoline">
 On: LAN link established and active.<br>
 Off: No LAN link
 </td>
</tr>
<tr>
 <td class="infohd">SYNC</td>
 <td class="infoline">
 On: ADSL link established and active<br>
 Blinking: DSL signal found<br>
 Off: No ADSL link<br>
 </td>
</tr>
<tr>
 <td class="infohd">TxRx</td>
 <td class="infoline">
 Flashes when ADSL data activity occurs.<br>
 May appear solid when data traffic is heavy.<br>
 </td>
</tr>
<tr>
 <td class="infohd">USB</td>
 <td class="infoline">
 On: ADSL link established and active<br>
 Blinking: DSL signal found<br>
 Off: No ADSL link<br>
 </td>
</tr>
<tr>
 <td class="infohd">Power</td>
 <td class="infoline">
 On: Unit is powered on<br>
 Off: Unit is powered off<br>
 </td>
</tr>
</table>
<br>
</p>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_vn708eur.jpg" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The rear panel contains the ports for the unit's data and power
connections.
<p align="center">
<table>
<tr>
 <td class="infohd">Label</td>
 <td class="infohd">Function</td>
</tr>
<tr>
 <td class="infohd">Power</td>
 <td class="infoline">Switches the unit on and off</td>
</tr>
<tr>
 <td class="infohd">DC INPUT</td>
 <td class="infoline">Connects to the supplied power converter cable</td>
</tr>
<tr>
 <td class="infohd">USB</td>
 <td class="infoline">Connects to the USB port on your PC</td>
</tr>
<tr>
 <td class="infohd">Ethernet</td>
 <td class="infoline">
 Connects the device to your PC's Ethernet port, or to the uplink port on your 
 LAN's hub, using the cable provided
 </td>
</tr>
<tr>
 <td class="infohd">PHONE</td>
 <td class="infoline">Provides an optional connection to your telephone</td>
</tr>
<tr>
 <td class="infohd">LINE</td>
 <td class="infoline">Connects the device to a telephone jack for DSL communication</td>
</tr>
<tr>
 <td class="infohd">RESET</td>
 <td class="infoline">Resets the device to the manufacturer�s default configuration</td>
</tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Install USB Driver</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>
Insert the <b>USB Driver CD</b> provided to you in your CD drive.<br>
<b>Note:</b> <font color="red">Do not plug your USB cable into the modem at this time.</font>
</li>
<li>Click on the <b>Start</b> menu in Windows and then <b>Run</b>.</li>
<li>
Type <b>d:/setup</b> then click on <b>OK</b>.
<table>
<tr>
 <td>
  <img src="./images/usbsetup1_vn708.jpg" border=0>
 </td>
 <td class="infotxt">
 (The letter <b>d</b> defines your CD ROM drive . This directory may be different on 
 some PC�s. Please consult your PC manual).
 </td>
</tr>
</table>
 <br>
</li>
<li>Plug in your <b>USB cable</b><br>
<p>
<img src="./images/usbsetup3_vn708.jpg" border=0>
</p>
<i>If a Microsoft digital signature dialog box displays</i>, click <b>Yes</b>.<br>
<p>  
	<img src="./images/usbsetup2_vn708.jpg" border=0>
</p>
</li>
<li>
Click <b>Done</b>, this will reboot your computer to complete installation. Select <b>No</b>
then click <b>Done</b> if you wish not to reboot your computer after installation is complete.
</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Check the modem for a public IP:</b><br>
<ol>
 <li>Open Internet Explorer and cancel any errors.
 <table>
<tr>
 <td valign="top">
  <img src="./images/usbsetup4_vn708.jpg" border=0>
 </td>
 <td class="infotxt" valign="top">
 <br><br>
 Type in the address bar:<br>
 <b>10.0.0.2</b> if the modem is connected <b>Ethernet</b> or<br>
 <b>10.0.0.3</b>
 if the modem is connected by <b>USB</b>.
 <br><br><br>
 At the login prompt type the Username <b>admin</b> and password <b>Visionnet</b>
 </td>
</tr>
</table>
 </li>
 <li>Click on <b>System View</b> on the left.</li> 
 <li>Check the <b>WAN</b> section for an IP address.<br>
  <img src="./images/usbsetup5_vn708.jpg" border=0><br><br>
 </li> 
 <li>If all settings are correct, open <b>Internet Explorer</b>.</li>
 <li>Try typing an IP address in the address bar.</li> 
 <li>Type in the IP Address <b><a href="http://204.116.67.198" target="_blank">204.116.67.198</a></b>.</li>
 <li>This should take you to the <b>Elkin City Schools Website</b>.</li>
 <li>If you do get to the site, try typing in <a href="http://www.elkincityschools.com" target="_blank">
 <b>www.elkincityschools.com</b></a>.</li> 
 <li>If you can't get to the site this way, then it is a <b>DNS</b> problem. Call us.</li>
</ol>
<b>Try pinging an IP address:</b>
<ol> 
 <li>Click <b>Start</b> and then Click <b>Run.</b></li> 
 <li>Type the word <b>command</b> and press <b>Enter.</b></li> 
 <li>An MS-DOS Prompt should appear.</li>
 <li>Type the word <b>ping</b> then a space, then <b>206.74.254.2</b></li>
 <li>Look to see if you get replies back</li> 
 <li>If you do, then the DSL is working but Internet Explorer is not.</li> 
 <li>If you can't get an IP address.</li> 
 <li><b>Power Cycle</b> the DSL Modem and the Computer.</li> 
</ol>
<b>Check for Firewall Software:</b><br>
<ol>
 <li>Ask the customer if they have any firewall software installed
 <br>(Zone Alarm, Zero Knowledge Freedom(Windows XP), Norton)</li>
 <li>If Zone alarm is installed, close Zone Alarm</li>
 <li>Right click on the red and yellow ZA symbol in the System tray 'bottom 
         right then click on the option to close Zone Alarm.</li>
 <li>Try to browse after closing Zone Alarm or any other Firewall software.</li> 
 <li>If it still doesn't work then the modem may need to be replace.</li>
 <li>call us to make sure our end is configured.</li>
</lo>
<br><br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
