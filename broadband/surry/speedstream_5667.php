<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5667 USB/Ethernet Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5667 USB/Ethernet DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The SpeedStream router may be connected to either an existing USB port or an Ethernet port. Both
connection methods are shown below. Surry only has a few of these modems in the field, they use
these modems for customers that live far out and use the <a href="visionnet_708eur.php">VisionNet 708EU/R</a>
for customers that are closer to telco.
</p>
<p>
To access the Internet through the SpeedStream Dual USB/Ethernet Router, the TCP/IP protocol must be
installed on your computer and configured with the same IP address and subnet as the router.
</p>
</p>
<p>
</td>
</tr>
<tr>
<td class="field">
Devices such as fax machines, caller ID boxes, or phones that share the same phone number as your DSL
account require a line filter, which prevents modem noise from disrupting the DSL signal on the phone line.
</p>
<br>
<img src="./images/dslphonefilter.png" border=0>

</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Software Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<img src="./images/cdrom_icon.png" border=0><br>
<ol>
<li>After hardware is installed, power on your PC. Power on modem.</li>
<li>Plug and play will detect the modem and ask you to install drivers.</li>
<li>Insert the Installation CD.</li>
<li>Direct the driver search to the CD-ROM drive.</li>
<li>Follow on-screen directions to complete the installation.</li>
</ol>
<br>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<center>
<img src="./images/frontpanel_ss5667.png" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td>
<td class="infohd">Power</td>
<td class="infohd">ADSL</td>
<td class="infohd">Act</td>
<td class="infohd">Enet</td>
<td class="infohd">USB</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/unit.png" border=0></center></td>
<td class="infoline">Power Off</td>
<td class="infoline">N/A</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port not connected; check Ethernet cable connection if using Ethernet interface</td>
<td class="infoline">USB port not connected; check USB cable connection if using USB interface</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/solid.png" border=0></center></td>
<td class="infoline">Power On</td>
<td class="infoline">Ready for Data Traffic</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port connected to LAN</td>
<td class="infoline">USB port connected to host</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/blinking.png" border=0></center></td>
<td class="infoline">N/A</td>
<td class="infoline">Searching for signal</td>
<td class="infoline">DSL traffic flow</td>
<td class="infoline">Ethernet traffic flow</td>
<td class="infoline">Traffic flow on the USB interface</td>
</tr>
<tr>
<td class="infohd">
<center>All Blinking</center></td>
<td class="infoline" colspan=5>Post failure</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following illustration shows the connectors on the rear panel of the SpeedStream router.
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_ss5667.png" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/ethernetsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/usbsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2> 
<b>Power Cycle:</b><br>
<ol>
 <li>Unplug DSL Modem, power down Computer.</li>
 <li>After 30 seconds, power up DSL Modem, leaving Computer Off.</li>
 <li>Once all lights are solid(Power,ADSL, Enet(if using Ethernet) or USB(if using USB))</li>
 <li>Boot computer, and try to browse.</li>
</ol>
<b>Release and Renew:</b><br> 
<ol>
<li>Click on Start, then on Run.</li> 
<li>Type the work <b>command</b> and press <b>Enter.</b></li> 
<li>At the prompt type <b>winipcfg</b></li> 
<li>Make sure the right adapter is showing in the white box.</li> 
<li>If not, Click the drop down list and click on the right adapter for Network card.</li> 
<li>Look at the IP address to see if it is right, for Speedstream's the IP Address should start with a 204.</li> 
<li>Click the release button to release the IP Address, the IP Address fields should clear to 0's.</li> 
<li>Click the renew button and check to see that the IP Address is in the 204.*.*.* range.</li> 
</ol>
<p>
If you get an IP but can't pull browsers homepage, try typing in a couple of address and see if they respond 
by going to <b>www.cnn.com</b>, <b>www.espn.com</b>, or <b>www.ibm.com</b>. If it goes to them then change their homepage to <b>www.surry.net</b>
or something else.
<ol>
<li>Open Internet Explorer.</li>
<li>Click on Tools at the top, then on Internet Options.</li>
<li>Change the homepage address and click Apply and OK.</li> 
<li>Close and reopen Internet Explorer, the new homepage should appear.</li></p>
</ol>
<b>Check LAN Settings:</b><br>
<ol>
<li>Open Internet Explorer click Tools then Internet Options.</li>
<li>Click on the Connection tab.</li>
<li>Click on the LAN settings button.</li>
<li>Make sure all boxes are unchecked.</li>
<li>Click OK.</li>
</ol>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>