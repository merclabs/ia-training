<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>BroadMax LinkMax HSA 300/HSA 300a</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>BroadMax LinkMax HSA 300/HSA 300a</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Tri-County at one time distributed the LinkMAX HSA300 and LinkMAX HSA300A ADSL modems,
but has stopped issuing customers this type of modem. Tri-County still has both LinkMax modems 
in the field but now issue customers the <a href="visionnet_202er-4.php">VisionNet 708EU/R</a> 
and <a href="visionnet_202er-4.php">202ER-4</a> ADSL modems.
</p>
<p>
<b><a href="#ts">See: Troubleshooting Information</a></b>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_HSA300a.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The Front panel for both modems look the same.
</p>
<b>HSA 300</b><br>
<ul> 
 <li>ADSL modem/bridge</li>
 <li>10Base-T interface</li> 
 <li>G.dmt/G.lite compliant</li>
 <li>Upgradeable firmware</li>
 <li>Friendly GUI software</li>
</ul> 
<b>HSA 300A</b><br>
<ul> 
 <li>ADSL modem/router</li>
 <li>10Base-T and USB interface</li> 
 <li>G.dmt/G.lite compliant</li>
 <li>Integrated PPPoE</li>
 <li>Upgradeable firmware</li>
</ul> 

</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->

<tr>
 <td class="heading" valign=top colspan=2><b><a name="ts">Troubleshooting Information</a></b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ul>
<li>Standard Power Cycle</li>
<li>Check Netowrk Settings</li>
<li>Check LAN Settings</li>
<li>Try to Ping</li>
</ul>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>