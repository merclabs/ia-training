<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5667 USB/Ethernet Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5667 USB/Ethernet DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
DSL service from Wilkes Telecommunications require a G.Lite compatible modem and 
may require a Network Interface Card (NIC) installed in your computer. Modems can 
be purchased from Wilkes Telecommunications or may be purchased from an outside computer 
peripheral dealer. Wilkes Telecommunications does not sell or install NIC cards. 
We recommend contacting a computer dealer to purchase and install this card in your computer.
<p>
<p>
DSL service from Wilkes Telecommunications is setup to use Dynamic Host Configuration 
Protocol (DHCP). This allows your computer to access the Wilkes network without having 
to assign a specific IP address to your computer. For more information on DHCP
see: <a href="http://www.dhcp-handbook.com/dhcp_faq.html#widxx" target="_blank">THE DHCP HANDBOOK</a>
</p>
DSL service will require a Noise Suppression Filter for each phone, fax or other type of
telephone equipment device. These filters will block unwanted static from regular voice calls 
or fax transmissions. One In-Line Filter is included with each account<br>
<img src="./images/dslphonefilter.png" border=0>

</td>
</tr>
<!--  Hidden: not used.
<tr>
<td class="heading" valign=top colspan=2><b>Software Installation</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<img src="./images/cdrom_icon.png" border=0><br>
<ol>
<li>After hardware is installed, power on your PC. Power on modem.</li>
<li>Plug and play will detect the modem and ask you to install drivers.</li>
<li>Insert the Installation CD.</li>
<li>Direct the driver search to the CD-ROM drive.</li>
<li>Follow on-screen directions to complete the installation.</li>
</ol>
<br>

</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/frontpanel_ss5667.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td>
<td class="infohd">Power</td>
<td class="infohd">ADSL</td>
<td class="infohd">Act</td>
<td class="infohd">Enet</td>
<td class="infohd">USB</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/unit.png" border=0></center></td>
<td class="infoline">Power Off</td>
<td class="infoline">N/A</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port not connected; check Ethernet cable connection if using Ethernet interface</td>
<td class="infoline">USB port not connected; check USB cable connection if using USB interface</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/solid.png" border=0></center></td>
<td class="infoline">Power On</td>
<td class="infoline">Ready for Data Traffic</td>
<td class="infoline">N/A</td>
<td class="infoline">Ethernet port connected to LAN</td>
<td class="infoline">USB port connected to host</td>
</tr>
<tr>
<td class="infoline">
<center><img src="./images/blinking.png" border=0></center></td>
<td class="infoline">N/A</td>
<td class="infoline">Searching for signal</td>
<td class="infoline">DSL traffic flow</td>
<td class="infoline">Ethernet traffic flow</td>
<td class="infoline">Traffic flow on the USB interface</td>
</tr>
<tr>
<td class="infohd">
<center>All Blinking</center></td>
<td class="infoline" colspan=5>Post failure</td>
</tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The following illustration shows the connectors on the rear panel of the SpeedStream Modem.
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/rearpanel_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/ethernetsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/usbsetup_ss5667.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Before you can use your computer over a network, it must have an IP
address. The modem contains a dynamic host configuration protocol
(DHCP) server that will automatically assign an IP address to the PC if the
PC is set to obtain an IP address from a DHCP server. If you have set up
your modem and computer according to the installation instructions, follow
these steps to get an IP address via DHCP if you are running a PC with
</p>
<b>Windows 95/98:</b>
<ol>
<li>Select <b>Start</b> > <b>Settings</b> > <b>Control Panel</b></li>
<li>Double click the <b>Network</b> icon</li>
<li>Select <b>TCP/IP</b> from the list of network components. Select <b>Properties</b>.</li>
<li>Select the <b>IP Address</b> tab and verify that <b>Obtain an IP address
automatically</b> is selected. If not, then select it.</li>
<li>Also check the <b>Advanced</b> tab, make sure <b>TCP/IP</b> is set as default protocol at the bottom.</li>
<li>Click <b>OK</b> twice.</li>
<li><b>Restart</b> if prompted.</li>
</ol>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
