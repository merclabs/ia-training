<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>SpeedStream 5200 Series</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>SpeedStream 5200 DSL Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
DSL service from Wilkes Telecommunications require a G.Lite compatible modem and 
may require a Network Interface Card (NIC) installed in your computer. Modems can 
be purchased from Wilkes Telecommunications or may be purchased from an outside computer 
peripheral dealer. Wilkes Telecommunications does not sell or install NIC cards. 
We recommend contacting a computer dealer to purchase and install this card in your computer.
<p>
<p>
DSL service from Wilkes Telecommunications is setup to use Dynamic Host Configuration 
Protocol (DHCP). This allows your computer to access the Wilkes network without having 
to assign a specific IP address to your computer. For more information on DHCP 
see: <a href="http://www.dhcp-handbook.com/dhcp_faq.html#widxx" target="_blank">THE DHCP HANDBOOK</a>
</p>
DSL service will require a Noise Suppression Filter for each phone, fax or other type of
telephone equipment device. These filters will block unwanted static from regular voice calls 
or fax transmissions. One In-Line Filter is included with each account<br>
<img src="./images/dslphonefilter.png" border=0>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><img src="./images/frontpanel_ss5200.png" border=0></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infohd">&nbsp;</td><td class="infohd">Power</td><td class="infohd">DSL</td><td class="infohd">Act</td><td class="infohd">Enet</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/unit.png" border=0></center></td><td class="infoline">Power Off</td><td class="infoline">DSL link not detected</td><td class="infoline">No Network activity</td><td class="infoline">Ethernet port not connected: check Ethernet cable connection</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/solid.png" border=0></center></td><td class="infoline">Power On</td><td class="infoline">Ready for Data Traffic</td><td class="infoline">N/A</td><td class="infoline">Ethernet port connected to LAN</td></td>
</tr>
<tr>
<td class="infoline"><center><img src="./images/blinking.png" border=0></center></td><td class="infoline">N/A</td><td class="infoline">Searching for Data</td><td class="infoline">DSL traffic flow</td><td class="infoline">Ethernet traffic flow</td></td>
</tr>
</table>

<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/rearpanel_ss5200.png" border=0>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr>
<td class="infotxt" colspan=2>From left to right:</td></tr>
</tr>
<tr>
<td class="infohd">Power</td> <td class="infoline">The bridge uses a standard AC power cord and has anON/OFF switch</td></tr>
</tr>
<tr>
<td class="infohd">DSL Port</td><td class="infoline">DSL connectivity is through this 8-pin, RJ-45 port.</td></tr>
</tr>
<tr>
<td class="infohd">Ethernet Ports</td><td class="infoline">One Ethernet 10Base-T port (8 pin, RJ-45)</td></tr>
</tr>
<tr>
<td class="infohd">Console Port</td><td class="infoline">Asynchronous RS232 connectivity is through this 8 pin,RJ-45 port (support only).</td></tr>
</tr>
</table>
<br>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Bridge Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>Place your bridge in a location where it will be well ventilated. Do not stack
it with other devices or place it on carpet.</li>
<li>Connect the bridge to your PC, using the red cable between the bridge�s
Ethernet port and the Ethernet port on your PC.</li>
<li>Connect the bridge to your DSL wall jack, using the purple label cable
between the bridge�s DSL port and the DSL wall jack.</li>
<li>Connect the bridge to an AC power outlet using the supplied power cord.</li>
<li>Switch the bridge ON.</li>
</ol>
The following illustration shows the SpeedStream 5200 Series Bridge when
connected:
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<img src="./images/basicsetup_ss5200.png" border=0>
<img src="./images/frontlights_ss5200.png" border=0>
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Before you can use your computer over a network, it must have an IP
address. The modem contains a dynamic host configuration protocol
(DHCP) server that will automatically assign an IP address to the PC if the
PC is set to obtain an IP address from a DHCP server. If you have set up
your modem and computer according to the installation instructions, follow
these steps to get an IP address via DHCP if you are running a PC with
</p>
<b>Windows 95/98:</b>
<ol>
<li>Select <b>Start</b> > <b>Settings</b> > <b>Control Panel</b></li>
<li>Double click the <b>Network</b> icon</li>
<li>Select <b>TCP/IP</b> from the list of network components. Select <b>Properties</b>.</li>
<li>Select the <b>IP Address</b> tab and verify that <b>Obtain an IP address
automatically</b> is selected. If not, then select it.</li>
<li>Also check the <b>Advanced</b> tab, make sure <b>TCP/IP</b> is set as default protocol at the bottom.</li>
<li>Click <b>OK</b> twice.</li>
<li><b>Restart</b> if prompted.</li>
</ol>
<br>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>