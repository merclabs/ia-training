<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>3Com HomeConnect ADSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>3Com HomeConnect ADSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The <b>3com HomeConnect ADSL Modem</b> can connect via both <b>Ethernet</b> or <b>USB</b>. 
No software installation required unless connecting via <b>USB</b>. Network Adapter should be set to
obtain an IP Address automaticlly with either connection type.<b>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_3comhc.jpg">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr><td class="infohd">No.</td><td class="infohd">LED</td>
<td class="infohd">Description</td></tr>
<tr><td class="infohd">1</td><td class="infohd">Cable Modem Power</td>
<td class="infoline">Indicates power is applied to the
cable modem. This LED is solid green when the cable
modem is on.</td></tr>
<tr><td class="infohd">2</td><td class="infohd">Cable Modem Status</td>
<td class="infoline">
<!-- Inside table start -->
<br>
The color of this LED indicates
the cable modem's operations status as:
<table>
<tr><td class="infohd">LED STATE</td><td class="infohd">DESCRIPTION</td></tr>
<tr><td class="infoline">BLINKING RED</td><td class="infoline">
Downstream channel is locked.
</td></tr>
<tr><td class="infoline">SOLID RED</td>
<td class="infoline">
Cable Modem is searching for a downstream channel.</td></tr>
<tr><td class="infoline">SOLID GREEN</td><td class="infoline">
Registration is complete and cable modem is operational
</td></tr>
</table>
<br>
<!-- Inside table end -->
</td></tr>
<tr><td class="infohd">3</td><td class="infohd">Cable Activity</td>
<td class="infoline">This LED flashes red when data is
transmitted to or from your cable company via the RF
(cable) port. Flashing red indicates traffic.</td></tr>
<tr><td class="infohd">4</td><td class="infohd">PC Link Ethernet</td>
<td class="infoline">This LED indicates that data is
transmitted to or from your PC via the Ethernet port. The
LED is solid green when there is connection between the
cable modem and an Ethernet port. If the LED is flashing
green, there is traffic over the Ethernet port. If this LED is
not lighted, then there is no connection between the cable
modem and an Ethernet port.</td></tr>
<tr><td class="infohd">5</td><td class="infohd">PC Link USB</td>
<td class="infoline">Indicates that data is being transmitted to
or from your PC over the USB port. This LED is solid green
when there is a connection between the cable modem and
a USB port. If the LED is flashing green, there is traffic over
the USB port. If this LED is not lighted, then there is no
connection between the cable modem and a USB port.</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_3comhc.jpg">
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr><td class="infohd">Port</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">Cable RF Connector</td>
<td class="infoline">This is where you connect the
coaxial cable that leads to your splitter or your cable walljack.</td></tr>
<tr><td class="infohd">USB Port</td><td class="infoline">
This is where you plug the included USB cable when performing a USB installation. The other end
connects to an active USB port on your computer
</td></tr>
<tr><td class="infohd">RJ-45 Ethernet Port</td><td class="infoline">
This is where you plug the
included RJ-45 Ethernet cable when performing an
Ethernet installation. The other end connects to the RJ-45
jack on your network interface card (NIC) or active Ethernet
port.
</td></tr>
<tr><td class="infohd">Power Jack</td><td class="infoline">
This is where you plug in the power
adapter that came with your cable modem. Remember to
use only the power supply that came with your cable
modem.
</td></tr>
<tr><td class="infohd">Reset Button</td><td class="infoline">
Use this button when you are having
trouble connecting to your broadband service provider
(BSP). This initiates a full reset of the cable modem and
re-initiates the channel scan that occurs every time your
cable modem is powered on. Click the button by inserting
a ballpoint pen into the hole and pressing until you feel the
button click.
</td></tr>
</table>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_3comhc.gif">
</p>
</td>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Ethernet Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Your cable modem supports Ethernet service.To install the
cable modem using an Ethernet connection:
<ul>
<li>Connect the cable line to the cable modem�s CATV cable
connector. Be careful not to bend the wire in the center of
the cable line when you connect it to the cable modem.</li>
<li>After hand-tightening the CATV cable connector, use an
adjustable wrench (not included) to firmly tighten it. Be
careful not to over-tighten the connector or you may
damage either the connector or the cable modem. If you
plan to have the cable line connected to a television as well
as the cable modem, you will need a cable line splitter (not
included).</li>
<li>Plug one end of the RJ-45 network cable into the cable
modem�s RJ-45 jack and the cable�s other end into the
existing network interface card installed in your computer.</li>
<li>Plug the cable modem�s power adapter into a wall outlet or
surge protector and into the cable modem�s power jack.
<li>Power on the computer.</li>
</ul>
</td>
<tr>
 <td class="heading" valign=top colspan=2><b>USB Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Your cable modem supports USB service.To install the cable
modem using a USB connection, follow the instructions for
your operating system.
</p>
<p>
<b>Applies to Windows 98:</b><br>
<ul>
<li>Make the following connections:</li>
 <ol type="a">
 <li>Connect the cable line to the cable modem�s CATV cable
  connector. Be careful not to bend the wire in the center
  of the cable line when you connect it to the cable
  modem.</li>
  <li>After hand-tightening the CATV cable connector, use an
  adjustable wrench (not included) to firmly tighten it. Be
  careful not to over-tighten the connector or you may
  damage either the connector or the cable modem. If
  you plan to have the cable line connected to a television
  Connecting the Cable Modem to Your Computer 13
  as well as the cable modem, you will need a cable line
  splitter (not included).</li>
  <li>Connect one end of the USB cable line to the cable
  modem�s USB port and the other end to the USB port on
  the computer.</li>
  <li> Plug the cable modem�s power adapter into a wall outlet
  or surge protector and into the cable modem�s power
  jack.</li>
  </ol>
<li>Power on the computer.
Windows detects the cable modem. The Found New
Hardware screen appears.</li>
<li>When the Add New Hardware Wizard screen appears,
insert the Cable Connections CD into the computer�s
CD-ROM drive.</li>
<li>Click Next.</li>
<li>Select Search for the best driver for your device.
(Recommended) and click Next.</li>
<li>Check the CD-ROM drive check box and click Next to
search for the necessary driver files.</li>
<li>If Windows finds an updated driver, select The updated
driver (Recommended) and click Next.</li>
<li>Click Next. The computer automatically copies the
necessary driver files from the CD.</li>
<li>The computer finishes copying the driver files and prompts
you to insert the Windows 98 CD into the CD-ROM drive.</li>
<li>Insert the Windows 98 CD and click OK.</li>
If the Copying Files dialog box appears, make sure that you
have inserted the correct CD and that you have pointed it
to the correct path. After you point the Add New Hardware
Wizard to the correct path, click OK.</li>
<li>Connecting the Cable Modem to Your Computer 15
The computer automatically copies the necessary system
files.</li>
<li>Click Finish after the computer has copied the necessary
files.</li>
<li>The System Settings Change dialog box appears.</li>
<li>Click Yes to restart the computer.</li>
<li>Verify that the cable modem is operating properly. To do
this, watch the cable modem Link Status and Power LEDs;
when they are solid green, the cable modem is ready for
use.</li>
</ol>
</p>
<b>If you are powering up the cable modem for the first time, allow 15 minutes for 
this process to finish.</b> 
 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Standard Cable Modem Troubleshooting Applies(Power Cycle, check Network, check Connection).
</td>
</tr>

</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>