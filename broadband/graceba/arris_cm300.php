<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Arris CM300 Cable Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Arris CM300 Cable Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The CM300 Cable Modem are The Touchstone Cable Modem provides an USB connection for easy hookup. The
Touchstone Cable Modem also provides an Ethernet connection for use with either
a single computer or home/office LAN. You can even hook up two separate computers
at the same time using both these connections. 
</p>
<p style="color:red">
<b>Note from Graceba:</b><br>
The Arris cable modems that we install have a standby button 
located beside the power outlet on the modem's back panel. This standby button 
disconnects LAN to WAN access. This button is easy to be press and will cutoff a 
customerís access to the Internet. To quickly identify this issue have a customer 
locate the cable modem and monitor the lights located on the front of the modem. 
If the standby button has been pressed then the Online light will flash at a 
constant pattern instead of remaining solid. Press the <b>Standby</b> button again to 
reconnect modem. 
<br><br>
</p>
<b>See: <img src="./images/number_one.png" border="0">  in Back Panel section below.</b>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_cm300.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>The front of the Cable Modem has the following indicators and controls</p>
<ol>
<li><b>Ethernet:</b> flickers when sending or receiving data to the PC over the Ethernet
connection; on steadily otherwise.</li>
<li><b>USB:</b> blinks when sending or receiving data to the PC over the USB connection;
on steadily otherwise.</li>
<li><b>Cable:</b> blinks when sending or receiving data on the cable system; on
steadily otherwise.</li>
<li><b>Online:</b> blinks when the Cable Modem is isolated from the Internet (by
pressing the Standby button on the rear of the Cable Modem); on when the
Cable Modem is connected to the Internet.</li>
<li><b>Power:</b> blinks when the Cable Modem is setting up; on steadily when ready for use.</li>
</ol> 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_cm300.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>The rear of the Cable Modem has the following connectors and controls:</p>
<ol>
<li><b style="color:red">Standby button:</b> press once to disconnect all PCs connected to the Cable
Modem from the network; press again to reconnect. The Online light shows you the current standby state.</li>
<li><b>Power:</b>plug the AC adapter in here.</li>
<li><b>USB connector:</b> for connecting the Cable Modem directly to a PC.</li>
<li><b>Ethernet connector:</b> for connecting the Cable Modem to a PC or home network LAN.</li>
<li><b>Reset button:</b></li> resets the Cable Modem as if you disconnected power. Use a
pointed non-metallic object to press this button.</li>
<li><b>Cable:</b> plug the coax cable in here.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Standard DSL/Cable Modem Troubleshooting applies. There's no additional software to 
install unless you are connecting via <b>USB</b> then you must install the <b>USB Driver</b> this
applies for all versions of Windows(98se, ME, 2000, XP). Set Ethernet Adapter to obtain 
an IP address automatically.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>