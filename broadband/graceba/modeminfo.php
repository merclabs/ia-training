<?

$modemname[] = "Arris";
$modelnumber[] = "CM300";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/arris_cm300.php";

$modemname[] = "Scientific Atlanta";
$modelnumber[] = "Webstar DPX100, DPX120";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/webstar_dpx100-120.php";

$modemname[] = "Scientific Atlanta";
$modelnumber[] = "Webstar DPX110";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/webstar_dpx110.php";

$modemname[] = "3Com";
$modelnumber[] = "Home Connect";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/3com_homeconnect.php";

$modemname[] = "VisionNet";
$modelnumber[] = "202ES";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/visionnet_202es.php";


$modemname[] = "VisionNet";
$modelnumber[] = "202ER-4";
$type[] = "Cable Modem";
$description[] = "None";
$documentation[] = "../".$id."/visionnet_202er-4.php";
?>
