<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>VisionNet 202ES-4 ADSL Ethernet Router</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>VisionNet 202ES-4 ADSL Ethernet Router</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The VisionNet 202ER-4 4-Port �Plug-N-Play� Ethernet Modem
Router provides fast Internet access for all of your residential and SOHO ADSL needs. The
202ER-4 incorporates 4 Ethernet ports to support the requirements
of the mass market.
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_vn202er-4.jpg" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The front panel contains lights called LEDs that indicate the status
of the unit.
<p align="center">
<table>
<tr>
 <td class="infohd">Label</td>
 <td class="infohd">Status</td>
</tr>
<tr>
 <td class="infohd">PPP</td>
 <td class="infoline">
 Solid green when using PPPoE or PPPoA, if the user has
 authenticated to the ISP�s server.
 </td>
</tr>
<tr>
 <td class="infohd">LAN</td>
 <td class="infoline">
 On: LAN link established and active.<br>
 Off: No LAN link
 </td>
</tr>
<tr>
 <td class="infohd">SYNC</td>
 <td class="infoline">
 On: ADSL link established and active<br>
 Blinking: DSL signal found<br>
 Off: No ADSL link<br>
 </td>
</tr>
<tr>
 <td class="infohd">TxRx</td>
 <td class="infoline">
 Flashes when ADSL data activity occurs.<br>
 May appear solid when data traffic is heavy.<br>
 </td>
</tr>
<tr>
 <td class="infohd">ACT</td>
 <td class="infoline">
 On: Device passed self test<br>
 Blinking: Device failed self test.<br>
 </td>
</tr>
<tr>
 <td class="infohd">Power</td>
 <td class="infoline">
 On: Unit is powered on<br>
 Off: Unit is powered off<br>
 </td>
</tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_vn202es-4.gif" border=0>
</center>
</td>
</tr>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The rear panel contains the ports for the unit's data and power
connections.
<p align="center">
<table>
<tr>
 <td class="infohd">Label</td>
 <td class="infohd">Function</td>
</tr>
<tr>
 <td class="infohd">Power</td>
 <td class="infoline">Switches the unit on and off</td>
</tr>
<tr>
 <td class="infohd">DC INPUT</td>
 <td class="infoline">Connects to the supplied power converter cable</td>
</tr>
<tr>
 <td class="infohd">ETHERNET</td>
 <td class="infoline">
 Ethernet port to connect PC's NIC Card to modem
 </td>
</tr>
<tr>
 <td class="infohd">CONSOLE</td>
 <td class="infoline">
 Console port
 </td>
</tr>
<tr>
 <td class="infohd">PHONE</td>
 <td class="infoline">Provides an optional connection to your telephone</td>
</tr>
<tr>
 <td class="infohd">LINE</td>
 <td class="infoline">Connects the device to a telephone jack for DSL communication</td>
</tr>
<tr>
 <td class="infohd">RESET</td>
 <td class="infoline">Resets the device to the manufacturer�s default configuration</td>
</tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Standard DSL/Cable Modem Troubleshooting applies.</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>