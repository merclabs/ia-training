<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Scientific Atlanta Webstar DPX100 and DPX120</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Scientific Atlanta Webstar DPX100 and DPX120</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The WebSTAR Model DPX100 or Model DPX120
Cable Modem offers the following outstanding features:
<ul>
<li>Two-way, high-speed data rates much faster than
56K analog modems, ISDN, or DSL</li>
<li>Plug and Play operation for easy setup and
installation</li>
<li>10/100BaseT Ethernet or USB connection</li>
<li>Clear LED display</li>
<li>Vertical or horizontal placement</li>
<li>CableLabs Data Over Cable System Interface
Specifications (DOCSIS) certification (for the Model
DPX100), and Euro-DOCSIS certification (for the
Model DPX120) ensures interoperability with
other certified cable modems and certified cable
service providers</li>
<li>Cable modem software can be upgraded
automatically by your cable service provider</li>
</ul>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_wsdpx.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<table>
<tr><td class="infohd">POWER</td><td class="infoline">

Illuminates solid green to indicate that power is being applied to the cable modem</td></tr>
<tr><td class="infohd">PC</td><td class="infoline">Illuminates solid green to indicate that an Ethernet/USB carrier is
present <br>
Blinks to indicate that Ethernet/USB data is being transferred between the 
PC and the cable modem</td></tr>
<tr><td class="infohd">DATA</td><td class="infoline">Blinks to indicate that data is being transferred between the modem and
the cable network</td></tr>
<tr><td class="infohd">CABLE</td><td class="infoline">Illuminates solid green when the modem is registered on the network and
fully operational. This indicator blinks to indicate one of the following conditions:
<ul>
<li>The cable modem is booting up and not ready for data</li>
<li>The cable modem is scanning the network and attempting to register</li>
<li>The cable modem has lost registration on the network and will continue
blinking until it registers again</li>
<li>Blinks very slowly (once every 5 seconds) to indicate that Cable Modem</li>
</ul>
Access Protection is enabled
</td></tr>
</table>
<p><b>Note:</b> Once the cable modem is successfully registered on the network, the
<b>POWER</b> and <b>CABLE</b> indicators illuminate continuously to indicate that the
cable modem is active and fully operational.
<br><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_wsdpx.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<table>
<br>
<tr><td class="infohd">POWER</td><td class="infoline">Connects the cable modem to the DC output of the AC power
adapter that is provided with your cable modem
Note: Only use the power cord and the AC power adapter that are
provided with your cable modem.</td></tr>
<tr><td class="infohd">RESET</td><td class="infoline">Reset-to-Default button (Factory Reset)
<br><b>CAUTION:</b><br>
This button is for maintenance purposes only. Do not use!</td></tr>
<tr><td class="infohd">ETHERNET</td><td class="infoline">RJ-45 Ethernet port connects to the Ethernet port on your PC</td></tr>
<tr><td class="infohd">USB</td><td class="infoline">12 Mbps USB port connects to the USB port on your PC</td></tr>
<tr><td class="infohd">CABLE IN</td><td class="infoline"> F-Connector connects to an active cable signal from your cable
service provider
<p>
<b>CAUTION:</b><br>
Do not connect your PC to both the Ethernet and USB ports at
the same time.
Note: You can connect two separate PCs to the cable modem at
the same time by connecting one to the Ethernet port and one to
the USB port. See Modem Installation next in
</p>
</td></tr>
</table><br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_wsdpx.jpg" border=0>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
Standard DSL/Cable Modem Troubleshooting applies. There's no additional software to 
install unless you are connecting via <b>USB</b> then you must install the <b>USB Driver</b> this
applies for all versions of Windows(98se, ME, 2000, XP). Set Ethernet Adapter to obtain 
an IP address automatically.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>