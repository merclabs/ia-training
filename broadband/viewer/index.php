<!-- <link rel="stylesheet" type="text/css" href="../../provider/css/main.css">-->
<?require_once("./provider/navigator/navigator.php");?>
<?require_once("./provider/".$id."/company.php");?>
<?require_once("./provider/".$id."/services.php");?>
<?require_once("./broadband/".$id."/modeminfo.php")?>
<h1><?echo($officialname);?></h1>
<p class="note" align="center">
<b>PROPRIETARY AND CONFIDENTIAL INFORMATION OF SPIRIT TELECOM<BR>
PRINTOUTS SHOULD BE SHREDDED WHEN DISCARDED.</b>
</p>
<table class="list_table" cellspacing=1>
 <tr>
  <td class="list_header" colspan=2 valign=top>Company Information</td>
 </tr>
<tr>
 <td class="list_title" valign=top><b>Official Company Name</b></td>
 <td class="list_content_normal" valign=top><?echo($officialname);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Street Address</b></td>
 <td class="list_content_normal" valign=top><?echo($address);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>City</b></td>
 <td class="list_content_normal" valign=top><?echo($city);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>State</b></td>
 <td class="list_content_normal" valign=top><?echo($state);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Zip</b></td>
 <td class="list_content_normal" valign=top><?echo($zip);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Mailing Address</b></td>
 <td class="list_content_normal" valign=top><?echo($mailingadddress);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Company Phone</b></td>
 <td class="list_content_normal" valign=top><?echo($companyphone);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Company Fax</b></td>
 <td class="list_content_normal" valign=top><?echo($companyfax);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>24-hour Trouble Number</b></td>
 <td class="list_content_normal" valign=top><?echo($twentyfourtrouble);?></td>
</tr>
</table>
<!--
<br><br>
 
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top colspan=2>Internet Services</td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Portal</b></td>
 <td class="list_content_normal" valign=top><a href="<?echo("http://".$portal);?>" target="_blank"><?echo($portal);?></a</td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Web Mail</b></td>
 <td class="list_content_normal" valign=top><a href="<?echo("http://".$webmail);?>" target="_blank"><?echo($webmail);?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Web Hosting</b></td>
 <td class="list_content_normal" valign=top><a href="<?echo("http://".$webhosting);?>" target="_blank"><?echo($webhosting);?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Domain Name</b></td>
 <td class="list_content_normal" valign=top><a href="<?echo("http://".$domain);?>" target="_blank"><?echo("$domain");?></a></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>POP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($popserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>SMTP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($smtpserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>FTP Server</b></td>
 <td class="list_content_normal" valign=top><?echo($ftpserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>News Server</b></td>
 <td class="list_content_normal" valign=top><?echo($newsserver);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Primary DNS</b></td>
 <td class="list_content_normal" valign=top><?echo($primarydns);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Secondary DNS</b></td>
 <td class="list_content_normal" valign=top><?echo($secondarydns);?></td>
</tr>
<tr>
 <td class="list_title" valign=top><b>Telnet Server</b></td>
  <td class="list_content_normal" valign=top><a href="<?echo("telnet://".$telnet);?>"><?echo("$telnet");?></a></td>
 </td>
</tr>
</table>
-->
<br><br>
 <!-- Modem Listing Information --> 
 <!-- start -->
<table class="list_table" cellspacing=1>
<tr>
 <td class="list_header" valign=top  colspan=5>Broadband Equipment Listing</td>
</tr>
<tr>
 <td class="list_header2" valign=top><b>Name</b></td>
 <td class="list_header2" valign=top><b>Model</b></td>
 <td class="list_header2" valign=top><b>Type</b></td>
 <td class="list_header2" valign=top><b>Description</b></td>
 <td class="list_header2" valign=top><b>Modem Info</b></td>
</tr>
<?
for($i = 0;$i < count($modemname);$i++){
$doc = substr($documentation[$i], 2); 
echo("<tr>");
echo("<td class=\"list_content_normal\" valign=top><b>$modemname[$i]</b></td>\n");
echo("<td class=\"list_content_normal\" valign=top>$modelnumber[$i]</td>\n");
echo("<td class=\"list_content_normal\" valign=top>$type[$i]</td>\n");
echo("<td class=\"list_content_normal\" valign=top>$description[$i]</td>\n");
echo("<td class=\"list_content_normal\" valign=top ><input type=\"button\" value=\"View Modem Information\" onclick=\"window.open('./broadband$doc','mwin','resizable=yes, scrollbars=yes,width=600,height=400')\"></td>\n");
echo("</tr>");
}
?>
</table>
 <!-- end -->
</p>
<br>
<p class="note">
Do not see a modem listed for this provider, click the <b>"Submit Report"</b> button to report it.</p>
<p align="center">
<input type="button" value="<< Back " onclick="location.href='?content=./broadband/main.php';">&nbsp;&nbsp;<input type="button" value="Submit Report" onclick="location.href='?content=./broadband/discovery/main.php'">
</p>
<!-- end -->
 <!-- End of Modem Listing -->
<!-- End Cut and Paste Here -->
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
 
