<?
// Get todays date
$texttoday = date("F j,Y [ l ]");
// Get Time.
$time = date(" g:i:s [ a ]"); 
/*
// Variable submitted from the form
$name
$email
$provider
$brand
$modnum
$contype
$description
$comment
*/
// Build e-mail address since all come from Infoave.
$full_email = $email;

// This writes to a file.
$lf = fopen("broadband/discovery/log.php", "a");
fwrite($lf, "<!-- Start of Entry  / Date: $texttoday by $name -->\n");
fwrite($lf, "<tr>\n");
fwrite($lf, "<td class=\"field\" valign=top colspan=2>\n");
fwrite($lf, "<table>\n");
fwrite($lf, "<tr><td class=\"infohd\">Date Submitted:</td><td class=\"infoline\">$texttoday</td></tr>\n");
fwrite($lf, "<tr><td class=\"infohd\">Time Submitted:</td><td class=\"infoline\">$time</td></tr>\n");
fwrite($lf, "<tr><td class=\"infohd\">Submitted by:</td><td class=\"infoline\">$name</td></tr>\n");
fwrite($lf, "<tr><td class=\"infohd\">E-Mail:</td><td class=\"infoline\"><a href=\"mailto:$full_email\">$full_email</a></td>\n");
fwrite($lf, "<tr><td class=\"infohd\">Provider:</td><td class=\"infoline\">$provider</td>\n");
fwrite($lf, "<tr><td class=\"infohd\">Brand:</td><td class=\"infoline\">$brand</td>\n");
fwrite($lf, "<tr><td class=\"infohd\">Model Number:</td><td class=\"infoline\">$modnum</td>\n");
fwrite($lf, "<tr><td class=\"infohd\">Conn. Type:</td><td class=\"infoline\">$contype</td></tr>\n");
fwrite($lf, "<tr><td class=\"infohd\">Description:</td><td class=\"infoline\">$description</td></tr>\n");
fwrite($lf, "<tr><td class=\"infohd\">Comments:</td><td class=\"infoline\">$comment</td></tr>\n");
fwrite($lf, "</table>\n");
fwrite($lf, "</td>\n");
fwrite($lf, "</tr>\n");
fwrite($lf, "<!-- End of Entry / Date: $texttoday by $name -->\n");
fclose($lf);

// Send me a short message when a information is requeste.

$subject = "DSL/Cable Modem Discovery [ $provider ]";
$message .= "------------------------------------------------\n";
$message .= "DSL/CABLE MODEM DISCOVERY REPORT FROM IA-TRAINING \n";
$message .= "------------------------------------------------\n";
$message .= "From: $name\n";
$message .= "Provider: $provider\n";
$message .= "Modem: $brand $modnum\n";
$message .= "Type: $contype\n";
$message .= "\n";
$message .= "Description: $description\n";
$message .= "\n";
$message .= "Comments: $comment\n";
$message .= "-------------------------------------------------\n";
$message .= "Reply to: $full_email\n";
$message .= "-------------------------------------------------\n";
// Who to send it to:
$webmaster = "cbarnette@infoave.net";
$header = "From: $name <$full_email>\r\n";

mail($webmaster, $subject, $message, $header);
?>
<h1>Thank you <?echo($name)?>.</h1>
<table class="list_table" cellspacing=1 width=450>
<tr>
 <td class="list_header" valign=top colspan=2>Your information has been sent...</td>
</tr>
<tr>
<td class="list_content" valign=top colspan=2>
Thank you for sending in new modem information on the <b><?echo($brand." ".$modnum);?></b>, this information has been e-mailed to 
the webmaster of <b>www.iatraining.net</b>. Check back soon to see if the new information you submitted has be posted to
this website. If you still need to locate information about this modem, please see the <a href="?content=./library/main.php">PDF Library</a>.
<br>
<br>
<b>Thank you for your input.</b><br>
<a href="mailto:cbarnette@infoave.net">webmaster</a>
</td>
</tr>
</table>
 <p align="center">
<input type="button" value="Back to Main Menu" onclick="location.href='?content=./broadband/main.php'">
</p>