<!-- Start of Entry  / Date: September 10,2003 [ Wednesday ] by Clayton Barnette -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 10,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 2:19:38 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Clayton Barnette</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:cbarnette@infoave.net">cbarnette@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Winco</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">D-Link</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">DSL-302G Combo</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2"><a href="http://www.dlink.com/products/?pid=67" target="_blank">The DSL-302G</a> uses ADSL (Asymmetric Digital Subscriber Line) technology to bring you Internet connection speeds up to 150 times faster than a 56K analog modem over a standard phone line, and two computers simultaneously can connect to the DSL-302G through its USB and Ethernet port to share its high-speed Internet connection.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">We do not escalate Winco DSL at this time, but need to list this modem to provide basic troubleshooting.</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 10,2003 [ Wednesday ] by Clayton Barnette -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by David Love -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 9:43:13 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">David Love</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:dalove@infoave.net">dalove@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">ATMC</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">speedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">5667</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">typical</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">none</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by David Love -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Michael Lambert Sr -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 11:54:46 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Michael Lambert Sr</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:mlambert@infoave.net">mlambert@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Pond Branch communications</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Next Level </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">e1014</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">black in color.. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Michael Lambert Sr -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 10:23:32 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net@infoave.net">bryantp@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">PTMC</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">DLINK </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">DCM200 DSL Modem</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Unsure about connection type but customer made no mention of needing PPPoE setup.





 

</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">Quick Install - http://tsd.dlink.com.tw/info.nsf/0/9cdef77c324dc35548256a54000b174d/$FILE/DCM-200_QIG_052101.PDF



LEDs

What are the Status and Link lights on the DCM-100 and DCM-200 modems supposed to look like when connecting my computer/network correctly to my ISP? 



 The two LED�s work as follows:



Initially, the Status light will blink fast and slow, as the modem tries to connect to the ISP. The LED should then become solid, once connected. You will then plug your Ethernet/USB cable into the modem, whichever is applicable.



The Link light should then become solid for Ethernet port connections. In some instances, the Link light will blink steadily. This condition is normal and is caused by broadcast packets sent from the ISP. This will not affect the operation or performance of the modem. For USB port connections, the Link LED will not light.



This really seems like it should be a plug-n-play modem.</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 11:09:47 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net@infoave.net">bryantp@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">PTMC</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">DLINK</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">DLINK 200 DCM200</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Another Manual although the modem we dealt with had 2 lights not 4? 

http://media-server.amazon.com/media/mole/MANUAL000011456.pdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 11:16:17 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Wilkes GA</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Paradyne</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">6350</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2"><a href="http://www.paradyne.com/technical_manuals/6350-A2-GN12-00.pdf" target="_blank">technical_manuals</a>



This seems to be the correct manual as the LEDs match up.  Thanks</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!--
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 11:27:45 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Unsure</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Alcatel Speedtouch</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">USB MODEL MODEM</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">
<a href="http://snappydsl.net/Snappyweb1/Support/Support_Technical_DSL_ModemSetup.asp?Level=1&fname=SpeedTough_USB_Light_Status" target="_blank">LEDs</a>
<br>
<a href="http://snappydsl.net/Snappyweb1/Support/Support_Technical_DSL_ModemSetup.asp?Level=1&fname=Alcatel_SpeedTouch_USB_Modem_98ME" target="_blank">USB install on 9x/Me</a>
<br>
</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">
<a href="http://home.cwjamaica.com/pdf/dsl/CWJ%20Alcatel%20SpeedTouch%20USB%20Mac%209.pdf" target="_blank">Alcatel USB MODEL on MAC</a>
<br>
<a href="http://home.cwjamaica.com/pdf/dsl/CWJ%20Alcatel%20SpeedTouch%20Pro%20Windows%202000.pdf" target="_blank">ALcatel SPeed Touch Pro model (shows config page)</a>
</td></tr>
</table>
</td>
</tr>
commented out because of  lack of provider to apply to. -->
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 11,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 11:39:46 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">TWLAKES</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Zyxel </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">P600 Series</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">

<a href="http://snappydsl.net/Snappyweb1/Support/Support_Technical_DSL_ModemSetup.asp?Level=1&fname=Other_Modem_Zyzel._Modem_Light_Status" target="_blank">ZyXEL Prestige P630 Modem Light Status</a>
<br>
<a href="http://snappydsl.net/Snappyweb1/Support/Support_Technical_DSL_ModemSetup.asp?Level=1&fname=Other_Modem_Zyzel_Router_Light_Status" target="_blank">ZyXEL Prestige P641 Router Light Status</a>
<br>
<a href="http://snappydsl.net/Snappyweb1/Support/Support_Technical_DSL_ModemSetup.asp?Level=1&fname=Zyxel_Router_641_Simple" target="_blank">Setup</a> 
</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED(all modems preconfigured, only power cycle needed.)</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 11,2003 [ Thursday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 12,2003 [ Friday ] by Michael Lambert Sr -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 12,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:30:25 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Michael Lambert Sr</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:mlambert@infoave.net">mlambert@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">prtc</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Efficient networks</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">Speedstream 5400</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">black and silver in color.  4 port, dsl, and usb. 
<a href="http://www.efficient.com/pdf/ss_multiport_sohodslrouters.pdf" target="_blank">SpeedStream 5400</a></td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 12,2003 [ Friday ] by Michael Lambert Sr -->
<!-- Start of Entry  / Date: September 12,2003 [ Friday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 12,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 9:16:52 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">TWLAKES</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Zyxel</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">Prestige 645</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">
<a href="http://home.earthlink.net/~greenzonesucks/error629/dslmodems/zyxelprestige645uhp.htm" target="_blank">Prestige 645</a>

<br> 

This model is different in troubleshooting from the model above (645M - UHP/ADSL Modem).  However the LEDs are EXACTLY the same.



Thanks</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 12,2003 [ Friday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 13,2003 [ Saturday ] by Tania Allen -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 13,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:05:30 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Tania Allen</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:tajulian@infoave.net">tajulian@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Bulloch</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Westell WireSpeed</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">C90-36R516-01 </td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Westel Wirespeed C90-36R516-01 - same as Horry</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 13,2003 [ Saturday ] by Tania Allen -->
<!-- Start of Entry  / Date: September 13,2003 [ Saturday ] by Tania Allen -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 13,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:11:42 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Tania Allen</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:tajulian@infoave.net">tajulian@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Planters</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Westel Wirespeed</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2"> B90-200010-04</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Unknown .. similar to the Wirespeed A90</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 13,2003 [ Saturday ] by Tania Allen -->
<!-- Start of Entry  / Date: September 13,2003 [ Saturday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 13,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:46:51 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">3Com / U.S. Robotics</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">CMX - 3Com / U.S. Robotics</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">White and similiar to the HomeConnect model cable modem.  Although the HomeConnect model has only ONE STATUS light
<br>
<a href="http://www.scatv.ne.jp/inet/modem/cmxmodem.html" target="_blank">Images</a>
<br>
<a href="http://home.earthlink.net/~greenzonesucks/error629/cablemodems/3com_cmx.htm" target="_blank">Lights and what they do</a>
<br>
<a href="http://www.astound.net/pdf/3com.pdf" target="_blank">Quick Install Guide</a> 
<br>
</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 13,2003 [ Saturday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 13,2003 [ Saturday ] by david whitten -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 13,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:54:03 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">david whitten</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:dwhitten@infoave.net">dwhitten@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">atlantic telephone</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">vision net</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">201 adsl</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">vision net 201 adsl modem.. lights (from left to right)...pwr, act, data, lnk, lan, ppp...all should be on with the data light flashing...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">i encountered this modem and had a hard time finding anything on it...i did come to the conclusion that it does have a built in router used for the ppp configuration (i think) :)....thankyou </td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 13,2003 [ Saturday ] by david whitten -->
<!-- Start of Entry  / Date: September 15,2003 [ Monday ] by pam gwinn -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 15,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 4:57:06 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">pam gwinn</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:pgwinn@infoave.net">pgwinn@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">home </td>
<tr><td class="infohd">Brand:</td><td class="infohd2">next level </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">e1013a</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">serial31081-2-13-001546  
mac -00.05.d8.09.d90f
Not sure of connection type just know its DSL</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 15,2003 [ Monday ] by pam gwinn -->
<!-- Start of Entry  / Date: September 16,2003 [ Tuesday ] by niya mcclurkin -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 16,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 9:30:12 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">niya mcclurkin</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:nmcclurkin@infoave.net">nmcclurkin@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">homexpressway </td>
<tr><td class="infohd">Brand:</td><td class="infohd2">motorala surfboard 4200 </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">p-n:484840-005-00 </td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">This cable modem have the same lights as the Horry 2way cable modem actually it is the same as the 2way surfboard cable modem. Everything is setup to obtain automatically.  Did the same trouble shooting as the Horry 2way cable modem.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 16,2003 [ Tuesday ] by niya mcclurkin -->
<!-- Start of Entry  / Date: September 16,2003 [ Tuesday ] by Michael Lambert Sr -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 16,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 10:32:32 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Michael Lambert Sr</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:mlambert@infoave.net">mlambert@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Lexington</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">com21 cable modem</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">com21</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">
<pre>
<b>Power - Green LED</b> 

Solid - Power is on 
Off - Power is off 

<b>ST/RF - Green LED</b> 

Solid - Cable modem registered and ready to transfer data 

Flashing (Fast green) - Registration in progress 

Flashing (Slow green) - Downstream RF carrier present and ranging in progress 

Off - No downstream RF carrier present 

<b>Tx - Green LED</b> 

Flashing - User data going through cable modem 

Off - No user data going through cable modem 

<b>Rx - Green LED </b>

Flashing - User data going through cable modem 

Off - No user data going through cable modem 

<b>CD/Link - Green LED</b> 

Solid - Ethernet carrier present 

Off - No Ethernet carrier present 
</pre>
</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>

</table>
</td>
</tr>
<!-- End of Entry / Date: September 16,2003 [ Tuesday ] by Michael Lambert Sr -->
<!-- Start of Entry  / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 17,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 12:03:40 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Ben Lomand</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Efficient NEtworks Speedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">5861</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Unsure</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 17,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 12:05:17 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Ben Lomand</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Efficient Networks Sppedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">4060 </td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline"><a href="http://home.earthlink.net/~greenzonesucks/error629/dslmodems/speedstream4060.htm" target="_blank">SpeedStream 4060</a></td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 17,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:07:11 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">PBT</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Next Level Residential Gateway </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">Residential Gateway </td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">VDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">This is the SAME MODEL that HORRY uses.  And we do ESCALATE PBT</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 17,2003 [ Wednesday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: September 17,2003 [ Wednesday ] by Shifone Jones -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 17,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 2:15:42 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Shifone Jones</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:shifonem@infoave.net">shifonem@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">ATMC</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">visionnet </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">708eu</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">sames lights as the one for wilkes, different setup  modem config page as the one the customer was seeing</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 17,2003 [ Wednesday ] by Shifone Jones -->
<!-- Start of Entry  / Date: September 19,2003 [ Friday ] by Tania Allen -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 19,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 5:42:00 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Tania Allen</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:tajulian@infoave.net">tajulian@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Penn</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Efficient Networks</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">Speedstream 5100</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">??</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 19,2003 [ Friday ] by Tania Allen -->
<!-- Start of Entry  / Date: September 19,2003 [ Friday ] by Nate Delanoy -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 19,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 9:57:26 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Nate Delanoy</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:natedelanoy@infoave.net">natedelanoy@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Citcom</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Zoom</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">X3</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">ADSL modem, has four lights Power (its on the right and is red in color), on the left from top to bottom has Line (Green solid), RXB, and LAN (Green solid). Manual has user set IP address to 10.0.0.7, Subnet of 255.255.255.0, Gateway of 10.0.0.2. When booting up comes up with configuration window asking user to enter a user anme and password (in Manual states user name as default of admin). once I had user enter this info he recieved a dialog box stating that it was writing to flash and that he needed to reboot the PC once user rebooted it restablished the connection.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 19,2003 [ Friday ] by Nate Delanoy -->
<!-- Start of Entry  / Date: September 19,2003 [ Friday ] by shifone jones -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 19,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 1:18:34 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">shifone jones</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:shifonem@infoave.net">shifonem@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">atmc </td>
<tr><td class="infohd">Brand:</td><td class="infohd2">visionet </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">adsl 200es</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">we have no infomation  for the configution of the hardware</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 19,2003 [ Friday ] by shifone jones -->

<!-- Start of Entry  / Date: September 23,2003 [ Tuesday ] by niya mcclurkin  -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 23,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 10:44:01 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">niya mcclurkin </td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:nmcclurkin@infoave.net">nmcclurkin@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">casscom</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">motorala surfboard </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">4101</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">it has the same lights as  do the Horry 2way cable modem. It is the same as the Horry 2 way cabla modem, the motorala 4101 surf board..</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 23,2003 [ Tuesday ] by niya mcclurkin  -->
<!-- Start of Entry  / Date: September 23,2003 [ Tuesday ] by shifone -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 23,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:10:57 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">shifone</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:shifonem@infoave.net">shifonem@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">atmc</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">visionet</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">200 es</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Modem config page , under operation mode, categories that he had that I could not assist with because I did not  have the info on the screen shots were,  bridged, ip address subnet and wan gateway</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 23,2003 [ Tuesday ] by shifone -->
<!-- Start of Entry  / Date: September 24,2003 [ Wednesday ] by David Love -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 24,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:33:46 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">David Love</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:davidlove@infoave.net">davidlove@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Winco</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Elastic Networks</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">doesn\'t know and couldn\'t find it on the modem</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">it is dsl -- don\'t know what kind.  Has 4 lights on it with pictures for labels.  This sounds familiar.  I think one of the 2 left ones is power and one of the 2 right ones is activity or something like that. I think we have screen shots of it somewhere, but I can\'t find them.  Customer was assigned ip address of 64.53.37.82 and we were able to release and renew.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 24,2003 [ Wednesday ] by David Love -->
<!-- Start of Entry  / Date: September 25,2003 [ Thursday ] by W J Clark -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 25,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 1:31:09 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">W J Clark</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:wjclark@infoave.net">wjclark@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Hargray</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Elastic networks</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">01-20037-01</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline"> Could not find elastic modems on the hargray page...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 25,2003 [ Thursday ] by W J Clark -->
<!-- Start of Entry  / Date: September 26,2003 [ Friday ] by Tim Blake -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 26,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 1:31:30 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Tim Blake</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:tblake_37@infoave.net">tblake_37@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">hargray</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">arries </td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">cm300</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">What operating systems are supported for USB?

The only operating systems that currently support USB networking are Windows 98SE, Windows 2000, and Windows ME and Windows XP. Operating systems not supporting USB Networking include:



Windows 98 First Edition 

Windows 95 or any earlier version of Microsoft Windows or DOS OS\' 

Macintosh 

Unix/Linux 

Can I connect 1 PC with USB and 1 PC with Ethernet?

Yes, provided the USB drivers are installed on the machine using USB.



Is it better for me to connect with USB or with the Ethernet?

USB is a shared connection. The Ethernet is not shared and is generally a faster connection.



</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">http://www.arrisi.com/consumer_products/faqs/index.asp#15</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 26,2003 [ Friday ] by Tim Blake -->
<!-- Start of Entry  / Date: September 28,2003 [ Sunday ] by Valerie New -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 28,2003 [ Sunday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 12:22:54 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Valerie New</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:vnew@infoave.net">vnew@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">hargray</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">arris</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">cm300a</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">need screen shots of the arris modem and what the lights are suppose to do </td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 28,2003 [ Sunday ] by Valerie New -->
<!-- Start of Entry  / Date: September 30,2003 [ Tuesday ] by WJ Clark -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">September 30,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 8:53:24 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">WJ Clark</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:wjclark@infoave.net">wjclark@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Lexington</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">turbo connect</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">cp2000(is the Com21 Comport Modem)</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2"> Does not dial in...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 30,2003 [ Tuesday ] by WJ Clark -->
<!-- Start of Entry  / Date: September 30,2003 [ Tuesday ] by Mike Lambert -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">September 30,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 2:34:10 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Mike Lambert</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:mlambert1@infoave.net">mlambert1@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">citizens Communications Systems NC</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Cayman</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">3220-h-w11</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Product Description  Netopia Cayman 3220-H - router

 

Form Factor External

 

Features DHCP support, NAT support, ARP support

 

Dimensions (WxDxH) 9 in x 8 in x 1.7 in

 

Weight 2.4 lbs

 

System Requirements UNIX, Microsoft Windows 95/98, Apple MacOS, Linux, Microsoft Windows 2000, Microsoft Windows NT, Microsoft Windows Millennium Edition, Microsoft Windows XP

 

Device Type Router

 

Data Transfer Rate 10 Mbps

 

Data Link Protocol Ethernet, ATM

 

Compliant Standards IEEE 802.3 , ITU G.992.1 (G.DMT), ITU G.992.2 (G.Lite)

 

Network / Transport Protocol TCP/IP, DLC/LLC, ICMP/IP, IPSec, PPPoE, PPPoA

 

Remote Management Protocol SNMP, Telnet, HTTP

 

Digital Signaling Protocol ADSL

 

Routing Protocol RIP-1, RIP-2

 





http://www.netopia.com/equipment/pdf/spec/3220_ds.pdf



http://www.netopia.com/equipment/pdf/cayman/3220hw_80211_qs.pdf



</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: September 30,2003 [ Tuesday ] by Mike Lambert -->
<!-- Start of Entry  / Date: October 1,2003 [ Wednesday ] by Joey -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 1,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 5:24:15 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Joey</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">lexcominc</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">westel wirespeed</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">c90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">westel wirespeed model # c90</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 1,2003 [ Wednesday ] by Joey -->
<!-- Start of Entry  / Date: October 1,2003 [ Wednesday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 1,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 5:45:47 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">LEXCOMM</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">COM21</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">unsure</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Anyway we can update the troubleshooting section to reflect the LEDs.  The LEDs are NOT always green in color.



Thanks



http://support.gci.net/kb/kb39</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">We have gotten several calls where the modem reflects amber LEDs and the techs have been confused on the issue.



Thanks</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 1,2003 [ Wednesday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: October 1,2003 [ Wednesday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 1,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 6:30:45 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Lexcom </td>
<tr><td class="infohd">Brand:</td><td class="infohd2">COM21</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">unsure</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">http://home.gci.net/~help/cable.htm



more information where it comments about the AMBER of LEDs ...  IN this scenario the ST is the RF and CD is the link 



or here ... 

http://home.earthlink.net/~biaachmonkie/error629/cablemodems/com21_2000.htm</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">I aprpeicate it </td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 1,2003 [ Wednesday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: October 2,2003 [ Thursday ] by W J Clark -->
<!--
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 2,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:06:32 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">W J Clark</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:wjclark@infoave.net">wjclark@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">ComCast</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Westell</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">a-90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline"> 6 lights on front of modem...sync enet1 enet 2 enet 3 enet 4 and usb lights...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
-->
<!-- End of Entry / Date: October 2,2003 [ Thursday ] by W J Clark -->
<!-- Start of Entry  / Date: October 2,2003 [ Thursday ] by patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table width="100%">
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 2,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:25:10 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">PBT</td>
<tr><td class="infohd">Brand:</td><td class="infoline">NETOPIA</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">CAYMAN 3341 MODEL</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.netopia.com/equipment/pdf/manuals/3341ENT_qsg.pdf



It does have a web based configuration page:

192.168.1.254</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 2,2003 [ Thursday ] by patrick Bryant -->
<!-- Start of Entry  / Date: October 6,2003 [ Monday ] by Jessica Terenzi -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 6,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:44:52 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Jessica Terenzi</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jterenzi@infoave.net@infoave.net">jterenzi@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">planters rural</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westel </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">2000</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">white modem sits horizontial, very similar to the modem used by horry except the lights are Power, Ready, Link, and Activity...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 6,2003 [ Monday ] by Jessica Terenzi -->
<!-- Start of Entry  / Date: October 7,2003 [ Tuesday ] by Carmen Ragsdale -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 7,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:27:08 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Carmen Ragsdale</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:cragsdale@infoave.net">cragsdale@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Cass Comm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Motorala</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">SB5100</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">This is the modem my customer had...it is black...here is a link with the modem 



http://broadband.motorola.com/consumers/products/sb5100/default.asp

</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 7,2003 [ Tuesday ] by Carmen Ragsdale -->
<!-- Start of Entry  / Date: October 7,2003 [ Tuesday ] by Carmen Ragsdale -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 7,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 9:39:39 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Carmen Ragsdale</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:cragsdale@infoave.net">cragsdale@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Wilkes GA</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">paradyne reach dsl</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">?</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Dont have model number..she only has 3 lights on her modem-> pwr, line, and txrx are all on solid...txrx flashes at times...It is similiar the modem that hargray has..</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 7,2003 [ Tuesday ] by Carmen Ragsdale -->
<!-- Start of Entry  / Date: October 7,2003 [ Tuesday ] by Pete Ho -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 7,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:44:18 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Pete Ho</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:peteho@infoave.net">peteho@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">PBTComm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Netopia</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">Cayman 3300 Series</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">The Netopia Cayman 3300 series ADSL can be setup either though bridged or router mode.  The lights on the one the customer I spoke to were as follows:



  1) Ethernet link (should be solid green when connected)



  2) Ethernet Traffic (flashes green w/ activity on the LAN)



  3) DSL Traffic (flashes green w/ activity over the WAN)



  4) DSL Sync (blinks when training or line disconnected.  sold when the box is trained)



  5) PPPoE Active (solid when PPPoE has been negotiated.  Otherwise, not lit)



  6) Power (solid green w/ power)</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">I do not know if Rob submitted this or not, so I am submitting information on this DSL per Patrick.



He sent me several links on the DSL.  The best one is : 

 http://www.netopia.com/equipment/pdf/manuals/3300_userguide_v72.pdf



Others follow:



http://www.netopia.com/equipment/pdf/spec/3300ent.pdf

http://www.netopia.com/equipment/products/cayman/3000/3300.html

http://www.dsl-warehouse.co.uk/support_files/Datasheets/cayman3341_ds.pdf



All links were provided by patrick.



The user guide (first link) was the longest doc he sent a link for, but includes screenshots on the DSL setup pages as well as trouble shooting info.



The DSL setup is accessed via http, at the address 192.168.1.254.  It should require a username//password to access.  For admin access, you use the username admin.  The password for that could be anything, and is probably setup by PBT.  With the customer we were working on, the password admin let us in the admin account.



From there, you should get to a webpage as shown on page 27 of the userguide pdf.



Of particular note is the Status field.  The user guide says it will say either \"up\" when the DSL is synced and PPPoE was established or \"down\" when the DSL cannot establish the connection.



The customer I talked to, had \"not connected\" in the status field.  On a brief lookover on the user manual, i did not see a reset function, but I did manage to get the customer connected by clicking the \"manage my accounts\" link upon the left.  Customer entered username, password and confirm password fields, and hit submit.  From there it took him to the netopia.com website.  He could get webpages from there.</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 7,2003 [ Tuesday ] by Pete Ho -->
<!-- Start of Entry  / Date: October 8,2003 [ Wednesday ] by jonetta -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 8,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 6:30:56 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">jonetta</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:jonetta@infoave.net">jonetta@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">wilkesga</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">paradyne dsl modem</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">6350-a4-200</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">black w/ power, ethernet, line and tx/rx lights on it. All green. Tx/Rx... flashes.

  This is all the customer could tell me about the modem. 

  Wasn\'t connecting. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 8,2003 [ Wednesday ] by jonetta -->
<!-- Start of Entry  / Date: October 9,2003 [ Thursday ] by Bryan l Gaither -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 9,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 1:47:40 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Bryan l Gaither</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:Brgaither@infoave.net">Brgaither@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">PBT</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Netopia Cayman</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">3300</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Information is list under another ISP.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 9,2003 [ Thursday ] by Bryan l Gaither -->
<!-- Start of Entry  / Date: October 9,2003 [ Thursday ] by Damon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 9,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 10:31:13 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Damon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:damonn@infoave.net">damonn@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">3com</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2"> us robotics cmx </td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">has a section for the pc and a section for the cable, both with connection and activity lights...power cycle fixed it...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">3com us robotics cmx cable modem. product # 3cr292b-de56 on the 3com web site, modem # 2940. closest thing i could find on their site to it was 3CR292-DE56, discontinued, user guide at http://support.3com.com/infodeli/tools/cable/1969-00.pdf and quick install at http://support.3com.com/infodeli/tools/cable/1970-00.pdf , main page for modem at http://www.3com.com/products/en_US/detail.jsp?tab=support&pathtype=support&sku=3CR292-DE56 , also the pdf files (as well as a few others) are stored at ftp://work:infoave@ftp.mr-wolfie.com</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 9,2003 [ Thursday ] by Damon -->
<!-- Start of Entry  / Date: October 10,2003 [ Friday ] by Mike Paul -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 10,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 3:52:24 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Mike Paul</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:mipaul@infoave.net">mipaul@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Penn</td>
<tr><td class="infohd">Brand:</td><td class="infoline">best data</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">smart one DSL 800R</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">power, link (ethernet), link (ADSL) solid

recv/transmit/act flash intermittently</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">

has built-in router (uses 192.168.1.x IP)

</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 10,2003 [ Friday ] by Mike Paul -->
<!-- Start of Entry  / Date: October 10,2003 [ Friday ] by Damon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 10,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:35:41 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Damon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:damonn@infoave.net">damonn@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">north penn access</td>
<tr><td class="infohd">Brand:</td><td class="infoline">best data</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">smart one dsl800r</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">has built in router, gateway of 192.168.1.1, which i couldnt get to pull up. be sure to read all the web pages as some of them are kind of confusing. in this situation we can ping ip, but not domain, and cant pull the router page...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">http://www.bestdata.com/tech/DSL800rconfig.htm

http://www.bestdata.com/tech/dsl800rdocumentation.htm

http://www.bestdata.com/tech/DSL800rtrouble.htm

http://www.bestdata.com/tech/DSL800rinitialtrouble.htm

http://www.bestdata.com/tech/DSL800rfaqreq.htm

http://www.bestdata.com/tech/DSL%20Router%20Commander%20Software%20Manual.pdf

http://www.bestdata.com/tech/DSL800RTechnical%20Manual.pdf

http://www.bestdata.com/tech/ATMOS%20Console%20Commands%20Reference%20Manual.pdf

</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 10,2003 [ Friday ] by Damon -->
<!-- Start of Entry  / Date: October 10,2003 [ Friday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 10,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:44:00 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Penn</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Efficient Networks Sppedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">5861(5800 Series) </td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.zen13155.zen.co.uk/en5861-usercli.pdf

http://www.longshome.com/router_notes.htm

http://support.efficient.com/drivers/drivers/pdfs/5800.pdf



This is apparently a new modem for North Penn.  The PDF above does reflect the correct information.  The below PDF we could not verify as the correct CONFIG PAGE but the same IP (192.168.254.254) did show as CUSTOMER\'s gateway.



http://www.nemontel.net/Williston/Speedstream_5861.htm</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">Additional links

http://www.stanq.com/flowpoint.pdf

</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 10,2003 [ Friday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: October 10,2003 [ Friday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 10,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:45:17 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryantp@infoave.net">bryantp@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Penn</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Best Data </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">800 Series</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">main Link: http://www.bestdata.com/DSL800EU.htm



 the links for the modem: http://www.bestdata.com/tech/DSL800rconfig.htm

http://www.bestdata.com/tech/dsl800rdocumentation.htm

http://www.bestdata.com/tech/DSL800rtrouble.htm

http://www.bestdata.com/tech/DSL800rinitialtrouble.htm

http://www.bestdata.com/tech/DSL800rfaqreq.htm



-----------------------------------------------------------------------------------------------------------------------



Note 2                        DAMONN                        10/10/2003 22:35:34           

http://www.bestdata.com/tech/DSL%20Router%20Commander%20Software%20Manual.pdf

http://www.bestdata.com/tech/DSL800RTechnical%20Manual.pdf

http://www.bestdata.com/tech/ATMOS%20Console%20Commands%20Reference%20Manual.pdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 10,2003 [ Friday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: October 11,2003 [ Saturday ] by joey -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 11,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:11:49 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">joey</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">northpennaccess.net</td>
<tr><td class="infohd">Brand:</td><td class="infoline">smart 1 data dsl</td>
<tr><td class="infohd">Model Number:</td><td class="infoline"> model 800r series</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">customer called in using 1 of these modems that were not listed under northpenn</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 11,2003 [ Saturday ] by joey -->
<!-- Start of Entry  / Date: October 12,2003 [ Sunday ] by Dan Thornton -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 12,2003 [ Sunday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 11:09:14 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Dan Thornton</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:dthornton@infoave.net">dthornton@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Wilkes</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Paradyne</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">Hot Reach</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">Wilkes customer has Paradyne modem.  No Paradyne modem listed on the Wilkes DSL support.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 12,2003 [ Sunday ] by Dan Thornton -->
<!-- Start of Entry  / Date: October 13,2003 [ Monday ] by shifone moore -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 13,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:50:43 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">shifone moore</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:shifonem@infoave.net">shifonem@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">winco</td>
<tr><td class="infohd">Brand:</td><td class="infoline">elastic network  </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">stormport 400-1</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">new modem customer just got from winco, NO INFO ON winco site.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 13,2003 [ Monday ] by shifone moore -->
<!-- Start of Entry  / Date: October 13,2003 [ Monday ] by david whitten -->
<!--
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 13,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:57:58 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">david whitten</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:dwhitten@infoave.net">dwhitten@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">ptmc</td>
<tr><td class="infohd">Brand:</td><td class="infoline">d-link</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">dsl 302g</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">two tone grey standing modem...lights are listed vertically with spaces in between the lights...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">i had one of these modems today... i did download the manual from http://www.dlink.com/products/support.asp?pid=67 but it really didnt help... the lights for the modem are pwr, status, adsl, ethernet, usb, link, and act....this modem was provided by ptmc...</td></tr>
</table>
</td>
</tr>
-->
<!-- End of Entry / Date: October 13,2003 [ Monday ] by david whitten -->
<!-- Start of Entry  / Date: October 14,2003 [ Tuesday ] by rob canipe -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 14,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:10:16 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">rob canipe</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:rcanipe@infoave.net@infoave.net">rcanipe@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">b90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">this is the same as Horry i assume... it is set to use DHCP for the customer i had... </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 14,2003 [ Tuesday ] by rob canipe -->
<!-- Start of Entry  / Date: October 14,2003 [ Tuesday ] by robert canipe -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 14,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:17:26 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">robert canipe</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:rcanipe@infoave.net@infoave.net">rcanipe@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">htc</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">b90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">just like the other one i sent you from casscomm but this customer tells me she has a connection she has to use to get online</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 14,2003 [ Tuesday ] by robert canipe -->
<!-- Start of Entry  / Date: October 14,2003 [ Tuesday ] by joey -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 14,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:44:18 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">joey</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">graceba</td>
<tr><td class="infohd">Brand:</td><td class="infoline">blonder tongue data xpress  modem</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">customer couldn\'t find model #</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.blondertongue.com/media/pdfs/catalog_classes/data.pdf



</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 14,2003 [ Tuesday ] by joey -->
<!-- Start of Entry  / Date: October 15,2003 [ Wednesday ] by Robert Canipe -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 15,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 5:08:26 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">Robert Canipe</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:rcanipe@infoave.net@infoave.net">rcanipe@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">bulloch</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">westell wirespeed</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">c90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">same as the Horry one as far as i can tell</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">None Provided</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 15,2003 [ Wednesday ] by Robert Canipe -->
<!-- Start of Entry  / Date: October 15,2003 [ Wednesday ] by Ben Pryce -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 15,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 6:39:02 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Ben Pryce</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bpryce@infoave.net">bpryce@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Twin lakes</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Aztec</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">305eu</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Customer says he got it from twlakes.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 15,2003 [ Wednesday ] by Ben Pryce -->
<!-- Start of Entry  / Date: October 15,2003 [ Wednesday ] by William Gordon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 15,2003 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:32:50 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">William Gordon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:gordonb@infoave.net@infoave.net">gordonb@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">casscomm.com</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Surf Board </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sb5100</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">has these lights ...power, recieve,send,online,PC,standby  ...its a black modem ..this one was connected through USB ...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 15,2003 [ Wednesday ] by William Gordon -->
<!-- Start of Entry  / Date: October 17,2003 [ Friday ] by David Love -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 17,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 1:07:34 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">David Love</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:davidlove@infoave.net">davidlove@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Wilkes (nu-z)</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">Paradyne</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">Reachdsl 6350-a4-200</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">This is a dsl modem -- it is black -- the link I will list is not for the same exact model, but the lights were correct</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">http://www.paradyne.com/technical_manuals/6350-A2-GN10-10.pdf</td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 17,2003 [ Friday ] by David Love -->
<!-- Start of Entry  / Date: October 17,2003 [ Friday ] by Mike Lambert -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 17,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 2:48:47 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Mike Lambert</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:mlambert@infoave.net">mlambert@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">united</td>
<tr><td class="infohd">Brand:</td><td class="infoline">next level</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">e1015a</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">no setup need for xp.. set ip to obtain and added dns numbers.. unable to find any information on this modem.. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 17,2003 [ Friday ] by Mike Lambert -->
<!-- Start of Entry  / Date: October 17,2003 [ Friday ] by Ben Pryce -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 17,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 3:15:43 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Ben Pryce</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bpryce@infoave.net">bpryce@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Twin Lakes</td>
<tr><td class="infohd">Brand:</td><td class="infoline">D-link </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">302g</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.dlink.com/products/?pid=67</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 17,2003 [ Friday ] by Ben Pryce -->
<!-- Start of Entry  / Date: October 17,2003 [ Friday ] by Courtney Wilkins -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 17,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:01:15 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Courtney Wilkins</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:cgwilkins@infoave.net">cgwilkins@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">PBTcomm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">netopia </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">cayman 3341</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.netopia.com/equipment/pdf/manuals/3341ENT_qsg.pdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 17,2003 [ Friday ] by Courtney Wilkins -->
<!-- Start of Entry  / Date: October 17,2003 [ Friday ] by Dan Mc Mahon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 17,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:36:36 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Dan Mc Mahon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:damcmahon@infoave.net">damcmahon@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">planters.net</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell wirespeed</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">b90-200010-04</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">this particular customer has a static IP address</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 17,2003 [ Friday ] by Dan Mc Mahon -->
<!-- Start of Entry  / Date: October 18,2003 [ Saturday ] by Tania Allen -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 18,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 12:27:05 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Tania Allen</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:tajulian@infoave.net">tajulian@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Loretto</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Scientific Atlanta</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">Webstar DPX 100 </td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">4 lights (Power, PC, Data and Cable), Vertical mount, modem is white. Can connect with Ethernet or USB.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">http://www.scientificatlanta.com/customers/Source/749919.pdf</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 18,2003 [ Saturday ] by Tania Allen -->
<!-- Start of Entry  / Date: October 20,2003 [ Monday ] by W J Clark -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 20,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 11:19:17 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">W J Clark</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:wjclark@infoave.net">wjclark@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Hargray</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Elastic networks</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">01-20037-01 Rev D</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline"> Has a total of 4 lights across fornt of modem...</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 20,2003 [ Monday ] by W J Clark -->
<!-- Start of Entry  / Date: October 20,2003 [ Monday ] by Patrick Bryant -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 20,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 4:47:43 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Patrick Bryant</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bryant@infoave.net">bryant@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Twin Lakes</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Broadmax </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">300</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">SDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://broadmax.net/support/download/HSA300A_UserManual_rev14.pdf



This link provides front panel and rear panel screenshots with descriptions.  Additionally, it shows the COFNIGRUATION page with a walkthrough. 



Thanks</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 20,2003 [ Monday ] by Patrick Bryant -->
<!-- Start of Entry  / Date: October 20,2003 [ Monday ] by robert canipe -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 20,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:01:53 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">robert canipe</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:rcanipe@infoave.net">rcanipe@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">enhanced telecommunications</td>
<tr><td class="infohd">Brand:</td><td class="infoline">unknown</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">unknown</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">they have a DSL modem... customer told me there was no name or model #... we need to find out what they have.... ty</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 20,2003 [ Monday ] by robert canipe -->
<!-- Start of Entry  / Date: October 21,2003 [ Tuesday ] by Robert Canipe -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 21,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 5:03:00 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Robert Canipe</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:rcanipe@infoave.net">rcanipe@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">united</td>
<tr><td class="infohd">Brand:</td><td class="infoline">zyxel </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">645 adsl modem</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.mclink.it/risorse/hardware/zyxel645userguide.pdf



here is the link</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 21,2003 [ Tuesday ] by Robert Canipe -->
<!-- Start of Entry  / Date: October 21,2003 [ Tuesday ] by William Gordon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 21,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 6:51:16 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">William Gordon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:gordonb@infoave.net">gordonb@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Hargray</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Elastics network</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">part# 02-00037-01 serial#M1023000-ba-d-400  02-00037-02</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">lightning bolt= power

monitor= unknown

fork with arrows=unknown

2 paralell arrows pointing in both directions=Tfer

....has it setup for broadband .through an Ethernet without a rouder</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 21,2003 [ Tuesday ] by William Gordon -->
<!-- Start of Entry  / Date: October 21,2003 [ Tuesday ] by jessica terenzi -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infohd2">October 21,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infohd2"> 10:21:27 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infohd2">jessica terenzi</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infohd2"><a href="mailto:jterenzi@infoave.net@infoave.net">jterenzi@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infohd2">Hargray</td>
<tr><td class="infohd">Brand:</td><td class="infohd2">ARIS</td>
<tr><td class="infohd">Model Number:</td><td class="infohd2">cm300</td>
<tr><td class="infohd">Conn. Type:</td><td class="infohd2">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infohd2">black horizontal modem. 5 lights E-NET, USB, CABLE, ONLINE, POWER.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infohd2">normal troubleshooting, does have a stanby button on the back. Also needs software if connected with a USB cable. </td></tr>
<tr><td class="infohd">Status:</td><td class="infohd2">POSTED</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 21,2003 [ Tuesday ] by jessica terenzi -->
<!-- Start of Entry  / Date: October 21,2003 [ Tuesday ] by jessica terenzi -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 21,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:22:56 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">jessica terenzi</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jterenzi@infoave.net@infoave.net">jterenzi@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Lexcom</td>
<tr><td class="infohd">Brand:</td><td class="infoline">broadmax</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">HSA 300A</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">VDSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sme as what tri-county has. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 21,2003 [ Tuesday ] by jessica terenzi -->
<!-- Start of Entry  / Date: October 23,2003 [ Thursday ] by joey cartier -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 23,2003 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:47:37 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">joey cartier</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">ericcson pipe writer</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">#hm200-c</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline"> never heard of this modem b4 customer says he got it from casscomm.....</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 23,2003 [ Thursday ] by joey cartier -->
<!-- Start of Entry  / Date: October 24,2003 [ Friday ] by Michele Minor -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 24,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:35:13 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Michele Minor</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:minorm@infoave.net">minorm@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Citcom</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Speedstram</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">5861</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">The power and the link lights are normally on.  The test light normally blinks occasionally.   The wan, lant and a lanr light that is on when there is activity going on in the modem.  The modem is a router with an ethernet connection.  There is a modem config page, it is 192.168.254.254.  This allows you to enter in the user/pass and to see the ip address of the nic card.     </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 24,2003 [ Friday ] by Michele Minor -->
<!-- Start of Entry  / Date: October 24,2003 [ Friday ] by Ben Pryce -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 24,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:50:07 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Ben Pryce</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bpryce@infoave.net">bpryce@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Pen</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Best Data </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">800r</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Was in dsl coudlnt do much cause her adsl link was off.  Here is a good site me and pat found.

http://howto.gmavt.net/DSL/Modems/</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 24,2003 [ Friday ] by Ben Pryce -->
<!-- Start of Entry  / Date: October 27,2003 [ Monday ] by joey cartier -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 27,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 6:19:41 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">joey cartier</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">chesnet.net</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell wirespeed</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">#c90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">westell wirespeed c90 same as horry </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 27,2003 [ Monday ] by joey cartier -->
<!-- Start of Entry  / Date: October 28,2003 [ Tuesday ] by joey cartier -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">October 28,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 4:11:14 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">joey cartier</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">horry</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell wire speed</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">B90</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">were have tha model #A,C but not B</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: October 28,2003 [ Tuesday ] by joey cartier -->
<!-- Start of Entry  / Date: November 3,2003 [ Monday ] by Travis Wilson -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 3,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:53:47 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Travis Wilson</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:Traviswilson@infoave.net">Traviswilson@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">MadisonRiver Group</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Speedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">5200</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Madisonriver\'s Speedstream 5200 can be setup in either Brigded mode or Router - if set to bridge they cannot access the http://192.168.254.254 page and the only troubleshooting we should do is set to auto obtain and powercycle - The only way we can determine which setup is being used is by going to the Remedy page, checking the \"Connection Type\" and seeing if it is set to \"PPPOE\" or \"RBE\"(bridge mode, cannot access page) - HOWEVER it does not always show which type, in fact it rarely shows it.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 3,2003 [ Monday ] by Travis Wilson -->
<!-- Start of Entry  / Date: November 4,2003 [ Tuesday ] by Jason -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 4,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 2:39:14 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Jason</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jasonaustin@infoave.net">jasonaustin@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">North Penn Access</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Best Data Smart One</td>
<tr><td class="infohd">Model Number:</td><td class="infoline"> 800r</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">The modem has two sides one side has a power light a link light and a activity light. The other side has a link light..a rx and a tx light.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 4,2003 [ Tuesday ] by Jason -->
<!-- Start of Entry  / Date: November 4,2003 [ Tuesday ] by Joey Cartier -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 4,2003 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:36:49 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Joey Cartier</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jcartier@infoave.net">jcartier@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Motorola</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sb 5100</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Motorola sb 5100</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">http://www.wildwestelectronics.net/sb-5100.html</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 4,2003 [ Tuesday ] by Joey Cartier -->
<!-- Start of Entry  / Date: November 8,2003 [ Saturday ] by David Love -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 8,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 12:20:46 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">David Love</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:davidlove@infoave.net">davidlove@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Bulloch</td>
<tr><td class="infohd">Brand:</td><td class="infoline">westell</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">wirespeed</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Just wanted to mention that you might want to remove the pppoe info off the westell screen as this is misleading</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 8,2003 [ Saturday ] by David Love -->
<!-- Start of Entry  / Date: November 8,2003 [ Saturday ] by Daniel Mc Mahon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 8,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 2:21:50 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Daniel Mc Mahon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:damcmahon@infoave.net">damcmahon@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">bulloch</td>
<tr><td class="infohd">Brand:</td><td class="infoline">speedstream</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">5260</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">ADSL modem. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 8,2003 [ Saturday ] by Daniel Mc Mahon -->
<!-- Start of Entry  / Date: November 10,2003 [ Monday ] by Bryan Loeper -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 10,2003 [ Monday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 3:45:54 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Bryan Loeper</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:bloeper@infoave.net">bloeper@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">ATMC</td>
<tr><td class="infohd">Brand:</td><td class="infoline">VisioNnet</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">200ES</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://www.atmc.net/support/dsl/dslinstall.htm

I set up connection using IATraining\'s information, which says that a static IP of 10.0.0.1 should be used w/ this modem for ATMC.  It didn\'t work, so I checked ATMC\'s page, and, per step 4, it should be obtain automatically.  Set customer up this way, and it worked.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 10,2003 [ Monday ] by Bryan Loeper -->
<!-- Start of Entry  / Date: November 14,2003 [ Friday ] by Jessica Terenzi -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 14,2003 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 6:43:44 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Jessica Terenzi</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:jterenzi@infoave.net@infoave.net">jterenzi@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">homexpressway</td>
<tr><td class="infohd">Brand:</td><td class="infoline">nextlevel</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">e1015a</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">black horizontal model with five lights...power, diagnostic, lan link activity, wan link, and act. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 14,2003 [ Friday ] by Jessica Terenzi -->
<!-- Start of Entry  / Date: November 15,2003 [ Saturday ] by Michele Minor -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 15,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:02:59 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Michele Minor</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:minorm@infoave.net">minorm@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Hargray</td>
<tr><td class="infohd">Brand:</td><td class="infoline">CPE</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">020010001</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">The two lights on the dsl modem are the power and the txfr lights and they are normally on. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 15,2003 [ Saturday ] by Michele Minor -->
<!-- Start of Entry  / Date: November 15,2003 [ Saturday ] by Carmen Ragsdale -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 15,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:44:11 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Carmen Ragsdale</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:cragsdale@infoave.net">cragsdale@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Horry</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Westell  wirespeed </td>
<tr><td class="infohd">Model Number:</td><td class="infoline">b90-200010-04</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">This customer had this modem, it seems similiar to the other model on the dsl page</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 15,2003 [ Saturday ] by Carmen Ragsdale -->
<!-- Start of Entry  / Date: November 15,2003 [ Saturday ] by Ed Hubbard -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 15,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 11:26:35 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Ed Hubbard</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:edhub38@infoave.net@infoave.net">edhub38@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Casscomm</td>
<tr><td class="infohd">Brand:</td><td class="infoline">3comm home connect</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">?</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://help.sitestar.net/cable_dsl/10039995.pdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 15,2003 [ Saturday ] by Ed Hubbard -->
<!-- Start of Entry  / Date: November 15,2003 [ Saturday ] by Ed Hubbard -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">November 15,2003 [ Saturday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 12:22:51 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Ed Hubbard</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:edhub38@infoave.net@infoave.net">edhub38@infoave.net@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Graceba</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Gadline</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">?</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">2 way Cable Modem</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">http://helpdesk.powergate.ca/checking_your_cable_modem.htm</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: November 15,2003 [ Saturday ] by Ed Hubbard -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:58:38 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 8:59:59 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:01:12 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:02:42 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:04:44 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:05:09 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:05:40 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:06:15 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:07:08 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:08:24 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:09:42 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:10:30 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:11:27 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">fsdfsdaf</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfsdf">sdfsdf</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Brand:</td><td class="infoline">sdfsdaf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sfsdfsdaf</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdfsadfsdf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by fsdfsdaf -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by Clayton Barnette -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:32:22 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Clayton Barnette</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:claytonbarnette@yahoo.com">claytonbarnette@yahoo.com</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Hardynet</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Paradyne</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">400 and 1020</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">DSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">These modems were posted on the IA Training website. </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by Clayton Barnette -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by Clayton -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:36:00 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Clayton</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfas">sdfas</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">dsfas</td>
<tr><td class="infohd">Brand:</td><td class="infoline">fasdfsadf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sadfasdfa</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdafsafsf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by Clayton -->
<!-- Start of Entry  / Date: March 16,2004 [ Tuesday ] by Clayton -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 16,2004 [ Tuesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 9:37:42 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Clayton</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:sdfas">sdfas</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">dsfas</td>
<tr><td class="infohd">Brand:</td><td class="infoline">fasdfsadf</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">sadfasdfa</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">Unknown</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">sdafsafsf</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 16,2004 [ Tuesday ] by Clayton -->
<!-- Start of Entry  / Date: March 17,2004 [ Wednesday ] by dustin hubbard -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 17,2004 [ Wednesday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:53:13 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">dustin hubbard</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:rdhubbard@infoave.net">rdhubbard@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">planttel</td>
<tr><td class="infohd">Brand:</td><td class="infoline">paradyne reach 6250 hotwire modem</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">6250</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">DSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">i did find out they use static settings.</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 17,2004 [ Wednesday ] by dustin hubbard -->
<!-- Start of Entry  / Date: March 18,2004 [ Thursday ] by Aften Adkins -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 18,2004 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 7:33:05 [ am ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Aften Adkins</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:adkinsa@infoave.net">adkinsa@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Atlantic</td>
<tr><td class="infohd">Brand:</td><td class="infoline">DrayTek</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">Vigor 2500 Modem/Router</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">Lights:
activity
adsl line
adsl data
lan 1, 2, 3, 4

config page 192.168.1.1</td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 18,2004 [ Thursday ] by Aften Adkins -->
<!-- Start of Entry  / Date: March 18,2004 [ Thursday ] by Mike Whited -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 18,2004 [ Thursday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 10:06:36 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Mike Whited</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:mikewhited@infoave.net">mikewhited@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">Atlantic</td>
<tr><td class="infohd">Brand:</td><td class="infoline">Visionnet</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">200es</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">ADSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">\"See Configuring the Visonnet router for detailed instructions.\"

This section is missing.  </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 18,2004 [ Thursday ] by Mike Whited -->
<!-- Start of Entry  / Date: March 19,2004 [ Friday ] by Dan Mc Mahon -->
<tr>
<td class="field" valign=top colspan=2>
<table>
<tr><td class="infohd">Date Submitted:</td><td class="infoline">March 19,2004 [ Friday ]</td></tr>
<tr><td class="infohd">Time Submitted:</td><td class="infoline"> 4:50:23 [ pm ]</td></tr>
<tr><td class="infohd">Submitted by:</td><td class="infoline">Dan Mc Mahon</td></tr>
<tr><td class="infohd">E-Mail:</td><td class="infoline"><a href="mailto:damcmahon@infoave.net">damcmahon@infoave.net</a></td>
<tr><td class="infohd">Provider:</td><td class="infoline">sand hills</td>
<tr><td class="infohd">Brand:</td><td class="infoline">zoom</td>
<tr><td class="infohd">Model Number:</td><td class="infoline">X3 5560</td>
<tr><td class="infohd">Conn. Type:</td><td class="infoline">DSL</td></tr>
<tr><td class="infohd">Description:</td><td class="infoline">it is the same zoom modem that citcom uses as far as i can tell.  </td></tr>
<tr><td class="infohd">Comments:</td><td class="infoline">None Provided</td></tr>
</table>
</td>
</tr>
<!-- End of Entry / Date: March 19,2004 [ Friday ] by Dan Mc Mahon -->
