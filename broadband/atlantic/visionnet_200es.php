<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>VisionNet 200ES ADSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>VisionNet 200ES ADSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The VisionNet Modems are ATMC's newest DSL modems, most new customers will have this modem.
Converting a customers regular phone into a Digital Subscriber Line (DSL) can cause audible noises
(high pitched tones and static) when you try to talk on the phone without a filter. Make sure the 
customer installed line filters on each telephone or device that shares the same phone number as the DSL 
line to eliminate this noise; this include answering machines and fax machines. The filters will enable 
both Internet access and normal phone use at the same time.
<br>
<img src="./images/linefilters.gif">
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_vn200es.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

<p align="center">
<img src="./images/sidepanel_vn200es.gif" border=0>
<table width="500">
<tr><td class="infohd">LEDs</td><td class="infohd">Color</td><td class="infohd">Status</td><td class="infohd">Meaning</td></tr>
<tr><td class="infohd" rowspan=2>ADSL</td><td class="infoline" rowspan=2>Green</td><td class="infoline">Flashing</td><td class="infoline">Trying to connect ADSL Line.<td></tr>
<tr><td class="infoline">Solid</td><td class="infoline">ADSL Line is Linked.</td></tr>
<tr><td class="infohd">Tx/Rx</td><td class="infoline">Green</td><td class="infoline">Flashing</td><td class="infoline">Data is transmitted or received to the ADSL line.</td></tr>
<tr><td class="infohd" rowspan=2>LAN</td><td class="infoline" rowspan=2>Green</td><td class="infoline">Flashing</td><td class="infoline">Data is transmitted or received to the LAN side.</td></tr>
<tr><td class="infoline">Solid</td><td class="infoline">Ethernet interface is active.</td></tr>
<tr><td class="infohd">Activity</td><td class="infoline">Green</td><td class="infoline">Flashing</td><td class="infoline">The device ready to work.</td></tr>
<tr><td class="infohd" rowspan=2>Power</td><td class="infoline" rowspan=2>Red</td><td class="infoline">Solid</td><td class="infoline">Input Power is applied.</td></tr>
<tr><td class="infoline">Off</td><td class="infoline">Power Off.</td></tr>
</table>
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_vn200es.gif" border=0>
</p>
</td>
</tr>

<tr>
<td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/basicsetup_vn200es.gif" border=0>
</p>
<i><b>Note:</b> If the ADSL does not become solid within 2 minutes and you have verified a
secure RJ-11 connection on the LINE port and wall jack, reconnect the ADSL
line to the <b>SWAPPER</b> port.</i>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>When do I need to use the SWAPPER port?</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The <b>LINE</b> port and <b>SWAPPER</b> port cannot be used at the same time. If the <b>ADSL LED</b> does
not become solid in 2 minutes, unplug the RJ-11 from the LINE port and reconnect it to the
<b>SWAPPER</b> port. In most cases, the <b>LINE</b> jack is utilized in typical <b>ADSL</b> connections. In
some areas, this may not be the case, so <b>DQ Technology</b> provides an alternative pin configuration
(the <b>SWAPPER</b> port) in case the standard was changed at some point in your wall jack.
<p align="center">
<img src="./images/swapperport_vn200es.gif" border=0>
</p>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Configuring Your PC to Access the 200ES</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol type="a">
<li>From the <b>Start</b> menu on the tool bar, select <b>Settings</b> > <b>Control Panel</b>, then double click on
the <b>Network icon</b>.</li>
<li>The <b>Network window</b> appears. Select the <b>Configuration</b> tab, scroll to the installed network
component window and select <b>TCP/IP</b> for the NIC (Network Interface Card). Click the
<b>Properties</b> button.</li>
<li>The <b>TCP/IP Properties</b> window will appear. Select the <b>Specify an IP Address</b> option,
then enter the <b>IP Address</b> as <b>10.0.0.1</b> and <b>Subnet Mask</b> as <b>255.0.0.0</b>.</li>
<p>
<img src="./images/network_vn200es.png" border=0>
</p>
<li>
You will be asked to <b>restart</b> your computer to complete the settings. Click <b>Yes</b> to <b>restart</b>
your computer.
<p>
<img src="./images/restart_vn200es.png" border=0>
</p></li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Quick Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>
<p>Start your <b>web browser</b> and then type <b>http://10.0.0.2</b> at web address.</p>
<img src="./images/addressbar.png" border=0> 
<br><br>
</li>
<li>
<p>The Enter Network Password window appears, type <b>"admin"</b> in User Name field and 
<b>"visionnet"</b> in the password field.</p>
<img src="./images/visionnetpass.png" border=0> 
<br><br>
</li>
<li>
<p>If the <b>Quick Start</b> setup page is not automatically opened, click <b>Quick Start</b> link on the left side on <b>200ER�s</b> configuration page.</p>
<img src="./images/quickstart.png" border=0> 
<br><br>
</li>
<li>
 <ol type="a">
 <li>Type the <b>VPI</b> and <b>VCI</b> provided on thier account information card.</li>
 <li>Set the <b>BRIDGE</b> bar to Disabled.</li>
 <li>In the <b>PPP</b> section, type the <b>User Name and Password</b> provided.
 <li>Click <b>Connect</b> to Finish quick setup.</li>
 </ol>
</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Other System Configurations</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>LAN Page</b>
<p>
<ol type="a">
<li>Click the <b>LAN</b> option in the <b>Configuration</b> menu and you will see the following screen.
<p>
<img src="./images/lan_vn200es.png" border=0>
</p>
</li>
<li>The factory <b>default</b> is set as shown on screen.</li>
<li>If you want to customize the setting for the <b>Ethernet interface</b> on the <b>200ES</b>, enter or change
the above information in the LAN windows.</li>
<li>When Setting are completed, click <b>Submit</b>. You can click <b>Reset</b> to bring back the last
setting; this function can only be used before you click Submit.</li>
</ol>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>WAN Page</b>
<p>
<ol type="a">
<li>Click the <b>WAN</b> option in the <b>Configuration</b> menu and you will see the following screen.
<p>
<img src="./images/wan_vn200es.png" border=0>
</p>
</li>
<li>In the <b>ENCAPSULATION</b> bar, you can use the drop-down menu to select the encapsulation
method supplied by your ISP. <b>1483 Bridged IP LLC</b> is the most common and is the <b>default</b>
configuration.</li>
<li>Enter the <b>VPI</b> and <b>VCI</b> (0,35 is most common) provided by your ISP. Please confirm with
your ADSL service provider.</li>
<li>The <b>ADSL</b> connects and communicates with the <b>WAN interface (Internet)</b> via <b>ATM</b>. In the
<b>ATM</b> structure, the ADSL circuit has a <b>VC (Virtual Circuit)</b>. You can set up one or several
Virtual Interfaces to make a connection, communicate individually or simultaneously.
You can configure ATM settings for each virtual circuit which does not affect other virtual
circuits. The screen that appears depends on the operation mode you selected.
In the ATM Interface bar, you can set eight ATM VC�s. Choose Yes to enable your VC
settings.<br>
<i><b>The default is set to 0. Do not set 1~7 unless instructed by your ISP.</b></i></li>
<li>In the Modulation bar, you can use the pull-down menu to select the modulation protocol.
The factory <b>default</b> is set to <b>Autosense-G.dmt</b> first.</li>
<li>When Settings are completed, click <b>Submit</b>. You can click <b>Reset</b> to bring back the last
setting; this function can only be used before you click <b>Submit</b>.</li>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<b>Save Setting Page</b>
<p>
<ol type="a">
<li>Click the <b>Save Setting</b> option in the <b>Configuration</b> menu and you will see the following screen.
<p>
<img src="./images/save_vn200es.png" border=0>
</p>
</li>
<li>Click <b>Submit</b> to save settings.</li>
</p>
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Fast-Track Install Instructions</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<!-- the folloing content came from ATMC http://www.atmc.net/support/sinternet/pop-ups/fastrak-install.htm -->
		<ul>
				<li>
					<b>Step One - Phone Line Filters</b>
					<br>Put phone line filters on every telephone device (phone, fax, etc.) you have plugged into any outlet, except for your ADSL modem. Two filters are provided with your DSL installation. You must have a phone line filter on every telephone device. If you need more filters to accommodate telephone devices, unplug these devices from the outlets until you are able to contact ATMC and purchase more filters. For detailed instructions on filter installation, see <br><a href="http://www.atmc.net/support/sinternet/forms/filterinstall.pdf" target="_blank">DSL Filter Installation Guide*.</a><b>[.pdf]</b>
				</li>
				<li>
					<b>Step Two - Connect the VisionNet DSL Modem</b>
						<ol>
							<li>
							Connect one end of the telephone cable to the modem LINE port and the other end to the telephone outlet. 
							</li>
							<li>
							Connect the RJ45 Ethernet cable to the LAN port of the modem and the other end to the PC�s Ethernet Port NIC Card. 
							</li>
							<li>
							Plug the power adapter cord into the modem's DC IN port and the other end to the main outlet. Turn on the power switch. 
							</li>
						</ol>
					Approximately one minute after connecting the modem, make sure ADSL light is green. If the ADSL light does not turn green there is a problem with the connection. Make sure that you have a line filter on every telephone device and that you do not have a filter on the ADSL modem. In addition, if you have more than one phone number, verify that the ADSL modem is plugged into the correct line.
				</li>
				<li>
				<b>Step Three � Opening Control Panel</b>
				<br>To complete Steps Four and Five, you will first need to access the Control Panel. From the desktop, click the Start button and choose Settings and then choose Control Panel. If you are using Windows XP, click Switch to Classic View on the right side of the screen.
				</li>
				<li>
				<b>Step Four - Configuring Your Network Adapter's TCP/IP Properties</b>
				<br>In the TCP/IP properties of the Network Interface Card (called local area connection in Windows 2000 and XP) network adapter to obtain an IP address automatically. See <a href="javascript:void(window.open('http://www.atmc.net/support/sinternet/pop-ups/adapter-config.htm','','width=760,height=500,scrollbars=yes'))">Configuring Your Network Adapter's TCP/IP Properties</a> for detailed instructions.
				</li>
				<li>
				<b>Step Five� Internet Explorer Setting</b>
				<br>In IE, make support.atmc.net your homepage. Under Connections, verify that dial-up settings is set to never dial a connection and no settings under LAN Settings have a check mark beside them. See <a href="javascript:void(window.open('http://www.atmc.net/support/sinternet/pop-ups/dsl-setting-ie.htm','','width=760,height=500,scrollbars=yes'))">DSL Settings for Internet Explorer</a> for detailed instructions.
				</li>
				<li>
				<b>Step Six - Configuring the 200ER router</b>
				<br>Open the Visionnet 200ER router configuration page by typing http://10.0.0.2 into your web browser's address bar. When the Network Password box appears, type �admin� for the User Name and �visionnet� for the Password. In the Quick Start menu, enter the username and password provided on the Account Information Card. Then, click the Connect button to finish the setup. See Configuring the Visonnet router for detailed instructions.
				</li>
			</ul>
<br>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
Standard DSL Troubleshooting steps apply.
<br>
</td>
</tr>
-->
</table> 
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

