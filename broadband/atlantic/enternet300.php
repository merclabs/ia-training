<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>EnterNet 300 User's Guide</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top>EnterNet 300 User's Guide</td>
</tr>
<tr>
<td class="heading" valign=top><b>Contents</b>&nbsp;&nbsp;&nbsp;&nbsp;<a href="3com_duallink.php"><b>[ Back to 3 Com Dual Link ]</b></a></td>
</tr>
<tr>
<td class="field" valign=top>
<p>
<ul>
  <li><a href="#Introducing EnterNet 300">Introducing EnterNet 300</a></li>
  <li><a href="#System Requirements">System Requirements</a></li>
  <li><a href="#Installing EnterNet 300 for Windows">Installing EnterNet 300 for Windows</a>
   <ul><li><a href="#The EnterNet Installer, Step-by-Step">The EnterNet Installer, Step-by-Step</a>
  		<ul><li><a href="#The &quot;Existing Installation Detected&quot; panel">The &quot;Existing Installation Detected&quot; Panel</a></li>
            <li><a href="#The &quot;Welcome&quot; Panel">The &quot;Welcome&quot; Panel</a></li>
            <li><a href="#The &quot;Software License Agreement&quot; Panel">The &quot;Software License Agreement&quot; Panel</a></li>
            <li><a href="#The &quot;Choose Destination Location&quot; Panel">The &quot;Choose Destination Location&quot; Panel</a></li>
            <li><a href="#The &quot;Choose Folder&quot; Panel">The &quot;Choose Folder&quot; Panel</a></li>
            <li><a href="#The &quot;Setup Complete&quot; Panel">The &quot;Setup Complete&quot; Panel</a>           </li>
        </ul>
      </li>
      <li><a href="#Summary">Summary</a></li>
   </ul>
  </li>
  <li><a href="#Getting Help for EnterNet">Getting Help for EnterNet</a></li>
  <li><a href="#Opening the EnterNet Folder">Opening the EnterNet Folder</a></li>
  <li><a href="#EnterNet Connection Profiles">EnterNet Connection Profiles</a>
   <ul>
      <li><a href="#Creating a Connection Profile">Creating a Connection Profile</a>
	  <ul>
          <li><a href="#Connection Name">Connection Name</a></li>
          <li><a href="#User Name and Password">User Name and Password</a></li>
          <li><a href="#Devices">Devices</a></li>
          <li><a href="#Finishing the Profile">Finishing the Profile</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a href="#Connecting with EnterNet">Connecting with EnterNet</a></li>
  <li><a href="#System Tray Icon">System Tray Icon</a></li>
  <li><a href="#Disconnecting with EnterNet">Disconnecting with EnterNet</a></li>
  <li><a href="#Connection Messages">Connection Messages</a></li>
  <li><a href="#Connection Details">Connection Details</a></li>
  <li><a href="#Troubleshooting EnterNet">Troubleshooting EnterNet</a>    
    <ul>
      <li><a href="#Installation Errors">Installation Errors</a></li>
      <li><a href="#Connection Errors">Connection Errors</a>        
        <ul>
          <li><a href="#PPP Negotiate Timeout">PPP Negotiate Timeout</a></li>
          <li><a href="#Authentication Failed">Authentication Failed</a></li>
          <li><a href="#Connection Cancelled by User">Connection Cancelled by User</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a href="#Uninstalling EnterNet">Uninstalling EnterNet</a></li>
</ul>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="Introducing EnterNet 300"><b>Introducing EnterNet 300</b>
</a></td>
</tr>
<tr>
<td class="field" valign=top><p>
EnterNet 300 for Windows is desktop connectivity software used by the world�s
largest and most reputable ISPs to enable their end-user customers to gain access to their
high-speed Internet access services. World wide, more users rely on EnterNet for high speed
Internet access than any other PPPoE desktop connectivity product.
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="System Requirements"><b>System Requirements</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p><ul>
  <li>
EnterNet 300 for Windows minimum system requirements include:
    
  </li>
  <li>Microsoft Windows 9x, Windows
  NT 4.0, or Windows 2000
    
  </li>
  <li>Pentium� class processor or
  equivalent
    
  </li>
  <li>Recommended minimum of 16MB
  RAM
    
  </li>
  <li>Minimum of 4MB free hard drive
  space
    
  </li>
  <li>Microsoft-compatible Ethernet
  adapter with an NDIS 3.0 or higher compliant driver.
    </li>
</ul></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b>Introduction Menu</b></td>
</tr>
<tr>
<td class="field" valign=top><p>At the introductory menu, choose the "Install EnterNet 300 for Windows" button
</p><p><img src="images/enternet300.gif" width="400" height="313" alt="" border="0"></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Installing EnterNet 300 for Windows">Installing EnterNet 300 for Windows</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>Using ATMC Waverunner DSL high speed
service is very similar in setup to that of Dial Up Networking with Windows.&nbsp;Just as you create a connection profile with Dialup Networking, you will
create a connection profile with the software included on the CD-ROM. Insert the CD-ROM and run the setup.exe program
on the CD.&nbsp;You may choose the default
location of the install, or an alternative location. Once installed, you will be
required to reboot your computer.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="The EnterNet Installer, Step-by-Step"><b>The EnterNet Installer, Step-by-Step</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p>This section will take you step-by-step through all the panels presented to
you by the EnterNet installer.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="The &quot;Existing Installation Detected&quot; panel"><b>The &quot;Existing Installation Detected&quot; panel</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p>If you already have EnterNet 300 installed on your computer, the EnterNet
installer will ask if you would like to upgrade to this version. If you select &quot;Yes&quot;,
EnterNet 300 installation will continue and any pre-existing EnterNet Connection Profiles from the prior installation
will be copied into the new installation. 
</p>
<p><img src="images/enternet300_1.gif" width="474" height="119" alt="" border="0"><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="The &quot;Welcome&quot; Panel">The &quot;Welcome&quot; Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>If you are installing EnterNet 300 for the first time, or if you were
presented the &quot;Existing Installation Detected&quot; panel and you clicked the &quot;Yes&quot; button,
the next panel you will encounter is the EnterNet &quot;Welcome&quot; panel. This panel will advise you to exit all other programs before proceeding with the installation process.</p>
<p><img src="images/enternet300_2.jpg" width="372" height="277" alt="" border="0"><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="The &quot;Software License Agreement&quot; Panel">
The &quot;Software License Agreement&quot; Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>This panel presents you with an opportunity to view the EnterNet ReadMe file,
which contains helpful information about installing EnterNet and getting support for it.
Clicking the &quot;View ReadMe&quot; button will launch your default text editor (usually Microsoft Notepad) and
open the EnterNet ReadMe file for your review. The installer will remain paused while you
review the file. You should close the ReadMe file before proceeding with EnterNet installation.
</p>
<p>
This panel also presents the Software License Agreement, the terms of which
you must accept in
order to proceed with EnterNet installation. By pressing the &quot;Yes&quot;
button you are indicating that
you have read, understand, and agree to comply with the terms and conditions
of the Software
License Agreement.
</p>
<p><img src="images/enternet300_3.jpg" width="372" height="277" alt="" border="0"><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="The &quot;Choose Destination Location&quot; Panel">
The &quot;Choose Destination Location&quot; Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>Here you are informed of the program's default installation directory. Click
the Next button to
accept the default installation location (recommended), or click Browse to
specify a different
installation location.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="The &quot;Choose Folder&quot; Panel">
The &quot;Choose Folder&quot; Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>
If you wish to install EnterNet to other than the default installation
folder, clicking the Browse
button on the Choose Destination Location panel will present the &quot;Choose
Folder&quot; panel.
You can either manually type the path to the desired installation location in
the Path window, or
you can navigate to the desired folder in the Directories tree. If the folder
where you wish to install
EnterNet does not yet exist, you will need to create it with Windows Explorer
before you can select
it here. If you need to create the folder, do so now. Before the newly
created folder will appear in
the Choose Folder panel, you must close Choose Folder and click the Browse
button on the
&quot;Choose Destination Location&quot; panel again. Once the desired
installation location is reflected in
the Path window, click the OK button to close the Choose Folder panel and
resume EnterNet
installation.
</p>
<p>
NOTE: Installing EnterNet on a network drive, as would occur if the
installation location were
selected from the Network button on the Choose Folder panel, is not
supported.&nbsp; 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="The &quot;Setup Complete&quot; Panel">
The &quot;Setup Complete&quot; Panel</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p> When the EnterNet installer has completed its tasks, you will be presented
with this final panel. Please note that a system restart is required after installing EnterNet. The
requisite restart may take longer than usual. This is due to the remaining, behind-the-scenes tasks
being performed by the EnterNet installer and by Windows. This process may result in as much as
two minutes of apparent inactivity, depending on the speed and configuration of your system.
After rebooting, you will have a new program folder on your desktop called EnterNet which is the software that is used to connect to the Internet using the 1 Megabit DSL modem.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="Summary"><b>Summary</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p>By successfully completing the above steps you have installed EnterNet 300
for Windows. You may now begin using EnterNet to establish a connection to your ISP. The next section of this User�s Guide, &quot;Getting Help with EnterNet&quot;, describes ways you can learn more about EnterNet 300 for Windows. Review the getting help section if you like, or jump ahead to &quot;Opening the EnterNet Folder&quot; if you would like to get started
right away using EnterNet 300.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Getting Help for EnterNet">Getting Help for EnterNet</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>On-line help is available for most EnterNet features. To use on-line help,
either select &quot;Help Contents&quot; from the EnterNet folder application, select &quot;EnterNet 300 Help&quot; from the &quot;NTS EnterNet 300&quot; folder on the Windows Start menu, or, when present, click the &quot;?&quot; button in the upper right-hand corner of any EnterNet screen. While viewing a Help page, hovering your cursor over some Help file graphics will cause the cursor to change into a hand. This indicates the availability of additional information. Simply click on the graphic to view an informational pop-up.</p>
<p>Most EnterNet panels offer ToolTips to clarify the purpose or function of a feature or parameter. Momentarily hover the cursor over a region of an EnterNet panel to see a ToolTip, if available.&nbsp;</p>
<p>For additional assistance with EnterNet, you may wish to visit the EnterNet Frequently Asked Questions (FAQ) page at the following URL: http://www.nts.com/support/faq/index.html. 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Opening the EnterNet Folder">Opening the EnterNet Folder</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>You can easily access EnterNet by launching either the EnterNet 300 shortcut
on your desktop or you can launch EnterNet 300 from the Windows Start menu. From the Windows Start menu, navigate to the folder where the EnterNet 300 application was installed. By default, the Start menu path will
be: </p>
<blockquote>
  <p>
Start\Programs\NTS EnterNet 300</p>
</blockquote>
<p>
Launching either of the above Windows shortcuts will start the EnterNet
folder application, which is where you will create and launch the Connection Profiles and Profiles that
serve to provide EnterNet with programmatic information about each specific profile, and to
authenticate you onto the desired network. 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="EnterNet Connection Profiles">EnterNet Connection Profiles</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>The configuration information you will need to establish a network connection
will be provided to you by your service provider. You will use this information to create
Connection Profiles that will be stored in the EnterNet folder. Connection Profiles are located in the
right-hand pane of the EnterNet folder. For convenience, you can create Windows shortcuts to these
profiles and place the shortcuts on your desktop, in the Quick Launch portion of your Windows
taskbar, or in any other folder you specify.</p>
<p><img src="images/enternet300_5.jpg" width="343" height="251" alt="" border="0"><br><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="Creating a Connection Profile"><b>Creating a Connection Profile</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p>Double click on the Create New Profile Icon. This will launch the
Connection Profile Wizard, which will guide you step by step through the process of creating a new Connection Profile.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Connection Name">Connection Name</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>You will be asked for a profile name. Call it ATMC and click Next</p>
<p><img src="images/enternet300_6.gif" width="384" height="260" alt="" border="0"><br><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="User Name and Password">User Name and Password</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>You will then be asked for your userid and
password information. They are both case-sensitive: &quot;Password&quot; and &quot;PASSWORD&quot; differ
from &quot;password.&quot; You must enter them exactly as they are specified.
 </p>
<p>Note, you must add a @ATMC.ca to your userid.&nbsp; Note that it is .ca and not
the normal .net. &nbsp; Normally you can use the .net and .ca interchangeably but in
this one case, you must use .ca.</p>
<p>When you have finished, click Next to continue. If you wish
to modify information entered on a previous panel, click Back.</p>
<p><img src="images/enternet300_7.gif" width="384" height="259" alt="" border="0"><br><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Devices">Devices</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>If you are using Windows NT, you might be prompted for a choice of adaptors. Select your Ethernet Adaptor and not the PPPoE. 
</p>
<p><img src="images/enternet300_8.gif" width="384" height="280" alt="" border="0"><br></p>
<p>Note: If your computer has only one adapter valid for display in the
drop-down list, the EnterNet Profile Wizard will automatically select the adapter for you and will not
display this panel. Instead, it will skip to the next step in the creation of your profile.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Devices">Services</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>The Profile Wizard panel will now prompt you to select an authentication server from the list in the lower frame. Select the only server that should be listed, NCSHLT-GWY. It will be inserted into the Server Selection box. Then, click Next. 
</p>
<p><img src="images/enternet300_9.jpg" width="372" height="252" alt="" border="0"><br><br></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Finishing the Profile">Finishing the Profile</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>The last panel the Profile Wizard will display is the Finish Connection
panel. In the text field you will find the name of your profile as it was entered in the first panel of the Profile Wizard. If you are satisfied that you have correctly completed the Connection Profile, click the Finish button. You may click the Back button to review and change any of the information you provided to the Profile Wizard. Once you have clicked the Finish button, your new profile will appear in the right-hand pane of the EnterNet folder (the same window as the Create New Profile icon). 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Connecting with EnterNet">Connecting with EnterNet</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>That�s it! &nbsp; You are now ready to connect. Double click on the newly created ATMC Icon in the EnterNet folder, ensure that the username and password are correct, and then click the Connect button. After about 10 Seconds, you will notice in the system tray (typically on the right of your task bar�the opposite side of where the start button is located and near where the Time and Date are displayed) a connection ICON similar to the dialup networking one you might be used to. 
</p>
<p><img src="images/enternet300_10.jpg" width="73" height="61" alt="" border="0"><br><br></p>
<p>
See the user�s guide section titled &quot;System Tray&quot; for more
information about the various states of the EnterNet system tray
icon.
</p>
<p>Once a connection has been successfully established, the Connection panel will disappear from your screen. If the attempt to establish a connection fails, the Connection panel will remain on your screen and a message indicating the reason for the failure will be present in the Messages window of the Connection panel, directly above the Connect button. This information will help you and your service provider determine and correct the problem that led to your connection failure. You will find more information about error messages in the &quot;Connection Errors&quot; section of this User�s Guide.</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="System Tray Icon">System Tray Icon</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>Once you have launched an EnterNet Connection Profile, EnterNet will place a
status icon in the
Windows system tray, which by Windows default appears in the lower right
corner of your screen,
at the end of the Windows taskbar.
The EnterNet status icon provides you with information about the current
state of your connection.
</p>
<p>
The icons reflect the following information:
</p>
<ul>
  <li>Connection status</li>
  <li>Send data status</li>
  <li>Receive data status</li>
  <li>Network Health (based on the speed with which your service provider�s DNS servers</li>
  <li>respond to an ICMP ping.)</li>
</ul>
<p>Refer to the images below to better understand the meaning of each EnterNet
system tray icon state.
</p>
<table border="0" width="100%" height="403">
  <tr>
    <td class="field" width="33%" height="93">
    <img src="images/enternet300_11.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="33%" height="93">
    <img src="images/enternet300_12.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="34%" height="93">
    <img src="images/enternet300_13.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
  </tr>
  <tr>
    <td class="field" width="33%" height="35" valign="top">&quot;Connect&quot; button not yet
      pressed</td>
    <td class="field" width="33%" height="35" valign="top">attempting to connect</td>
    <td class="field" width="34%" height="35" valign="top">connected, idle</td>
  </tr>
  <tr>
    <td class="field" width="33%" height="93">
      <img src="images/enternet300_14.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="33%" height="93">
    <img src="images/enternet300_15.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="34%" height="93">
    <img src="images/enternet300_16.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
  </tr>
  <tr>
    <td class="field" width="33%" height="36" valign="top">
connected, sending
data&nbsp;
      </td>
    <td class="field" width="33%" height="36" valign="top">
connected, receiving
data&nbsp;
      </td>
    <td class="field" width="34% height="36" valign="top">
sending and receiving
      </td>
  </tr>
  <tr>
    <td class="field" width="33%" height="93">
    <img src="images/enternet300_17.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="33%" height="93">
    <img src="images/enternet300_18.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
    <td class="field" width="34%" height="93">
    <img src="images/enternet300_19.gif" width="108" height="91" alt="" border="0"><br><br>
      </td>
  </tr>
  <tr>
    <td class="field" width="33%" height="29" valign="top">
Network Health
good
      </td>
    <td class="field" width="33%" height="29" valign="top">
Network Health marginal&nbsp;
      </td>
    <td class="field" width="34%" height="29" valign="top"> Network
Health bad
      </td>
  </tr>
</table> 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Disconnecting with EnterNet"><font COLOR="#000081">Disconnecting with EnterNet</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>
There are several ways to disconnect with EnterNet. If you want both to terminate your session and close the Active Connection Profile, right-click on the EnterNet system tray icon and select Exit from the pop-up menu. If you want to terminate your session but leave the active Connection Profile open, either select Disconnect from the system tray pop-up menu, or select Connection Details, and press the Disconnect button when the Connection Details panel appears (See Connection Details). 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Connection Messages">Connection Messages</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>Throughout the process of establishing your connection, the Messages field of the Connections panel will keep you updated of its status. The messages that will be displayed, along with a brief explanation of each, are as follows:
</p>
<ul>
  <li>Beginning Negotiation--Initial contact made with remote server. Negotiating for</li>           		
  <li>authentication.</li>
  <li>Authenticating--Authentication successfully taking place.</li>
  <li>Receiving Network Parameters--Negotiating network configuration parameters.</li>
  <li>Updating Network Parameters--IP addresses received from server and passed on to</li>
  <li>TCP/IP stack bound to EnterNet adapter.</li>
</ul> 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Connection Details">Connection Details</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>
If you
right click on the symbol and choose Connection Details, you can bring up statistics about your connection such as IP address. The Connection Details panel provides a variety of general information about your connection, including the time and date of its initiation and its overall duration. In the event of problems with your connection, click on the Details button and the panel will enlarge, providing you with additional information which may be of use to technical support personnel. (See &quot;Disconnecting with EnterNet&quot;).</p>
<p><img src="images/enternet300_20.jpg" width="214" height="279" alt="" border="0"></p>
</td>
</tr>
<tr>
 <td class="master" valign=top><a name="Troubleshooting EnterNet"><b>Troubleshooting EnterNet</b></a></td>
</tr>
<tr>
 <td class="heading" valign=top><a name="Installation Errors"><b>Installation Errors</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p> The most commonly occurring Set-Up error is &quot;Unable to Connect to Server.&quot; In this case, it may be necessary to uninstall EnterNet in order to verify the connectivity of your TCP/IP stack over the Ethernet. 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><a name="Connection Errors"><b>Connection Errors</b></a></td>
</tr>
<tr>
<td class="field" valign=top><p>Below is a list of the error messages you might receive during connection establishment, along with an accompanying explanation:</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="PPP Negotiate Timeout">PPP Negotiate Timeout</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p><ul>
  <li>Ethernet cable or modem not responding.</li>
  <li>IP Addresses not exchanged from Server.</li>
</ul>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Authentication Failed">Authentication Failed</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p> 
<ul>
  <li>User Name or Password not configured at server or enabled for dial-in access.</li>
  <li>When connecting into NT RAS, include NT Domain Name in front of the password.</li>
</ul>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Connection Cancelled by User">Connection Cancelled by User</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p><ul><li>Cancel button while establishing a connection.</li>
</ul></p>
</td>
</tr>
<tr>
 <td class="heading" valign=top><b><a name="Uninstalling EnterNet">Uninstalling EnterNet</a></b></td>
</tr>
<tr>
<td class="field" valign=top><p>To uninstall EnterNet, click on Start&gt;Programs&gt;EnterNet&gt;Uninstall EnterNet and follow the on screen prompts. Note that EnterNet must not be running at the time of removal. A system restart will be necessary after EnterNet has been removed from your system.
 
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>