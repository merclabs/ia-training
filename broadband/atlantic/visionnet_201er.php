<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>VisionNet ADSL 201ER</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>VisionNet ADSL 201ER</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The VisionNet ADSL 201ER router has an integrated ADSL (Asymmetric Digital Subscriber Line)modem
which enables high speed Internet access from your Ethernet local area
network (LAN). The 201ER brings high-speed connections to home users, small offices and
telecommuters.
</p>
<p>
The ADSL 201ER is designed to .plug and play.. Setup of the 201ER is as simple as
connecting it to a PC that is equipped with an Ethernet adapter card and then connecting it to an
ADSL outlet.
</p>

<p>
<b>DSL Line Filters:</b><br>
You will need to install a filter on each telephone or device that shares the DSL line to
eliminate this noise; phones or devices that share the same telephone number as your ADSL service. Other
devices where a filter should be placed include answering machines and fax machines. The filters will enable both
Internet access and normal phone use at the same time.
</p>
<img src="./images/linefilter_vn201er.gif" border=0>
<br>
<b><font color="red">Note: Don't install line filters on same line that DSL is plugged into.</font><br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_vn708eur.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The front panel contains lights called LEDs that indicate the status
of the unit from left to right.
<p align="center">
<table width="90%">
<tr>
 <td class="infohd">LED</td>
 <td class="infohd">Color</td>
 <td class="infohd">Status</td>
 <td class="infohd">Meaning</td>
</tr>
<tr>
 <td class="infohd" rowspan=2>PWR</td><td class="infoline" rowspan=2>Red</td>
 <td class="infoline">Solid</td><td class="infoline">Power detected</td>
</tr>
<tr><td class="infoline">Off</td><td class="infoline">Power Off</td></tr>
<tr>
 <td class="infohd" rowspan=2>ACT</td><td class="infoline" rowspan=2>Green</td>
 <td class="infoline">Solid</td><td class="infoline">Device passed self test.</td>
</tr>
<tr><td class="infoline">Flashing</td><td class="infoline">Device failed self test.</td></tr>
<tr>
 <td class="infohd">DATA</td><td class="infoline">Green</td>
 <td class="infoline">Flashing</td><td class="infoline">Data is transmitted through the ADSL line.</td>
</tr>
<tr>
 <td class="infohd" rowspan=2>LINK</td><td class="infoline" rowspan=2>Green</td>
 <td class="infoline">Flashing</td><td class="infoline">Trying to connect the ADSL line.</td>
</tr>
<tr><td class="infoline">Solid</td><td class="infoline">ADSL line has synchronized with your service provider.</td></tr>
<tr>
 <td class="infohd" rowspan=2>LAN</td><td class="infoline" rowspan=2>Green</td>
 <td class="infoline">Flashing</td><td class="infoline">Data is transmitted or received to the LAN side.</td>
</tr>
<tr><td class="infoline">Solid</td><td class="infoline">Ethernet interface is active.</td></tr>
<tr>
 <td class="infohd" rowspan=2>PPP</td><td class="infoline" rowspan=2>Green</td>
 <td class="infoline">Solid</td><td class="infoline">PPP connection.</td>
</tr>
<tr><td class="infoline">Off</td><td class="infoline">PPP disconnection.</td></tr>
</table>
<br>
<b>* Note: The PPP LED will only work when PPPoE or PPPoA encapsulation is selected</b>
</p>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_vn201er.gif" border=0>
</center>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
The rear panel contains the ports for the unit's data and power
connections. They are listed below from left to right.
<p align="center">
<table>
<tr>
 <td class="infohd">Label</td>
 <td class="infohd">Function</td>
</tr>
<tr>
 <td class="infohd">RESET</td>
 <td class="infoline">Resets the device to the manufacturer�s default configuration</td>
</tr>
<tr>
 <td class="infohd">LINE</td>
 <td class="infoline">Connects the device to a telephone jack for DSL communication</td>
</tr>
<tr>
 <td class="infohd">CONSOLE</td>
 <td class="infoline">Connects to the device via a console for configuration.</td>
</tr>
<tr>
 <td class="infohd">PHONE</td>
 <td class="infoline">Provides an optional connection to your telephone</td>
</tr>
<tr>
 <td class="infohd">Ethernet</td>
 <td class="infoline">
 Connects the device to your PC's Ethernet port, or to the uplink port on your 
 LAN's hub, using the cable provided
 </td>
</tr>
<tr>
 <td class="infohd">USB</td>
 <td class="infoline">Connects to the USB port on your PC</td>
</tr>
<tr>
 <td class="infohd">DC INPUT</td>
 <td class="infoline">Connects to the supplied power converter cable</td>
</tr>
<tr>
 <td class="infohd">Power</td>
 <td class="infoline">Switches the unit on and off</td>
</tr>
</table>
<br>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Basic Set-Up</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/setup_vn201er.gif" border=0>
<br>
</td>
</tr>
<!-- 
<tr>
 <td class="heading" valign=top colspan=2><b>Configuring Your PC to Access the 201ER</b></td>
</tr>

disabled on 06-Feb-2004 10:05 AM per doyle after e-mail from bryan leoper saying that
everything needs to be set to obtain an IP address automaticlly. 
 -->
<!--
<tr>
<td class="field" valign=top colspan=2>
To allow your PC to access the <b>201ER</b> the <b>network adapter</b> for the <b>Network Interface Card</b> 
should be set to "<b>obtain an IP address automatically</b>".(This should be done for all version of Windows.)
<br><br>
Next the on the <b>Connection</b> tab in <b>Internet Options</b>, select <b>Never dial a connection</b>, then
click on the <b>LAN settings</b> button, then uncheck anything that is check on this screen, then click <b>OK</b>.
<br><br>
<p align="center">
<img src="./images/internet_options_vn201er.jpg" border=0> 
</p>
<br>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Accessing the 201ER</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li>
<p>Start your <b>web browser</b> and then type <b>http://10.0.0.2</b> at web address.</p>
<img src="./images/addressbar.png" border=0> 
<br><br>
</li>
<li>
<p>The Enter Network Password window appears, type <b>"admin"</b> in User Name field and 
<b>"visionnet"</b> in the password field.</p>
<img src="./images/visionnetpass.png" border=0> 
<br><br>
</li>
<li>
<p>If the <b>Quick Start</b> setup page is not automatically opened, click <b>Quick Start</b> link on the left side on <b>200ER�s</b> configuration page.</p>
<img src="./images/quickstart.png" border=0> 
<br><br>
</li>
<li>
 <ol type="a">
 <li>Type the <b>VPI</b> and <b>VCI</b> provided on thier account information card.</li>
 <li>Set the <b>BRIDGE</b> bar to Disabled.</li>
 <li>In the <b>PPP</b> section, type the <b>User Name and Password</b> provided.
 <li>Click <b>Connect</b> to Finish quit setup.</li>
 </ol>
</li>
</ol>
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Fast-Track Install Instructions</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
<!-- the folloing content came from ATMC http://www.atmc.net/support/sinternet/pop-ups/fastrak-install.htm -->
		<ul>
				<li>
					<b>Step One - Phone Line Filters</b>
					<br>Put phone line filters on every telephone device (phone, fax, etc.) you have plugged into any outlet, except for your ADSL modem. Two filters are provided with your DSL installation. You must have a phone line filter on every telephone device. If you need more filters to accommodate telephone devices, unplug these devices from the outlets until you are able to contact ATMC and purchase more filters. For detailed instructions on filter installation, see <br><a href="http://www.atmc.net/support/sinternet/forms/filterinstall.pdf" target="_blank">DSL Filter Installation Guide*.</a><b>[.pdf]</b>
				</li>
				<li>
					<b>Step Two - Connect the VisionNet DSL Modem</b>
						<ol>
							<li>
							Connect one end of the telephone cable to the modem LINE port and the other end to the telephone outlet. 
							</li>
							<li>
							Connect the RJ45 Ethernet cable to the LAN port of the modem and the other end to the PC�s Ethernet Port NIC Card. 
							</li>
							<li>
							Plug the power adapter cord into the modem's DC IN port and the other end to the main outlet. Turn on the power switch. 
							</li>
						</ol>
					Approximately one minute after connecting the modem, make sure ADSL light is green. If the ADSL light does not turn green there is a problem with the connection. Make sure that you have a line filter on every telephone device and that you do not have a filter on the ADSL modem. In addition, if you have more than one phone number, verify that the ADSL modem is plugged into the correct line.
				</li>
				<li>
				<b>Step Three � Opening Control Panel</b>
				<br>To complete Steps Four and Five, you will first need to access the Control Panel. From the desktop, click the Start button and choose Settings and then choose Control Panel. If you are using Windows XP, click Switch to Classic View on the right side of the screen.
				</li>
				<li>
				<b>Step Four - Configuring Your Network Adapter's TCP/IP Properties</b>
				<br>In the TCP/IP properties of the Network Interface Card (called local area connection in Windows 2000 and XP) network adapter to obtain an IP address automatically. See <a href="javascript:void(window.open('http://www.atmc.net/support/sinternet/pop-ups/adapter-config.htm','','width=760,height=500,scrollbars=yes'))">Configuring Your Network Adapter's TCP/IP Properties</a> for detailed instructions.
				</li>
				<li>
				<b>Step Five� Internet Explorer Setting</b>
				<br>In IE, make support.atmc.net your homepage. Under Connections, verify that dial-up settings is set to never dial a connection and no settings under LAN Settings have a check mark beside them. See <a href="javascript:void(window.open('http://www.atmc.net/support/sinternet/pop-ups/dsl-setting-ie.htm','','width=760,height=500,scrollbars=yes'))">DSL Settings for Internet Explorer</a> for detailed instructions.
				</li>
				<li>
				<b>Step Six - Configuring the 200ER router</b>
				<br>Open the Visionnet 200ER router configuration page by typing http://10.0.0.2 into your web browser's address bar. When the Network Password box appears, type �admin� for the User Name and �visionnet� for the Password. In the Quick Start menu, enter the username and password provided on the Account Information Card. Then, click the Connect button to finish the setup. See Configuring the Visonnet router for detailed instructions.
				</li>
			</ul>
<br>
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<br>
Standard DSL Troubleshooting steps apply.
<br>
</td>
</tr>
-->
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
