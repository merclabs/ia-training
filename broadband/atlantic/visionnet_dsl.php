<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>VisionNet DSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>VisionNet DSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
The VisionNet Modems are ATMC's newest DSL modems. Newer customers will be using these modems however the 3Com and Speedstreams are still in use. The VisionNets are setup for DHCP. No PC settings are required. Make sure the IP address and DNS are set to auto obtain. 
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top><img src="./images/VN_full_t.jpg" width="198" height="172" alt="" border="0"></td><td class="field" valign=top>
<p> <b>Lights Required:</b>
<ul><li>ADSL (Green)</li>
<li>LAN (Green)</li>
<li>Power (Red)</li></ul>
Note: The Activity and TX/RX lights will blink when the modem is in use.
</p> 
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2><img src="./images/rearpanel_vn200er.gif"></td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>Using DHCP to Assign an IP Address</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
Before you can use your computer over a network, it must have an IP
address. The router contains a dynamic host configuration protocol
(DHCP) server that will automatically assign an IP address to the PC if the
PC is set to obtain an IP address from a DHCP server. If you have set up
your router and computer according to the installation instructions, follow
these steps to get an IP address via DHCP if you are running a PC with
</p>
Windows 95/98:
<ol>
<li>Select <b>Start</b> > <b>Settings</b> > <b>Control Panel</b></li>
<li>Double click the <b>Network</b> icon</li>
<li>Select <b>TCP/IP</b> from the list of network components. Select <b>Properties</b>.</li>
<li>Select the <b>IP Address</b> tab and verify that <b>Obtain an IP address
automatically</b> is selected. If not, then select it.</li>
<li>Also check the <b>Advanced</b> tab, make sure <b>TCP/IP</b> is set as default protocol at the bottom.</li>
<li>Click <b>OK</b> twice.</li>
<li><b>Restart</b> if prompted.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Configuring the Visonnet Routerfor DHCP</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>You will use your PC�s web browser software to access the 200ER�s router configuration</p>
<p><b>Accessing the 200ER configuration pages.</b>
<ul><li>Start your web browser and then type http://10.0.0.2 at web address.</li> 
<li>The Enter Network Password window appears, type �admin� in User Name and �visionnet� in Password.</li> 
<li>Configuring the Quick Start setup page</li>
<li>If the Quick Start setup page is not automatically opened, click Quick Start link on the left side on 200ER�s configuration page.</li> 
<li>Enter your username and password in the username and password field. Then, click Connect button to finish the setup.</li> 
<br><br><img src="./images/quickstart.gif" width="495" height="365" alt="" border="0">
<li>Then click the Save Settings link on the left side on 200ER�s configuration page. Make sure there is a mark beside Save settings to flash and reboot and click the Submit button. Then close the web browser.</li>
<li>Return to FastTrak ADSL Installation Guide for Visionnet Modem</li>
</p>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Configuring the Visonnet Router for Static IP</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Accessing the 200ER configuration pages.</b>
<ul><li>Under Operation Mode, change the Encapsulation to 1483 Bridged IP LLC. Enter the IP address, subnet mask, and gateway provided to you in the Static IP Information email sent to you by ATMC. Under DNS, enter the Primary and Secondary DNS provided in the statip IP email.</li> 
<li>Under PPP, do NOT enter a username or password. Then, click Connect button to finish the setup. </li> 
<li>Then click the Save Settings link on the left side on 200ER�s configuration page. Make sure there is a mark beside Save settings to flash and reboot and click the Submit button. Then close the web browser.</li>
<br><br><img src="./images/quickstartip.gif" width="495" height="365" alt="" border="0">
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>
<b>ADSL Light Flashing or Not On:</b>
<br>If the ADSL Light is flashing or is not on you will want to check the following:
<ol><li>The DSL line is secure in the port labeled"Line" in the back of the modem.</li> 
<li>Be sure the DSL line going into the modem is not filtered, but all other analog telephone devices are filtered.</li></ol>
</p>
<p>After verifying the above you can remove the power cord for 30 seconds. After 30 seconds plug the power cord back in and switch the modem on. Within 1-5 minutes the ADSL light should become a solid green. You may need to repeat the process above, and if you are still unable get the ADSL Light to be solid green escalate issue.</p>
<p><b>LAN Light not On:</b>
<br>If the LAN Link Light is not on be sure that the ethernet cable is secure in the back of the DSL modem and that the other end of the cable is secure in your computer (or other device such as a router or a hub). Your computer, or other device, must also be on in order for this light to be on. 
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>

