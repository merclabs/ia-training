<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../css/content.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>3Com HomeConnect ADSL Modem Dual Link</title>
</head>
<body class="document">
<table class="list_table" cellspacing=1>
<tr>
<td class="list_header" valign=top colspan=2><b>3Com HomeConnect ADSL Modem Dual Link</b></td>
</tr>
<tr>
<td class="list_header2" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>This modem uses PPPoE and comes with the software needed for installation.<br>
<font color="red"><b>However WindowsXP users can use the default PPPoE software in the connection wizard</b></font>.</p>
</td>
</tr>
<tr>
 <td class="list_header2" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_3CP4130.jpg"></td>
</p>
</tr>
<tr>
 <td class="list_header2" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>No image available.</td>
</tr>
<tr>
<td class="list_header2" valign=top colspan=2><b>Quick Installation Guide</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><ol><li>The installation should start when you insert the CD into your computer. If it does not automatically start, simply double click on the SETUP file on the CD.<li> 
<li>Carefully read and follow the on-screen instructions.</li>
<li>With the exception of the screens listed below, use the pre-chosen settings.</li> 
<li>At the Select Internet Service Provider window select <b>Other ISP</b>, then select Next.</li> 
<li>At the Encapsulation Method window select <b>PPP over Ethernet</b>, and then select Next.</li> 
<li>Carefully read and follow the on-screen prompts.</li></ol> 
</p>
<img src="./images/3comdual_install1.gif" width="275" height="251" alt="" border="0">
<img src="./images/3comdual_install2.gif" width="275" height="251" alt="" border="0">
</td></tr>
<tr>
 <td class="list_header2" valign=top colspan=2><b>EnterNet 300 Quick Installation Guide</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p><b>Introducing EnterNet 300</b></p>
<p>EnterNet 300 for Windows is desktop connectivity software used by the world's largest and most reputable ISPs to enable their end-user customers to gain access to their high-speed Internet access services. World wide, more users rely on EnterNet for high speed Internet access than any other PPPoE desktop connectivity product.</p>
<p><b><a href="enternet300.php">Installation Intructions</a></b></p>
<p><b>System Requirements</b></p>
<p>EnterNet 300 for Windows minimum system requirements include:
<br>
<ul><li>Microsoft Windows 9x, Windows Me, Windows NT 4.0 SP3, or Windows 2000</li>
<li>Pentium� class processor or equivalent</li> 
<li>Recommended minimum of 16MB RAM</li> 
<li>Minimum of 4MB free hard drive space</li>
<li>Microsoft-compatible Ethernet adapter with an NDIS 3.0 or higher compliant driver</li>
</ul></p>
</td>
</tr>
<tr>
 <td class="list_header2" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p>Verify that there is not a filter or a splitter between the DSL modem and the jack and that there is a filter between all other telephony devices in your home and the outlet. If you have more than one phone number in the house, make sure the DSL modem is plugged into the outlet for the phone number on which the DSL is installed. Look at the lights on your modem. If the USB or LAN light is not green, go to Procedures for LAN or USB light out. If the DSL light on the modem is not green, unplug and reconnect all plugs between modem computer and telephone jack. Reboot the computer and modem. </p>
<p><b>Procedures for LAN or USB light out:</b>
<ul><li>Powercycle the computer and DSL modem.</li> 
<li>Unplug the LAN or USB cable from the computer and the modem, then reconnect the cable.</li>
<li>Verify that the cable is correct type and the condition is good.</li>
<li>Reinstall Ethernet or USB drivers.</li> 
</ul>
</p>
<p><b>Cannot Connect</b></p>
<p>If the PPPoE software connects, but when you access a page you receive a "server not found" error, do the following: 
<ul><li>Try going to a variety of sites. If the problem only occurs with one or two web sites, the problem is with the web sites you are trying to access and not your Internet service. 
If the problem occurs with multiple web sites, conduct a ping test consisting of pinging yourself (IP number for your computer is given by holding the mouse over the connection icon), then pinging yahoo.com (216.32.74.51). If you cannot ping, delete the NIC card in Network Settings or for USB, delete and reinstall modem. If you can ping, delete any DNS entries in Network Setting and reboot the computer.</li>
<li>Check for firewalls or proxies and disable.</li></ul></p>
</td>
</tr>
</table>
<table class="main" cellspacing=1>
<tr>
<td class="master" valign=top  colspan=3>3Com HomeConnect ADSL Modem Dual Link</td>
</tr>
<tr>
<td class="list_header2" valign=top width=10%><b>LED</b></td>
<td class="list_header2" valign=top width=30%><b>State</b></td>
<td class="list_header2" valign=top width=60%><b>Description</b></td>
</tr>
<TR>
<td class="field" valign=middle rowSpan=6><B>Alert</B></TD>
<td class="field" valign=top>Off</TD>
<td class="field" valign=top>The modem is operating properly</TD></TR>
<TR>
<td class="field" valign=top>Flashing Orange</TD>
<td class="field" valign=top>During boot, the modem is checking for the download of new software. 
      This lasts for about 4 seconds.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Green slowly</TD>
<td class="field" valign=top>While the modem is initializing. This lasts for 30 to 40 seconds after 
      booting the modem.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Green rapidly</TD>
<td class="field" valign=top>The reset button is being held in and the modem has just been powered 
      on. This lets you know that the modem has recognized the reset request. It 
      lasts for about 4 seconds.</TD>
</TR>
<TR>
<td class="field" valign=top>Orange</TD>
<td class="field" valign=top>An unexpected error has occurred, but it does not affect the operation 
      of the modem.</TD>
</TR>
<TR>
<td class="field" valign=top>Red</TD>
<td class="field" valign=top>A non-recoverable error has occurred. If this happens, the modem will automatically reboot.</TD>
</TR>
<TR>
<td class="field" valign=top><B>Power</B></TD>
<td class="field" valign=top>Green</TD>
<td class="field" valign=top>Power is on.</TD>
</TR>
<TR>
<td class="field" valign=middle rowSpan=3><B>LAN Status</B></TD>
<td class="field" valign=top>Off</TD>
<td class="field" valign=top>No Ethernet signal has been detected.</TD>
</TR>
<TR>
<td class="field" valign=top>Green</TD>
<td class="field" valign=top>A PC or hub is properly connected to the Ethernet port.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Green</TD>
<td class="field" valign=top>A proper connection exists and data is present on the Ethernet 
  port.</TD>
</TR>
<TR>
<td class="field" valign=middle rowSpan=5><B>USB Status</B></TD>
<td class="field" valign=top>Off</TD>
<td class="field" valign=top>No USB signal has been detected.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Orange</TD>
<td class="field" valign=top>A PC is properly connected to the USB port but the modem has not "enumerated." (When you plug in the modem, the PC senses voltage differences in the USB network and proceeds to query (enumerate) the modem for type, vendor, functionality and bandwidth required.)</TD>
</TR>
<TR>
<td class="field" valign=top>Orange</TD>
<td class="field" valign=top>The modem has enumerated with the PC. This means that the operating system running on the PC has recognized the modem.</TD>
</TR>
<TR>
<td class="field" valign=top>Green</TD>
<td class="field" valign=top>The modem's software has established communications with the 3Com USB driver software running on the PC.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Green</TD>
<td class="field" valign=top>The modem is working and data is present on the USB port.</TD>
</TR>
<TR>
<td class="field" valign=middle rowSpan=4><B>ADSL Status</B></TD>
<td class="field" valign=top>Off</TD>
<td class="field" valign=top>No ADSL signal has been detected</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Orange</TD>
<td class="field" valign=top>The modem is attempting to synchronize with the DSL service provider's equipment.</TD>
</TR>
<TR>
<td class="field" valign=top>Green</TD>
<td class="field" valign=top>The link has been established between your modem and the DSL service provider's equipment.</TD>
</TR>
<TR>
<td class="field" valign=top>Flashing Green</TD>
<td class="field" valign=top>The link is up and there is data on the ADSL port</TD>
</TR>
</TABLE>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>
