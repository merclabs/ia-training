<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Zoom X3 5560 ADSL Modem</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Zoom X3 5560 ADSL Modem</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>

</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/frontpanel_zoom_x3_5560.jpg" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<table width=500>
<tr><td class="infohd">LEDs</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">LAN</td><td class="infoline">Lights wehn LAN connection is active.</td></tr>
<tr><td class="infohd">RXD</td><td class="infoline">Blinks when unit is transmitting or receiving data.</td></tr>
<tr><td class="infohd">LINK</td><td class="infoline">Blinks when unit is performing its startup sequence.<br>
Stays on when unit is connected to the ADSL line.</td></tr>
<tr><td class="infohd">POWER</td><td class="infoline">Lights when power switch on back panel is turned on.</td></tr>
</table>
</div>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Quick Install</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<ol>
<li><u><b>Macintosh, Linux, Windows NT, Windows 95 Users:</b></u>
<br>
Do not need to install the CD-ROM software.
<br><br>
<u><b>Windows 98/98SE, ME, 2000, and Windows XP Users: Install the software before connecting the hardware.</b></u>
<br>
<ol type="a">
<li>
Insert the supplied CD-ROM into your CD-ROM drive. The CD should start automaticlly
and the <b>Main Menu</b> should display.(<b>Note:</b> If the CD dose not start automaticlly
<b>click</b> on <b>start</b> then <b>run</b> and type <b>D:\setup.exe</b>, where <b>D</b> should 
reflect the letter of your CD-ROM dirve.)
<p align="center">
<img src="./images/install.png" border="0">
</p>
</li>
<li>Click the <b>ADSL Modem Installation Wizard</b> button, and then click the ethernet 
option. Then software installation proceeds automaticlly.</li>
<li>When the process is complete, you will be prompted to click <b>Finish</b>, your computer
will shutdown so you can connect the hardware.
<p align="center">
<img src="./images/shutdown.png" border="0">
</p>
</li>
</ol>
<li><b>After the computer shuts down, the customer must connect all cables and hardware.</b>
<ol type="a">
<li>Plug one end of the included power adapter to the <b>PWR</b> jack and the other end into 
a power strip or wall receptacle.</li>
<li>Turn the <b>X3</b> on by pushing in the <b>ON/OFF</b> toggle switch. The <b>PWR</b> light on the 
unit's front panel turns on.</li>
<li>Plug one end of the supplied phone cord into the unit's <b>ADSL</b> jack, and the 
other end into the <b>ADSL</b> wall jack.</li>
<li>Plug one end of the <b>Straight-Through Ethernet</b> cable into the modem's <b>ETHERNET</b> jack
and plug the other end into your computers corresponding <b>Ethernet port</b>.(<b>Note:</b> Alternatively
you can connect the <b>X3</b> to a Network Hub via this <b>ETHERNET</b> jack.) if your
hub has an uplink or daisy chain port, you can use the supplied Straight-Through ethernet cable to
connect the two. If your hub has a numbered port, you will need a crossover cable(sold separately.)</li>
</li>
</ol>
<li>Turn your computer back on.</li>
<li>The <b>X3</b> performs a startup sequence-- The <b>LINK</b> light blinks.
When the start up sequence is complete, the <b>LINK</b> Light will change from blinking
to solid.(For more information see: <b>Back Panel</b> section.) See next, <b>Establishing Communicating
with the X3</b>.</li>
</ol>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Establishing Communicating with the X3</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2> 
<ol>
<li>
 <b><u>Windows 98/98SE, ME, 2000, Windows XP Users:</u></b><br>
 You will see a <b>Zoom Web Console</b> on your desktop. Double-click to display
 the <b>Network Password</b> dialog box.
 <p align="center">
 <img src="./images/icon.png" border="0">
 </p>
 <b><u>Macintosh, Linux, Windows NT, Windows 95 Users:</u></b>
 <br>
 There will not be a <b>Zoom Web Console</b> icon on your desktop.Instead, open your 
 web browser, type: <b>http://10.0.0.2</b> and press <b>Enter</b> to display the <b>Network Password</b>
 dialog box.
 <p align="center">
 <img src="./images/userpass.png" border="0">
 </p>
 Enter the following information:<br>
 User name = <b>admin</b><br>
 Password = <b>zoomadsl</b><br>
 <br>
 Then click <b>OK</b>.
 <br><br>
  <i><b>Remember:</b> User/password is case sensitive.</i>
</li>
<li>The <b>Basic Setup</b> page displays. You are now communicating with your <b>X3</b>. This page displays details about
the type of device you are using and your Internet connection.
<p align="center">
 <img src="./images/basicsetup.png" border="0">
</p>
<ol>
<li>Check that the VPI, VCI, Encapcilation settings match those provided by your ISP.</li>
<li>Enter your <b>User name</b> and <b>Password</b> supplied by your ISP.</li>
<li><b><i>Optional:</i></b> Enter a <b>Service</b> name if your ISP has supplied one.</li>
<li>Click <b>Save Changes</b> and then click <b>Write settings to Flash and Reboot</b> twice. 
Once reboot is complete and the unit has reset itself, your <b>X3</b> is ready to use. Try going to
another webpage with your web browser.</li>
</ol>
</li>
</ol>
</td>
</tr> 
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<p align="center">
<img src="./images/rearpanel_zoom_x3_5560.gif" border=0>
</p>
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<div align="center">
<table width=500>
<tr><td class="infohd">Port</td><td class="infohd">Description</td></tr>
<tr><td class="infohd">PWR</td><td class="infoline">Port to connect the unit to the power adapter.</td></tr>
<tr><td class="infohd">ON/OFF</td><td class="infoline">Toggle switch to turn the unit on or off.</td></tr>
<tr><td class="infohd">RESET</td><td class="infoline">Button to reset the unit to its system default settings.</td></tr>
<tr><td class="infohd">ETHERNET</td><td class="infoline">Port to connect the unit to a Network Hub or to the Ethernet(10BaseT) of a computer.</td></tr>
<tr><td class="infohd">PHONE</td><td class="infoline">Port to connect a phone to the unit.</td></tr>
<tr><td class="infohd">ADSL</td><td class="infoline">Port to connect the unit to the ADSL telephone wall jack.</td></tr>
</table>
</div>
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
ADSL modem, has four lights Power (its on the right and is red in color), on the 
left from top to bottom has Line (Green solid), RXB, and LAN (Green solid). 
Manual has user set IP address to <b>10.0.0.7</b>, Subnet of <b>255.255.255.0</b>, Gateway of 
<b>10.0.0.2</b>. When booting up comes up with configuration window asking user to enter 
a user name and password (in Manual states user name as default of <b>admin</b>). once 
user enters this info they recieved a dialog box stating that it was <b>writing 
to flash</b> and that they needed to <b>reboot the PC</b> once user rebooted the modem <b>established</b> 
a connection.
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>