<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<link rel="stylesheet" type="text/css" href="../../provider/css/main.css">
<?require_once("../../provider/navigator/navigator.php");?>
<?require_once("../../provider/".$id."/company.php");?>
<?require_once("../../provider/".$id."/services.php");?>
<?require_once("../".$id."/modeminfo.php")?>
<title>Bulloch DSL Modems</title>
</head>
<body>
<table class="main" cellspacing=1>
<tr>
 <td class="master" valign=top colspan=2>Bulloch DSL Modems</td>
</tr>
<tr>
<td class="heading" valign=top colspan=2><b>General Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
<font color="red">When troubleshooting Bulloch ADSL modems, do not unplug the DSL 
modem; have the customer reboot computer instead.</font> 
<br>
<b>Bulloch uses internal ADSL modems, they do not use PPOE or PPOA</b>.
</td>
</tr>
<!--
<tr>
 <td class="heading" valign=top colspan=2><b>Front Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
 <td class="heading" valign=top colspan=2><b>Back Panel</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
text here
</td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
image here
</td>
</tr>
-->
<tr>
 <td class="heading" valign=top colspan=2><b>Troubleshooting Information</b></td>
</tr>
<tr>
<td class="field" valign=top colspan=2>
If the customer has a static IP of: <b>209.221.38.x</b>, their gateway should be: 
<b>209.221.38.254</b> or if they have the Static IP of: <b>209.221.39.x</b>, the 
gateway should be: <b>209.221.39.254</b>
<br>
<b>They have a choice of Infoave or US Carriers DNS Servers.</b> 
<br>
<ul>
<li><b>Ours DNS:</b>
<li><b>Primary:</b> 206.74.254.2
<li><b>Secondary:</b> 204.116.57.2
</ul>
<ul>
<li><b>US Carrier's DNS:</b>
<li><b>Primary:</b> 209.221.47.26                          
<li><b>Secondary:</b> 209.221.47.30
</ul>
<p>
<i>They will NOT have a user/pass unless they use Bulloch email. If they use any other 
email, they will not have a username and password with Bulloch. 
If you have checked all the required steps, please send the customer to Customer 
Service with a Lead Tech approval.</i>
</p>
</td>
</tr>
</table>
<p align="right">
<input type="button" value="Close Window" onclick="window.close()">
</p>
</body>
</html>