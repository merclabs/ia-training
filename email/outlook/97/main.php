<h1>Microsoft Outlook 97</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Back to E-Mail Clients</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/main.php">Back to Microsoft Outlook Express</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/97/main.php#1">Accessing Settings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/97/main.php#2">Services</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/97/main.php#3">General Tab</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/97/main.php#4">Advance Options</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/97/main.php#5">Connection Tab</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>

<p>
<h3><a name="1">Accesing Settings</a></h3>
<p>
To access the account settings in <b>Outlook 97</b>, click on <b>Tools</b> then <b>Services...</b>. A  
<b>Services</b> winodow should display.
</p>
<img src="./email/outlook/97/images/tools_services.jpg" alt="Accesing Settings.">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Services</a></h3>
<p>
In the <b>Services</b> window, select <b>Internet Mail</b>, If you want to created a <u>new</u> mail account, 
click the <b>Add</b> button to create a <u>new account</u>, if you want to <u>edit</u> an <u>existing</u> account, 
click the <b>Properties</b> button.
</p>
<img src="./email/outlook/97/images/services.jpg" alt="Select Internet Mail.">
</p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">General Tab</a></h3>
<p>
The General Tab should display.
<ul>
<li><b>Full name:</b> Enter the name of the customer here.
<li><b>E-mail address:</b> Enter the e-mail address that was assigned.
<li><b>Internet Mail server</b> Enter the incoming mail server for the provider.
<li><b>Account name:</b> Enter the user name for the account.
<li><b>Password:</b> Enter the password for the account. 
</ul>
</p>
<img src="./email/outlook/97/images/general_tab.jpg" alt="General Tab - Server Information.">
</p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="4">Advanced Settings</a></h3>
<br>
<p>
To view the advanced settings click on the <b>Advanced Options</b> button, then enter the
Outgoing mail server address, then click <b>OK</b>.
</p>
<img src="./email/outlook/97/images/advance_options.jpg" alt="Advanced Settings - Outgoing Mail Server.">
</p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="5">Connection Tab</a></h3>
<p>
Click on the <b>Connection Tab</b> to change the connection method used by <b>Outlook</b>, either 
<b>Network(LAN)</b> or <b>Modem</b>. Other settings should remain the same.  
</p>
<br>
<img src="./email/outlook/97/images/connection_tab.jpg" alt="Connection Tab - Connection settings.">
</p>


<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->