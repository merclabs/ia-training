<h1>Microsoft Outlook Express 5 - Configure Existing Account</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/main.php">Outlook Express 5</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/configure_account.php#1">Account Settings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/configure_account.php#2">Internet Accounts</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/configure_account.php#3">General Tab</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/configure_account.php#4">Servers Tab</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/configure_account.php#5">Connection Tab</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>

<p>
<h3><a name="1">Account Settings</a></h3>
<p>
Open <b>Microsoft Outlook Express</b>, then click on <b>Tools</b>, select <b>Accounts</b>.
</p>
<img src="./email/outlook/outlook5/images/account_settings.jpg" alt="Account Settings">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Internet Accounts</a></h3>
<p>
An <b>Internet Accounts</b> window should display, then click on the <b>Mail</b> tab. If you already have
an account created, it should be listed here. To <u>edit</u> properties of this account select it, then 
click the <b>Properties</b> button. 
</p>
<img src="./email/outlook/outlook5/images/internet_accounts.jpg" alt="Internet Accounts">
</p>
<p class="note">
<b>Note:</b> Always check to see if more than one account is listed here if the customer is
receiving server errors and all settings are ok.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">General Tab</a></h3>
<p>
The <b>General</b> tab should display. To edit any information on this page, simply clear the
field and re-enter the correct information in its place. 
</p>
<img src="./email/outlook/outlook5/images/general_tab.jpg" alt="General Tab">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="4">Servers Tab</a></h3>
<p>
The <b>Servers</b> tab should display. To edit any information on this page, simply clear the
field and re-enter the correct information in its place. 
</p>
<img src="./email/outlook/outlook5/images/servers_tab.jpg" alt="Servers Tab">
</p>
<p class="note">
If you have a <b>Cyberlink</b> customer, be sure to check the <b>My server requires authentication</b> box.
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="5">Connection Tab</a></h3>
<p>
The only change that may be needed on this page is to put a check in <b>always connect 
to this account using</b>, then to select <b>Local Area Network</b> from the drop-down 
menu for broadband customers.
</p>
<img src="./email/outlook/outlook5/images/connection_tab.jpg" alt="Connection Tab">
</p>
<p class="note">
Click <b>Apply</b> for settings to take effect. 
</p>

<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

