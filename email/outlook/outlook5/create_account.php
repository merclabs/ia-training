<h1>Microsoft Outlook Express 5 - Create New Account</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/main.php">Outlook Express 5</a></td></tr>

</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>

<p>
<h3>Account Settings</h3>
<p>
Open <b>Microsoft Outlook Express</b>, then click on <b>Tools</b> then <b>Accounts</b>.
</p>
<img src="./email/outlook/outlook5/images/account_settings.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3>Internet Accounts</h3>
<p>
An <b>Internet Accounts</b> window should dispaly, select the <b>Mail Tab</b>, then click the <b>Add</b> button, then select <b>Mail</b>.
</p>
<img src="./email/outlook/outlook5/images/internet_accounts.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3>Internet Connection Wizard - Display Name</h3>
<p>
This should start the <b>Internet Connection Wizard</b>, the first screen asks for a <b>Display name</b>. This
name can be anything that the customer wants it to be. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_display_name.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3>Internet Connection Wizard - E-mail Address</h3>
<p>
The next step asks for the <b>E-Mail address</b> that was issued to the user by the <b>Provider</b>. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_email_address.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3>Internet Connection Wizard - Mail Servers</h3>
<p>
The next screen asks for the <b>mail server addresses</b> of the <b>Provider</b>, both <b>Incoming</b> and <b>Outgoing</b>.
Enter both <b>mail server addreses</b> then click <b>Next</b>. 
</p>
<img src="./email/outlook/outlook5/images/icw_email_server_addresses.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p class="note">
This information can be found on the <b>Account Information Card</b> that is issued by the Provider. If the customer 
does not know this information check the <a href="https://intranet.spirittelecom.com/pages_spiritintranet/internet_licenseelisting.php" target="_blank">Licensee Listings</a> 
</p>

<p>
<h3>Internet Connection Wizard - User Name and Password</h3>
<p>
The next screen asks for the <b>user/password</b> that was issued by the <b>Provider</b> to the customer. Enter
the <b>user/pass</b> then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_user_password.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3>Internet Connection Wizard - Finish</h3>
<p>
This completes <u>creating a new account</u> in <b>Outlook Express 5</b>. Click the <b>Finish</b> button to complete the wizard.

</p>
<img src="./email/outlook/outlook5/images/icw_finish.jpg" border=0>
</p>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
