<h1>Microsoft Outlook Express 5 - </h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/main.php">Outlook Express 5</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#1">Add New Identity</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#2">Identity User Name</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#3">Internet Connection Wizard - Display Name</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#4">Internet Connection Wizard - E-Mail Address</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#5">Internet Connection Wizard - Mail Servers</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#6">Internet Connection Wizard - User Name and Password</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#7">Internet Connection Wizard - Finish</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#8">Switch to Identity...</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>

<p>
<h3><a name="1">Add New Identity</a></h3>
<p>
To create a <u>new</u> <b>Identity</b> in Outlook, click on <b>File</b>, then <b>Identities</b> then
<b>Add new Identity...</b>.
</p>
<img src="./email/outlook/outlook5/images/main_identity.jpg" border=0 alt="Add New Identity">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Identity User Name</a></h3>
<p>
The <b>New Identity</b> window should display, enter the desired <b>user name</b> for this identity you are creating, then 
click <b>OK</b>.
</p>
<p>
<img src="./email/outlook/outlook5/images/identity_username.jpg" border=0 alt="Identity User Name">
</p>
<p class="note">
<b>Note:</b> User name can be anything that the customer wants it to be...
</p>
<p>
An <b>Identity Added</b> prompt should display informing you that the Identity has been added, and asks if you would like
to switch to the new identity. If you click <b>Yes</b>, Outlook will switch to the <u>new</u> identity you just created. 
<p class="note">
If you clicked No, you will have to switch to the new identity manually, see <a href="?content=./email/outlook/outlook5/create_identity.php#8">Switching Identities</a> below.
</p>
</p>
<p>
<img src="./email/outlook/outlook5/images/identity_added.jpg" border=0 alt="Prompt to Switch to Identity">
</p>

</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">Internet Connection Wizard - Display Name</a></h3>
<p>
After switching to the new identity you need to <u>create</u> a new account for this identity.
</p>
<p>
Click on <b>Tools</b> then <b>Accounts</b>, then click on the <b>Mail</b> tab, select <b>Add</b>, then <b>Mail</b>.
This should start the <b>Internet Connection Wizard</b>, the first screen asks for a <b>Display name</b>. This
name can be anything that the customer wants it to be. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_display_name.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="4">Internet Connection Wizard - E-mail Address</a></h3>
<p>
The next step asks for the <b>E-Mail address</b> that was issued to the user by the <b>Provider</b>. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_email_address.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="5">Internet Connection Wizard - Mail Servers</a></h3>
<p>
The next screen asks for the <b>mail server addresses</b> of the <b>Provider</b>, both <b>Incoming</b> and <b>Outgoing</b>.
Enter both <b>mail server addreses</b> then click <b>Next</b>. 
</p>
<img src="./email/outlook/outlook5/images/icw_email_server_addresses.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->
<p class="note">
This information can be found on the <b>Account Information Card</b> that is issued by the Provider. If the customer 
does not know this information check the <a href="https://intranet.spirittelecom.com/pages_spiritintranet/internet_licenseelisting.php" target="_blank">Licensee Listings</a> 
</p>

<p>
<h3><a name="6">Internet Connection Wizard - User Name and Password</a></h3>
<p>
The next screen asks for the <b>user/password</b> that was issued by the <b>Provider</b> to the customer. Enter
the <b>user/pass</b> then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook5/images/icw_user_password.jpg" border=0>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="7">Internet Connection Wizard - Finish</a></h3>
<p>
This completes <u>creating a new account</u> in <b>Outlook Express 5</b>. Click the <b>Finish</b> button to complete the wizard.

</p>
<img src="./email/outlook/outlook5/images/icw_finish.jpg" border=0 alt="Finish">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="8">Switching Identities</a></h3>
<p>
If you clicked <b>No</b> at the prompt to <i>switch</i> to the new identity, you need to switch
to the new identity by clicking on <b>File</b>, then <b>Switch Identity...</b>
</p>
<img src="./email/outlook/outlook5/images/switch_identity.jpg" border=0 alt="Switching to Identity">
</p>
<p>
The <b>Switch Identity</b> window should display, select the desired identity you wish to switch to, then click <b>OK</b> to
switch to that identity. 
</p>
</p>
<img src="./email/outlook/outlook5/images/select_identity.jpg" border=0 alt="Select identity">
</p>
<p class="note">
If you need to create a new mail account after switching identities, click <a href="content_link"><a href="?content=./email/outlook/outlook5/create_identity.php#3">here</a>
</p>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

