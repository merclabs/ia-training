<h1>Microsoft Outlook Express 4 - Create New Account</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/main.php">Outlook Express 4</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#1">Account Settings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#2">Internet Accounts</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#3">Internet Connection Wizard - Display Name</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#4">Internet Connection Wizard - E-mail Address</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#5">Internet Connection Wizard - Mail Servers</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#6">Internet Connection Wizard - User Name and Password</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/outlook4/create_account.php#7">Internet Connection Wizard - Finish</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>

<p>
<h3><a name="1">Account Settings</a></h3>
<p>
Open <b>Microsoft Outlook Express</b>, then click on <b>Tools</b> then <b>Accounts</b>.
</p>
<img src="./email/outlook/outlook4/images/account_settings.gif" alt="Account Settings">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Internet Accounts</a></h3>
<p>
An <b>Internet Accounts</b> window should dispaly, select the <b>Mail Tab</b>, then click the <b>Add</b> button, then select <b>Mail</b>.
</p>
<img src="./email/outlook/outlook4/images/internet_accounts_blank.gif" alt="Internet Accounts">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">Internet Connection Wizard - Display Name</a></h3>
<p>
This should start the <b>Internet Connection Wizard</b>, the first screen asks for a <b>Display name</b>. This
name can be anything that the customer wants it to be. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook4/images/icw_step1.gif" alt="Internet Connection Wizard - Display Name">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="4">Internet Connection Wizard - E-mail Address</a></h3>
<p>
The next step asks for the <b>E-Mail address</b> that was issued to the user by the <b>Provider</b>. Then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook4/images/icw_step2.gif" alt="Internet Connection Wizard - E-mail Address">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="5">Internet Connection Wizard - Mail Servers</a></h3>
<p>
The next screen asks for the <b>mail server addresses</b> of the <b>Provider</b>, both <b>Incoming</b> and <b>Outgoing</b>.
Enter both <b>mail server addreses</b> then click <b>Next</b>. 
</p>
<img src="./email/outlook/outlook4/images/icw_step3.gif" alt="Internet Connection Wizard - Mail Servers">
</p>
<p class="note">
This information can be found on the <b>Account Information Card</b> that is issued by the Provider. If the customer 
does not know this information check the <a href="https://intranet.spirittelecom.com/pages_spiritintranet/internet_licenseelisting.php" target="_blank">Licensee Listings</a> 
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="6">Internet Connection Wizard - User Name and Password</a></h3>
<p>
The next screen asks for the <b>user/password</b> that was issued by the <b>Provider</b> to the customer. Enter
the <b>user/pass</b> then click <b>Next</b>.
</p>
<img src="./email/outlook/outlook4/images/icw_step4.gif" alt="Internet Connection Wizard - User Name and Password">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->


<p>
<h3><a name="7">Internet Connection Wizard - Finish</a></h3>
<p>
This completes <u>creating a new account</u> in <b>Outlook Express 4</b>. Click the <b>Finish</b> button to complete the wizard.
</p>
<img src="./email/outlook/outlook4/images/icw_step5.gif" alt="Internet Connection Wizard - Finish">
</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->