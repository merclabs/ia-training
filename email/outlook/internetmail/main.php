<h1>Microsoft Internet Mail - Create|Configure New Account</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/main.php">Microsoft Outlook Express</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/internetmail/main.php#1">Internet Mail Options</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/internetmail/main.php#2">Internet Mail Server Tab</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/outlook/internetmail/main.php#3">Internet Mail Connection Tab</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
If an e-mail account already exists, you can use the same steps below to <u>configure</u> an existing 
mail account in <b>Internet Mail</b>. 
</td></tr>
</table>
</p>
<p>
<h3><a name="1">Internet Mail Options</a></h3>
<p>
Open Internet Mail, click on the <b>Mail Menu</b>, then Options. 
</p>
<img src="./email/outlook/internetmail/images/internet_mail_options.jpg" alt="Internet Mail Options">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="2">Internet Mail Server Tab</a></h3>
<p>
<ul>
 <li>Enter the <b>Customers Name</b> in the Name Field 
 <li>Enter their <b>Email Address</b> in the Email Address Field. 
 <li>Enter the <b>Outgoing mail server address</b> in Outgoing Mail (SMTP). 
 <li>Enter the <b>Incoming mail server address</b> in Incoming Mail (POP3). 
 <li>Select <b>Logon using</b> then Enter their <b>Username</b> and <b>Password</b> as required. 
</ul>
</p>
<img src="./email/outlook/internetmail/images/internet_mail_server_tab.jpg" alt="Internet Mail Server Tab">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->

<p>
<h3><a name="3">Internet Mail Connection Tab</a></h3>
<p>
Click on the <b>Connection Tab</b>, select the connection type that you wish to use, then click apply to complete set-up.  
</p>
<img src="./email/outlook/internetmail/images/internet_mail_connection_tab.jpg" alt="Internet Mail Connection Tab">
</p>
<p class="note">
Make sure that <b>Disconnect when finished sending and receiving</b> is not checked, this 
can cause <b>Internet Mail</b> to disconnect the user after checkin mail. 
</p>

<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->