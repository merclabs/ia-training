<h1>Eudora 6.0</h1>
<p align="center">
<img src="./email/eudora/6.0/images/splash.gif" alt="Eudora 6.0">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/new_account_wizard.php">New Account Wizard</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/check_mail.php">Check Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/options.php">Eudora 6.0 Options</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/getting_started.php">Getting Started</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/checking_mail.php">Checking Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/incoming_mail.php">Incoming Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/composing_mail.php">Composing Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/sending_mail.php">Sending Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/internet_dialup.php">Internet Dialup</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/replying.php">Replying</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/attachments.php">Attachments</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/fonts.php">Fonts</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/display.php">Display</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/view_mail.php">Viewing Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/mailboxes.php">Mailboxes</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/styled_text.php">Styled Text</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/spell_checking.php">Spell Checking</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/auto_completion.php">Auto Completion</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/date_display.php">Date Display</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/label.php">Label</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/getting_attention.php">Getting Attention</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/background_tasks.php">Background Tasks</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/automation.php">Automation</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/extra_warning.php">Extra Warnings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/mapi.php">MAPI</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/advanced_network.php">Advanced Network</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/auto_configure.php">Auto Configure</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/kerberos.php">Kerberos</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/moodwatch.php">Moodwatch</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/statistics.php">Statistics</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/miscellaneous.php">Miscellaneous</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/junk_mail.php">Junk Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/junk_mail_extras.php">Junk Mail Extras</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/6.0/content_concentrator.php">Content Concentrator</a></td></tr>

</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->