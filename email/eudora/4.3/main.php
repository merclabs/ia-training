<h1>Eudora 4.3</h1>
<p align="center">
<img src="./email/eudora/4.3/images/splash.gif" alt="Eudora 4.3">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/new_account_wizard.php">New Account Wizard</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/check_mail.php">Check Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/options.php">Eudora 4.3 Options</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/getting_started.php">Getting Started</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/checking_mail.php">Checking Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/incoming_mail.php">Incoming Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/sending_mail.php">Sending Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/internet_dialup.php">Internet Dialup</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/replying.php">Replying</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/attachments.php">Attachments</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/fonts.php">Fonts</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/display.php">Display</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/view_mail.php">Viewing Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/mailboxes.php">Mailboxes</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/styled_text.php">Styled Text</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/spell_checking.php">Spell Checking</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/auto_completion.php">Auto Completion</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/date_display.php">Date Display</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/label.php">Label</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/getting_attention.php">Getting Attention</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/background_tasks.php">Background Tasks</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/automation.php">Automation</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/extra_warning.php">Extra Warnings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/mapi.php">MAPI</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/advanced_network.php">Advanced Network</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/auto_configure.php">Auto Configure</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/kerberos.php">Kerberos</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/miscellaneous.php">Miscellaneous</a></td></tr>

</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->