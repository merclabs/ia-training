<h1>Eudora 4.3 - Checking Mail</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/4.3/main.php">Eudora 4.3</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>
<p>
<h3>Checking Mail - Main Window</h3>
Open <b>Eudora 4.3</b>.
</p>
<p>
<img src="./email/eudora/4.3/images/eudora_open.gif" alt="Checking Mail - Main Window">
</p>
<p>
<h3>Checking Mail - Check Mail</h3>
Then click on <b>File</b> then <b>Check Mail</b>. 
</p>
<p>
<img src="./email/eudora/4.3/images/eudora_check_mail.gif" alt="Check Mail - Check Mail">
</p>
<p>
<h3>Checking Mail - Password</h3>
You should get a password prompt, enter your <b>Password</b>, then click <b>OK</b>.
</p>
<p>
<img src="./email/eudora/4.3/images/eudora_password.gif" alt="Checking Mail - Password">
</p>
<p>
<h3>Checking Mail - Inbox</h3>
<b>Eudora 4.3</b> should sound after check mail, then open to the <b>Inbox</b>.
</p>
<p>
<img src="./email/eudora/4.3/images/eudora_inbox.gif" alt="Checking Mail - Inbox">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->