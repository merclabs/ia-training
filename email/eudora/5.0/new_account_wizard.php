<h1>Eudora 5.0 - New Account Wizard</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/5.0/main.php">Eudora 5.0</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
When you first open <b>Eudora 5.0</b>, if no account settings exist, you are greeted with the <b>New Account Wizard</b>.
Information from the customers <b>Account Information Card</b> should be used to
configure <b>Eudora</b> to check mail.
</td></tr>
</table>
</p>
<p class="note">
If <b>Eudora 5.0</b> already has account settings entered that you have doubts about,
and you want to enter new settings, try deleting the <b>eudora.ini</b> file then restart 
<b>Eudora 5.0</b> to start the <b>New Account Wizard</b>.
</p>
<p>
<h3>New Account Wizard -  Greeting</h3>
Click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_1.gif" alt="New Account Wizard -  Greeting">
</p>
<p>
<h3>New Account Wizard - Account Settings</h3>
Select <b>create a brand new e-mail account</b>, then click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_2.gif" alt="New Account Wizard - Account Settings">
</p>
<p>
<h3>New Account Wizard - Personal Information</h3>
Type in a descriptive name for this account(<b>example:</b> <i>Work, Personal E-mail, Johnq</i>). Then click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_3.gif" alt="New Account Wizard - Personal Information">
</p>
<p>
<h3>New Account Wizard - E-Mail Address</h3>
Enter the e-mail address.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_4.gif" alt="New Account Wizard - E-Mail Address">
</p>
<p class="note">
This information can be found on the customer's <b>Account Information Card</b>. If the customer does not have their
<b>Account Information Card</b> or know this information, notify an <b>LT</b> to get permission to send them to 
<b>Customer Service</b>.
</p>
<p>
<h3>New Account Wizard - Login Name</h3>
Enter the <b>User Name</b>. Click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_5.gif" alt="New Account Wizard - Login Name">
</p>
<p>
<h3>New Account Wizard - Incoming E-Mail Server</h3>
Enter the <b>Incoming Mail Server</b>. Then click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_6.gif" alt="New Account Wizard - Incoming E-Mail Server">
</p>
<p>
<h3>New Account Wizard - Outgoing E-Mail Server</h3>
Enter the <b>Outgoing Mail Server</b>. Then click <b>Next</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_7.gif" alt="New Account Wizard - Outgoing E-Mail Server">
</p>
<p class="note">
Leave the check in <b>Allow Authentication</b>, by default it is checked. 
</p>
<p>
<h3>New Account Wizard - Success</h3>
This completes the <b>New Account Wizard</b>, click <b>Finish</b>.
</p>
<p>
<img src="./email/eudora/5.0/images/new_account_8.gif" alt="New Account Wizard - Success">
</p>
<p>
<h3>New Account Wizard - Eudora 5.0 Inbox</h3>
Once the wizard is complete, <b>Eudora 5.0</b> should now open.
</p>
<p>
<img src="./email/eudora/5.0/images/eudora_open.gif" alt="New Account Wizard - Eudora 5.0 open">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->