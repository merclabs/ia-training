<h1>Eudora Light 3.0</h1>
<p align="center">
<img src="./email/eudora/3.0/images/splash.gif" alt="Eudora Light 3.0">
</p>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/options.php">Eudora Light 3.0 Options</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/check_mail.php">Check Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/getting_started.php">Getting Started</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/personal_info.php">Personal Info</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/hosts.php">Hosts</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/checking_mail.php">Checking Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/sending_mail.php">Sending Mail</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/replying.php">Replying</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/attachments.php">Attachments</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/fonts_display.php">Fonts & Display</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/styled_text.php">Styled Text</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/mailbox_columns.php">Mailbox Columns</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/getting_attention.php">Getting Attention</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/extra_warning.php">Extra Warnings</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/dialup.php">Dialup</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/advanced_network.php">Advanced Network</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/mapi.php">MAPI</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/kerberos.php">Kerberos</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/miscellaneous.php">Miscellaneous</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/auto_configure.php">Auto Configure</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->