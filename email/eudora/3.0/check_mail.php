<h1>Eudora Light 3.0 - Checking Mail</h1>
<p>
<table class="list_table">
<tr><td class="list_header">Table of Content</td></tr>
<tr><td class="content_link"><a href="?content=./email/main.php">Main Menu</a></td></tr>
<tr><td class="content_link"><a href="?content=./email/eudora/3.0/main.php">Eudora Light 3.0</a></td></tr>
</table>
</p>
<p>
<table class="list_table">
<tr><td class="list_header">General Information</td></tr>
<tr><td>
None
</td></tr>
</table>
</p>
<p>
<h3>Checking Mail - Main Window</h3>
Open <b>Eudora Light 3.0</b>.
</p>
<p>
<img src="./email/eudora/3.0/images/eudora_open.gif" alt="Checking Mail - Main Window">
</p>
<p>
<h3>Checking Mail - Check Mail</h3>
Then click on <b>File</b> then <b>Check Mail</b>. 
</p>
<p>
<img src="./email/eudora/3.0/images/eudora_check_mail.gif" alt="Check Mail - Check Mail">
</p>
<p>
<h3>Checking Mail - Password</h3>
You should get a password prompt, enter your <b>Password</b>, then click <b>OK</b>.
</p>
<p>
<img src="./email/eudora/3.0/images/eudora_password.gif" alt="Checking Mail - Password">
</p>
<p>
<h3>Checking Mail - Inbox</h3>
<b>Eudora Light 3.0</b> should sound after checking mail, then open to the <b>Inbox</b>.
</p>
<p>
<img src="./email/eudora/3.0/images/eudora_inbox.gif" alt="Checking Mail - Inbox">
</p>
<!-- Back to Top button -->
<p align="right">
<a href="#top"><img src="./images/top.gif" border="0" alt="Back to Top"></a>
</p>
<!-- Back to Top button -->