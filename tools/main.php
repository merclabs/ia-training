<h1>Network and Internet Tools</h1>
<!--
<p align="center">
<i>Under Construction</i>
</p>

<div align="center">
<table border="0" width="75%" valign="center" cellspacing=15>
  <tr>
    <td align="center"> </td>
    <td align="center"> </td>
  </tr>
  <tr>
    <td align="center"> </td>
    <td align="center"> </td>
  </tr>
</table>
</div>
-->
<p>
<h3>Use www.network-tools.com to Look up a Domain or IP Address</h3>
<p class="note">
Type in a <b>Domain Name</b> or <b>IP Address</b> to lookup. Clicking <b>Look It Up</b> will take you to the 
<i>www.network-tools.com</i> web site with the entry box prepopulated with the <b>URL</b> you entered here.
The <u>default</u> lookup is set to <i><b>Express Lookup</b></i> on that site, you can customize your
lookup after you click the <b>Look It Up</b> button here.
</p>
<p class="note">
<b>Note:</b> If you leave the <b>Domain or IP</b> box blank, your current <b>IP</b> will be used by default.
</p>
<div align="center">
<form action="http://www.network-tools.com" method="GET">
<b>Enter a Domain or IP:</b> <input type="text" size="45" name="host">&nbsp;<input type="submit" value="Look It Up">
</form>
</div>
</p>