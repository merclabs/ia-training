/*
Program Name: clock.js

This program displays a clock on a webpage.
It displays the current time on the user's system.
Note: if their systems time is wrong there is nothing 
you can do to correct this on their end, check the 
timezone on their system.

Written by:
Clayton Barnette
cbarnette@infoave.net
Date Code: 042102
*/
function showclock(){
var d,h,m,s,dayornight,ctime;
d = new Date();

// This set the Hour
h = d.getHours();

// set am or pm 
if(h < 12){
dayornight = "AM";
}else{
dayornight = "PM";
};

// set to 12 hour clock
if(h > 12){
h = h - 12;
}
/*
uncomment this section of code below to show Hours in 
double digit form like so: 01:04:03 pm
*/
//if(h < 10){
//h = "0" + h;
//}

// This part set the hour to 12 to keep hour from showing up
// in single digits if hours equals 0 like so: 0:04:03 am
if(h == "0"){
h = 12;
}

// This part Sets Minutes
m = d.getMinutes();
/*
This corrects minutes so that minutes don't
show up in single digit form. like "12:4:03 pm".
If minutes are less than 9, it adds a "0" to 
the front of minutes like so: 12:04:03 pm 
Note: the same is done for Seconds and can be done
for Hours if so desired.
*/
if(m < 10){
m = "0" + m;
}

// Sets Seconds
s = d.getSeconds();
/*
This corrects seconds so that seconds don't
show up in single digit form. like "12:04:3 pm".
If seconds are less than 9 it adds a "0" to 
the front of seconds like so: 12:04:03 pm 
*/
if(s < 10){
s = "0" + s;
}

/*
This part sets ctime to the current time on 
the visitors computer. If theirs is wrong
this time will be wrong as well..."Oh well.:-("
*/
ctime = h + ":" + m + ":" + s + " " + dayornight;

/* 
This part sets the interval between times the clock 
function is updated to display the current time,
1000 is equal to 1 second.
*/
setTimeout("showclock()",1000);

/* 
This is the return statement that sets the HTML
text element to display the current time.
*/
return document.clock.clock.value = ctime;
}

