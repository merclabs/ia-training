==================================================================

                      Netscape Navigator 4.0 

==================================================================

Netscape Navigator is subject to the terms 
detailed in the license agreement accompanying it.

       ************************************************
        IMPORTANT!  Before going any further, please
        read and accept the terms in the file LICENSE.
       ************************************************

Release notes for this version of Netscape Navigator are
available online.  After starting Netscape Navigator, select Release
Notes from the Help menu. This will take you to the following
URL, which has release notes for Navigator Standard Edition only:

 http://home.netscape.com/eng/mozilla/4.0/relnotes/windows-4.0.html

New features and known problems of this release are listed there.

Please be as specific as possible about the version of Netscape 
Navigator you are using, as well as your hardware information and 
which version of system software you're using.  Be sure to include
a test case for the problem, including a URL if possible.

==================================================================

                 Installation Instructions

==================================================================

* Installation

   To run the 32-bit Navigator, you MUST have a 32-bit TCP/IP stack.
   Both Windows 95 and Windows NT provide built-in 32bit TCP/IP
   stacks that you can set up.  You can also get TCP/IP
   software from third-party vendors, such as Trumpet Software
   or Ftp Software. 

   To run the 16-bit Navigator, you MUST have a 16-bit TCP/IP stack.
   You can get (16-bit) TCP/IP software from third-party vendors, such as
   Trumpet Software and Ftp Software.

   To obtain a copy of Netscape Navigator, download one of the
   self-extracting executable files into a temporary directory.

        For 32-bit : n32e408.exe (export encryption)
                     n32d408.exe (domestic encryption)

        For 16-bit : n16e408.exe (export encryption)
                     n16d408.exe (domestic encryption)

   Double-click on the self-extracting file
   to start the installation process automatically.


* Windows 95 users, if you are using any versions prior to this
  release, you will get multiple uninstall entries in your
  Add/Remove program.  If you prefer not having multiple
  entries, you should do an uninstall to remove your previous
  version of Navigator before you install the new release.

* The Netscape Audio Plug-in will only work if you have a sound
  card installed and configured properly.  It will not work
  with the PC speaker driver. Please check your sound card manual
  to configure the sound card.

* If you had previously installed CoolTalk Watchdog, then you must
  uninstall the version you have and reboot your machine before
  installing the latest version.  You might experience some
  problems running Cooltalk with Matrox MGA Millennium graphic 
  adapters using early Windows95 drivers. Make sure that you are 
  running version 2.22.039 or later.

* If you have an existing version of Communicator 4.0 installed, it
  is recommended you uninstall it before installing Navigator 4.0.
  This can be done from the Control Panel (Add/Remove Programs)
  in Win95/NT4.0 or the Netscape Communicator program group in
  Windows 3.1/NT3.51. 


==================================================================

                        Running Netscape Navigator

==================================================================

What do I need to run Netscape Navigator?

There are two requirements for Netscape Navigator:

    1. A direct Ethernet connection to the network (providing intranet or 
       Internet access), or a dialup SLIP or PPP account from an Internet 
       service provider.

    2. TCP/IP stack.

You can get a direct connection to the Internet through a service
provider in your area.  Your network administrator can help
you connect to your intranet. Your service provider or your network
administrator can also provide you with a SLIP or a PPP
account.

==================================================================

                         International

==================================================================

The Netscape installer will overwrite existing OLE2 DLLs with the
latest versions for US Windows. This will not affect 32-bit
Windows. Occasionally, the Japanese user may see an English
language OLE2 error message after installing Netscape Navigator.
Currently, there are no Japanese versions available for the OLE2 DLLs.

==================================================================

                          Known Bugs

==================================================================

* Please check the release notes for Netscape Navigator 
  for a list of known bugs. Release notes are available at the 
  following URL:

   http://home.netscape.com/eng/mozilla/4.0/relnotes/windows-4.0.html

