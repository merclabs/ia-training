
         NETSCAPE CLIENT PRODUCTS LICENSE AGREEMENT
           REDISTRIBUTION OR RENTAL NOT PERMITTED

  These Terms apply to Netscape Communicator Standard Edition,
  Deluxe Edition, Internet Access Edition, and Professional
  Edition, Netscape Publishing Suite, Netscape Navigator and
  Netscape Navigator Gold.

             GENERAL LICENSE TERMS & CONDITIONS

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING THE
SOFTWARE PRODUCTS LISTED ON THE PRODUCT SCHEDULE, QUOTATION AND
OFFER FORM, OR INVOICE (THE "PRODUCTS"), THE INDIVIDUAL OR ENTITY
WHICH HAS LICENSED THE PRODUCT(S) ("LICENSEE") IS CONSENTING TO BE
BOUND BY AND IS BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE
DOES NOT AGREE TO ALL OF THE TERMS OF THIS AGREEMENT, THE BUTTON
INDICATING NON-ACCEPTANCE MUST BE SELECTED, AND LICENSEE MUST NOT
INSTALL OR USE THE SOFTWARE.  (Depending on the method of
acquisition, the licensed Products will be listed on a Product
Schedule, Quotation and Offer form, or invoice.  The term "Product
Schedule" shall be used herein to refer to whichever of these
documents is applicable.)

1. AGREEMENT.  The "Agreement" governing Licensee's use of the
Product(s) consists of these General License Terms and Conditions
("General Terms"), each set of product specific license terms and
conditions which follow ("Product Terms"), and, if provided, the
(i) Corporate End User Order Form and Product Schedule or (ii)
Quotation and Offer form, as applicable.  If more than one license
agreement was provided for this Product, and the terms vary, the
order of precedence of those license agreements is as follows: a
signed agreement, a license agreement available for review on the
Netscape website, a printed or electronic agreement that states
clearly that it supersedes other agreements, a printed agreement
provided with a Product, an electronic agreement provided with a
Product.  The General Terms apply to all Products on the Product
Schedule, and each set of Product Terms applies only to the
individual Products identified in the Product Terms sheet.  All
Products are licensed independently of one another.  As used in
this Agreement, for residents of Europe, the Middle East or Africa,
"Netscape" shall mean Netscape Communications Ireland Limited; for
residents of Japan, "Netscape" shall mean Netscape Communications
(Japan), Ltd.; for residents of all other countries, "Netscape"
shall mean Netscape Communications Corporation.  In this Agreement
"Licensor" shall mean Netscape except as otherwise set forth
herein.  If Licensee acquired the Product(s) as a bundled component
of a third party product or service, then such third party shall be
Licensor.  Any third party software provided together with a
Product with such third party's electronic or printed license
agreement is included for use at Licensee's option, and any use of
such software shall be governed by the third party's license
agreement and not by this Agreement, except to the extent that this
Agreement indicates otherwise with respect to specific third party
software.

2. TERM AND TERMINATION.  This Agreement shall remain in effect
until terminated in accordance with this Section or as otherwise
provided in this Agreement.  Licensee may terminate this Agreement
at any time by written notice to Licensor.  Licensor may terminate
this Agreement immediately in the event of (i) any breach of
Section 6 or 8 by Licensee or (ii) a material breach by Licensee
which is not cured within 30 days of written notice by Licensor.
Upon termination, Licensee shall discontinue use and certify as
destroyed, or return to Licensor, all copies of the Product(s).
Licensee's obligation to pay accrued charges and fees shall survive
any termination of this Agreement.  Within 30 calendar days after
termination of the Agreement, Licensee shall pay to Licensor all
sums then due and owing.

3. FEES AND TAXES.  If Licensee is purchasing a license for the
Product(s) directly from Netscape, all fees are exclusive of taxes,
withholdings, duties or levies (collectively herein "Levies"),
however designated or computed, and Licensee shall be responsible
for paying all such Levies except for taxes based on Netscape's net
income.  If Licensee is exempt from such Levies, Licensee shall
provide to Netscape a valid tax or other Levy exemption certificate
acceptable to the taxing or other levying authority.

4. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product(s) shall remain in Netscape and/or
its suppliers.  Licensee acknowledges such ownership and
intellectual property rights and will not take any action to
jeopardize, limit or interfere in any manner with Netscape's or its
suppliers' ownership of or rights with respect to the Product(s).
The Product(s) are protected by copyright and other intellectual
property laws and by international treaties.  Title and related
rights in the content accessed through the Product(s) are the
property of the applicable content owner and are protected by
applicable law.  The license granted under this Agreement gives
Licensee no rights to such content.  Any copy shall contain all
notices regarding proprietary rights as contained in the Product
originally delivered by Licensor.

5. RESTRICTIONS.  Except as otherwise expressly permitted in this
Agreement, Licensee may not: (i) modify or create any derivative
works of any Product or documentation, including translation or
localization (Licensees code written to published APIs (application
programming interfaces) for the Product(s) shall not be deemed
derivative works); (ii) decompile, disassemble, reverse engineer,
or otherwise attempt to derive the source code for any Product
(except to the extent applicable laws specifically prohibit such
restriction); (iii) redistribute, encumber, sell, rent, lease,
sublicense, use the Products in a timesharing or service bureau
arrangement, or otherwise transfer rights to any Product; (iv) copy
any Product (except for an archival copy which must be stored on
media other than a computer hard drive) or documentation; (v)
remove or alter any trademark, logo, copyright or other proprietary
notices, legends, symbols or labels in the Product(s); (vi) modify
any header files or class libraries in any Product; (vii) create or
alter tables or reports relating to the database portion of the
Product (except as necessary for operating the Product); (viii)
publish any results of benchmark tests run on any Product to a
third party without Netscape's prior written consent; (ix) use the
database provided for use with any Product except in conjunction
with the relevant Product; or (x) use any Product on a system with
more CPUs than the number licensed, by more Users than have been
licensed, on more computers than the number licensed, or by more
developers than the number licensed, as applicable.

6. LIMITED WARRANTY.  Provided Licensee has paid the applicable
license fees, for 90 days after the date of shipment to Licensee
(date of shipment meaning either the date Licensor shipped the
Product on media or the date on which Licensee downloaded the
Product from an authorized Netscape download site) of each Product
(the "Warranty Period"), Licensor warrants that (i) the media on
which the Product is delivered will be free of defects in material
and workmanship under normal use; and (ii) the unmodified Product,
when properly installed and used, will substantially achieve the
functionality described in the applicable documentation.  THE
EXPRESS WARRANTY SET FORTH HEREIN CONSTITUTES THE ONLY WARRANTY
WITH RESPECT TO THE PRODUCT(S).  LICENSOR AND ITS SUPPLIERS DO NOT
MAKE, AND HEREBY EXCLUDE, ALL OTHER REPRESENTATIONS OR WARRANTIES
OF ANY KIND WHETHER EXPRESS OR IMPLIED (EITHER IN FACT OR BY
OPERATION OF LAW) WITH RESPECT TO ANY PRODUCT OR TEST DATA INCLUDED
IN ANY PRODUCT.  LICENSOR AND ITS SUPPLIERS EXPRESSLY DISCLAIM ALL
WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE OR NONINFRINGEMENT OF THIRD PARTIES' RIGHTS.  LICENSOR AND
ITS SUPPLIERS DO NOT WARRANT THAT THE PRODUCT(S) WILL MEET
LICENSEES REQUIREMENTS OR WILL OPERATE IN THE COMBINATIONS WHICH
MAY BE SELECTED BY LICENSEE OR THAT THE OPERATION OF THE PRODUCT(S)
WILL BE SECURE, ERROR-FREE OR UNINTERRUPTED AND LICENSOR HEREBY
DISCLAIMS ANY AND ALL LIABILITY ON ACCOUNT THEREOF.  THE SECURITY
MECHANISMS IMPLEMENTED BY THE PRODUCT(S) HAVE INHERENT LIMITATIONS,
AND LICENSEE MUST DETERMINE THAT THE PRODUCT(S) SUFFICIENTLY MEET
LICENSEE&IACUTE;S REQUIREMENTS.  LICENSOR AND ITS SUPPLIERS SHALL
HAVE NO OBLIGATIONS UNDER THE WARRANTY PROVISIONS SET FORTH HEREIN
IF LICENSEE SUBJECTS THE MEDIA TO ACCIDENT OR ABUSE; ALTERS,
MODIFIES OR MISUSES THE PRODUCT(S); USES THE PRODUCT(S)
INCORPORATED, ATTACHED OR IN COMBINATION WITH NON-NETSCAPE SOFTWARE
OR ON ANY COMPUTER SYSTEM OTHER THAN THAT FOR WHICH THE PRODUCT IS
INTENDED; OR LICENSEE VIOLATES THE TERMS OF THIS AGREEMENT.  THE
EXTENT OF LICENSOR'S DUTY UNDER THIS LIMITED WARRANTY SHALL BE THE
CORRECTION OR REPLACEMENT OF ANY PRODUCT WHICH FAILS TO MEET THIS
WARRANTY.  IN THE EVENT OF A BREACH OF THIS WARRANTY, AND IF
LICENSEE PROVIDES LICENSOR WITH A WRITTEN REPORT DURING THE
WARRANTY PERIOD, LICENSOR WILL USE REASONABLE EFFORTS TO CORRECT OR
REPLACE PROMPTLY, AT NO CHARGE TO LICENSEE, THE ERRORS OR
FAILURES.  THIS IS LICENSEE'S SOLE AND EXCLUSIVE REMEDY FOR BREACH
OF ANY EXPRESS OR IMPLIED WARRANTIES HEREUNDER.  NOTWITHSTANDING
THE FOREGOING, SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF
CERTAIN IMPLIED WARRANTIES; HOWEVER, THE EXCLUSIONS OF LICENSOR'S
WARRANTY IN THIS LIMITED WARRANTY SECTION SHALL APPLY TO THE
FULLEST EXTENT PERMITTED BY APPLICABLE LAW.  THIS AGREEMENT DOES
NOT EXCLUDE ANY WARRANTIES THAT MAY NOT BE EXCLUDED BY LAW AND ANY
LIABILITY ARISING HEREUNDER SHALL BE LIMITED TO THE CORRECTION OR
REPLACEMENT OF THE APPLICABLE PRODUCT, AT LICENSOR'S OPTION.

7. LIMITATION ON LIABILITY.  (a) IN NO EVENT WILL LICENSOR OR ITS
SUPPLIERS OR RESELLERS BE LIABLE FOR ANY INDIRECT, SPECIAL,
INCIDENTAL OR CONSEQUENTIAL DAMAGES, OR ANY DIRECT DAMAGES WITH
RESPECT SOLELY TO ANY DATABASE PRODUCT PROVIDED WITH THE PRODUCT,
INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER
COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE POSSIBILITY
THEREOF, AND REGARDLESS OF WHETHER ANY CLAIM IS BASED UPON ANY
CONTRACT, TORT OR OTHER LEGAL OR EQUITABLE THEORY.  (b) WITH THE
EXCEPTION OF DEATH OR PERSONAL INJURY CAUSED BY THE NEGLIGENCE OF
LICENSOR TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH LIMITATION, IN
NO EVENT WILL LICENSOR OR ITS SUPPLIERS OR RESELLERS BE LIABLE FOR
ANY AMOUNTS IN THE AGGREGATE IN EXCESS OF THE LICENSE FEES RECEIVED
BY LICENSOR FROM LICENSEE HEREUNDER FOR THE PRODUCT GIVING RISE TO
SUCH DAMAGES, NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF
ANY LIMITED REMEDY OR INVALIDITY OF SUBSECTION (a) ABOVE.  SOME
JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF
INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS EXCLUSION AND
LIMITATION MAY NOT BE APPLICABLE.  LICENSEE IS SOLELY RESPONSIBLE
FOR ANY LIABILITY ARISING OUT OF ANY CONTENT PROVIDED BY LICENSEE
AND/OR ANY MATERIAL TO WHICH USERS CAN LINK THROUGH SUCH CONTENT.
ANY DATA INCLUDED IN A PRODUCT UPON SHIPMENT FROM LICENSOR IS FOR
TESTING USE ONLY AND LICENSOR HEREBY DISCLAIMS ANY AND ALL
LIABILITY ARISING THEREFROM.  THE EXTENT OF LICENSOR'S LIABILITY
FOR THE LIMITED WARRANTY SECTION SHALL BE AS SET FORTH THEREIN.

8. ENCRYPTION.  If Licensee wishes to use the cryptographic
features of any Product, then Licensee may need to obtain and
install a signed digital certificate from a certificate authority
or a certificate server in order to utilize the cryptographic
features.  Licensee may be charged additional fees for
certification services.  Licensee is responsible for maintaining
the security of the environment in which the Product is used and
the integrity of the private key file used with the Product.  In
addition, the use of digital certificates is subject to the terms
specified by the certificate provider, and there are inherent
limitations in the capabilities of digital certificates.  If
Licensee is sending or receiving digital certificates, Licensee is
responsible for familiarizing itself with and evaluating such terms
and limitations.  If the Product is a version with FORTEZZA,
Licensee will need to obtain PC Card Readers and FORTEZZA Crypto
Cards from another vendor to enable the FORTEZZA features.

9. EXPORT CONTROL.  Licensee agrees to comply with all export laws
and restrictions and regulations of the U.S. Department of State,
Department of Commerce or other United States or foreign agency or
authority, and not to export or re-export any Product or any direct
product thereof in violation of any such restrictions, laws or
regulations, or without all necessary approvals.  Neither the
Product(s) nor the underlying information or technology may be
downloaded or otherwise exported or re-exported (i) into (or to a
national or resident of) Cuba, Iraq, Libya, Sudan, North Korea,
Iran, Syria or any other country to which the U.S. has embargoed
goods; or (ii) to anyone on the U.S. Treasury Department's list of
Specially Designated Nationals or the U.S. Commerce Department's
Table of Denial Orders.  By downloading or using the Product(s),
Licensee agrees to the foregoing and represents and warrants that
it is not located in, under the control of, or a national or
resident of any such country or on any such list.  As applicable,
each party shall obtain and bear all expenses relating to any
necessary licenses and/or exemptions with respect to its own export
of the Product(s) from the U.S.

If the Product(s) are identified as being not-for-export (for
example, on the box, media or in the installation process), then,
unless Licensee has an exemption from the United States Department
of State, the following applies: EXCEPT FOR EXPORT TO CANADA FOR
USE IN CANADA BY CANADIAN CITIZENS, THE PRODUCT(S) AND ANY
UNDERLYING TECHNOLOGY MAY NOT BE EXPORTED OUTSIDE THE UNITED STATES
OR TO ANY FOREIGN ENTITY OR "FOREIGN PERSON" AS DEFINED BY U.S.
GOVERNMENT REGULATIONS, INCLUDING WITHOUT LIMITATION, ANYONE WHO IS
NOT A CITIZEN, NATIONAL OR LAWFUL PERMANENT RESIDENT OF THE UNITED
STATES.  BY DOWNLOADING OR USING THE SOFTWARE, LICENSEE AGREES TO
THE FOREGOING AND WARRANTS THAT IT IS NOT A "FOREIGN PERSON" OR
UNDER THE CONTROL OF A "FOREIGN PERSON."

10. HIGH RISK ACTIVITIES.  The Product(s) are not fault-tolerant
and are not designed, manufactured or intended for use or resale as
on-line control equipment in hazardous environments requiring
fail-safe performance, such as in the operation of nuclear
facilities, aircraft navigation or communication systems, air
traffic control, direct life support machines, or weapons systems,
in which the failure of any Product could lead directly to death,
personal injury, or severe physical or environmental damage ("High
Risk Activities").  Accordingly, Licensor and its suppliers
specifically disclaim any express or implied warranty of fitness
for High Risk Activities.  Licensee agrees that Licensor and its
suppliers will not be liable for any claims or damages arising from
the use of any Product in such applications.

11. U.S. GOVERNMENT END USERS.  The Product is a "commercial item,"
as that term is defined in 48 C.F.R. 2.101 (Oct. 1995), consisting
of "commercial computer software" and "commercial computer software
documentation," as such terms are used in 48 C.F.R. 12.212 (Sept.
1995).  Consistent with 48 C.F.R. 12.212 and 48 C.F.R. 227.7202-1
through 227.7202-4 (June 1995), all U.S. Government End Users
acquire the Product with only those rights set forth herein.

12. EDUCATIONAL USERS.  If Licensee is a qualifying educational or
nonprofit institution within the United States or Canada, certain
Netscape client and server products are available for free download
from the Netscape download site, and selected other client and
server products are available at an educational discount.
Qualifying educational institutions are grammar schools, junior
high schools and high schools; junior colleges, colleges and
universities that are accredited and issue two-year, four-year or
advanced degrees; public libraries; and state departments of
education.  Students, faculty and staff at qualifying educational
institutions are authorized to use the software products obtained
through an education program, for educational purposes only.  Only
charitable nonprofit organizations that have been preapproved by
Netscape qualify for free or discounted Netscape products.
Hospitals do not qualify for this program.  For more information on
programs for educational and nonprofit institutions, please visit
the following website:
http://home.netscape.com/comprod/business_solutions/education/index.html

13. MISCELLANEOUS.  (a) This Agreement constitutes the entire
agreement between the parties concerning the subject matter hereof
and supersedes all prior and contemporaneous agreements and
communications, whether oral or written, between the parties
relating to the subject matter hereof, and all past courses of
dealing or industry custom.  The terms and conditions hereof shall
prevail over any conflicting purchase order or other written
instrument submitted by Licensee.  (b) This Agreement may be
amended only by a writing signed by both parties.  (c) This
Agreement shall be governed by the laws of the State of California,
U.S.A., without reference to its conflict of law provisions.  (d)
Unless otherwise agreed in writing, all disputes relating to this
Agreement (excepting any dispute relating to intellectual property
rights) shall be subject to final and binding arbitration in Santa
Clara County, California, under the auspices of JAMS/EndDispute,
with the losing party paying all costs of arbitration.  (e) This
Agreement shall not be governed by the United Nations Convention on
Contracts for the International Sale of Goods.  (f) If any
provision in this Agreement should be held illegal or unenforceable
by a court having jurisdiction, such provision shall be modified to
the extent necessary to render it enforceable without losing its
intent, or severed from this Agreement if no such modification is
possible, and other provisions of this Agreement shall remain in
full force and effect.  (g) The controlling language of this
Agreement is English.  If Licensee has received a translation into
another language, it has been provided for Licensee's convenience
only.  (h) A waiver by either party of any term or condition of
this Agreement or any breach thereof, in any one instance, shall
not waive such term or condition or any subsequent breach thereof.
(i) The provisions of this Agreement which require or contemplate
performance after the expiration or termination of this Agreement
shall be enforceable notwithstanding said expiration or
termination.  (j) Licensee may not assign or otherwise transfer by
operation of law or otherwise this Agreement or any rights or
obligations herein without the prior express written consent of
Licensor, which will not be unreasonably withheld.  (k) This
Agreement shall be binding upon and shall inure to the benefit of
the parties, their successors and permitted assigns.  (l) Neither
party shall be in default or be liable for any delay, failure in
performance (excepting the obligation to pay) or interruption of
service resulting directly or indirectly from any cause beyond its
reasonable control.  (m) The relationship between Licensor and
Licensee is that of independent contractors and neither Licensee
nor its agents shall have any authority to bind Licensor in any
way.  (n) If any dispute arises under this Agreement, the
prevailing party shall be reimbursed by the other party for any and
all legal fees and costs associated therewith.  (o) The headings to
the sections of this Agreement are used for convenience only and
shall have no substantive meaning.

14. LICENSEE OUTSIDE THE U.S.  If Licensee is located outside the
U.S., then the provisions of this Section shall apply.  (i) If
Licensee is purchasing licenses directly from Netscape and if
Netscape and Licensee are not located in the same country, then, if
any applicable law requires Licensee to withhold amounts from any
payments to Netscape hereunder Licensee shall effect such
withholding, remit such amounts to the appropriate taxing
authorities and promptly furnish Netscape with tax receipts
evidencing the payments of such amounts, and the sum payable by
Licensee upon which the deduction or withholding is based shall be
increased to the extent necessary to ensure that, after such
deduction or withholding, Netscape receives and retains, free from
liability for such deduction or withholding, a net amount equal to
the amount Netscape would have received and retained absent such
required deduction or withholding.  (ii) Les parties aux prsents
confirment leur volont que cette convention de mme que tous les
documents y compris tout avis qui sy rattach, soient redigs en
langue anglaise.  (translation: "The parties confirm that this
Agreement and all related documentation is and will be in the
English language.")  (iii) Licensee is responsible for complying
with any local laws in its jurisdiction which might impact its
right to import, export or use the Product(s), and Licensee
represents that it has complied with any regulations or
registration procedures required by applicable law to make this
license enforceable.

        NETSCAPE CLIENT PRODUCT TERMS AND CONDITIONS

1. AGREEMENT.  The Agreement governing Licensee's use of the
Product(s) identified above ("Client Products") consists of these
Netscape Client Product Terms and Conditions, the General Terms,
and, if provided, the (i) Corporate End User Order Form and Product
Schedule or (ii) Quotation and Offer form, as applicable.
Regarding the use of any third party software included as part of
the default Client Product installation: if a license agreement is
presented for acceptance the first time that third party software
is invoked, then that license agreement shall govern the use of
that third party software; if no license is presented for
acceptance, then the use of that third party software shall be
governed by this Agreement, but the term "Licensor," with respect
to such third party software, shall mean the manufacturer of that
software and not Netscape.

2. LICENSE GRANT.  Subject to payment of applicable license fees,
if any, Licensor grants Licensee a non-exclusive and
non-transferable license to use the executable code version of the
Client Product(s) and accompanying documentation according to the
terms and conditions of this Agreement.  Netscape Navigator,
Netscape Navigator Gold and Netscape Communicator Standard Edition
are referred to herein as "Standard Software."  Netscape
Communicator Professional Edition that Licensee is using for a
limited time for the purpose of evaluating whether to purchase an
ongoing license is referred to herein as "Evaluation Software."
Together they are referred to herein as "Free Software."  Netscape
Communicator Professional Edition, Netscape Communicator Internet
Access Edition, Netscape Communicator Deluxe Edition, and Netscape
Publishing Suite are referred to herein as "Professional Software."
Licensee may not customize the Client Products unless Licensee has
licensed either the Netscape Client Customization Kit or Netscape
Mission Control Desktop, and then only to the extent permitted in
the license agreement accompanying that product.  Licensee may:

  A. For Standard Software:

      Reproduce the Standard Software for personal or internal
business use, provided any copy must contain all of the original
Standard Software's proprietary notices.  Users of Standard
Software are not entitled to hard-copy documentation, support or
telephone assistance unless the entity from which Licensee received
Standard Software provides support.  Licensee may not redistribute
the Standard Software unless Licensee has separately entered into a
distribution agreement with Netscape such as the Unlimited
Distribution Program Agreement.

  B. For Evaluation Software:

      Use the Evaluation Software for a limited time for the
purpose of determining whether Licensee wishes to purchase a
license for Netscape Communicator Professional Edition.  The
evaluation period for use of Evaluation Software by or on behalf of
a commercial entity is limited to ninety (90) days; evaluation use
by others is not subject to this ninety (90) day limit.

  C. For Professional Software Packaged Products:

      a.  Use the Professional Software on a single computer,
except that (i) it may also be used on a second computer if only
one copy is used at a time, and (ii) if the Professional Software
is Netscape Communicator Professional Edition and was licensed by a
company or organization for use by an employee, then Licensee may
allow that employee to use a copy of Netscape Communicator
Professional Edition at home.  The home copy can either be copied
from the employee's computer at work or downloaded from the
Netscape website at no cost.  The Documentation may not be
duplicated for home users, and no technical assistance will be
provided for home use.

      b.  Use the Professional Software on a network if a licensed
copy of the Professional Software has been acquired for each person
permitted to access the Professional Software through the network.

      c.  If Licensee has purchased a license for multiple copies
of the Professional Software, make the total number of copies of
the Professional Software (but not the documentation) stated on the
packing slip(s) or invoice(s) provided any copy must contain all of
the original Professional Software's proprietary notices.  The
number of copies on the packing slip(s) or invoice(s) is the total
number of copies that may be made for all platforms.  Additional
copies of documentation may be purchased from Licensor.

  D. For Professional Products Charters Program Licenses:

      a.  Make the total number of copies of the Professional
Software and accompanying documentation indicated on the Product
Schedule, provided any copy must contain all of the original
Professional Software's proprietary notices.  The number of copies
on the Product Schedule is the total number of copies that may be
made for all platforms.

      b.  Sublicense the right to use and reproduce the Client
Product(s) and related documentation under this Agreement to
subsidiaries of Licensee provided Licensee is responsible for each
such entity complying with the terms of this Agreement.

3. FEES.  There is no license fee for Standard Software or
Evaluation Software.  License fees are required for Professional
Software.  Licensee is only entitled to a refund for Professional
Software if one is offered by Licensee's place of purchase.

4. DISCLAIMER OF WARRANTY FOR FREE SOFTWARE.  FREE SOFTWARE IS
PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTY OF ANY KIND,
INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT IS FREE OF
DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR
NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE
OF THE FREE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE FREE
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, LICENSEE AND NOT LICENSOR
OR ITS SUPPLIERS OR RESELLERS ASSUMES THE ENTIRE COST OF ANY
SERVICE AND REPAIR.  IN ADDITION, THE SECURITY MECHANISMS
IMPLEMENTED BY THE FREE SOFTWARE HAVE INHERENT LIMITATIONS, AND
LICENSEE MUST DETERMINE THAT THE FREE SOFTWARE SUFFICIENTLY MEETS
ITS REQUIREMENTS.  THIS DISCLAIMER OF WARRANTY CONSTITUTES AN
ESSENTIAL PART OF THIS AGREEMENT.  NO USE OF THE FREE SOFTWARE IS
AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.


Netscape Client Software EULA           Rev. 062498

