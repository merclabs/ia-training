<?
// Add times here and content to mainreminders.php page.

// Entry Temple
$reminder_title[] = "New Provider - Blue Mountain Cable";
$reminder_text[] = "
Information on this new provider is listed on the 
<a href=\"http://www.iatraining.net/newdesign/?content=./broadband/main.php#b\">Broadband</a> and <a href=\"http://www.iatraining.net/newdesign/?content=./lt_admin/escalations/main.php\">Escalation Procedures</a> page
of IA Training.
";


$reminder_title[] = "VIRUS ALERT: Bagle N-P";
$reminder_text[] = "
<p>
There is a new virus circulating the Internet that arrives via e-mail and looks like a message from Customer Service alerting you to a problem. 
</p>
<p>
The subject of the e-mail may contain any of the following: 
<ul>
<li>E-mail account security warning. 
<li>Notify about using the e-mail account. 
<li>Warning about your e-mail account. 
<li>Important notify about your e-mail account. 
</ul>
The best defense against Viruses is to have updated Virus Software on your computer, but keep in mind also the virus will not work if you do not open the attachment. 
</p>
<p>
We have posted some information about the virus and how to clean an infected computer on our web utilities site: <a href=\"http://www.homeigo.net/\" target=\"_blank\">http://www.homeigo.net/</a>.
</p>
<p>
Thank you.
<br>
LaSalle  M. Smith<br> 
Customer Relations Manager<br>
Internet Services<br>
Spirit Telecom";



$reminder_title[] = "HTC customers moving DSL Modem to another jack.";
$reminder_text[] = "<p>
Troubleshooting Procedures for DSL Modem Ready Light/ADSL light flashing:
</p>
<ol> 
<li>Making sure user has not moved the PC or modem from the DSL line/jack that the modem was originally installed on.
<p class=\"note\"> 
--If they have moved the modem to another jack, it will not work.  <u>Not</u> all jacks in the home are DSL ready.  There is a charge to activate another jack for the DSL service and they would need to contact our customer service if they have moved the PC to another room and need jack activate.
</p>
<li>Make sure if the DSL line is plugged in securely in the DSL jack of the modem and the jack in the wall.
<li>Make sure the DSL line running to modem and jack does not have a phone filter on it. (small gray rectangle device phone line plugs into)
<li>If all the above are fine, then escalate to TierII support.  No PC troubleshooting necessary.  
</ol>
<p>Thanks, Doyle</p>";




$reminder_title[] = "Filters on Self Install";
$reminder_text[] = "<p>
It has been brought to my attention that a few ADSL self install tickets have been escalated to third level where they discovered that the
problem was due to the customer not adding the filters to the necessary lines. Please make sure your guys remember to ask the customer about 
filters and make sure they have one on everything that connects to the phone line! I have reminded my guys also as they have missed them, too.</p>";


$reminder_title[] = "Closing Tickets";
$reminder_text[] = "<p class=\"note\">
<b>Important Note:</b> We need to close tickets when we finish with customers. Most of the
time especially in the Madison queue when we have corrected their issue so, 
make it a new habit to close the tickets.</p>";


$reminder_title[] = "MR Speedstream modems";
$reminder_text[] = "<p> 
If a customer wants to switch between USB and ethernet on
their computer, they have to follow the same procedure as if
they were disconnecting the modem and hooking it up to
another PC. They have to release the IP address on the
current adapter they are using, and then renew it on the
adapter they are wanting to switch to. It sounds fairly
simple but many techs have been overlooking this.</p>";



// Entry Temple
//$reminder_title[] = "";
//$reminder_text[] = "
//test text2.";
?>
